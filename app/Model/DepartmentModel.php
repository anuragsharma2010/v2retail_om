<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DepartmentModel extends Model
{
    public $table = 'departments';

    protected $fillable = [

        'dept_name', 'dept_code', 'dept_desc', 'div_id', 'dept_status',

    ];
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }

}
