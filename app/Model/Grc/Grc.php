<?php

namespace App\Model\Grc;

use Illuminate\Database\Eloquent\Model;

class Grc extends Model
{
    protected $table = 'grc';
    protected $fillable = [
        'pstng_date', 'site','article','mvt','quantity','amount_lc', 'vendor', 'article_doc', 'item'
    ];

    public function articledesc()
    {
        return $this->belongsTo('Master\Models\ArticlesModel', 'article', 'article');
    }
}
