<?php

namespace App\Model\Po;

use Illuminate\Database\Eloquent\Model;

class Po extends Model
{
    protected $fillable =[
        'po_date','site','doc_date','del_date','purchase_doc','vendor','article','pgr','item','net_price','act_po_q','act_po_v','gr_qty','pend_po_q','pend_po_v'
    ];
    protected $table = 'pending_po';

    public function articledesc()
    {
        return $this->belongsTo('Master\Models\ArticlesModel', 'article', 'article');
    }
}
