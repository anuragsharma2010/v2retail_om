<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DivisionModel extends Model
{
    public $table = 'sop_divisions';

    protected $fillable = [

        'div_name', 'div_code', 'div_desc', 'div_status',

    ];
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }

}
