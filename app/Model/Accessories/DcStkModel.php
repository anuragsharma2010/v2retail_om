<?php

namespace App\Model\Accessories;

use Illuminate\Database\Eloquent\Model;


class DcStkModel extends Model
{
    protected $table = 'dc_stk';
    public $timestamps = false;
    protected $fillable = [
       "link_article_code",
        "st_to_dc_in_tr_stk_v",
        "msa_v02_stk_v",
        "msa_v01_stk_v",
        "msa_v04_a_stk_v",
        "msa_v04_b_stk_v",
        "msa_v11_stk_v",
        "ssnl_0034_stk_v",
        "new_grt_0032_stk_v",
        "pack_0031_stk_v",
        "old_grt_0033_stk_v",
        "grt_0035_stk_v",
        "st_to_dc_in_tr_stk_q",
        "msa_v02_stk_q",
        "msa_v01_stk_q",
        "msa_v04_a_stk_q",
        "msa_v04_b_stk_q",
        "msa_v11_stk_q","ssnl_0034_stk_q","new_grt_0032_stk_q",
        "pack_0031_stk_q","old_grt_0033_stk_q","grt_0035_stk_q",
        "cm_pend_po_q","cm_1_pend_po_q","cm_2_pend_po_q","cm_3_pend_po_q",
        "cm_pend_po_v","cm_1_pend_po_v","cm_2_pend_po_v","cm_3_pend_po_v"
        ];
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }

    
}
