<?php

namespace App\Model\Accessories;

use Illuminate\Database\Eloquent\Model;


class StStockModel extends Model
{
    protected $table = 'st_stock';
    public $timestamps = false;
    protected $fillable = [
        "store_code",
        "link_article_code",
        "0001_st_cl_stk_p_v",
        "0002_st_cl_stk_p_v",
        "0003_st_cl_stk_p_v",
        "0004_st_cl_stk_p_v",
        "0005_st_cl_stk_p_v",
        "0006_st_cl_stk_p_v",
        "0007_st_cl_stk_p_v",
        "0008_st_cl_stk_p_v",
        "0009_st_cl_stk_p_v",
        "0010_st_cl_stk_p_v",
        "st_stk_in_tr_v",
        "st_prd_stk_v",
        "0001_st_cl_stk_p_q",
        "0002_st_cl_stk_p_q",
        "0003_st_cl_stk_p_q",
        "0004_st_cl_stk_p_q",
        "0005_st_cl_stk_p_q",
        "0006_st_cl_stk_p_q",
        "0007_st_cl_stk_p_q",
        "0008_st_cl_stk_p_q",
        "0009_st_cl_stk_p_q",
        "00010_st_cl_stk_p_q",
        "st_stk_in_tr_q",
        "sto_value",
        "sto_quatity",
        "st_prd_stk_q",
        ];
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }

    
}
