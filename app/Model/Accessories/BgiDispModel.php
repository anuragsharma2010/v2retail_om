<?php

namespace App\Model\Accessories;

use Illuminate\Database\Eloquent\Model;


class BgiDispModel extends Model
{
    protected $table = 'bgi_disp';
    public $timestamps = false;
    protected $fillable = [
       "st_code","st_name","segment","division","sub_division",
        "maj_cat","st_cd_maj_cat_rng_desc","st_cd_maj_cat_rng_mvgr_desc",
        "ssn","mvgr_matrix","rng","m1_q","m2_q","m3_q","m4_q","m5_q","m6_q",
        "m7_q","m8_q","m9_q","m10_q","m11_q","m12_q","m13_q","m1_v","m2_v","m3_v",
        "m4_v","m5_v","m6_v","m7_v","m8_v","m9_v","m10_v","m11_v","m12_v","m13_v"
        ];
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }

    
}
