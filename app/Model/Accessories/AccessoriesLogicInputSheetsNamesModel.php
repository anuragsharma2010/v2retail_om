<?php

namespace App\Model\Accessories;

use Illuminate\Database\Eloquent\Model;

class AccessoriesLogicInputSheetsNamesModel extends Model
{
    protected $table = 'accessories_logic_input_sheets_names';
    protected $fillable = [
        'excel_sheet_name', 'link_table_info',
        ];

    
}
