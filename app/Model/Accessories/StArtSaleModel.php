<?php

namespace App\Model\Accessories;

use Illuminate\Database\Eloquent\Model;


class StArtSaleModel extends Model
{
    protected $table = 'st_art_sale';
    public $timestamps = false;
    protected $fillable = [
       "store_code","link_article_code","f_m_y","day_number","sale_v","sale_q","gm_v"
        ];
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }

    
}
