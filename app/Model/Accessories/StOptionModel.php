<?php

namespace App\Model\Accessories;

use Illuminate\Database\Eloquent\Model;


class StOptionModel extends Model
{
    protected $table = 'st_option';
    public $timestamps = false;
    protected $fillable = [
     "store_code","store_name","sub_division","major_category_description",
        "range_segment","mvgr_matrix","season","wg_display_type","fg_display_type",
        "segment_mvgr_package_size","fg_fix_density","stcd_majcat_desc",
        "stcd_majcat_desc_rng_mvgr_desc","stcd_majcat_desc_rng_desc","major_category_segment_option_m_1",
        "major_category_segment_option_m_2","major_category_segment_option_m_3",
        "major_category_segment_option_m_4","major_category_segment_option_m_5",
        "null","major_category_segment_option_m_6","major_category_segment_option_m_7",
        "major_category_segment_option_m_8","major_category_segment_option_m_9",
        "major_category_segment_option_m_10","major_category_segment_option_m_11",
        "major_category_segment_option_m_12","major_category_segment_option_m_13",
        "major_category_segment_display_q_m_1","major_category_segment_display_q_m_2",
        "major_category_segment_display_q_m_3","major_category_segment_display_q_m_4",
        "major_category_segment_display_q_m_5","major_category_segment_display_q_m_6",
        "major_category_segment_display_q_m_7","major_category_segment_display_q_m_8",
        "major_category_segment_display_q_m_9","major_category_segment_display_q_m_10","major_category_segment_display_q_m_11","major_category_segment_display_q_m_12","major_category_segment_display_q_m_13","major_category_segment_sales_q_m_1","major_category_segment_sales_q_m_2","major_category_segment_sales_q_m_3","major_category_segment_sales_q_m_4","major_category_segment_sales_q_m_5","major_category_segment_sales_q_m_6","major_category_segment_sales_q_m_7","major_category_segment_sales_q_m_8","major_category_segment_sales_q_m_9","major_category_segment_sales_q_m_10","major_category_segment_sales_q_m_11","major_category_segment_sales_q_m_12","major_category_segment_sales_q_m_13"
        ];
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }

    
}
