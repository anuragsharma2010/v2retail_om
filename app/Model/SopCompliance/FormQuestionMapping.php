<?php

namespace App\Model\SopCompliance;

use Illuminate\Database\Eloquent\Model;

class FormQuestionMapping extends Model
{
    //
    protected $table = 'sop_form_question_mapping';
}
