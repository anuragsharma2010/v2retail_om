<?php

namespace App\Model\SopCompliance;

use Illuminate\Database\Eloquent\Model;

class SurveyFormSubmissionValue extends Model
{
    //
    protected $table = 'sop_survey_form_submission_value';

    protected $fillable = [
        'survey_form_submission_id', 'store_id', 'field_id', 'field_value_id', 'value', 'scorable', 'option_name',
    ];

    public function fieldValue(){
		return $this->hasOne('\App\Model\SopCompliance\FormFieldValue','id','field_value_id');
	}
	public function field(){
		return $this->hasOne('\App\Model\SopCompliance\FormField','id','field_id');
	}
	public function fieldRadio(){
		return $this->hasOne('\App\Model\SopCompliance\FormFieldValue','value');
	}
}
