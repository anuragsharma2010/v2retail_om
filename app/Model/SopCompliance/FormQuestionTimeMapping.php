<?php

namespace App\Model\SopCompliance;

use Illuminate\Database\Eloquent\Model;

class FormQuestionTimeMapping extends Model
{
    //
    protected $table = 'sop_form_question_time_mappings';
     public $timestamps = false;
    protected $fillable = [
        'form_group_id','form_question_id','form_id','start_time','end_time',
    ];

    public function groups(){
			return $this->belongsTo('Master\Models\GroupsModel','form_group_id');
	}
}
