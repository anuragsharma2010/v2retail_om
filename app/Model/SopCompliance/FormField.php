<?php

namespace App\Model\SopCompliance;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class FormField extends Model
{
    //
    protected $table = 'sop_form_fields';
     protected $fillable = [
        'field_type','field_name','field_placeholder','form_field_required','form_field_validation_rules', 'form_question_id', 'status', 'user_id',
    ];
    public function FormFieldValueMany()
    {
        return $this->hasMany('\App\Model\SopCompliance\FormFieldValue','form_field_id');
    }
    public function FormFieldValue()
    {
        return $this->hasOne('\App\Model\SopCompliance\FormFieldValue','form_field_id');
    }

    public function getFieldPlaceholderAttribute($value) {
        return Str::ucfirst(Str::lower($value));
    }
}
