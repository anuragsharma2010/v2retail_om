<?php

namespace App\Model\SopCompliance;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    //
    protected $table = 'sop_forms';

    protected $fillable = [
        'form_name', 'form_label','type','objective','instruction','method', 'enc_type', 'form_id', 'user_id', 'status',
    ];

    public function user()
    {
        return $this->belongsTo('\App\User','user_id');
    }

    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }
}
