<?php

namespace App\Model\SopCompliance;

use Illuminate\Database\Eloquent\Model;

class FormFieldValue extends Model
{
    //
    protected $table = 'sop_form_field_values';
     protected $fillable = [
        'form_field_id', 'form_field_value','form_field_option_name','associate_fields','associate_question', 'is_scorable', 'user_id', 'status',
    ];
}
