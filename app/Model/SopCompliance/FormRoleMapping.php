<?php

namespace App\Model\SopCompliance;

use Illuminate\Database\Eloquent\Model;

use Spatie\Permission\Traits\HasRoles;


class FormRoleMApping extends Model
{
    protected $table = 'sop_form_role_mappings';
    public $timestamps = false;
    protected $fillable = [
        'form_id', 'role_id',
    ];

    public function role(){
		return $this->belongsTo('Spatie\Permission\Models\Role');
	}
}
