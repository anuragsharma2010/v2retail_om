<?php

namespace App\Model\SopCompliance;

use Illuminate\Database\Eloquent\Model;

class FormQuestionTimeTaken extends Model
{
    //
    protected $table = 'sop_form_question_time_taken';
    // public $timestamps = false;
    protected $fillable = [
        'group_id','qus_id','user_id','act_start_time','act_end_time'
    ];
    
}
