<?php

namespace App\Model\SopCompliance;

use Illuminate\Database\Eloquent\Model;

class SurveyFormSubmission extends Model
{
    //
    protected $table = 'sop_survey_form_submission';

    protected $fillable = [
        'store_id', 'user_id', 'form_id', 'question_id', 'time_taken', 'date', 'latitude', 'longitude', 'start_time', 'end_time', 'status',
    ];
    public function question(){
		return $this->belongsTo('\App\Model\SopCompliance\FormQuestion','question_id');
	}

	public function submissionValues(){
		return $this->hasMany('\App\Model\SopCompliance\SurveyFormSubmissionValue','survey_form_submission_id');
	}
	 public function busgtedTime()
    {
        return $this->belongsTo('\App\Model\SopCompliance\FormQuestionTimeMapping','question_id','form_question_id');
    }

    public function store()
    {
        return $this->belongsTo('Master\Models\StoresModel', 'store_id');
    }
	public function user()
    {
        return $this->belongsTo('\App\User', 'user_id');
    }
}
