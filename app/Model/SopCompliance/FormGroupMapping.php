<?php

namespace App\Model\SopCompliance;

use Illuminate\Database\Eloquent\Model;

class FormGroupMapping extends Model
{
    

    protected $table = 'sop_form_group_mappings';
    public $timestamps = false;
    protected $fillable = [
        'form_id', 'group_id',
    ];

	public function groups(){
		return $this->belongsTo('Master\Models\GroupsModel','group_id');

	}
    
}
