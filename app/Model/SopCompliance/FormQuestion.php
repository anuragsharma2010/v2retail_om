<?php

namespace App\Model\SopCompliance;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class FormQuestion extends Model
{
    //
    protected $table = 'sop_form_questions';
    protected $fillable = [
        'question','question_alias','show_on_dashboard', 'sort_order','type','objective','instruction','method', 'enc_type', 'form_id', 'user_id', 'status',
    ];

    public function user()
    {
        return $this->belongsTo('\App\User','user_id');
    }

    public function SOPFormFieldMany()
    {
        return $this->hasMany('\App\Model\SopCompliance\FormField','form_question_id');
    }

    public function FormFieldValueMany()
    {
        return $this->hasMany('\App\Model\SopCompliance\FormFieldValue','form_field_id');
    }
     public function GroupTimeMany()
    {
        return $this->hasMany('\App\Model\SopCompliance\FormQuestionTimeMapping','form_question_id');
    }

    public function getQuestionAttribute($value) {
        return Str::ucfirst(Str::lower($value));
    }

    public function process()
    {
        return $this->belongsTo('App\Model\SopCompliance\Form', 'form_id', 'id');
    }

    public function roles()
    {
        return $this->hasMany('\App\Model\SopCompliance\FormRoleMApping','form_id','form_id');
    }
   
}
