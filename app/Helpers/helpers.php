<?php
function changeDateFormate($date,$date_format){
    return \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format($date_format);    
}
function imploadValue($types){
    $strTypes = implode(",", $types);
    return $strTypes;
  }
 
  function explodeValue($types){
    $strTypes = explode(",", $types);
    return $strTypes;
  }
  function prd($arr){
      echo "<pre>";
      $newArr = print_r($arr);
      echo "</pre>";
      die;
      return $newArr;
  }
  function pr($arr){
        echo "<pre>";
        $newArr = print_r($arr);
        echo "</pre>";
        return $newArr;
    }
  function remove_special_char($text) {
 
        $t = $text;
 
        $specChars = array(
            ' ' => '-',    '!' => '',    '"' => '',
            '#' => '',    '$' => '',    '%' => '',
            '&amp;' => '',    '\'' => '',   '(' => '',
            ')' => '',    '*' => '',    '+' => '',
            ',' => '',    '₹' => '',    '.' => '',
            '/-' => '',    ':' => '',    ';' => '',
            '<' => '',    '=' => '',    '>' => '',
            '?' => '',    '@' => '',    '[' => '',
            '\\' => '',   ']' => '',    '^' => '',
            '_' => '',    '`' => '',    '{' => '',
            '|' => '',    '}' => '',    '~' => '',
            '-----' => '-',    '----' => '-',    '---' => '-',
            '/' => '',    '--' => '-',   '/_' => '-',   
             
        );
 
        foreach ($specChars as $k => $v) {
            $t = str_replace($k, $v, $t);
        }
        return $t;
  }
function excelTransformDate($value)
{
    $date = \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
    return  \Carbon\Carbon::parse($date)->format('Y-m-d');
}