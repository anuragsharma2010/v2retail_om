<?php
namespace App\Helpers;

use Mail;
use Config;
use view;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class NotificationHelper {

    public static function sendNotification($NotificationTitle, $NotificationBodyMsg, $deviceToken) {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($NotificationTitle);
        $notificationBuilder->setBody($NotificationBodyMsg)
                            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => 'my_data']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = $deviceToken;

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
    }

    public static function sendNotificationOnMultipleDevice($NotificationTitle, $NotificationBodyMsg, $deviceTokenArray = array()) {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($NotificationTitle);
        $notificationBuilder->setBody($NotificationBodyMsg)
                            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => 'my_data']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        // You must change it to get your tokens
        $tokens = $deviceTokenArray;

        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
    }
}