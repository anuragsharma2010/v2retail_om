<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidFile implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(...$extension)
    {
       $this->extension = $extension;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;
        return (in_array($value->getClientOriginalExtension(), $this->extension));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return  'Please select valid file format! e.g - ' . implode(",",$this->extension);
    }
}
