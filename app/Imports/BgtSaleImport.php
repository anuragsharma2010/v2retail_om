<?php

namespace App\Imports;


use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Model\Accessories\BgtSaleModel;
class BgtSaleImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
       $dataObj=new BgtSaleModel();
       $fields=$dataObj->getTableColumns();
       
       $validateArray=array();
       foreach($fields as $fieldRow){
           $validateArray[$fieldRow]=$row[$fieldRow];
       }
            
       
     //  echo "hi.model..";die();
       return new BgtSaleModel($validateArray);
    }
}