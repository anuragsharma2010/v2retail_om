<?php

namespace App\Imports;


use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Model\Accessories\StOptionModel;
class StOptionImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
       $dataObj=new StOptionModel();
       $fields=$dataObj->getTableColumns();
       
       $validateArray=array();
       foreach($fields as $fieldRow){
           if($fieldRow=='null')
           {
               continue;
           }
           $validateArray[$fieldRow]=$row[$fieldRow];
       }
            
       
     //  echo "hi.model..";die();
       return new StOptionModel($validateArray);
    }
}