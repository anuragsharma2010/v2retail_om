<?php

namespace App\Imports;


use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Model\Accessories\BgtRevisedSaleModel;
class BgtRevisedSaleImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
       $dataObj=new BgtRevisedSaleModel();
       $fields=$dataObj->getTableColumns();
       
       $validateArray=array();
       foreach($fields as $fieldRow){
           $validateArray[$fieldRow]=$row[$fieldRow];
       }
            
       
     //  echo "hi.model..";die();
       return new BgtRevisedSaleModel($validateArray);
    }
}