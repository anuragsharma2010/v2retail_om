<?php

namespace App\Http\Middleware;
//use Api\Models\AgentsOtpModel;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\User;
class CustomApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    public function handle($request, Closure $next)
    {
      //echo "hi....";die();

       if($request->header('token')==config('TaskTracker.token_key_api'))
       {
          //  $user = User::find(1);
           
            
           
          
           if(trim($request->header('Authorization'))){
            $api=$request->header('Authorization'); 
            
            $user=User::where('api_login',$api)->first();
            
            if(isset($user->api_login)){
           
            if($request->header('Authorization')!=$user->api_login){

               // $agentUser = AgentsOtpModel::where(['token_key' => $request->user_key])->first();
                return Response()->json([
                        'status' => 0,
                        'message' => "Authorization not match"
                    ], 503);


            }
           
            return $next($request);
            }else {
                return Response()->json([
                        'status' => 0,
                        'message' => "Not authorized"
                    ], 503);
            }
            
           }else {
                return Response()->json([
                        'status' => 0,
                        'message' => "Not authorized"
                    ], 503);
           }
          
            return $next($request);
       } else {

            return Response()->json([
                'status' => 0,
                'message' => "token error"
            ], 503);
       }

    }
}
