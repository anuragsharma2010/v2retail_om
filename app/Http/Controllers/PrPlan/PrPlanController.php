<?php

namespace App\Http\Controllers\PrPlan;

use App\Http\Controllers\Controller;
use App\Model\Grc\Grc;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\GrcImport;
use App\Exports\Export;

class PrPlanController extends Controller
{
    public function majCat()
    {
        return view('prplan.majcat.index');
    }

    public function majCatImport(Request $request)
    {
        return view('prplan.majcat.import');
    }

    public function majCatSeg()
    {
        return view('prplan.majcatseg.index');
    }

    public function majCatSegImport(Request $request)
    {
        return view('prplan.majcatseg.import');
    }

    public function majCatSegMvgr()
    {
        return view('prplan.majcatsegmvgr.index');
    }

    public function majCatSegMvgrImport(Request $request)
    {
        return view('prplan.majcatsegmvgr.import');
    }

    public function referenceArticle()
    {
        return view('prplan.referencearticle.index');
    }

    public function referenceArticleImport(Request $request)
    {
        return view('prplan.referencearticle.import');
    }

    public function optionsArticle()
    {
        return view('prplan.optionsarticle.index');
    }

    public function optionsArticleImport(Request $request)
    {
        return view('prplan.optionsarticle.import');
    }

    public function autoPr()
    {
        return view('prplan.autopr.index');
    }

    public function autoPrImport(Request $request)
    {
        return view('prplan.autopr.import');
    }

}
