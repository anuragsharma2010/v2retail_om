<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;

use App\Model\SopCompliance\Form;
use App\Model\SopCompliance\FormRoleMapping;
use App\Model\SopCompliance\FormGroupMapping;
use App\Model\SopCompliance\FormQuestion;
use App\Model\SopCompliance\FormField;
use App\Model\SopCompliance\FormFieldValue;
use App\Model\SopCompliance\FormQuestionTimeMapping;
use App\Model\SopCompliance\SurveyFormSubmission;
use App\Model\SopCompliance\SurveyFormSubmissionValue;


use Master\Models\StoresModel;
use Models\FormModel;
use Master\Models\GroupsModel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()){

            $data =new StoresModel();
            $data->total_stores =  DB::table('stores')->count();

            $currDate =  date('Y-m-d');
            //DB::enableQueryLog();
            $top_5_effic_stores =  DB::select("SELECT fs.store_id, s.st_name, COUNT(fg.`form_id`) AS processCount FROM `sop_forms` AS f
            LEFT JOIN `sop_form_group_mappings` AS fg ON fg.form_id = f.id
            LEFT JOIN `sop_survey_form_submission` AS fs ON fs.`form_id` = fg.`form_id`
            LEFT JOIN `stores` AS s ON s.`id` = fs.`store_id`
            WHERE s.`st_status` = '1' AND fs.date='".$currDate."'
            GROUP BY s.`id`
            HAVING MAX(fg.`form_id`)
            LIMIT 5");
            $data->st_lables    =   array_column($top_5_effic_stores,'st_name');
            $data->processCount =   array_column($top_5_effic_stores,'processCount');
            //   $query = DB::getQueryLog();
            $data->total_groups =  DB::table('groups')->count();
            $data->total_processes =  DB::table('sop_forms')->count();
            $data->total_efficience =  DB::select("SELECT COUNT(*) AS count  FROM sop_form_questions AS q
                  JOIN sop_form_fields AS f ON f.form_question_id=q.id AND f.field_type='radio' AND f.parent_id='0' AND q.parent_id='0' AND q.show_on_dashboard='1'
                  JOIN sop_form_field_values AS v ON v.form_field_id=f.id AND (form_field_option_name='Yes' || form_field_option_name='1')
                  JOIN sop_survey_form_submission_value AS sv ON sv.field_id=f.id AND sv.field_value_id=v.id
                  GROUP BY q.id");

    //prd($top_5_effic_stores);
            //$data = json_decode(json_encode($data));date('Y-m-d')
        //echo "hi...";die();SELECT count(*) FROM `sop_survey_form_submission` WHERE `store_id` = 1 AND `form_id` = 2 AND `date` = '2020-03-11'

            //form data start
            $forms=Form::orderBy('id','DESC')->get();

            foreach($forms as $val)
            {
               $process_id = $val->id;
               $group_ids=FormGroupMapping::where('form_id',$process_id)->pluck('group_id')->toArray();
               $stores=StoresModel::whereIn('st_group',$group_ids)->get();
               $questioncount = FormQuestion::where('form_id', $process_id)->where('parent_id', 0)->count();
               $val->stores=$stores;

               foreach($val->stores as $key=>$store){
                $availed_score =0;
                $percentage = 0;
                $yesCount= 0;
                $questionnumberofyesCount = DB::select("SELECT COUNT(*) AS count  FROM sop_form_questions AS q
                  JOIN sop_form_fields AS f ON f.form_question_id=q.id AND f.field_type='radio' AND f.parent_id='0' AND q.parent_id='0' AND q.show_on_dashboard='1'
                  JOIN sop_form_field_values AS v ON v.form_field_id=f.id AND (form_field_option_name='Yes' || form_field_option_name='1')
                  JOIN sop_survey_form_submission_value AS sv ON sv.field_id=f.id AND sv.field_value_id=v.id
                  WHERE q.form_id = '".$process_id."'
                  GROUP BY q.id");
                if(!empty($questionnumberofyesCount)){
                    $yesCount = $questionnumberofyesCount[0]->count;
                }else{
                    $yesCount = 0;
                }

                $availed_score = $yesCount;
                $percentage = $questioncount>0 ? round((($availed_score/$questioncount)*100),2) :'0' ;
                $store->percentage= $percentage;
            }
            }

            //form data end

        return view('home',compact('data','forms'));
        }else{
            return view('auth/login');
        }
    }
}
