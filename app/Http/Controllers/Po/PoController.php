<?php

namespace App\Http\Controllers\Po;

use App\Http\Controllers\Controller;
use App\Model\Po\Po;
use Master\Models\ArticlesModel;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\PoImport;
use App\Exports\Export;
use DB;

class PoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pos = Po::orderBy('created_at', 'asc');
        $date = $request->month?'01-'.$request->month:date('Y-m-d');
        $pos = $pos->whereMonth('po_date', '=', Carbon::parse($date)->format('m'))->whereYear('po_date', '=', Carbon::parse($date)->format('Y'));
        $pos = $pos->paginate($request->perpage??10);
        return view('po.index',compact('pos'));
     }
    public function import(Po $pendingPO)
    {
        return view('po.import');
    }
    public function importTemplate()
    {
        $headings = ['DATE','Site','Doc Date','Del Date','Purch.Doc','Vendor/supplying site','Article','PGr','Item','Net Price','ACT-PO-Q','ACT-PO-V','GR Qty','PEND-PO-Q','PEND-PO-V'];
        return (new Export(collect([]),$headings))->download('po.xlsx');
    }
    public function importSave(Request $request)
    {
        $this->validate($request, [
            'po_file' => ['required', new \App\Rules\ValidFile('xlsx')]
        ],['po_file.required' => 'This field is required!']);
     
        if($request->submit=='Submit'){

            $sheets = Excel::toCollection(new PoImport, $request->po_file);
            foreach ($sheets[0] as $row) 
            {
            //   prd(count($sheets[0]));  dd($row);
                $poDate     =  (Carbon::parse($row['date'])->format('Y-m-d')=='1970-01-01') ? excelTransformDate($row['date']) : Carbon::parse($row['date'])->format('Y-m-d');
                $doc_date   =  (Carbon::parse($row['doc_date'])->format('Y-m-d')=='1970-01-01') ? excelTransformDate($row['doc_date']) : Carbon::parse($row['doc_date'])->format('Y-m-d');
                $del_date   =  (Carbon::parse($row['del_date'])->format('Y-m-d')=='1970-01-01') ? excelTransformDate($row['del_date']) : Carbon::parse($row['del_date'])->format('Y-m-d');
                $purchdoc   =  $row['purchdoc'];
                $article    =  $row['article'];
                Po::updateOrCreate(
                    ['purchase_doc' => $purchdoc, 'article' => $article],
                    ['po_date' => $poDate, 'site' => $row['site'], 'doc_date' => $doc_date, 'del_date' => $del_date, 'purchdoc' => $purchdoc, 'vendor' => $row['vendorsupplying_site'], 'article' => $row['article'],'pgr' => $row['pgr'],'item' => $row['item'],'net_price' => $row['net_price'],'act_po_q' => $row['act_po_q'],'act_po_v' => $row['act_po_v'],'gr_qty' => $row['gr_qty'],'pend_po_q' => $row['pend_po_q'],'pend_po_v' => $row['pend_po_v']]
                );
            }
            //if($excelData){
            return redirect()->route('po.index')
                ->with('success','PO Imported successfully');
            //}
        }
   }
 }
