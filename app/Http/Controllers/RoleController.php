<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Spatie\Permission\Models\Role;
use App\Model\RolesModel;
use Spatie\Permission\Models\Permission;

use DB;
use App\Model\DepartmentModel;
use App\Model\DivisionModel;

class RoleController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    function __construct()

    {

         $this->middleware('permission:role-list');

         $this->middleware('permission:role-create', ['only' => ['create','store']]);

         $this->middleware('permission:role-edit', ['only' => ['edit','update']]);

         $this->middleware('permission:role-delete', ['only' => ['destroy']]);

    }


    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(Request $request)

    {
        
        $roles = New RolesModel();
        $fields=$roles->getTableColumns(); 
            if(isset($request->search) && isset($request->searchtxt)){
          foreach ($fields as $value) {
              if($value=='id' or 
                      $value=='created_at' 
                      or $value=='updated_at' or $value=='st_status'){
                  continue;
              }
             $roles=$roles->orWhere($value,'like','%'.$request->searchtxt.'%');  
           }
         $roles=$roles->orderBy('id','DESC')->paginate(15);
       }else {
           $roles=$roles->orderBy('id','DESC')->paginate(15);
       }
       $roles->searchtxt=$request->searchtxt?$request->searchtxt:'';
        foreach($roles as $key=>$rowVal){
           if($rowVal->div_id){
            $divData=DivisionModel::where('id',$rowVal->div_id)->first();
            if(isset($divData->id))
            $rowVal->div_name=$divData->div_name;
           }
           if($rowVal->dept_id){
            $rowVal->dept_name="test";
            $deptData=DepartmentModel::where('id',$rowVal->dept_id)->first();
            if(isset($deptData->id))
            $rowVal->dept_name=$deptData->dept_name;
           }
            
        }
        $roles->searchtxt=$request->searchtxt?$request->searchtxt:'';
        return view('roles.index',compact('roles'))

            ->with('i', ($request->input('page', 1) - 1) * 5);

    }


    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
        $divisions = DivisionModel::orderBy('id', 'DESC')->get();
        $department = DepartmentModel::orderBy('id', 'DESC')->get();
        $permission = Permission::get();

        return view('roles.create',compact('permission','divisions','department'));

    }


    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {
        $messages = array(
            'required' => 'This field is required.',
        );
        
        $this->validate($request, [

            'name' => 'required|unique:roles,name',
            'div_id' =>'required',
          //  'permission' => 'required',
            'dept_id'=>'required',

        ],$messages);


       // $role = Role::create(['name' => $request->input('name'),'div_id' => $request->input('div_id'),'dept_id' => $request->input('dept_id')]);
        $role = new Role();
        $role->name = $request->input('name');
        $role->div_id = $request->input('div_id');
        $role->dept_id = $request->input('dept_id');
        $role->save();
        $role->syncPermissions($request->input('permission'));


        return redirect()->route('roles.index')

                        ->with('success','Role created successfully');

    }

    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)
    {
        $flage_ajax=isset($_REQUEST['flage_ajax'])?$_REQUEST['flage_ajax']:'';
        
        if(isset($flage_ajax)){
           $dataDept = DepartmentModel::where('div_id',$id)->orderBy('id', 'DESC')->get(); 
          // dd($dataDept);
           return view('roles.option',compact('dataDept'));
        }else {
        $role = Role::find($id);

        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")

            ->where("role_has_permissions.role_id",$id)

            ->get();
        
        
        return view('roles.show',compact('role','rolePermissions'));
        }

    }


    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        $role = Role::find($id);
         
        $permission = Permission::get();
        $divisions = DivisionModel::orderBy('id', 'DESC')->get();
        $department = new DepartmentModel();
        $stoption='';
        if(isset($role->div_id) && $role->div_id!=0){
        $department = DepartmentModel::where('div_id',$role->div_id)->orderBy('id', 'DESC')->get();
        foreach($department as $dept_val){
           if($role->dept_id>0)                 
           $stoption=$stoption.'<option value="'.$dept_val->id.'" selected="" >'.$dept_val->dept_name.'</option>';
           else                       
            $stoption=$stoption.'<option value="'.$dept_val->id.'" >'.$dept_val->dept_name.'</option>';
        }
        }
        
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)

            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')

            ->all();


        return view('roles.edit',compact('role','permission','rolePermissions','divisions','department','stoption'));

    }


    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        $messages = array(
            'required' => 'This field is required.',
        );
        
        $this->validate($request, [

            'name' => 'required',
            'div_id' =>'required',
          //  'permission' => 'required',
            'dept_id'=>'required',

        ],$messages);
        

        $role = Role::find($id);
//        {
//            return view('roles.edit',compact('role','permission','rolePermissions','divisions','department'));
//
//        }

        $role->name = $request->input('name');
        $role->div_id = $request->input('div_id');
        $role->dept_id = $request->input('dept_id');
        $role->save();


        $role->syncPermissions($request->input('permission'));


        return redirect()->route('roles.index')

                        ->with('success','Role updated successfully');

    }

    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        DB::table("roles")->where('id',$id)->delete();

        return redirect()->route('roles.index')

                        ->with('success','Role deleted successfully');

    }
    public function getInfo(request $request){
        echo "hi.....";die();
    }

}
