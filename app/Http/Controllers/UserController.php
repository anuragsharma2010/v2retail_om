<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\User;
use App\Users;
use App\Model\DepartmentModel;
use App\Model\DivisionModel;
use Spatie\Permission\Models\Role;
use Master\Models\StoresModel;
use DB;
use Illuminate\Support\Facades\Auth;
use Hash;


class UserController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(Request $request)

    {
        $user = Auth::user();
        
        // Get the currently authenticated user's ID...
        
        $id = Auth::id();
        $flage_ajax=isset($_REQUEST['flage_ajax'])?$_REQUEST['flage_ajax']:'';
        $data = new User();
        $fields=$data->getTableColumns(); 
//        if(isset($request->search) && isset($request->searchtxt)){
//        $data = User::where('id',"!=",$id)->where('id',"!=",$id)->orderBy('id','DESC')->paginate(10);
//        }else {
//            
//        }
        if(isset($request->search) && isset($request->searchtxt)){
          foreach ($fields as $value) {
              if($value=='id' or 
                      $value=='created_at' 
                      or $value=='updated_at' or $value=='st_status'){
                  continue;
              }
             $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');  
           }
         $data=$data->orderBy('id','DESC')->paginate(15);
       }else {
           $data=$data->orderBy('id','DESC')->paginate(15);
       }
       $data->searchtxt=$request->searchtxt?$request->searchtxt:'';
        return view('users.index',compact('data'))

            ->with('i', ($request->input('page', 1) - 1) * 5);

    }


    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

      $divisions = DivisionModel::orderBy('id', 'DESC')->get();
      $department = DepartmentModel::orderBy('id', 'DESC')->get();

      $roles = Role::pluck('name','name')->all();
      $user=new User();
        return view('users.create',compact('roles','divisions','department','user'));

    }


    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {
        $messages = array(
            'required' => 'This field is required.',
        );

        $this->validate($request, [

            'name' => 'required',
            'div_id'=>'required',
            'dept_id'=>'required',
            'email' => 'required|email|unique:users,email',
            'confirm-password'=>'required',
            'password' => 'required|same:confirm-password',

           // 'roles' => 'required'

            ],$messages);
        if(trim($request->dept_id) && ucfirst($request->dept_id)==ucfirst('none')){
            
            $this->validate($request, [

            'name' => 'required',
            'div_id'=>'required',
            'dept_id'=>'required',
            'store_id'=>'required',
            'email' => 'required|email|unique:users,email',
            'confirm-password'=>'required',
            'password' => 'required|same:confirm-password',

           // 'roles' => 'required'

            ],$messages);
            
            
        }
        else if(trim($request->dept_id) && ucfirst($request->dept_id)!=ucfirst('none')){
            $this->validate($request, [

            'name' => 'required',
            'div_id'=>'required',
            'dept_id'=>'required',
            'email' => 'required|email|unique:users,email',
            'confirm-password'=>'required',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'

        ],$messages);
        }
                


        $input = $request->all();

        $input['password'] = Hash::make($input['password']);
       // if($input['dept_id'])
        
         if(trim($input['dept_id']) && ucfirst($input['dept_id'])==ucfirst('none')){
             $input['dept_id']=0;
             
         }   
     
         
        $user = User::create($input);
        
        $user->assignRole($request->input('roles'));


        return redirect()->route('users.index')

                        ->with('success','User created successfully');

    }


    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id,Request $request)

    {
       $flage_ajax=isset($_REQUEST['flage_ajax'])?$_REQUEST['flage_ajax']:'';
       //echo $request->flage_ajax;die();
        if(isset($flage_ajax) && $flage_ajax==1){
           $dataDept = DepartmentModel::where('div_id',$id)->orderBy('id', 'DESC')->get(); 
          // dd($dataDept);
           return view('roles.option',compact('dataDept'));
        }
        else if(isset($flage_ajax) && $flage_ajax==2){
             $dataDept = StoresModel::orderBy('id', 'DESC')->get(); 
          // dd($dataDept);
           return view('users.store_option',compact('dataDept'));
        }
        else if(isset($flage_ajax) && $flage_ajax==3){
             $dataDept = Role::where('dept_id',$id)->orderBy('id', 'DESC')->get(); 
          // dd($dataDept);
           return view('users.role_option',compact('dataDept'));
        }
        else {
        $user = User::find($id);
        return view('users.show',compact('user'));
        }
    }


    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        $user = User::find($id);
        $divisions = DivisionModel::orderBy('id', 'DESC')->get();
        $department = DepartmentModel::orderBy('id', 'DESC')->get();
//        if($user->dept_id>0){
//        $roles = Role::where('dept_id',$user->dept_id)->pluck('name','name')->all();
//        }
         $datastore = StoresModel::orderBy('id', 'DESC')->get(); 

        $userRole = $user->roles->pluck('id','id')->all();
        
        $stoption='';
        if(isset($user->div_id) && $user->div_id!=0){
        $department = DepartmentModel::where('div_id',$user->div_id)->orderBy('id', 'DESC')->get();
        foreach($department as $dept_val){
           if($user->dept_id==$dept_val->id)                 
           $stoption=$stoption.'<option value="'.$dept_val->id.'" selected="" >'.$dept_val->dept_name.'</option>';
           else                       
            $stoption=$stoption.'<option value="'.$dept_val->id.'" >'.$dept_val->dept_name.'</option>';
        }
        }
        $roleOpt='';
        if(isset($user->dept_id) && $user->dept_id > 0){
            
        $roles = Role::where('dept_id',$user->dept_id)->orderBy('id', 'DESC')->get();
       
       
        foreach($roles as $role_val){
         
           if(in_array($role_val->id, $userRole))                 
           $roleOpt=$roleOpt.'<option value="'.$role_val->id.'" selected="" >'.$role_val->name.'</option>';
           else                       
            $roleOpt=$roleOpt.'<option value="'.$role_val->id.'" >'.$role_val->name.'</option>';
        }
        }
        
        return view('users.edit',compact('user','userRole','divisions','department','stoption','roleOpt','datastore'));

    }


    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {
         $messages = array(
            'required' => 'This field is required.',
        );
        $this->validate($request, [

            'name' => 'required',
            'div_id'=>'required',
            'dept_id'=>'required',
            'email' => 'required|email',

            'password' => 'same:confirm-password',

          //  'roles' => 'required'

        ],$messages);
        if(trim($request->dept_id) && ucfirst($request->dept_id)==ucfirst('none')){
             $this->validate($request, [

            'name' => 'required',
          
            'store_id'=>'required',
            //|unique:users,email
            'email' => 'required|email',
           
            'password' => 'same:confirm-password',

           // 'roles' => 'required'

            ],$messages);
        }
         else if(trim($request->dept_id) && ucfirst($request->dept_id)!=ucfirst('none')){
            $this->validate($request, [

            'name' => 'required',
          
           // 'store_id'=>'required',|unique:users,email
            'email' => 'required|email',
           
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ],$messages);
        }

          $user = User::find($id);
        $input = $request->all();

        if(!empty($input['password'])){

            $input['password'] = Hash::make($input['password']);

        }else{

             $input['password'] = $user->password;

        }

         if(trim($input['dept_id']) && ucfirst($input['dept_id'])==ucfirst('none')){
             $input['dept_id']=0;
             
         }
      
        
        $user->update($input);

        DB::table('model_has_roles')->where('model_id',$id)->delete();


        $user->assignRole($request->input('roles'));


        return redirect()->route('users.index')

                        ->with('success','User updated successfully');

    }


    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        User::find($id)->delete();

        return redirect()->route('users.index')

                        ->with('success','User deleted successfully');

    }

}
