<?php

namespace App\Http\Controllers\OtbPlan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\PoImport;
use App\Exports\Export;

class OtbPlanController extends Controller
{
    public function majCat()
    {
        return view('otbplan.majcat.index');
    }

    public function majCatImport(Request $request)
    {
        return view('otbplan.majcat.import');
    }

    public function majCatSeg()
    {
        return view('otbplan.majcatseg.index');
    }

    public function majCatSegImport(Request $request)
    {
        return view('otbplan.majcatseg.import');
    }

    public function majCatSegMvgr()
    {
        return view('otbplan.majcatsegmvgr.index');
    }

    public function majCatSegMvgrImport(Request $request)
    {
        return view('otbplan.majcatsegmvgr.import');
    }

    public function referenceArticle()
    {
        return view('otbplan.referencearticle.index');
    }

    public function referenceArticleImport(Request $request)
    {
        return view('otbplan.referencearticle.import');
    }

    public function optionsArticle()
    {
        return view('otbplan.optionsarticle.index');
    }

    public function optionsArticleImport(Request $request)
    {
        return view('otbplan.optionsarticle.import');
    }
}
