<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Model\DivisionModel;
use App\User;
use Spatie\Permission\Models\Role;
use DB;

class DivisionController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $data = new DivisionModel();
        $fields = $data->getTableColumns();
        if (isset($request->search) && isset($request->searchtxt)) {
            foreach ($fields as $value) {
                if ($value == 'id' or
                        $value == 'created_at'
                        or $value == 'updated_at' or $value == 'st_status') {
                    continue;
                }
                $data = $data->orWhere($value, 'like', '%' . $request->searchtxt . '%');
            }
            $data = $data->orderBy('id', 'DESC')->paginate(15);
        } else {
            $data = $data->orderBy('id', 'DESC')->paginate(15);
        }
        $data->searchtxt = $request->searchtxt ? $request->searchtxt : '';


        return view('division.index', compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function edit($id) {
        $DataObj = DivisionModel::where(['id' => $id])->first();

        return view('division.create', ['DataObj' => $DataObj, 'id' => $id]);
    }

    public function create() {
        $DivisionObj = new DivisionModel();
        return view('division.create', ['DivisionObj' => $DivisionObj]);
    }

    public function show(Request $request) {
        echo "hi..show...";
        die();
    }

    public function store(Request $request) {

        $DataObj = new DivisionModel();
        
         $messages = array(
            'required' => 'This field is required.',
        );
        $validator = Validator::make($request->all(), [
                    'div_name' => 'required|string|max:50',
//                    'div_code' => 'required|string|max:90',
//                    'div_desc' => 'required|string|max:200',
                    'div_status' => 'required',
        ],$messages);

        if ($validator->fails()) {

            return view('division.create', ['errors' => $validator->errors(), 'DataObj' => $request]);
        }
        $input = $request->all();

        $DataObj->create($input);
        return redirect()->route('division.index')
                        ->with('success', 'Division created successfully');
    }

    public function update(Request $request) {

        $validator = Validator::make($request->all(), [
                    'div_name' => 'required|string|max:50',
//                    'div_code' => 'required|string|max:90',
//                    'div_status' => 'required',
                    'id' => 'required'
        ]);

        if ($validator->fails()) {
            return view('division.create', ['errors' => $validator->errors(), 'DataObj' => $request]);
        }

        if (isset($request->id)) {
            $DataObj = DivisionModel::find($request->id);
            $DataObj->update($request->all());
            return redirect()->route('division.index')
                            ->with('success', 'Division Updated successfully');
        } else {
            return "Record not found";
        }
    }

    public function destroy($id) {

        DivisionModel::find($id)->delete();

        return redirect()->route('division.index')
                        ->with('success', 'Division deleted successfully');
    }

}
