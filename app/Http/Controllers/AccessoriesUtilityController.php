<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Model\Accessories\AccessoriesLogicInputSheetsNamesModel;

use App\Model\Accessories\StStockModel;
use App\Model\Accessories\StOptionModel;
use App\Imports\StStockImport;
use App\Imports\StOptionImport;

use App\Model\Accessories\DcStkModel;
use App\Imports\DcStkImport;

use App\Http\Controllers\Controller;

use App\User;

use Spatie\Permission\Models\Role;

use Maatwebsite\Excel\Facades\Excel;
//BgtSale
use App\Model\Accessories\BgtSaleModel;
use App\Imports\BgtSaleImport;

//BgtRevisedSale
use App\Model\Accessories\BgtRevisedSaleModel;
use App\Imports\BgtRevisedSaleImport;

//StArtSale
use App\Model\Accessories\StArtSaleModel;
use App\Imports\StArtSaleImport;
//bgi_disp
use App\Model\Accessories\BgiDispModel;
use App\Imports\BgiDispImport;
use DB;

use Hash;
//use Illuminate\Support\Facades\Route;


class AccessoriesUtilityController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(Request $request)
    {


       $table_id=isset($request->table_id) ? $request->table_id : '' ;
       $dataObjTable=AccessoriesLogicInputSheetsNamesModel::groupBy('link_table_info')->get();
       $data =new AccessoriesLogicInputSheetsNamesModel();
       $data->table_list=$dataObjTable;

       if($table_id){
         $tableObj= AccessoriesLogicInputSheetsNamesModel::where('id',$table_id)->orderBy('id')->first();
         if(isset($tableObj->link_table_info)){

             Switch ($tableObj->link_table_info) {

                 case "st_stock":{
                    $dataobj=new StStockModel();
                    $fields=$dataobj->getTableColumns();
                    $data=new StStockModel();

                     if(isset($request->search) && isset($request->searchtxt)){
                         foreach ($fields as $value) {

                           $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');
                          }

                         $data=$data->paginate(15);

                     }else {
                         $data=StStockModel::paginate(15);
                     }
                    $data->fields=$fields;
                    $data->table_id=$table_id;
                    $data->table_list=$dataObjTable;

                     return view('accessories_utility.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5 );
                 }
                 case "st_option":{

                    $dataobj=new StOptionModel();
                    $fields=$dataobj->getTableColumns();
                    $data=new StOptionModel();

                     if(isset($request->search) && isset($request->searchtxt)){
                         foreach ($fields as $value) {

                           $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');
                          }

                         $data=$data->paginate(15);

                     }else {
                         $data=StOptionModel::paginate(15);
                     }
                    $data->fields=$fields;
                    $data->table_id=$table_id;
                    $data->table_list=$dataObjTable;

                     return view('accessories_utility.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5 );

                    break;
                 }
                 case "dc_stock":{

                 $dataobj=new DcStkModel();
                    $fields=$dataobj->getTableColumns();
                    $data=new DcStkModel();

                     if(isset($request->search) && isset($request->searchtxt)){
                         foreach ($fields as $value) {

                           $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');
                          }

                         $data=$data->paginate(15);

                     }else {
                         $data=DcStkModel::paginate(15);
                     }
                    $data->fields=$fields;
                    $data->table_id=$table_id;
                    $data->table_list=$dataObjTable;

                    return view('accessories_utility.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5 );
                 }
                  case "st_art_sale":{

                 $dataobj=new StArtSaleModel();
                    $fields=$dataobj->getTableColumns();
                    $data=new StArtSaleModel();

                     if(isset($request->search) && isset($request->searchtxt)){
                         foreach ($fields as $value) {

                           $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');
                          }

                         $data=$data->paginate(15);

                     }else {
                         $data=StArtSaleModel::paginate(15);
                     }
                    $data->fields=$fields;
                    $data->table_id=$table_id;
                    $data->table_list=$dataObjTable;

                    return view('accessories_utility.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5 );
                 }
                 //bgt_sale
                  case "bgt_sale":{

                 $dataobj=new BgtSaleModel();
                    $fields=$dataobj->getTableColumns();
                    $data=new BgtSaleModel();

                     if(isset($request->search) && isset($request->searchtxt)){
                         foreach ($fields as $value) {

                           $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');
                          }

                         $data=$data->paginate(15);

                     }else {
                         $data=BgtSaleModel::paginate(15);
                     }
                    $data->fields=$fields;
                    $data->table_id=$table_id;
                    $data->table_list=$dataObjTable;

                    return view('accessories_utility.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5 );
                 }
                 //BgtRevisedSaleModel
                   case "bgt_revised_sale":{

                 $dataobj=new BgtRevisedSaleModel();
                    $fields=$dataobj->getTableColumns();
                    $data=new BgtRevisedSaleModel();

                     if(isset($request->search) && isset($request->searchtxt)){
                         foreach ($fields as $value) {

                           $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');
                          }

                         $data=$data->paginate(15);

                     }else {
                         $data=BgtRevisedSaleModel::paginate(15);
                     }
                    $data->fields=$fields;
                    $data->table_id=$table_id;
                    $data->table_list=$dataObjTable;

                    return view('accessories_utility.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5 );
                 }
                  //bgi_disp
                  case "bgi_disp":{

                 $dataobj=new BgiDispModel();
                    $fields=$dataobj->getTableColumns();
                    $data=new BgiDispModel();

                     if(isset($request->search) && isset($request->searchtxt)){
                         foreach ($fields as $value) {

                           $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');
                          }

                         $data=$data->paginate(15);

                     }else {
                         $data=BgiDispModel::paginate(15);
                     }
                    $data->fields=$fields;
                    $data->table_id=$table_id;
                    $data->table_list=$dataObjTable;

                    return view('accessories_utility.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5 );
                 }
         }
       }



        }

        return view('accessories_utility.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5 );
    }


    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

       // $roles = Role::pluck('name','name')->all();

        return view('accessories_utility.create');

    }


    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)
    {
        $table_name=isset($request->table_name)? $request->table_name :'';
        $submit=isset($request->submit)? $request->submit :'';
        $file=isset($request->upoadfile)? $request->upoadfile :'';

        switch ($table_name){
           case "st_stock" : {
               if(isset($submit)){
                 //   print_r("hi....");die();
                   $dataTableObj=new AccessoriesLogicInputSheetsNamesModel();
                   $dataTableObj->excel_sheet_name = $file->getClientOriginalName();
                   $dataTableObj->link_table_info = $table_name;
                   $data=$dataTableObj->save();
                   if($dataTableObj->save())
                   {
                       //StStockModel
                       //echo "hi";die();
                       StStockModel::query()->delete();
                       $test=Excel::import(new StStockImport, $file);
                       return redirect()->route('accessories-utility.index')

                        ->with('success','Updated successfully');
                   }
                }
               break;
            }
            case "st_option" : {
               if(isset($submit)){
                 //   print_r("hi....");die();
                   $dataTableObj=new AccessoriesLogicInputSheetsNamesModel();
                   $dataTableObj->excel_sheet_name = $file->getClientOriginalName();
                   $dataTableObj->link_table_info = $table_name;
                   $data=$dataTableObj->save();
                   if($dataTableObj->save())
                   {
                       //StStockModel
                       //echo "hi";die();
                       StOptionModel::query()->delete();
                       $test=Excel::import(new StOptionImport, $file);
                       return redirect()->route('accessories-utility.index')

                        ->with('success','Updated successfully');
                   }
                }
               break;
            }
            case "dc_stock" : {
               if(isset($submit)){
                 //   print_r("hi....");die();
                   $dataTableObj=new AccessoriesLogicInputSheetsNamesModel();
                   $dataTableObj->excel_sheet_name = $file->getClientOriginalName();
                   $dataTableObj->link_table_info = $table_name;
                   $data=$dataTableObj->save();
                   if($dataTableObj->save())
                   {
                       //StStockModel
                       //echo "hi";die();
                       DcStkModel::query()->delete();
                       $test=Excel::import(new DcStkImport, $file);
                       return redirect()->route('accessories-utility.index')

                        ->with('success','Updated successfully');
                   }
                }
               break;
            }
                case "st_art_sale" : {
               if(isset($submit)){
                 //   print_r("hi....");die();
                   $dataTableObj=new AccessoriesLogicInputSheetsNamesModel();
                   $dataTableObj->excel_sheet_name = $file->getClientOriginalName();
                   $dataTableObj->link_table_info = $table_name;
                   $data=$dataTableObj->save();
                   if($dataTableObj->save())
                   {
                       //StStockModel
                       //echo "hi";die();
                       StArtSaleModel::query()->delete();
                       $test=Excel::import(new StArtSaleImport, $file);
                       return redirect()->route('accessories-utility.index')

                        ->with('success','Updated successfully');
                   }
                }
               break;
            }

            //BgtSale
            case "bgt_sale" : {
               if(isset($submit)){
                 //   print_r("hi....");die();
                   $dataTableObj=new AccessoriesLogicInputSheetsNamesModel();
                   $dataTableObj->excel_sheet_name = $file->getClientOriginalName();
                   $dataTableObj->link_table_info = $table_name;
                   $data=$dataTableObj->save();
                   if($dataTableObj->save())
                   {
                       //StStockModel
                       //echo "hi";die();
                       StArtSaleModel::query()->delete();
                       $test=Excel::import(new BgtSaleImport, $file);
                       return redirect()->route('accessories-utility.index')

                        ->with('success','Updated successfully');
                   }
                }
               break;
            }
            // //BgtRevisedSaleModel
            case "bgt_revised_sale" : {
               if(isset($submit)){
                 //   print_r("hi....");die();
                   $dataTableObj=new AccessoriesLogicInputSheetsNamesModel();
                   $dataTableObj->excel_sheet_name = $file->getClientOriginalName();
                   $dataTableObj->link_table_info = $table_name;
                   $data=$dataTableObj->save();
                   if($dataTableObj->save())
                   {
                       //StStockModel
                       //echo "hi";die();
                       BgtRevisedSaleModel::query()->delete();
                       $test=Excel::import(new BgtRevisedSaleImport, $file);
                       return redirect()->route('accessories-utility.index')

                        ->with('success','Updated successfully');
                   }
                }
               break;
            }
            //BgiDispModel
              case "bgt_display" : {
               if(isset($submit)){
                 //   print_r("hi....");die();
                   $dataTableObj=new AccessoriesLogicInputSheetsNamesModel();
                   $dataTableObj->excel_sheet_name = $file->getClientOriginalName();
                   $dataTableObj->link_table_info = $table_name;
                   $data=$dataTableObj->save();
                   if($dataTableObj->save())
                   {
                       //StStockModel
                       //echo "hi";die();
                       BgiDispModel::query()->delete();
                       $test=Excel::import(new BgiDispImport, $file);
                       return redirect()->route('accessories-utility.index')

                        ->with('success','Updated successfully');
                   }
                }
               break;
            }
        }


    }


    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

//        $user = User::find($id);
//
//        return view('users.show',compact('user'));

    }


    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

//        $user = User::find($id);
//
//        $roles = Role::pluck('name','name')->all();
//
//        $userRole = $user->roles->pluck('name','name')->all();
//
//
//        return view('users.edit',compact('user','roles','userRole'));

    }


    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

//        $this->validate($request, [
//
//            'name' => 'required',
//
//            'email' => 'required|email|unique:users,email,'.$id,
//
//            'password' => 'same:confirm-password',
//
//            'roles' => 'required'
//
//        ]);
//
//
//        $input = $request->all();
//
//        if(!empty($input['password'])){
//
//            $input['password'] = Hash::make($input['password']);
//
//        }else{
//
//            $input = array_except($input,array('password'));
//
//        }
//
//
//        $user = User::find($id);
//
//        $user->update($input);
//
//        DB::table('model_has_roles')->where('model_id',$id)->delete();
//
//
//        $user->assignRole($request->input('roles'));
//
//
//        return redirect()->route('users.index')
//
//                        ->with('success','User updated successfully');

    }


    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

//        User::find($id)->delete();
//
//        return redirect()->route('users.index')
//
//                        ->with('success','User deleted successfully');

    }

}
