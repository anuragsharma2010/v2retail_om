<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
//use Illuminate\Notifications\Notification;
//use Illuminate\Notifications\Notifiable;
//use Notification;
use App\User;
use App\UserMeta;
use App\Activity;
use App\Task;
use App\TaskAssign;
use App\Project;
use App\ProjectMember;
use App\Notification;
use Illuminate\Http\Request;
use Validator;
use Config;
use DB;

class UserController extends Controller
{

   public function login(Request $request){
       $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'device_id' => 'required',
            'fcm_id' => 'required',
            'latitude' => 'required',
            'longitude' => 'required'
        ]);
        if ($validator->fails()) {
            $this->return = array('status'=>0, 'message'=>$validator->errors()->first());
            $status = 200;
        } else{
          if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            //print_r(Auth::user()); die;
            if (Auth::user()->status == 1) {
              $user=Auth::User();
              
              User::where('id', Auth::User()->id)->update(array('device_id' =>  request('device_id'),'fcm_id' =>  request('fcm_id'),'latitude' =>  request('latitude'),'longitude' =>  request('longitude')));
              //$userUpdate->update($userinput);
              //$user['roles'] = $user->getRoleNames();

              if($user->store_id && $user->store_id!=null){
                    $storeData =  DB::select("SELECT s.st_code,s.st_name,s.st_longitude,s.st_latitude  FROM stores AS s where s.id = '".$user->store_id."' and s.st_status='1'");

                    //dd($storeData);
                    if(!empty($storeData)){
                        $user->store_name=$storeData[0]->st_name;
                        $user->store_code=$storeData[0]->st_code;
                        $user->store_latitude=$storeData[0]->st_latitude;
                        $user->store_longitude=$storeData[0]->st_longitude;
                    }else{
                        $user->store_name='';
                        $user->store_code='';
                        $user->store_latitude='';
                        $user->store_longitude='';
                    }
                }else{
                    $user->store_name='';
                    $user->store_code='';
                    $user->store_latitude='';
                    $user->store_longitude='';
                }

              $user['role_id'] = $user->getRoleNames()[0];
              unset($user['roles']);
              /*foreach ($user['roles'] as $rolekey => $rolevalue) {
                # code...
                echo $rolevalue->id; die;
              }*/
              $api_key='Bearer ' . $user->createToken('laravel')->accessToken;
              /*$userObj = User::find(Auth::User()->id);
              $userObj->api_login=$api_key;
              $userObj->save();*/
              unset($user->api_login);
              $this->return = array('status'=>1, 'message'=>trans('api.LOGIN_SUCCESS'), 'jsonData'=>$user);
              $this->return['jsonData']['token'] = $api_key;
              $status = 200;
            }else{
              $this->return = array('status'=>0, 'message'=>trans('api.ACCOUNT_DEACTIVATE'));
              $status = 200;
            }
          } else {
               $this->return = array('status'=>0, 'message'=>trans('api.INVALID_CREDENTIALS'));
               $status = 200;
          }
        }
         return response()->json($this->return, $status);
    }
    
    public function userDetais(Request $request){
        if(Auth::check()){
            //$user=Auth::User();
            $user = User::find(Auth::user()->id);
            //$user->profile_pic = WEBSITE_PROFILE_PIC_IMG_URL.$user->profile_pic;
            //echo WEBSITE_PROFILE_PIC_IMG_URL; die;
            $this->return['message'] = 'User details';
            $this->return['status'] = 1;
            $this->return['jsonData'] = $user;
            $status = 200;
        } else{
            $this->return['message'] = 'User Not Found';
            $this->return['status'] = 0;
            $status = 200;
        }
          return response()->json($this->return, $status);
    }

    public function memberslist(Request $request)
    {
        if($request->project_id && $request->project_id!=0){
            $projectMemberData = Project::with('manyprojectMember')->where('id', $request->project_id)->get()->toArray();
            //echo '<pre>'; print_r($projectMemberData); die;
            //echo '<pre>'; print_r($projectMemberData[0]['manyproject_member']); die;
            foreach ($projectMemberData[0]['manyproject_member'] as $projectMemberDatakey => $projectMemberDatavalue) {
                # code...
                $userDataIds[] = $projectMemberDatavalue['user_id'];
            }
            //echo $implodeUserId = implode(',', $userDataIds);
        }else{
            $userDataIds = [];
        }
        $data = User::with(['roles'=> function($query){
                    // selecting fields from staff table
                    $query->select(['roles.name']);
                }])->select('users.id','users.name')->whereNotIn('id', $userDataIds)->orderBy('id','DESC')->get()->toArray();
        //echo '<pre>'; print_r($data->getRoleNames()); die;
        //echo '<pre>'; print_r($data->get()->toArray()); die;
        if($data){
            $this->return['status'] = 1;
            $this->return['message'] = 'Members list';
            $this->return['jsonData'] = $data;
            $status = 200;
        }else{
            $this->return['message'] = 'Members not found.';
            $this->return['status'] = 0;
            $status = 200;
        }
        return response()->json($this->return, $status);
    }

    public function clientlist(Request $request)
    {
        $role_id = Config::get('global.role_id.client');
        $data = User::select('users.*')->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                        ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                        ->where('role_id', '=', $role_id)->get()->toArray();

        //echo '<pre>'; print_r($data->getRoleNames()); die;
        //echo '<pre>'; print_r($data->get()->toArray()); die;
        if($data){
            $this->return['status'] = 1;
            $this->return['message'] = 'Client list';
            $this->return['jsonData'] = $data;
            $status = 200;
        }else{
            $this->return['message'] = 'Client not found.';
            $this->return['status'] = 0;
            $status = 200;
        }
        return response()->json($this->return, $status);
    }

    public function updateUserdetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone_no' => 'required|numeric',
            'dob' => 'required',
            'address' => 'required',
            'employ_code' => 'required|max:10'
        ]);
        
        if($validator->fails()) {
              $this->return['message'] = $validator->errors()->first();
               $this->return['status'] = 0;
             $status = 200;
        } else{
            
        $user = User::find(Auth::user()->id);
        $input = $request->all();
        if ($request->profile_pic) {

            $profile_pic = 'profile-pic-' . substr(encrypt(time()),0,20) . '.' . $request->profile_pic->getClientOriginalExtension();
            $success = $request->profile_pic->move(public_path('profile_pic'), $profile_pic);

            $input['profile_pic'] = $success ? $profile_pic : null;
            }
        //$input['created_by'] = Auth::user()->id;
        //$input['status'] = 1;
        if($user->fill($input)->save()){
            $this->return['message'] = 'User detail update successfull.';
            $this->return['status'] = 1;
            //unset($user->api_login);
            $this->return['jsonData'] = $user;
            $status = 200;
        }
        
        
        }
        return response()->json($this->return, $status);
    }

    public function updateProfileImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
        ]);
        
        if($validator->fails()) {
              $this->return['message'] = $validator->errors()->first();
               $this->return['status'] = 0;
             $status = 200;
        } else{
            
        $user = User::find(Auth::user()->id);
        $input = $request->all();
        if ($request->profile_pic) {

            $profile_pic = 'profile-pic-' . substr(encrypt(time()),0,20) . '.' . $request->profile_pic->getClientOriginalExtension();
            $success = $request->profile_pic->move(public_path('profile_pic'), $profile_pic);

            $input['profile_pic'] = $success ? $profile_pic : null;
            }
        //$input['created_by'] = Auth::user()->id;
        //$input['status'] = 1;
        if($user->fill($input)->save()){
            $this->return['message'] = 'User profile picture update successfull.';
            $this->return['status'] = 1;
            $this->return['jsonData'] = $user;
            $status = 200;
        }
        
        
        }
        return response()->json($this->return, $status);
    }
    
    public function userActivity(Request $request)
    {
         
        $user_id = Auth::user()->id;
        $activity = Activity::with('task','addedByUser','project','milestone','comment')->whereRaw("find_in_set($user_id,user_id)")->get()->toArray();
        $count = 0;
        foreach($activity as $activity_user){
            
         $user_ids = explode(',',$activity_user['user_id']);
         foreach($user_ids as $user_id){
             $activity[$count]['user_data'][] = User::select('id','name')->find($user_id)->toArray();
         }
        $count++;    
        }
        $input['status'] = 1;   
        
        $this->return['message'] = 'User Activity';
         $this->return['status'] = 1;
        $this->return['jsonData'] = $activity;
        $status = 200;
        return response()->json($this->return, $status);
    }

    public function notificationList(Request $request) {

        $user = Auth::user();
        if ($user) {
             $readNotifications = $user->notifications->where('read_at' , '!=' , NULL)->toArray();
            $unreadNotifications = $user->unreadNotifications->toArray();
            if ($unreadNotifications) {
                $arr['unread_count'] = count($unreadNotifications);
                foreach ($unreadNotifications as $row) {
                    $arr['unreadNotification'][] = array(
                        'notification_id' => $row['id'],
                        'message' => $row['data']['message'],
                        'type' => $row['data']['type'],
                        'type_id' => $row['data']['id'],
                        'created_at' => $row['created_at'],
                        'read_at' => 0,
                        
                    );
                }
            } 
            if ($readNotifications) {
               
                 foreach ($readNotifications as $row) {
                    $arr['readNotification'][] = array(
                        'notification_id' => $row['id'],
                        'message' => $row['data']['message'],
                        'type' => $row['data']['type'],
                        'type_id' => $row['data']['id'],
                        'created_at' => $row['created_at'],
                        'read_at' => 1,
                        
                    );
                }
                
            } 
                if(isset($arr)){
                $this->return['message'] = 'Notification data.';
                $this->return['jsonData'] = $arr;
                $this->return['status'] = 1;
                $status = 200;
            } else {
                $this->return['message'] = 'No Notification found.';
                $this->return['status'] = 0;
                $status = 200;
            }
        } else {
            $this->return['message'] = 'User not exist.';
            $this->return['status'] = 2;
            $status = 200;
        }

        return response()->json($this->return, $status);
    }

    public function logout(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longitude' => 'required'
        ]);
        if ($validator->fails()) {
            $this->return = array('status'=>0, 'message'=>$validator->errors()->first());
            $status = 200;
        } else{
            $user = Auth::user();
            //echo '<pre>'; print_r($user->toArray()); die;
            $user->device_id = null;
            $user->fcm_id = null;
            $user->latitude = request('latitude');
            $user->longitude = request('longitude');
            
            //$user->api_login='';
             
            $user->save();

            $request->user()->token()->revoke();
            //$this->guard()->logout(); 
            /*Auth::guard('api')->logout();
            $request->session()->flush();
            $request->session()->regenerate();*/

            $this->return['message'] = 'Successfully Logged Out.';
            $this->return['status'] = 1;
            $status = 200;
            
        }
        return response()->json($this->return, $status);
    }

    public function changeNotificationStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'notification_id' => 'required',
            'notification_status' => 'required'
        ]);
        if($validator->fails()) {
            $this->return['message'] = $validator->errors()->first();
            $this->return['status'] = 0;
            $status = 200;
        } else{
            $Notification = Notification::find($request->notification_id);
            $input = $request->all();
            //$inputData['id'] = $request->notification_id;
            if($request->notification_status==1){
                $inputData['read_at'] = date('Y-m-d H:i:s');
            }else{
                $inputData['read_at'] = null;
            }
            //$inputData['updated_at'] = date('Y-m-d H:i:s');
            if($Notification->fill($inputData)->save()){
                $this->return['message'] = 'Notification status change successfull.';
                $this->return['status'] = 1;
                $this->return['jsonData'] = $Notification;
                $status = 200;
            }else{
                $this->return['message'] = 'Something went wrong.';
                $this->return['status'] = 0;
                $status = 200;
            }
        }
        return response()->json($this->return, $status);
    }

}
