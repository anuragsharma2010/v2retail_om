<?php

namespace App\Http\Controllers\Api\SopCompliance;

use App\SurveyFormSubmissionValue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SurveyFormSubmissionValueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SurveyFormSubmissionValue  $surveyFormSubmissionValue
     * @return \Illuminate\Http\Response
     */
    public function show(SurveyFormSubmissionValue $surveyFormSubmissionValue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SurveyFormSubmissionValue  $surveyFormSubmissionValue
     * @return \Illuminate\Http\Response
     */
    public function edit(SurveyFormSubmissionValue $surveyFormSubmissionValue)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SurveyFormSubmissionValue  $surveyFormSubmissionValue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SurveyFormSubmissionValue $surveyFormSubmissionValue)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SurveyFormSubmissionValue  $surveyFormSubmissionValue
     * @return \Illuminate\Http\Response
     */
    public function destroy(SurveyFormSubmissionValue $surveyFormSubmissionValue)
    {
        //
    }
}
