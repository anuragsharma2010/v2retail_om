<?php
namespace App\Http\Controllers\Api\SopCompliance;

use Master\Models\StoresModel;
use App\Model\SopCompliance\Form;
use App\Model\SopCompliance\FormQuestion;
use App\Model\SopCompliance\FormField;
use App\Model\SopCompliance\FormFieldValue;
use App\Model\SopCompliance\SurveyFormSubmission;
use App\Model\SopCompliance\SurveyFormSubmissionValue;
use App\Model\SopCompliance\FormRoleMapping;

use App\Model\SopCompliance\FormQuestionTimeMapping;
use App\Model\SopCompliance\FormQuestionTimeTaken;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Helpers\NotificationHelper;
use Spatie\Permission\Models\Role;
use Config;
use Carbon\Carbon;
use File;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longitude' => 'required'
        ]);
        if ($validator->fails()) {
            $this->return = array('status'=>0, 'message'=>$validator->errors()->first());
            $status = 200;
        } else{
        $user=Auth::User();

        $user_id=Auth::User()->id;
        $store_id=Auth::User()->store_id;

        $role_id = $user->roles->first()->id;

        if($role_id){

            $formIdData = FormRoleMapping::where('role_id', $role_id)->get()->pluck('form_id');

            if($formIdData->toArray()){
                $formIds = $formIdData->toArray();

                $sopFormData = Form::with('user')->whereIn('id', $formIds)->where('status', '1')->get();

                foreach ($sopFormData as $sopFormDatakey => $sopFormDatavalue) {
                    # code...
                    $sopFormDatavalue->form_name=strtoupper($sopFormDatavalue->form_name);
                    $sopFormDatavalue->form_label=strtoupper($sopFormDatavalue->form_label);
                    $sopFormDatavalue->store_id=$user->store_id;

                    //echo $user->store_id; die;
                    if($user->store_id && $user->store_id!=null){
                        $storeData =  DB::select("SELECT s.st_code  FROM stores AS s where s.id = '".$user->store_id."'");
                        if(!empty($storeData)){
                            $sopFormDatavalue->store_code=$storeData[0]->st_code;
                        }else{
                            $sopFormDatavalue->store_code='';
                        }
                    }else{
                        $sopFormDatavalue->store_code='';
                    }

                    $questioncount = FormQuestion::join('sop_form_fields', function ($join) {
                                        $join->on('sop_form_questions.id', '=', 'sop_form_fields.form_question_id')
                                            ->where('sop_form_fields.field_type','=','radio')
                                            ->where('sop_form_fields.parent_id','=','0');
                                    })->join('sop_form_field_values', function ($join) {
                                        $join->on('sop_form_fields.id', '=', 'sop_form_field_values.form_field_id')
                                            ->where('sop_form_field_values.is_scorable','=','1');
                                    })
                                    ->where('sop_form_questions.form_id', $sopFormDatavalue->id)->where('sop_form_questions.parent_id', 0)->count();

                    // $questioncount = FormQuestion::where('form_id', $sopFormDatavalue->id)->where('parent_id', 0)->count();
                    $availed_score =0;
                    $percentage = 0;
                    $yesCount= 0;


                    $questionnumberofyesCount = DB::select("SELECT COUNT(*) AS count  FROM sop_form_questions AS q
                              JOIN sop_form_fields AS f ON f.form_question_id=q.id AND f.field_type='radio' AND f.parent_id='0' AND q.parent_id='0'
                              JOIN sop_form_field_values AS v ON v.form_field_id=f.id AND (v.is_scorable='1')
                              JOIN sop_survey_form_submission AS fs ON fs.question_id=q.id AND fs.form_id=q.form_id
                              JOIN sop_survey_form_submission_value AS sv ON sv.field_id=f.id AND sv.field_value_id=v.id
                              WHERE q.form_id = '".$sopFormDatavalue->id."' AND fs.store_id = '".$store_id."' AND fs.date = '".date('Y-m-d')."'
                              GROUP BY q.id");

                             // dd($questionnumberofyesCount);
                    //echo '<pre>'; print_r($questionnumberofyesCount);
                    //print_r($questionnumberofyesCount[0]->count);
                    if(!empty($questionnumberofyesCount)){
                        foreach ($questionnumberofyesCount as $questionnumberofyesCountkey => $questionnumberofyesCountvalue) {
                            # code...
                            $yesCount += $questionnumberofyesCountvalue->count;
                        }
                        //$yesCount = $questionnumberofyesCount[0]->count;
                        $availed_score = $yesCount;
                    }else{
                        $yesCount = 0;
                        $availed_score = 0;
                    }
                    //$yesCount= $questionnumberofyesCount ? $questionnumberofyesCount[0]->count :'0';

                    //$availed_score = $questioncount - $yesCount;
                    $percentage = $questioncount>0 ? round((($availed_score/$questioncount)*100),2) :'0' ;

                    $sopFormData[$sopFormDatakey]->availed_score = $availed_score.'.00';
                    $sopFormData[$sopFormDatakey]->percentage = $percentage.'%';
                    if(!empty($questioncount) && $questioncount!=''){
                        $sopFormData[$sopFormDatakey]->total_score = $questioncount.'.00';
                    }else{
                        $sopFormData[$sopFormDatakey]->total_score = '00.00';
                    }
                     $currDate = date('Y-m-d');
                 
                    $perFormQuestionCount = FormQuestion::where('form_id', $sopFormDatavalue->id)->where('parent_id', 0)->where('status', '1')->count();
                 
                   
                    $perFormQuestionSubmittedCount = SurveyFormSubmission::where(array('form_id'=>$sopFormDatavalue->id,'date'=>$currDate))->count();
                   // pr($perFormQuestionCount);
                    
                 //   prd($perFormQuestionSubmittedCount);
                    // 1=>'Completed', 2=>'Partial Completed', 3=>'Not Completed'
                    if($perFormQuestionCount == $perFormQuestionSubmittedCount){
                        $sopFormData[$sopFormDatakey]->form_status = 1; // Completed

                    }else if($perFormQuestionCount > $perFormQuestionSubmittedCount){
                        $sopFormData[$sopFormDatakey]->form_status = 2; // Partial Completed
                    }else{
                        $sopFormData[$sopFormDatakey]->form_status = 3; // Not Completed
                    }
                    //$sopFormData[$sopFormDatakey]->form_status = 1;
                    $sopFormData[$sopFormDatakey]->server_date = date('d-m-Y');
                }

                if($sopFormData) {
                    $this->return['message'] = 'SopCompliance Form Data';
                    $this->return['status'] = 1;
                    $this->return['jsonData'] = $sopFormData;
                    $status = 200;
                } else{
                     $this->return['message'] = 'No SopCompliance Form Data Found';
                     $this->return['status'] = 0;
                    $status = 200;
                }
            }else{
                $this->return['message'] = 'Form data not found.';
                $this->return['status'] = 0;
                $status = 200;
            }
        }else{
            $this->return['message'] = 'User role not found.';
            $this->return['status'] = 0;
            $status = 200;
        }
    }
        return response()->json($this->return, $status);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function show(Form $form)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function edit(Form $form)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Form $form)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function destroy(Form $form)
    {
        //
    }

    public function sop_form_data(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'form_id' => 'required',
            'store_id' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);
        if ($validator->fails()) {
            $this->return = array('status'=>0, 'message'=>$validator->errors()->first());
            $status = 200;
        } else{
        //echo request('form_id'); die;
        if(isset($request->form_id) && request('form_id')!=''){
            $user_id=Auth::User()->id;
            $sopFormData = FormQuestion::where('form_id', request('form_id'))->where('parent_id', 0)->where('status', '1')->orderBy('sort_order', 'ASC')->get();
            //dd(Auth::User()->store_id);
            $group_id = StoresModel::where('id',Auth::User()->store_id)->get()->first();
            //dd($group_id);
            foreach ($sopFormData as $sopFormDatakey => $sopFormDatavalue) {

                $SurveyFormSubmission = SurveyFormSubmission::where('form_id', $request->form_id)->where('store_id', $request->store_id)->where('question_id', $sopFormDatavalue->id)->where('user_id', Auth::user()->id)->where('date', date('Y-m-d'))->get()->first();
                //dd($SurveyFormSubmission);
              if($SurveyFormSubmission){
                $sopFormData[$sopFormDatakey]->question_status = 1;
              }else{
                $sopFormData[$sopFormDatakey]->question_status = 0;
              }
                # code...3
                //echo Str::ucfirst(Str::lower($sopFormDatavalue->question)); die;
                $sopFormData[$sopFormDatakey]->question = Str::ucfirst(Str::lower($sopFormDatavalue->question));
                //dd($group_id->st_group);
                //echo $sopFormDatavalue->id;
                //echo $group_id->st_group;
                if($group_id->st_group){
                    $bgt_time = FormQuestionTimeMapping::where('form_group_id', $group_id->st_group)->where('form_question_id',$sopFormDatavalue->id)->first();
                }else{
                    $bgt_time = [];
                }
                //dd($bgt_time);
                $act_time = FormQuestionTimeTaken::where('user_id', $user_id)->where('group_id', $group_id->st_group)->where('qus_id',$sopFormDatavalue->id)->where('date', date('Y-m-d'))->where('form_id', request('form_id'))->first();
                // dd($act_time);

                $sopFormData[$sopFormDatakey]->server_time = date('H:i');
                $sopFormData[$sopFormDatakey]->server_date = date('d-m-Y');
                $diff='';

                $sopFormData[$sopFormDatakey]->question_active = 10;
                $sopFormData[$sopFormDatakey]->bgt_start_time = $bgt_time?date('H:i',strtotime($bgt_time->start_time)):'00:00';
                $sopFormData[$sopFormDatakey]->bgt_end_time = $bgt_time?date('H:i',strtotime($bgt_time->end_time)):'00:00';

                $sopFormData[$sopFormDatakey]->bgt_start_time_format = $bgt_time?date('h:i A',strtotime($bgt_time->start_time)):'00:00';
                $sopFormData[$sopFormDatakey]->bgt_end_time_format = $bgt_time?date('h:i A',strtotime($bgt_time->end_time)):'00:00';

                $diff = strtotime($sopFormData[$sopFormDatakey]->bgt_start_time) - strtotime($sopFormData[$sopFormDatakey]->server_time);
                $sopFormData[$sopFormDatakey]->duration = $diff<0?0:$diff;

                $sopFormData[$sopFormDatakey]->act_start_time = $act_time?date('H:i',strtotime($act_time->act_start_time)):'00:00';
                $sopFormData[$sopFormDatakey]->act_end_time = $act_time?date('H:i',strtotime($act_time->act_end_time)):'00:00';

                $sopFormData[$sopFormDatakey]->act_start_time_format = $act_time?date('h:i A',strtotime($act_time->act_start_time)):'00:00';
                $sopFormData[$sopFormDatakey]->act_end_time_format = $act_time?date('h:i A',strtotime($act_time->act_end_time)):'00:00';

                $a = Carbon::parse($sopFormData[$sopFormDatakey]->act_end_time);
                $b= Carbon::parse($sopFormData[$sopFormDatakey]->act_start_time);
                // dd($act_time->act_end_time);
                $sopFormData[$sopFormDatakey]->time_taken = $b->diffInMinutes($a).' min';
                // $diff_in_minutes = $to->diffInMinutes($from);
                // dd($sopFormData[$sopFormDatakey]->time_taken);

                // $sopFormData[$sopFormDatakey]->time_taken = date('H:i',(strtotime($sopFormData[$sopFormDatakey]->act_start_time)-strtotime($sopFormData[$sopFormDatakey]->act_end_time)));
                //$sopFormData[$sopFormDatakey]->question_status = 1;
            }
            if($sopFormData) {
                $this->return['message'] = 'SopCompliance Form Data';
                $this->return['status'] = 1;
                $this->return['jsonData'] = $sopFormData;
                $status = 200;
            } else{
                $this->return['message'] = 'No SopCompliance Form Data Found';
                $this->return['status'] = 0;
                $status = 200;
            }
        }else{
            $this->return['message'] = 'Form id missing.';
            $this->return['status'] = 0;
            $status = 200;
        }
    }
        return response()->json($this->return, $status);
    }

    public function question_start_time(Request $request)
    {
        $form=new FormQuestionTimeTaken();
        $validator = Validator::make($request->all(), [
            'question_id' => 'required',
            'form_id' => 'required'
        ]);
        if ($validator->fails()) {
            $this->return = array('status'=>0, 'message'=>$validator->errors()->first());
            $status = 200;
        } else{
        //dd($request->all());
        if(isset($request->question_id) && request('question_id')!=''){
            $user_id= Auth::User()->id;
            $group_id = StoresModel::where('id',Auth::User()->store_id)->get()->first();
            //dd($group_id);
            $form->user_id = $user_id ? $user_id:'';
            $form->group_id = $group_id->st_group;
            $form->qus_id = $request->question_id;
            $form->form_id = $request->form_id;
            $form->date = date('Y-m-d');
            $form->act_start_time = $act_start_time = date('H:i:s');
            //dd($form);
            $form->save();

            $this->return['message'] = 'Successfully.';
            $this->return['status'] = 1;
            $this->return['act_start_time'] = $act_start_time;
            $this->return['act_start_time_format'] = date('h:i A', strtotime($act_start_time));
            $this->return['jsonData'] = '';
            $status = 200;
        }else{
            $this->return['message'] = 'Question id missing.';
            $this->return['status'] = 0;
            $status = 200;
        }
    }
        return response()->json($this->return, $status);
    }

    public function question_store_list(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'question_id' => 'required'
        ]);
        if ($validator->fails()) {
            $this->return = array('status'=>0, 'message'=>$validator->errors()->first());
            $status = 200;
        } else{
              if(isset($request->question_id) && request('question_id')!=''){
                  $data=  DB::select("SELECT q.question, COUNT(*) AS count  FROM sop_form_questions AS q
                              JOIN sop_form_fields AS f ON f.form_question_id=q.id AND f.field_type='radio' AND f.parent_id='0' AND q.parent_id='0'
                              JOIN sop_form_field_values AS v ON v.form_field_id=f.id AND (form_field_option_name='Yes')
                              JOIN sop_survey_form_submission_value AS sv ON sv.field_id=f.id AND sv.field_value_id=v.id
                              GROUP BY q.id");
                  foreach($data as $key => $val){
                    $data[$key]->totalStore = DB::table('stores')->count();
                  }
                  //$data = json_encode($data);
                  $this->return['message'] = 'Submit successfully.';
                  $this->return['status'] = 1;
                  $this->return['jsonData'] = $data;
                  $status = 200;
            }else{
                $this->return['message'] = 'Question id missing.';
                $this->return['status'] = 0;
                $status = 200;
            }
        }
            return response()->json($this->return, $status);
    }


    public function sop_form_question_fields(Request $request)
    {
        //
        //echo request('form_id'); die;
        if(isset($request->form_question_id) && request('form_question_id')!=''){

            $user_store_id = Auth::User()->store_id;

            $user_id=Auth::User()->id;
            if(request('date') != ''){
                $sopQuestionData = FormQuestion::find(request('form_question_id'));
                $form_id = $sopQuestionData->form_id;
                $sopFormFieldData = FormField::with('FormFieldValueMany')->where('form_question_id', request('form_question_id'))->where('parent_id', 0)->get();
                //echo '<pre>'; print_r($sopFormFieldData->toArray()); die;
                foreach ($sopFormFieldData as $sopFormFieldDatakey => $sopFormFieldDatavalue) {
                    # code...3
                    //echo $sopFormFieldDatavalue->field_name;
                    $sopFormFieldData[$sopFormFieldDatakey]->field_placeholder = Str::ucfirst(Str::lower($sopFormFieldDatavalue->field_placeholder));
                    //$sopFormFieldData[$sopFormFieldDatakey]->question_time = date('H:i:s');
                    //echo '<pre>'; print_r($sopFormFieldDatavalue->FormFieldValueMany->toArray()); die;
                    if(!empty($sopFormFieldDatavalue->FormFieldValueMany)){
                        foreach ($sopFormFieldDatavalue->FormFieldValueMany as $form_field_value_many_key => $form_field_value_many_value) {
                            //echo 'fdfd'; die;
                            $SurveyFormSubmission = SurveyFormSubmission::where('form_id', $form_id)->where('store_id', $user_store_id)->where('question_id', $request->form_question_id)->where('user_id', Auth::user()->id)->where('date', request('date'))->get()->first();
                            //echo '<pre>'; print_r($SurveyFormSubmission->toArray()); die;  
                            //dd($SurveyFormSubmission);
                            if($SurveyFormSubmission){
                                $SurveyFormSubmission01 = SurveyFormSubmissionValue::where('survey_form_submission_id', $SurveyFormSubmission->id)->where('store_id', $user_store_id)->where('field_id', $form_field_value_many_value->form_field_id)->where('field_value_id', $form_field_value_many_value->id)->get()->first();

                                //echo '<pre>'; print_r($SurveyFormSubmission01); die;
                                if(!empty($SurveyFormSubmission01)){
                                    //echo '<pre>'; print_r($SurveyFormSubmission01); die;
                                    if($sopFormFieldDatavalue->field_type=='radio'){
                                        //echo $SurveyFormSubmission01->value; die;
                                        if($SurveyFormSubmission01->value==1){
                                            $sopFormFieldDatavalue->FormFieldValueMany[$form_field_value_many_key]->status = 'sel';
                                        }else{
                                            $sopFormFieldDatavalue->FormFieldValueMany[$form_field_value_many_key]->status = 'unsel';
                                        }
                                    }elseif($sopFormFieldDatavalue->field_type=='file'){
                                        $img_path = 'form_images/'.'f_id_'.$form_id.'/';
                                        $imagepath = url($img_path . $SurveyFormSubmission01->value);
                                        $sopFormFieldDatavalue->FormFieldValueMany[$form_field_value_many_key]->form_field_value = $imagepath;
                                    }else{
                                        $sopFormFieldDatavalue->FormFieldValueMany[$form_field_value_many_key]->form_field_value = $SurveyFormSubmission01->value;
                                    }
                                }else{
                                    $sopFormFieldDatavalue->FormFieldValueMany[$form_field_value_many_key]->status = $sopFormFieldDatavalue->FormFieldValueMany[$form_field_value_many_key]->status;
                                }
                            }
                            # code...$sopFormFieldDatavalue->FormFieldValueMany
                            //echo $form_field_value_many_value->associate_question; die;
                            if(!empty($form_field_value_many_value->associate_fields) && $form_field_value_many_value->associate_fields!='' && $form_field_value_many_value->associate_fields!=null){
                                $associate_fieldsArray = explode(',', $form_field_value_many_value->associate_fields);
                                $sopFormData = FormField::with('FormFieldValueMany')->whereIn('id', $associate_fieldsArray)->get();
                                $sopFormFieldDatavalue->FormFieldValueMany[$form_field_value_many_key]->associate_fields_data = $sopFormData;
                            }else{
                                $sopFormFieldDatavalue->FormFieldValueMany[$form_field_value_many_key]->associate_fields_data = [];
                            }

                            if(!empty($form_field_value_many_value->associate_question) && $form_field_value_many_value->associate_question!='' && $form_field_value_many_value->associate_question!=null){
                                $associate_questionArray = explode(',', $form_field_value_many_value->associate_question);
                                $sopFormData = FormQuestion::with('SOPFormFieldMany.FormFieldValueMany')->whereIn('id', $associate_questionArray)->where('status', '1')->orderBy('sort_order', 'ASC')->get();
                                $sopFormFieldDatavalue->FormFieldValueMany[$form_field_value_many_key]->associate_question_data = $sopFormData;
                            }else{
                                $sopFormFieldDatavalue->FormFieldValueMany[$form_field_value_many_key]->associate_question_data = [];
                            }
                            //$sopFormFieldData[$sopFormFieldDatakey]->form_field_value_many = [];
                        }
                    }
                }
            }else{
                $sopFormFieldData = FormField::with('FormFieldValueMany')->where('form_question_id', request('form_question_id'))->where('parent_id', 0)->get();
                foreach ($sopFormFieldData as $sopFormFieldDatakey => $sopFormFieldDatavalue) {
                    # code...3
                    //echo $sopFormFieldDatavalue->field_name;
                    $sopFormFieldData[$sopFormFieldDatakey]->field_placeholder = Str::ucfirst(Str::lower($sopFormFieldDatavalue->field_placeholder));
                    //$sopFormFieldData[$sopFormFieldDatakey]->question_time = date('H:i:s');
                    if(!empty($sopFormFieldDatavalue->FormFieldValueMany)){
                        foreach ($sopFormFieldDatavalue->FormFieldValueMany as $form_field_value_many_key => $form_field_value_many_value) {
                            # code...
                            //echo $form_field_value_many_value->associate_question; die;
                            if(!empty($form_field_value_many_value->associate_fields) && $form_field_value_many_value->associate_fields!='' && $form_field_value_many_value->associate_fields!=null){
                                $associate_fieldsArray = explode(',', $form_field_value_many_value->associate_fields);
                                $sopFormData = FormField::with('FormFieldValueMany')->whereIn('id', $associate_fieldsArray)->get();
                                $sopFormFieldDatavalue->FormFieldValueMany[$form_field_value_many_key]->associate_fields_data = $sopFormData;
                            }else{
                                $sopFormFieldDatavalue->FormFieldValueMany[$form_field_value_many_key]->associate_fields_data = [];
                            }

                            if(!empty($form_field_value_many_value->associate_question) && $form_field_value_many_value->associate_question!='' && $form_field_value_many_value->associate_question!=null){
                                $associate_questionArray = explode(',', $form_field_value_many_value->associate_question);
                                $sopFormData = FormQuestion::with('SOPFormFieldMany.FormFieldValueMany')->whereIn('id', $associate_questionArray)->where('status', '1')->orderBy('sort_order', 'ASC')->get();
                                $sopFormFieldDatavalue->FormFieldValueMany[$form_field_value_many_key]->associate_question_data = $sopFormData;
                            }else{
                                $sopFormFieldDatavalue->FormFieldValueMany[$form_field_value_many_key]->associate_question_data = [];
                            }
                            //$sopFormFieldData[$sopFormFieldDatakey]->form_field_value_many = [];
                        }
                    }
                }
            }

            //$user_id= Auth::User()->id;
            /*$group_id = StoresModel::where('id',Auth::User()->store_id)->get()->first();
            //dd($group_id);
            $form=new FormQuestionTimeTaken();
            $form->user_id = $user_id ? $user_id:'';
            $form->group_id = $group_id->st_group;
            $form->qus_id = $request->form_question_id;
            $form->act_start_time = $act_start_time = date('H:i:s');
            //dd($form);
            $form->save();*/

            if($sopFormFieldData) {
                $this->return['message'] = 'SopCompliance Form Fields Data';
                $this->return['status'] = 1;
                $this->return['jsonData'] = $sopFormFieldData;
                $status = 200;
            } else{
                $this->return['message'] = 'No SopCompliance Form Fields Data Found';
                $this->return['status'] = 0;
                $status = 200;
            }
        }else{
            $this->return['message'] = 'Form question id missing.';
            $this->return['status'] = 0;
            $status = 200;
        }
        return response()->json($this->return, $status);
    }

    public function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }


    public function sop_form_submission(Request $request)
    {
        //echo request('form_id'); die;
        $validator = Validator::make($request->all(), [
            'form_id' => 'required',
            'store_id' => 'required',
            'form_question_id' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'data' => 'required',
        ]);
        if ($validator->fails()) {
            $this->return = array('status'=>0, 'message'=>$validator->errors()->first());
            $status = 200;
        }else{
            $data = json_decode(request('data'));

            //echo '<pre>'; print_r($data); die;

            //echo Auth::User()->store_id; die;

            $group_id = StoresModel::where('id',Auth::User()->store_id)->get()->first();
            //dd($group_id);

            $user_id = Auth::User()->id;

            //echo '<pre>'; print_r($data); die;

            /*$this->return['message'] = 'Submit successfully.';
                $this->return['status'] = 1;
                $this->return['jsonData'] = request('data');
                $status = 200;
            //dd(json_decode($data)); die;*/

            if(!empty($data)){

                foreach ($data as $questionkey => $questionvalue) {
                    if($questionvalue->field_type=='radio' && $questionvalue->form_field_required=='1'){
                            if(array_search('sel', array_column($questionvalue->form_field_value_many, 'status')) !== false) {
                                //echo 'value is in multidim array';
                            }else {
                                $this->return = array('status'=>0, 'message'=>'Fields are required.');
                                $status = 200;
                                return response()->json($this->return, $status);
                            }
                        }
                    foreach ($questionvalue->form_field_value_many as $form_field_value_key => $form_field_value_data) {
                        if($questionvalue->form_field_required=='1'){
                            if(!isset($form_field_value_data->form_field_value) || isset($form_field_value_data->form_field_value) && $form_field_value_data->form_field_value==''){
                                $this->return = array('status'=>0, 'message'=>'Fields are required.');
                                $status = 200;
                                return response()->json($this->return, $status);
                            }
                        }


                        if(!empty($form_field_value_data->associate_fields_data)){
                                foreach ($form_field_value_data->associate_fields_data as $asfquestionkey => $asfquestionvalue) {
                                   if($asfquestionvalue->field_type=='radio' && $asfquestionvalue->form_field_required=='1'){
                                        if(array_search('sel', array_column($asfquestionvalue->form_field_value_many, 'status')) !== false) {
                                            //echo 'value is in multidim array';
                                        }else {
                                            $this->return = array('status'=>0, 'message'=>'Fields are required.');
                                            $status = 200;
                                            return response()->json($this->return, $status);
                                        }
                                    }

                                        foreach ($asfquestionvalue->form_field_value_many as $form_field_value_key => $form_field_value_data4) {
                                            # code...
                                            if($asfquestionvalue->form_field_required=='1'){
                                                if(!isset($form_field_value_data4->form_field_value) || isset($form_field_value_data4->form_field_value) && $form_field_value_data4->form_field_value==''){
                                                    $this->return = array('status'=>0, 'message'=>'Fields are required.');
                                                    $status = 200;
                                                    return response()->json($this->return, $status);
                                                }
                                            }
                                        } 
                                }

                            }


                    }
                }


                //echo 'test'; die;

                $inputdata['form_id'] = $request->form_id;
                $inputdata['store_id'] = $request->store_id;
                $inputdata['question_id'] = $request->form_question_id;
                $inputdata['latitude'] = $request->longitude;
                $inputdata['longitude'] = $request->longitude;
                $inputdata['date'] = date('Y-m-d');
                $inputdata['user_id'] = Auth::user()->id;

                //print_r($inputdata); die;

                if($SurveyFormSubmission = SurveyFormSubmission::create($inputdata)){

                    //dd($SurveyFormSubmission);
                    $last_insert_id = $SurveyFormSubmission->id;

                    $inputdata1['survey_form_submission_id'] = $last_insert_id;
                    $inputdata1['store_id'] = $request->store_id;

                    //$user_id= Auth::User()->id;

                    //$group_id = StoresModel::where('id',Auth::User()->store_id)->get()->first();
                    //dd($group_id);
                    $form = FormQuestionTimeTaken::where(array('group_id'=>$group_id->st_group,'user_id'=>$user_id,'qus_id'=>$request->form_question_id, 'form_id'=>request('form_id')))->where('date', date('Y-m-d'))->update(array('act_end_time' => date('H:i:s')));
 

                foreach ($data as $questionkey => $questionvalue) {
              
                    # code...
                    //echo '<pre>'; print_r($questionvalue); die;
                        //$inputdata1['survey_form_submission_id'] = $last_insert_id;
                        //$inputdata1['store_id'] = $request->store_id;
                        $inputdata1['field_id'] = $questionvalue->id;

                        foreach ($questionvalue->form_field_value_many as $form_field_value_key => $form_field_value_data) {
                            # code...  
                            if($questionvalue->field_type=='radio'){
                                if($form_field_value_data->status=='sel'){
                                    $ffvalue=FormFieldValue::find($form_field_value_data->id);
                                    //dd($ffvalue->is_scorable);
                                    $inputdata1['value'] = 1;
                                    $inputdata1['option_name'] = $ffvalue->form_field_option_name;
                                    $inputdata1['scorable'] = $ffvalue->is_scorable;
                                    $inputdata1['field_value_id'] = $form_field_value_data->id;
                                    //$form_field_value_data = $form_field_value_data->status;
                                    if($SurveyFormSubmissionValue = SurveyFormSubmissionValue::create($inputdata1)){

                                        $inputdata1['option_name'] = NULL;
                                        $inputdata1['scorable'] = NULL;

                                    }
                                }
                            }else{
                                if($questionvalue->field_type!='file'){
                                    if(!empty($form_field_value_data->form_field_value) && $form_field_value_data->form_field_value !='' ){
                                        $inputdata1['value'] = $form_field_value_data->form_field_value;
                                        $inputdata1['field_value_id'] = $form_field_value_data->id;
                                        if($SurveyFormSubmissionValue = SurveyFormSubmissionValue::create($inputdata1)){

                                        }
                                    }
                                }
                            }

                            if(!empty($form_field_value_data->associate_fields_data)){

                                foreach ($form_field_value_data->associate_fields_data as $asfquestionkey => $asfquestionvalue) {
                                    
                                    # code...
                                    //echo '<pre>'; print_r($asfquestionvalue); die;

                                        //$inputdata1['survey_form_submission_id'] = $last_insert_id;
                                        //$inputdata1['store_id'] = $request->store_id;
                                        $inputdata1['field_id'] = $asfquestionvalue->id;

                                        foreach ($asfquestionvalue->form_field_value_many as $form_field_value_key => $form_field_value_data4) {
                                            # code...
                                            if($asfquestionvalue->field_type=='radio'){
                                                if($form_field_value_data4->status=='sel'){
                                                    $ffvalue=FormFieldValue::find($form_field_value_data4->id);
                                                    $inputdata1['value'] = 1;
                                                    $inputdata1['option_name'] = $ffvalue->form_field_option_name;
                                                    $inputdata1['scorable'] = $ffvalue->is_scorable;
                                                    $inputdata1['field_value_id'] = $form_field_value_data4->id;
                                                    //$form_field_value_data4 = $form_field_value_data4->status;
                                                    if($SurveyFormSubmissionValue = SurveyFormSubmissionValue::create($inputdata1)){
                                                        $inputdata1['option_name'] = NULL;
                                                        $inputdata1['scorable'] = NULL;
                                                    }
                                                }
                                            }else{
                                                if($asfquestionvalue->field_type!='file'){
                                                    if(!empty($form_field_value_data4->form_field_value) && $form_field_value_data4->form_field_value !='' ){
                                                        $inputdata1['value'] = $form_field_value_data4->form_field_value;
                                                        $inputdata1['field_value_id'] = $form_field_value_data4->id;
                                                        if($SurveyFormSubmissionValue = SurveyFormSubmissionValue::create($inputdata1)){

                                                        }
                                                    }
                                                }
                                            }
                                        }

                                }

                            }

                            if(!empty($form_field_value_data->associate_question_data)){
                                foreach ($form_field_value_data->associate_question_data as $associate_question_datakey => $associate_question_datavalue) {
                                    # code...

                                    $inputdata2['form_id'] = $request->form_id;
                                    $inputdata2['store_id'] = $request->store_id;
                                    $inputdata2['question_id'] = $associate_question_datavalue->id;
                                    $inputdata2['latitude'] = $request->longitude;
                                    $inputdata2['longitude'] = $request->longitude;
                                    $inputdata2['date'] = date('Y-m-d');
                                    $inputdata2['user_id'] = Auth::user()->id;


                                    if($SurveyFormSubmission1 = SurveyFormSubmission::create($inputdata2)){
                                        $last_insert_id1 = $SurveyFormSubmission1->id;

                                        $inputdata3['survey_form_submission_id'] = $last_insert_id1;
                                        $inputdata3['store_id'] = $request->store_id;


                                        foreach ($associate_question_datavalue->s_o_p_form_field_many as $questionkey1 => $questionvalue1) {
                                            
                                            $inputdata3['field_id'] = $questionvalue1->id;

                                            foreach ($questionvalue1->form_field_value_many as $form_field_value_key1 => $form_field_value_data1) {
                                                # code...
                                                if($questionvalue1->field_type=='radio'){
                                                    if($form_field_value_data1->status=='sel'){
                                                        $ffvalue=FormFieldValue::find($form_field_value_data1->id);
                                                        $inputdata3['value'] = 1;
                                                        $inputdata3['option_name'] = $ffvalue->form_field_option_name;
                                                        $inputdata3['scorable'] = $ffvalue->is_scorable;
                                                        $inputdata3['field_value_id'] = $form_field_value_data1->id;
                                                        //$form_field_value_data1 = $form_field_value_data1->status;
                                                        if($SurveyFormSubmissionValue = SurveyFormSubmissionValue::create($inputdata3)){
                                                            $inputdata3['option_name'] = NULL;
                                                            $inputdata3['scorable'] = NULL;
                                                        }
                                                    }
                                                }else{
                                                    if($questionvalue1->field_type!='file'){
                                                        if(!empty($form_field_value_data1->form_field_value) && $form_field_value_data1->form_field_value !='' ){
                                                            $inputdata3['value'] = $form_field_value_data1->form_field_value;
                                                            $inputdata3['field_value_id'] = $form_field_value_data1->id;
                                                            if($SurveyFormSubmissionValue = SurveyFormSubmissionValue::create($inputdata3)){

                                                            }
                                                        }
                                                    }
                                                }

                                                if(!empty($form_field_value_data1->associate_fields_data)){

                                                    foreach ($form_field_value_data1->associate_fields_data as $asfquestionkey1 => $asfquestionvalue1) {
                                                        
                                                        # code...
                                                        //echo '<pre>'; print_r($asfquestionvalue); die;

                                                            $inputdata3['field_id'] = $asfquestionvalue1->id;

                                                            foreach ($asfquestionvalue1->form_field_value_many as $form_field_value_key2 => $form_field_value_data2) {
                                                                # code...
                                                                if($asfquestionvalue1->field_type=='radio'){
                                                                    if($form_field_value_data2->status=='sel'){
                                                                        $ffvalue=FormFieldValue::find($form_field_value_data2->id);
                                                                        $inputdata3['value'] = 1;
                                                                        $inputdata3['option_name'] = $ffvalue->form_field_option_name;
                                                                        $inputdata3['scorable'] = $ffvalue->is_scorable;
                                                                        $inputdata3['field_value_id'] = $form_field_value_data2->id;
                                                                        //$form_field_value_data = $form_field_value_data->status;
                                                                        if($SurveyFormSubmissionValue = SurveyFormSubmissionValue::create($inputdata3)){
                                                                            $inputdata3['option_name'] = NULL;
                                                                            $inputdata3['scorable'] = NULL;
                                                                        }
                                                                    }
                                                                }else{
                                                                    if($asfquestionvalue1->field_type!='file'){
                                                                        if(!empty($form_field_value_data2->form_field_value) && $form_field_value_data2->form_field_value !='' ){
                                                                            $inputdata3['value'] = $form_field_value_data2->form_field_value;
                                                                            $inputdata3['field_value_id'] = $form_field_value_data2->id;
                                                                            if($SurveyFormSubmissionValue = SurveyFormSubmissionValue::create($inputdata3)){

                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                    }

                                                }
                                            }

                                        }
                                    }

                                }
                                //echo '<pre>'; print_r($questionvalue); die;

                            }
                        }
                    }
                }
            }
            /*$user_id=Auth::User()->id;
            $sopFormFieldData = FormField::with('FormFieldValueMany')->where('user_id', $user_id)->where('form_question_id', request('form_question_id'))->where('parent_id', 0)->get();
            foreach ($sopFormFieldData as $sopFormFieldDatakey => $sopFormFieldDatavalue) {
                # code...3
                //echo $sopFormFieldDatavalue->field_name;
                $sopFormFieldData[$sopFormFieldDatakey]->field_placeholder = Str::ucfirst(Str::lower($sopFormFieldDatavalue->field_placeholder));
                //$sopFormFieldData[$sopFormFieldDatakey]->question_time = date('H:i:s');
                if(!empty($sopFormFieldDatavalue->FormFieldValueMany)){
                    foreach ($sopFormFieldDatavalue->FormFieldValueMany as $form_field_value_many_key => $form_field_value_many_value) {
                        # code...
                        //echo $form_field_value_many_value->associate_question; die;
                        if(!empty($form_field_value_many_value->associate_fields) && $form_field_value_many_value->associate_fields!='' && $form_field_value_many_value->associate_fields!=null){
                            $associate_fieldsArray = explode(',', $form_field_value_many_value->associate_fields);
                            $sopFormData = FormField::with('FormFieldValueMany')->whereIn('id', $associate_fieldsArray)->get();
                            $sopFormFieldDatavalue->FormFieldValueMany[$form_field_value_many_key]->associate_fields_data = $sopFormData;
                        }else{
                            $sopFormFieldDatavalue->FormFieldValueMany[$form_field_value_many_key]->associate_fields_data = [];
                        }

                        if(!empty($form_field_value_many_value->associate_question) && $form_field_value_many_value->associate_question!='' && $form_field_value_many_value->associate_question!=null){
                            $associate_questionArray = explode(',', $form_field_value_many_value->associate_question);
                            $sopFormData = FormQuestion::with('SOPFormFieldMany.FormFieldValueMany')->whereIn('id', $associate_questionArray)->get();
                            $sopFormFieldDatavalue->FormFieldValueMany[$form_field_value_many_key]->associate_question_data = $sopFormData;
                        }else{
                            $sopFormFieldDatavalue->FormFieldValueMany[$form_field_value_many_key]->associate_question_data = [];
                        }
                        //$sopFormFieldData[$sopFormFieldDatakey]->form_field_value_many =
                    }
                }
            }*/

            /******* time data ********/
                $bgt_time = FormQuestionTimeMapping::where('form_group_id', $group_id->st_group)->where('form_question_id',request('form_question_id'))->first();
                //dd($bgt_time);
                $act_time = FormQuestionTimeTaken::where('user_id', $user_id)->where('group_id', $group_id->st_group)->where('qus_id',request('form_question_id'))->where('date', date('Y-m-d'))->where('form_id', request('form_id'))->first();
                //dd($bgt_time);

                $server_time = date('H:i');
                $diff='';

                $question_active = 10;
                $bgt_start_time = $bgt_time?date('H:i',strtotime($bgt_time->start_time)):'00:00';
                $bgt_end_time = $bgt_time?date('H:i',strtotime($bgt_time->end_time)):'00:00';

                $bgt_start_time_format = $bgt_time?date('h:i A',strtotime($bgt_time->start_time)):'00:00';
                $bgt_end_time_format = $bgt_time?date('h:i A',strtotime($bgt_time->end_time)):'00:00';

                $diff = strtotime($bgt_start_time) - strtotime($server_time);
                $duration = $diff<0?0:$diff;

                $act_start_time = $act_time?date('H:i',strtotime($act_time->act_start_time)):'00:00';
                $act_end_time = $act_time?date('H:i',strtotime($act_time->act_end_time)):'00:00';

                $act_start_time_format = $act_time?date('h:i A',strtotime($act_time->act_start_time)):'00:00';
                $act_end_time_format = $act_time?date('h:i A',strtotime($act_time->act_end_time)):'00:00';

                $a = Carbon::parse($act_end_time);
                $b= Carbon::parse($act_start_time);
                $time_taken = $b->diffInMinutes($a).' min';
                //$time_taken = date('H:i',(strtotime($act_end_time)-strtotime($act_start_time)));
            /******* time data end ********/
                $time_taken_in_minute = $b->diffInMinutes($a);
            /** update time taken in submission */
                SurveyFormSubmission::find($last_insert_id)->update(['time_taken'=>$time_taken_in_minute]);
            

                $this->return['message'] = 'Submit successfully.';
                $this->return['status'] = 1;
                $this->return['question_status'] = 1;
                $this->return['bgt_start_time'] = $bgt_start_time;
                $this->return['bgt_end_time'] = $bgt_end_time;
                $this->return['bgt_start_time_format'] = $bgt_start_time_format;
                $this->return['bgt_end_time_format'] = $bgt_end_time_format;
                $this->return['act_start_time'] = $act_start_time;
                $this->return['act_end_time'] = $act_end_time;
                $this->return['act_start_time_format'] = $act_start_time_format;
                $this->return['act_end_time_format'] = $act_end_time_format;
                $this->return['question_active'] = $question_active;
                $this->return['time_taken'] = $time_taken;
                $this->return['jsonData'] = request('data');
                $status = 200;
        }
        return response()->json($this->return, $status);
    }

    public function sop_form_image_submission(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'form_id' => 'required',
            'store_id' => 'required',
            'form_question_id' => 'required',
            'form_field_id' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'images' => 'required',
        ]);
        if ($validator->fails()) {
            $this->return = array('status'=>0, 'message'=>$validator->errors()->first());
            $status = 200;
        } else{
        //echo request('form_id'); die;
            //echo '<pre>'; print_r($request->images); die;
                    # code...
            $inputdata['date'] = date('Y-m-d');
            $inputdata['user_id'] = Auth::user()->id;

              $SurveyFormSubmission = SurveyFormSubmission::where('form_id', $request->form_id)->where('store_id', $request->store_id)->where('question_id', $request->form_question_id)->where('user_id', Auth::user()->id)->where('date', date('Y-m-d'))->get()->first();
                //dd($SurveyFormSubmission);
              if($SurveyFormSubmission){
                    $last_insert_id = $SurveyFormSubmission->id;
                        $inputdata1['survey_form_submission_id'] = $last_insert_id;
                        $inputdata1['field_id'] = $request->form_field_id;
                        $inputdata1['field_value_id'] = $request->field_value_id;
                        $inputdata1['store_id'] = $request->store_id;

                        $path = public_path('form_images');
                        if(!File::isDirectory($path)){
                            File::makeDirectory($path, 0777, true, true);
                        }
                        //$project = Project::find($request->project_id);
                        $form_path = public_path('form_images/'.'f_id_'.$request->form_id);
                        if(!File::isDirectory($form_path)){
                            File::makeDirectory($form_path, 0777, true, true);
                        }

                        $newPath = $form_path;
                        $attachment = 'form-images-' . substr(encrypt(rand()),0,20) . '.' . $request->images->getClientOriginalExtension();
                        $success = $request->images->move($newPath, $attachment);
                        $inputdata1['value'] = $success ? $attachment : null;

                        $SurveyFormSubmissionValueal = SurveyFormSubmissionValue::where('survey_form_submission_id', $last_insert_id)->where('store_id', $request->store_id)->where('field_id', $request->form_field_id)->where('field_value_id', $request->field_value_id)->get()->first();
                        //dd($SurveyFormSubmissionValueal);
                        if($SurveyFormSubmissionValueal){
                            $last_insert_id1 = $SurveyFormSubmissionValueal->id;

                            if($SurveyFormSubmissionValueal->fill($inputdata1)->save()){
                                $img_path = 'form_images/'.'f_id_'.$request->form_id.'/';
                                $SurveyFormSubmissionValueal->value = url($img_path . $SurveyFormSubmissionValueal->value);
                                $this->return['message'] = 'Submit successfully.';
                                $this->return['status'] = 1;
                                $this->return['jsonData'] = $SurveyFormSubmissionValueal;
                                $status = 200;
                            }
                        }else{

                        if($SurveyFormSubmissionValue = SurveyFormSubmissionValue::create($inputdata1)){
                            $img_path = 'form_images/'.'f_id_'.$request->form_id.'/';
                            $SurveyFormSubmissionValue->value = url($img_path . $SurveyFormSubmissionValue->value);
                            $this->return['message'] = 'Submit successfully.';
                            $this->return['status'] = 1;
                            $this->return['jsonData'] = $SurveyFormSubmissionValue;
                            $status = 200;
                        }else{
                            $this->return['message'] = 'Something went wrong.';
                            $this->return['status'] = 0;
                            $status = 200;
                        }
                    }

                }else{
                    $inputdata['form_id'] = $request->form_id;
                    $inputdata['store_id'] = $request->store_id;
                    $inputdata['question_id'] = $request->form_question_id;
                    $inputdata['latitude'] = $request->longitude;
                    $inputdata['longitude'] = $request->longitude;
                    $inputdata['status'] = 1;
                    if($SurveyFormSubmission = SurveyFormSubmission::create($inputdata)){
                        $last_insert_id = $SurveyFormSubmission->id;

                        $inputdata1['survey_form_submission_id'] = $last_insert_id;
                        $inputdata1['field_id'] = $request->form_field_id;
                        $inputdata1['field_value_id'] = $request->field_value_id;
                        $inputdata1['store_id'] = $request->store_id;

                        $path = public_path('form_images');
                        if(!File::isDirectory($path)){
                            File::makeDirectory($path, 0777, true, true);
                        }
                        //$project = Project::find($request->project_id);
                        $form_path = public_path('form_images/'.'f_id_'.$request->form_id);
                        if(!File::isDirectory($form_path)){
                            File::makeDirectory($form_path, 0777, true, true);
                        }

                        $newPath = $form_path;
                        $attachment = 'form-images-' . substr(encrypt(rand()),0,20) . '.' . $request->images->getClientOriginalExtension();
                        $success = $request->images->move($newPath, $attachment);
                        $inputdata1['value'] = $success ? $attachment : null;

                        if($SurveyFormSubmissionValue = SurveyFormSubmissionValue::create($inputdata1)){
                            $img_path = 'form_images/'.'f_id_'.$request->form_id.'/';
                            $SurveyFormSubmissionValue->value = url($img_path . $SurveyFormSubmissionValue->value);
                            $this->return['message'] = 'Submit successfully.';
                            $this->return['status'] = 1;
                            $this->return['jsonData'] = $SurveyFormSubmissionValue;
                            $status = 200;
                        }else{
                            $this->return['message'] = 'Something went wrong.';
                            $this->return['status'] = 0;
                            $status = 200;
                        }
                    }
                }
        }
        return response()->json($this->return, $status);
    }
}
