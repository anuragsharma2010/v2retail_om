<?php

namespace App\Http\Controllers\SopCompliance;

use App\Http\Controllers\Controller;
use App\Model\SopCompliance\FormQuestion;
use Illuminate\Http\Request;
use DB;
use App\User;
use App\Helpers\NotificationHelper;

class NotificationController extends Controller
{

    public function processNotification() 
    {
        // dd('jgkgjgkg');
        $notify_before = 10;
        $form_questions = FormQuestion::where('parent_id', 0)->where('status', '1')->orderBy('sort_order', 'asc')->get();
        $current_time = strtotime(date("H:i:s"));
        $notify = [];
        foreach($form_questions as $fq) {
            $role_ids = $fq->roles->pluck('role_id')->toArray();
            $user_ids =DB::table('users')
                        ->leftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
                        ->where('model_has_roles.model_type', '=', 'App\User')
                        ->whereIn('model_has_roles.role_id',$role_ids)
                        ->pluck('users.fcm_id')
                        ->toArray();
            // $user_ids = DB::table('model_has_roles')->whereIn('role_id',$role_ids)->pluck('model_id')->toArray();
            
            foreach($fq->GroupTimeMany as $time) {
                $start_time = strtotime($time->start_time);
                $before_time = $start_time - ($notify_before * 60);
                if($current_time>$before_time && $current_time<$start_time) {
                    $notify[] = ['process' => $fq->process->form_label,'question'=>$fq->question,'start_time'=>$time->start_time,'users'=>$user_ids];
                    $NotificationTitle = $fq->process->form_label . ' is about to start.';
                    $NotificationBodyMsg = $fq->process->form_label . ': "' . $fq->question . '" is going to start at '. $time->start_time;
                    NotificationHelper::sendNotificationOnMultipleDevice($NotificationTitle, $NotificationBodyMsg, $user_ids);
                }
            }
        }
        // dd($notify,$current_time);

        // $NotificationTitle = 'Process question starting';
        // $NotificationBodyMsg = 'Process name : "question title" is going to start at 15:20';
        // NotificationHelper::sendNotificationOnMultipleDevice($NotificationTitle, $NotificationBodyMsg, ['eZjlt-f2YEYuuVwapX5l_r:APA91bHGInGtZOpH9QiqOEACX48Y93GkbDBW4oPM3NpzIeR13A6P3n3VouvWs3O56GGEfRqTH9EFbv3lY96Ggq5eqgUWnxB6Dig-4KeMdXX-nqLsKYLapUtNrBY2ChXBcfqeU-E5C4gh']);
    }
}
