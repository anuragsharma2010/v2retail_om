<?php

namespace App\Http\Controllers\SopCompliance;

use App\Http\Controllers\Controller;
use App\Model\SopCompliance\FormQuestionMapping;
use Illuminate\Http\Request;
use DB;
use Auth;

class FormQuestionMappingController extends Controller
{

    function __construct()
    {
         /*$this->middleware('permission:form-list');
         $this->middleware('permission:form-create', ['only' => ['create','store']]);
         $this->middleware('permission:form-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:form-delete', ['only' => ['destroy']]);*/
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FormQuestionMapping  $formQuestionMapping
     * @return \Illuminate\Http\Response
     */
    public function show(FormQuestionMapping $formQuestionMapping)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FormQuestionMapping  $formQuestionMapping
     * @return \Illuminate\Http\Response
     */
    public function edit(FormQuestionMapping $formQuestionMapping)
    {
        //
        echo 'dfdfdfd'; die;
        return view('sopcompliance.formquestionmapping.edit',compact('FormQuestionMapping'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FormQuestionMapping  $formQuestionMapping
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FormQuestionMapping $formQuestionMapping)
    {
        //
        $this->validate($request, [
            'type' => 'required',
            'form_name' => 'required',
            'form_label' => 'required',

        ]);
        $form->type = $request->input('type');
        $form->form_name = $request->input('form_name');
        $form->form_label = $request->input('form_label');
        $form->objective = $request->input('objective');
        $form->instruction = $request->input('instruction');
        $form->save();
        return redirect()->route('forms.index')
        ->with('success','Form updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FormQuestionMapping  $formQuestionMapping
     * @return \Illuminate\Http\Response
     */
    public function destroy(FormQuestionMapping $formQuestionMapping)
    {
        //
    }
}
