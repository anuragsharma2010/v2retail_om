<?php

namespace App\Http\Controllers\SopCompliance;

use App\Http\Controllers\Controller;
use App\Model\SopCompliance\Form;
use App\Model\SopCompliance\FormRoleMapping;
use App\Model\SopCompliance\FormGroupMapping;
use App\Model\SopCompliance\FormQuestion;
use App\Model\SopCompliance\FormField;
use App\Model\SopCompliance\FormFieldValue;
use App\Model\SopCompliance\FormQuestionTimeMapping;
use App\Model\SopCompliance\FormQuestionTimeTaken;
use App\Model\SopCompliance\SurveyFormSubmission;
use App\Model\SopCompliance\SurveyFormSubmissionValue;
use Illuminate\Http\Request;
use Master\Models\StoresModel;
use Master\Models\GroupsModel;
use Spatie\Permission\Models\Role;
use DB;
use Auth;
use Spatie\Permission\Traits\HasRoles;
use Carbon\Carbon;

class FormQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $formQuestions = FormQuestion::orderBy('id','DESC')->paginate(10);
        return view('sopcompliance.question.index',compact('formQuestions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $questions=FormQuestion::select('id','question')->get();
        return view('sopcompliance.question.create',compact('questions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'question' => 'required',
            'sort_order' => 'required',
            //'form_label' => 'required',

        ]);
            $params=[
                'question' => $request->input('question'),
                'sort_order' => $request->input('sort_order'),
                'parent_id' => $request->input('parent_id'),
                //'objective' => $request->input('objective'),
                //'instruction' => $request->input('instruction'),
                'user_id' => Auth::user()->id,
                'status' => '1',
            ];

        $form = FormQuestion::create($params);

        return redirect()->route('questions.index')

                        ->with('success','Question Created successfully');
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FormQuestion  $formQuestion
     * @return \Illuminate\Http\Response
     */
    public function show(FormQuestion $formuestion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FormQuestion  $formQuestion
     * @return \Illuminate\Http\Response
     */
    public function edit($question_id)
    {
        $formQuestion=FormQuestion::find($question_id);
        //dd($formQuestion);
        $form=Form::find($formQuestion->form_id);
        $groups= FormGroupMapping::where('form_id',$form->id)->get();
        $roles = FormRoleMapping::where('form_id',$form->id)->get();
        $QuestionTimeMapping = FormQuestionTimeMapping::where(['form_id'=>$form->id, 'form_question_id'=>$question_id])->get();
        //dd($QuestionTimeMapping);
        return view('sopcompliance.form.edit_question',compact('groups','roles','form','formQuestion'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FormQuestion  $formQuestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FormQuestion $formQuestion)
    {
        dd($formQuestion);
        $this->validate($request, [
            'question' => 'required',
            'sort_order' => 'required',
        ]);
        $formQuestion->question = $request->input('question');
        $formQuestion->sort_order = $request->input('sort_order');
        $formQuestion->parent_id = $request->input('parent_id');
        $formQuestion->save();
        return redirect()->route('questions.index')
        ->with('success','Question updated successfully');
    }

    public function shortquestion(Request $request)
    {
        //dd($request->form_id);
        /*$this->validate($request, [
            'form_id' => 'required',
        ]);*/

        //dd('dddd');

        if(isset($request->form_id) && $request->form_id!=''){
            $formquestionData = FormQuestion::where(['form_id'=>$request->form_id, 'status'=>'1'])->orderBy('id', 'ASC')->get();
            //dd($formquestionData);
            foreach($formquestionData as $formquestionDatakey => $formquestionDataValue){
                $formquestionDatakeyinc = $formquestionDatakey+1;
                FormQuestion::where(['id'=>$formquestionDataValue->id, 'status'=>'1'])->update(array('sort_order' =>  $formquestionDatakeyinc));
            }
            return redirect()->route('forms.index')->with('success','Question sorting updated successfully');
        }else{
            return redirect()->route('forms.index')->with('error','Form id missing.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FormQuestion  $formQuestion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      
        DB::table("sop_form_questions")->where('id',$id)->delete();
        return redirect()->route('questions.index')
                        ->with('success','Question deleted successfully');
    }
}
