<?php

namespace App\Http\Controllers\SopCompliance\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Export;

use App\Model\SopCompliance\Form;
use App\Model\SopCompliance\FormRoleMapping;
use App\Model\SopCompliance\FormGroupMapping;
use App\Model\SopCompliance\FormQuestion;
use App\Model\SopCompliance\FormField;
use App\Model\SopCompliance\FormFieldValue;
use App\Model\SopCompliance\FormQuestionTimeMapping;
use App\Model\SopCompliance\FormQuestionTimeTaken;
use App\Model\SopCompliance\SurveyFormSubmission;
use App\Model\SopCompliance\SurveyFormSubmissionValue;

class ProcessReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request->all());
       if($request->method() == "GET"){
            return redirect()->route('forms.index');
        }
        $processData=[];

        $process = $request->input('process');
        $form=Form::find($process);
        $month_year =$request->input('month_year');
        $month_year=explode('-', $month_year);
        $data = [];
        $questions = FormQuestion::where('form_id', $process)->get();
       
        //dd($submitted[7]->submissionValues[0]->fieldValue);
        $submitted_by_date = SurveyFormSubmission::where('form_id', $process)->whereMonth('date', $month_year[0])->whereYear('date', $month_year[1])->groupBy('store_id','date')->get();
        $datahead=[];
        $headings = ['Date','StoreId','Store','Score'];
        foreach($questions as $que) {
            array_push($headings, $que->question);
            $datahead[$que->id]=$que->question;

            if($que->SOPFormFieldMany->count()>1){
                foreach($que->SOPFormFieldMany as $form_fields) {
                    if(($form_fields->field_type == 'text') || ($form_fields->field_type == 'textarea')) {
                        array_push($headings, $form_fields->field_name);
                        $datahead['-'.$form_fields->id]=$form_fields->field_name;
                    }
                }
            }
        }
         array_push($headings, 'submittedBy');
         //array_push($headings, 'State');
         //array_push($headings, 'city');
         array_push($headings, 'Time taken for submission');
     // dd($datahead);
       
        foreach($submitted_by_date as $j=>$sub_by_date){
             $submitted = SurveyFormSubmission::where('form_id', $process)->whereMonth('date', $month_year[0])
        ->whereYear('date', $month_year[1])->where(['store_id'=>$sub_by_date->store_id,'date'=>$sub_by_date->date])->get();
            $score=SurveyFormSubmission::where(['form_id'=>$process,'date'=>$sub_by_date->date,'sop_survey_form_submission.store_id'=>$sub_by_date->store_id])->rightJoin('sop_survey_form_submission_value','sop_survey_form_submission_value.survey_form_submission_id','=','sop_survey_form_submission.id')->where('sop_survey_form_submission_value.scorable','1')->count();
           
                    $processData[$j]['date'] =$sub_by_date->date;
                    $processData[$j]['store_id']= $sub_by_date->store->st_code;
                    $processData[$j]['store_code'] = $sub_by_date->store->st_name;
                    $processData[$j]['score'] = $score;
                    
        foreach($datahead as $k=>$head) {
            foreach($submitted as $sb) {
                if(($k==$sb->question_id) && ($sub_by_date->date==$sb->date)){
                    foreach($sb->submissionValues as $sbv=>$sbValue){
                        if(array_key_exists($head, $processData[$j])){
                           $processData[$j][$head.'-']=$sbValue->fieldValue->form_field_option_name;
                        }
                        $processData[$j][$head]=$sbValue->fieldValue->form_field_option_name;

                        if(empty($processData[$j][$head])){
                            $processData[$j][$head]=$sbValue->value ?? '--';
                        }
                        $slug=$k;
                        break;
                   }
                }else{
                 foreach($sb->submissionValues as $sbv=>$sbValue){
                 if(($k=='-'.$sbValue->field_id) && ($sub_by_date->date==$sb->date)){
                  
                    $processData[$j][$head]=$sbValue->value;
                    if(empty($processData[$j][$head])){
                            $processData[$j][$head]=$sbValue->value ?? '--';
                        }
                          $slug=$k;
                }
                }
            }
            }
            if($slug==0){
                $processData[$j][$head]='--';
            }
            $slug=0;
        }   
        $processData[$j]['submitted_by'] = $sub_by_date->user->name ?? '--'; 
        //$processData[$j]['state'] = $sub_by_date->store->State->state; 
        //$processData[$j]['city'] = $sub_by_date->user->name; 
        $minutes = SurveyFormSubmission::where(['form_id'=>$process,'date'=>$sub_by_date->date])->sum('time_taken');
        // $hours = intdiv($minutes, 60).':'. ($minutes % 60);
    
        $processData[$j]['time_taken'] = $minutes.' Minutes'; 
    }

      //dd($datahead);
      //dd($processData);
        return (new Export(collect([$processData]),$headings))->download($form->form_label.'.xlsx');
    } 

}
