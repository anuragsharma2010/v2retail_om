<?php

namespace App\Http\Controllers\SopCompliance;
use App\Http\Controllers\Controller;
use App\Model\SopCompliance\Form;
use App\Model\SopCompliance\FormRoleMapping;
use App\Model\SopCompliance\FormGroupMapping;
use App\Model\SopCompliance\FormQuestion;
use App\Model\SopCompliance\FormField;
use App\Model\SopCompliance\FormFieldValue;
use App\Model\SopCompliance\FormQuestionTimeMapping;
use App\Model\SopCompliance\FormQuestionTimeTaken;
use App\Model\SopCompliance\SurveyFormSubmission;
use App\Model\SopCompliance\SurveyFormSubmissionValue;
use Illuminate\Http\Request;
use Master\Models\StoresModel;
use Master\Models\GroupsModel;
use Spatie\Permission\Models\Role;
use Validator;
use Illuminate\Validation\Rule;
use DB;
use Auth;
use Spatie\Permission\Traits\HasRoles;
use Carbon\Carbon;

class FormController extends Controller
{
use HasRoles;
   
    function __construct()
    {
         $this->middleware('permission:form-list');
         $this->middleware('permission:form-create', ['only' => ['create','store']]);
         $this->middleware('permission:form-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:form-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $forms = new Form();
        $fields = $forms->getTableColumns();
        if (isset($request->search) && isset($request->searchtxt)) {
            foreach ($fields as $value) {
                if ($value == 'id' or
                        $value == 'created_at'
                        or $value == 'updated_at' or $value == 'status') {
                    continue;
                }
                $forms = $forms->orWhere($value, 'like', '%' . $request->searchtxt . '%');
            }
            $forms = $forms->orderBy('id', 'DESC')->paginate(10);
        } else {
            $forms = $forms->orderBy('id', 'DESC')->paginate(10);
        }
        $forms->searchtxt = $request->searchtxt ? $request->searchtxt : '';

        //dd($forms);

        return view('sopcompliance.form.index', compact('forms'))->with('i', ($request->input('page', 1) - 1) * 5);
        
        //$forms=Form::orderBy('id','DESC')->paginate(10);
        //return view('sopcompliance.form.index',Compact('forms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups=GroupsModel::select('group_code','id')->get();
        $roles = Role::select('name','roles.id')->leftJoin('sop_divisions','sop_divisions.id', '=', 'roles.div_id')->where('div_name','like','store')->get();
        $selectedGroups=[];
        $selectedRoles=[];
        return view('sopcompliance.form.create_new',compact('groups','roles','selectedGroups','selectedRoles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $messages = array(
            'required' => 'This field is required.',
        );
        $this->validate($request, [
                'form_name' => 'required|unique:sop_forms,form_name',
                'form_label' => 'required|unique:sop_forms,form_label',
                'roles' => 'required',
                'groups' => 'required',
            ],$messages);
            $params=[
                'form_name' => $request->input('form_name'),
                'form_label' =>$request->input('form_label'),
                'method' => 'POST',
                'enc_type' => 'multipart/form-data',
                'user_id' => Auth::user()->id,
                'status' => '0',
            ];
        $form = Form::create($params);
        //$form=Form::find(21);
        $roles=$request->input('roles');
        $groups=$request->input('groups');
         foreach($roles as $role){
            FormRoleMapping::create(['form_id'=>$form->id,'role_id'=>$role]);
         }   
         foreach($groups as $group){
            FormGroupMapping::create(['form_id'=>$form->id,'group_id'=>$group]);
         }  
        $groups= FormGroupMapping::where('form_id',$form->id)->get();
        $roles = FormRoleMapping::where('form_id',$form->id)->get();
        $formQuestions=FormQuestion::where('form_id',$form->id)->get();

        return view('sopcompliance.form.create_question',compact('groups','roles','form','formQuestions')); 
    }
    public function submitQuestion(Request $request)
    {
       // dd($request->all());
        if($request->method() == "GET"){
            return redirect()->route('forms.index');
        }

        if($request->input('form_id')){
            $req_form_id = $request->input('form_id');
        }else{
            $req_form_id = '';
        }

        $validator = Validator::make($request->all(), [
            'question' => ['required',
                                    Rule::unique('sop_form_questions')->where(function ($query) use ($req_form_id){
                                        $query->where('form_id', $req_form_id); 
                                    }),
                                ],
            'sort_order' => ['required',
                                    Rule::unique('sop_form_questions')->where(function ($query) use ($req_form_id){
                                        $query->where('form_id', $req_form_id); 
                                    }),
                                ],
            'option_type' => 'required',
        ], ['question.required'=>'Question name is required.', 'question.unique'=>'Question Title is already exits.', 'sort_order.required'=>'Question order is required.', 'sort_order.unique'=>'Question order is already exits.', 'option_type.required'=>'Option type is required.']);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
         $questions=$request->input('question');
         $question_fields=$request->input('option_type');
         $field_values=$request->input('field_option_value');
         $categories=$request->input('categories');
         $form_id=$request->input('form_id');
         if(!empty($questions)){
                $qdata=[
                'question' => $request->input('question'),
                'sort_order' => $request->input('sort_order'),
                'question_alias' => $request->input('question_alias'),
                'show_on_dashboard' => !empty($request->input('show_on_dashboard')) ? '1' : '0',
                'form_id'=>$form_id,
                'user_id' => Auth::user()->id,
                'status' => '1',
                ];

                $ques=FormQuestion::create($qdata);
                if(!empty($categories)){
                    foreach($categories as $ct=>$category){
                         if(!empty($request->input('categories')[$ct])){
                            $groupTimeMapping=[
                                'form_id' => $form_id,
                                'form_group_id' => $request->input('categories')[$ct],
                                'form_question_id'=>$ques->id,
                                'start_time'=>$request->input('start_time')[$ct],
                                'end_time'=>$request->input('end_time')[$ct],
                            ];
                            $timeMapping= FormQuestionTimeMapping::create($groupTimeMapping);
                        }
                    }
                }
                //dd($ques);
                if(!empty($question_fields)){
                    foreach($question_fields as $qf=>$question_field){
                        $qfield=[
                        'field_type' => $request->input('option_type')[$qf],
                        'field_name' => $request->input('field_name')[$qf],
                        'field_placeholder' => $request->input('field_placeholder')[$qf],
                        'form_field_required' => !empty($request->input('field_require')[$qf]) ? '1' : '0' ,
                        'form_field_validation_rules' => $request->input('validation_rules')[$qf],
                        'form_question_id'=>$ques->id,
                        'user_id' => Auth::user()->id,
                        'status' => '1',
                        ];
                        $fields=FormField::create($qfield);
                         if(!empty($field_values[$qf])){
                            foreach($field_values[$qf] as $qfv=>$field_value){
                                if(!empty($request->input('field_option_name')[$qf][$qfv])){
                                $fieldValue=[
                                    'form_field_option_name' => !empty($request->input('field_option_name')[$qf][$qfv]) ? $request->input('field_option_name')[$qf][$qfv] : NULL ,
                                    'form_field_value' => !empty($request->input('field_option_name')[$qf][$qfv]) ? $request->input('field_option_value')[$qf][$qfv] : NULL ,
                                    'is_scorable' => !empty($request->input('field_scorable')[$qf][$qfv]) ? $request->input('field_scorable')[$qf][$qfv] : '0' ,
                                    'form_field_id'=>$fields->id,
                                    'user_id' => Auth::user()->id,
                                    'status' => '1',
                                ];
                                $fvalue=FormFieldValue::create($fieldValue);
                            }else{
                                    $fieldValue=[
                                    'form_field_option_name' => NULL ,
                                    'form_field_value' =>  NULL ,
                                    'form_field_id'=>$fields->id,
                                    'user_id' => Auth::user()->id,
                                    'status' => '1',
                                     ];
                                $fvalue=FormFieldValue::create($fieldValue);
                                break;
                                }
                            }
                        }
                    }
                }
            }
            if($request->submit == "Save"){
                return redirect()->route('forms.index')
                        ->with('success','Process created successfully');
                    }else{
                    $form=Form::find($form_id);
                    $formQuestions=FormQuestion::where('form_id',$form->id)->get();
                    $groups= FormGroupMapping::where('form_id',$form->id)->get();
                    $roles = FormRoleMapping::where('form_id',$form->id)->get();
                    return view('sopcompliance.form.create_question',compact('groups','roles','form','formQuestions'));  
                    }
            }
        }
        public function questionFieldMapping(Request $request){
            dd($request->all());
            return redirect()->route('forms.index');
             if($request->submit == "Save"){
            return redirect()->route('forms.index')
                    ->with('success','Process created successfully');
                }else{
                $form=Form::find($form_id);
                $formQuestions=FormQuestion::where('form_id',$form->id)->get();
                $groups= FormGroupMapping::where('form_id',$form->id)->get();
                $roles = FormRoleMapping::where('form_id',$form->id)->get();
            return view('sopcompliance.form.create_question',compact('groups','roles','form','formQuestions'));  
                }
        }
        /**
         * Display the specified resource.
         *
         * @param  \App\Form  $form
         * @return \Illuminate\Http\Response
         */
    public function show(Form $form)
    {
        $formQuestions=FormQuestion::where('form_id',$form->id)->get();
        $groups= FormGroupMapping::where('form_id',$form->id)->get();
        $roles = FormRoleMapping::where('form_id',$form->id)->get();

        return view('sopcompliance.form.create_question',compact('groups','roles','form','formQuestions'));
    }   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function editProcess(Form $form)
    {
        $formQuestions=FormQuestion::where('form_id',$form->id)->get();
        $groups= FormGroupMapping::where('form_id',$form->id)->get();
        $roles = FormRoleMapping::where('form_id',$form->id)->get();
         return view('sopcompliance.form.create_question',compact('groups','roles','form','formQuestions'));  
    }
    public function edit(Form $form)
    {
        $groups=GroupsModel::select('group_code','id')->get();
        $roles = Role::select('name','roles.id')->leftJoin('sop_divisions','sop_divisions.id', '=', 'roles.div_id')->where('div_name','like','store')->get();
      
        $selectedGroups= FormGroupMapping::where('form_id',$form->id)->pluck('group_id')->toArray();
        $selectedRoles = FormRoleMapping::where('form_id',$form->id)->pluck('role_id')->toArray();
        return view('sopcompliance.form.create_new',compact('groups','roles','selectedGroups','selectedRoles','form'));  
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Form $form)
    {
        $this->validate($request, [
            'form_name' => 'required',
            'form_label' => 'required',
            'roles' => 'required',
            'groups' => 'required',
        ]);
        $roles=$request->input('roles');
        $groups=$request->input('groups');
        foreach($roles as $role){
            FormRoleMapping::updateOrCreate(['form_id'=>$form->id,'role_id'=>$role] , ['form_id'=>$form->id,'role_id'=>$role] );
            //FormRoleMapping::create(['form_id'=>$form->id,'role_id'=>$role]);
        }   
        foreach($groups as $group){
            //FormGroupMapping::create(['form_id'=>$form->id,'group_id'=>$group]);
            FormGroupMapping::updateOrCreate(['form_id'=>$form->id,'group_id'=>$group],['form_id'=>$form->id,'group_id'=>$group]);
        }  
        $form->form_name = $request->input('form_name');
        $form->form_label = $request->input('form_label');
        $form->save();
        $formQuestions=FormQuestion::where('form_id',$form->id)->get();
        $groups= FormGroupMapping::where('form_id',$form->id)->get();
        $roles = FormRoleMapping::where('form_id',$form->id)->get();

        return view('sopcompliance.form.create_question',compact('groups','roles','form','formQuestions'));
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //DB::table("sop_forms")->where('id',$id)->delete();
        return redirect()->route('forms.index');
                       // ->with('success','Form deleted successfully');
    }

    public function storeAssignedToProcess(Request $request)
    {

        $date = isset($request->dt) ? date('Y-m-d',strtotime($request->dt)) : date('Y-m-d');
        $process_id=base64_decode($request->process_id);
        $group_ids=FormGroupMapping::where('form_id',$process_id)->pluck('group_id')->toArray();
        //$stores=StoresModel::whereIn('st_group',$group_ids)->get();
        /***** store searching functionality 13march2020******/
        $stores = new StoresModel();
        if (isset($request->search) && isset($request->searchtxt)) {
            $stores = $stores->whereIn('st_group',$group_ids)->where('st_name', 'like', '%' . $request->searchtxt . '%')->get();
            //$stores = $stores->orderBy('id', 'DESC')->paginate(10);
        } else {
          $stores = $stores->whereIn('st_group',$group_ids)->get();
            //$stores = $stores->orderBy('id', 'DESC')->paginate(10);
        }
        $stores->searchtxt = $request->searchtxt ? $request->searchtxt : '';
        /***** store searching functionality 13march2020 end******/

        $questioncount = FormQuestion::join('sop_form_fields', function ($join) {
                            $join->on('sop_form_questions.id', '=', 'sop_form_fields.form_question_id')
                                ->where('sop_form_fields.field_type','=','radio')
                                ->where('sop_form_fields.parent_id','=','0');
                        })->join('sop_form_field_values', function ($join) {
                            $join->on('sop_form_fields.id', '=', 'sop_form_field_values.form_field_id')
                                ->where('sop_form_field_values.is_scorable','=','1');
                        })
                        ->where('sop_form_questions.form_id', $process_id)->where('sop_form_questions.parent_id', 0)->count();
        $processes=Form::select('id','form_label')->get();

        $availed_score =0;
        $percentage = 0;
        $yesCount= 0;
        foreach($stores as $key=>$store){
        // $questionnumberofyesCount = DB::select("SELECT COUNT(*) AS count  FROM sop_form_questions AS q
        //           JOIN sop_form_fields AS f ON f.form_question_id=q.id AND f.field_type='radio' AND f.parent_id='0' AND q.parent_id='0'
        //           JOIN sop_form_field_values AS v ON v.form_field_id=f.id AND (form_field_option_name='Yes')
        //           JOIN sop_survey_form_submission_value AS sv ON sv.field_id=f.id AND sv.field_value_id=v.id
        //           WHERE q.id = '".$process_id."'
        //           GROUP BY q.id");

        // $questionnumberofyesCount = DB::select("SELECT COUNT(*) AS count  FROM sop_form_questions AS q
        //           JOIN sop_form_fields AS f ON f.form_question_id=q.id AND f.field_type='radio' AND f.parent_id='0' AND q.parent_id='0'
        //           JOIN sop_form_field_values AS v ON v.form_field_id=f.id AND (form_field_option_name!='No')
        //           JOIN sop_survey_form_submission AS fs ON fs.question_id=q.id AND fs.form_id=q.form_id
        //           JOIN sop_survey_form_submission_value AS sv ON sv.field_id=f.id AND sv.field_value_id=v.id
        //           WHERE q.form_id = '".$process_id."' AND fs.store_id = '".$store->id."' AND fs.date = '".$date."'
        //           GROUP BY q.id");
            $questionnumberofyesCount=SurveyFormSubmission::where(['form_id'=>$process_id,'date'=>$date,'sop_survey_form_submission.store_id'=>$store->id])->rightJoin('sop_survey_form_submission_value','sop_survey_form_submission_value.survey_form_submission_id','=','sop_survey_form_submission.id')->where('sop_survey_form_submission_value.scorable','1')->count();
                if(!empty($questionnumberofyesCount)){
                    $yesCount = $questionnumberofyesCount;
                }else{
                    $yesCount = 0;
                }
                $availed_score = $yesCount;
                $percentage = $questioncount>0 ? round((($availed_score/$questioncount)*100),2) :'0' ;
                $stores[$key]['percentage']= $percentage;
            }
        return view('sopcompliance.report.stores_on_process',compact(['stores','process_id','percentage','processes']));
    } 
    public function submittedQuestions(Request $request)
    {
        $process_id=base64_decode($request->prosid);
        $store_id=base64_decode($request->stid);
       
        if(isset($request->dt)){
        $date=date('Y-m-d',strtotime($request->dt));
        }else{
            $date=date('Y-m-d');
        }
        $group_ids=FormGroupMapping::where('form_id',$process_id)->pluck('group_id')->toArray();
        $stores=StoresModel::whereIn('st_group',$group_ids)->get();
        $questions = SurveyFormSubmission::where(['form_id'=>$process_id,'store_id'=>$store_id,'date'=>$date])->get();

        foreach($questions as $key=>$question){
            $singleStore=StoresModel::where('id',$question->store_id)->first();
            //dd($question->id.'----'.$process_id.'------'.$singleStore->st_group);
            $budgeted_time = FormQuestionTimeMapping::where(['form_question_id'=>$question->question_id,'form_id'=>$process_id,'form_group_id'=>$singleStore->st_group])->first();
           // dd($budgeted_time);
            $questions[$key]['bgt_start_time'] = optional($budgeted_time)->start_time;
            $questions[$key]['bgt_end_time'] = optional($budgeted_time)->end_time;
            $actual_time=FormQuestionTimeTaken::where(['qus_id'=>$question->question_id,'group_id'=>$singleStore->st_group])->first();

            $questions[$key]['act_start_time'] = optional($actual_time)->act_start_time;
            $questions[$key]['act_end_time'] = optional($actual_time)->act_end_time;
                $a = Carbon::parse(optional($actual_time)->act_start_time);
                $b= Carbon::parse(optional($actual_time)->act_end_time);
                // dd($act_time->act_end_time);
                $questions[$key]['time_taken'] = $b->diffInMinutes($a).' Minutes';
            
        }
        
        //$questions = FormQuestion::whereIn('id', $question_ids)->get();
        //dd($questions->first());
        $stores=StoresModel::whereIn('st_group',$group_ids)->get();
        $processes=Form::select('id','form_label')->get();
        return view('sopcompliance.report.submitted_questions',compact(['questions','process_id','stores','processes','store_id']));
    }

    public function process_status(Request $request)
    {
      //print_r($request->all()['data']); die;
      //dd($request->all());
      $pIds = $request->all()['data'];
      if(!empty($pIds)){
        $forms = Form::whereIn('id', $pIds)->update(['status' => $request->all()['status']]);
        $this->return['message'] = 'Status update successfully.';
        $this->return['status'] = 1;
        $status = 200;
        return response()->json($this->return, $status);
        //return redirect()->route('forms.index')->with('success','Status updated successfully');
      }
    }
}
