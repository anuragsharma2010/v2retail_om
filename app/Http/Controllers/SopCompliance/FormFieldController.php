<?php

namespace App\Http\Controllers\SopCompliance;
use App\Http\Controllers\Controller;

use App\Model\SopCompliance\FormField;
use Illuminate\Http\Request;

class FormFieldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FormField  $FormField
     * @return \Illuminate\Http\Response
     */
    public function show(FormField $FormField)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FormField  $FormField
     * @return \Illuminate\Http\Response
     */
    public function edit(FormField $FormField)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FormField  $FormField
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FormField $FormField)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FormField  $FormField
     * @return \Illuminate\Http\Response
     */
    public function destroy(FormField $FormField)
    {
        //
    }
}
