<?php

namespace App\Http\Controllers\SopCompliance;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Master\Models\StoresModel;

use App\Model\SopCompliance\FormField;
use Illuminate\Http\Request;

class SopDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data=  DB::select("SELECT q.id, q.question as question, q.show_on_dashboard as show_dashboard, COUNT(*) AS count  FROM sop_form_questions AS q
                  JOIN sop_form_fields AS f ON f.form_question_id=q.id AND f.field_type='radio' AND f.parent_id='0' AND q.parent_id='0' 
                  JOIN sop_form_field_values AS v ON v.form_field_id=f.id AND (is_scorable!='1')
                  JOIN sop_survey_form_submission AS su ON su.question_id=q.id
                  JOIN sop_survey_form_submission_value AS sv ON sv.survey_form_submission_id =su.id AND sv.field_id=f.id AND sv.field_value_id =v.id AND (sv.scorable!='1')
                  GROUP BY q.id");
//AND q.show_on_dashboard='1'
      //dd($data);
      foreach($data as $key => $val){
        $data[$key]->totalStore = DB::table('stores')->count();
      }
      $data = json_decode(json_encode($data));
      //dd($data);
      return view('sopcompliance.dashboard',compact('data'));
    }

    public function show($qus_id)
    {
      $data=  DB::select("SELECT q.*,u.`*`, st.* FROM sop_form_questions AS q
                  JOIN sop_form_fields AS f ON f.form_question_id=q.id AND f.field_type='radio' AND f.parent_id='0' AND q.parent_id='0' AND q.show_on_dashboard='1'
                  JOIN sop_form_field_values AS v ON v.form_field_id=f.id AND (is_scorable !='1')
                  JOIN sop_survey_form_submission_value AS sv ON sv.field_id=f.id AND sv.field_value_id=v.id
                  JOIN users AS u ON u.id=q.user_id
                  JOIN stores AS st ON st.id=u.store_id
                  WHERE q.id=".$qus_id." GROUP BY q.id");

      $data = json_decode(json_encode($data));
      dd($data);
      return view('sopcompliance.sopdetails',compact('data'));
    }
}
