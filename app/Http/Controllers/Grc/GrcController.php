<?php

namespace App\Http\Controllers\Grc;

use App\Http\Controllers\Controller;
use App\Model\Grc\Grc;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\GrcImport;
use App\Exports\Export;

class GrcController extends Controller
{
    public function index(Request $request)
    {
        $grc = Grc::orderBy('created_at', 'asc');
        $date = $request->month?'01-'.$request->month:date('Y-m-d');
        $grc = $grc->whereMonth('pstng_date', '=', Carbon::parse($date)->format('m'))->whereYear('pstng_date', '=', Carbon::parse($date)->format('Y'));
        $grc = $grc->paginate($request->perpage??10);
        return view('grc.index', compact('grc'));
    }

    public function import(Request $request)
    {
        return view('grc.import');
    }

    public function importSave(Request $request)
    {
        $this->validate($request, [
            'file' => ['required', new \App\Rules\ValidFile('xlsx')]
        ],['file.required' => 'This field is required!']);
        
        $sheets = Excel::toCollection(new GrcImport, $request->file);

        foreach ($sheets[0] as $row) 
        {
            $pstng_date =  Carbon::parse($row['pstng_date'])->format('Y-m-d');
            Grc::updateOrCreate(
                ['pstng_date' => $pstng_date, 'article' => $row['article']],
                ['site' => $row['site'], 'mvt' => $row['mvt'], 'quantity' => $row['quantity'], 'amount_lc' => $row['amount_in_lc'], 'vendor' => $row['vendor'], 'article_doc' => $row['art_doc'], 'item' => $row['item']]
            );
        }

        return redirect()->route('grc.index')->with('success','GRC Imported successfully.');
    }

    public function importTemplate()
    {
        $headings = ['Pstng Date','Site','Article','MvT','Quantity','Amount in LC','Vendor','Art. Doc.','Item'];
        return (new Export(collect([]),$headings))->download('grc.xlsx');
    }
}
