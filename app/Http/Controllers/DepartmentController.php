<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Model\DepartmentModel;
use App\Model\DivisionModel;
use App\User;
use Spatie\Permission\Models\Role;
use DB;

class DepartmentController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $data = new DepartmentModel();
        $fields = $data->getTableColumns();
        if (isset($request->search) && isset($request->searchtxt)) {
            foreach ($fields as $value) {
                if ($value == 'id' or
                        $value == 'created_at'
                        or $value == 'updated_at' or $value == 'st_status') {
                    continue;
                }
                $data = $data->orWhere($value, 'like', '%' . $request->searchtxt . '%');
            }
            $data = $data->orderBy('id', 'ASC')->paginate(15);
        } else {
            //->leftJoin('sop_divisions', 'sop_divisions.id', '=', 'departments.div_id')
            $data = $data->select('departments.*','sop_divisions.id as div_id','sop_divisions.div_name as div_name')->leftJoin('sop_divisions', 'sop_divisions.id', '=', 'departments.div_id')->orderBy('departments.id', 'ASC')->paginate(15);
        }
        $data->searchtxt = $request->searchtxt ? $request->searchtxt : '';


        return view('department.index', compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function edit($id) {
        $DataObj = DepartmentModel::where(['id' => $id])->first();
        $DataObj->Divisions = DivisionModel::orderBy('id', 'DESC')->get();
        return view('department.create', ['DataObj' => $DataObj, 'id' => $id]);
    }

    public function create() {
        $DataObj = new DepartmentModel();
        $DataObj->Divisions = DivisionModel::orderBy('id', 'DESC')->get();
       
        return view('department.create', ['DataObj' => $DataObj]);
    }

    public function show(Request $request) {
        echo "hi..show...";
        die();
    }

    public function store(Request $request) {
        //echo "hi....";die();
        $DataObj = new DepartmentModel();
        $DataObj->Divisions = DivisionModel::orderBy('id', 'DESC')->get();
        $messages = array(
            'required' => 'This field is required.',
        );
        $validator = Validator::make($request->all(), [
                    'div_id' => 'required|string|max:50',
                    'dept_name' => 'required|string|max:50',
                    'dept_name' => 'required',
//                    'dept_code' => 'required|string|max:90',
//                    'dept_desc' => 'required|string|max:200',
                    'dept_status' => 'required',
        ],$messages);
       
        if ($validator->fails()) {

            return view('department.create', ['errors' => $validator->errors(), 'DataObj' => $DataObj,'request'=>$request]);
        }
        $input = $request->all();

        $DataObj->create($input);
        return redirect()->route('department.index')
                        ->with('success', 'Department created successfully');
    }

    public function update(Request $request) {
       if (isset($request->id)) {
            $DataObj = DepartmentModel::find($request->id);
            
            }
       $messages = array(
            'required' => 'This field is required.',
        );
        $validator = Validator::make($request->all(), [
                    'div_id' => 'required|string|max:50',
                    'dept_name' => 'required|string|max:50',
                    'dept_name' => 'required',
//                    'dept_code' => 'required|string|max:90',
//                    'dept_desc' => 'required|string|max:200',
                    'dept_status' => 'required',
        ],$messages);
        
        if ($validator->fails()) {
            
            $DataObj->Divisions = DivisionModel::orderBy('id', 'DESC')->get();
            return view('department.create', ['errors' => $validator->errors(), 'DataObj' => $DataObj,"request"=>$request]);
        }
         if (isset($request->id)) {
            $DataObj = DepartmentModel::find($request->id);
            //dd($request->all());
            $DataObj->update($request->all());
            return redirect()->route('department.index')
                            ->with('success', 'Department Updated successfully');
        } else {
            return "Record not found";
        }

       
    }

    public function destroy($id) {

        DepartmentModel::find($id)->delete();

        return redirect()->route('department.index')
                        ->with('success', 'Department deleted successfully');
    }
    

}
