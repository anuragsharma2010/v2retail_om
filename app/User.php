<?php


namespace App;


use Illuminate\Notifications\Notifiable;

use Laravel\Passport\HasApiTokens;

use Illuminate\Contracts\Auth\MustVerifyEmail;

use Illuminate\Foundation\Auth\User as Authenticatable;

use Spatie\Permission\Traits\HasRoles;


class User extends Authenticatable

{

    use HasApiTokens, Notifiable;

    use HasRoles;


    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'name', 'email', 'password', 'device_id', 'fcm_id', 'latitude', 'longitude','div_id','dept_id','store_id','status'

    ];


    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */
     public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }

    protected $hidden = [

        'password', 'remember_token',

    ];

}