<?php

namespace App\Exports;
  
use App\Model\Po\Po;
use Maatwebsite\Excel\Concerns\FromCollection;
  
class PoExport implements FromCollection
{
    public function collection()
    {
        return Po::all();
    }
}