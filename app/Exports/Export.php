<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;  
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class Export implements FromCollection,WithStrictNullComparison,WithHeadings,ShouldAutoSize,WithEvents
{
    use Exportable;

    protected $headings;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct($data, $headings)
    {
        $this->data = $data;
        $this->headings = $headings;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->data;
    }

    public function headings() : array
    {
        return $this->headings;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->getRowDimension('1')->setRowHeight(15);
                $event->sheet->getDelegate()->getStyle('A1:'.chr(64+sizeof($this->headings)).'1')->getFont()->setBold( true );
                $styleArray = [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ]
                ];                
                $event->sheet->getStyle('A1:'.chr(64+sizeof($this->headings)).'1')->applyFromArray($styleArray);
            },
        ];
    }
}