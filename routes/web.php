<?php
date_default_timezone_set('Asia/Kolkata');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     //return view('welcome');
//     //return view('auth/login');

// });

    Auth::routes();

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('process-notification','SopCompliance\NotificationController@processNotification')->name('process-notification');

    Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
    Route::resource('permissions','PermissionController');
    Route::resource('division','DivisionController');
    Route::resource('department','DepartmentController');
    /* accessories_logic_input_sheets_upload_utility path */
    Route::resource('accessories-utility','AccessoriesUtilityController');

    /* Sop compliance Routes*/
    Route::get('sop','SopCompliance\SopDashboardController@index')->name('sopDashboard');
    Route::get('sop/{qus_id}', 'SopCompliance\SopDashboardController@show')->name('sopDetails');
    Route::get('assignedstore/{process_id}','SopCompliance\FormController@storeAssignedToProcess')->name('assignedstore');
    Route::get('submittedQuestions','SopCompliance\FormController@submittedQuestions')->name('submittedQuestions');
    Route::match(array('GET', 'POST'),'process-report','SopCompliance\Report\ProcessReportController@index');

    Route::resource('questions','SopCompliance\FormQuestionController');
    Route::resource('forms','SopCompliance\FormController');
    Route::get('shortquestion/{form_id}','SopCompliance\FormQuestionController@shortquestion')->name('shortquestion');
    Route::post('process_status','SopCompliance\FormController@process_status')->name('process_status');
    Route::match(array('GET', 'POST'),'create_question','SopCompliance\FormController@submitQuestion')->name('create_question');
    Route::post('questionFieldMapping','SopCompliance\FormController@questionFieldMapping')->name('questionFieldMapping');
    /***edit Question Routes***/
     Route::get('question/{id}/edit','SopCompliance\FormQuestionController@edit')->name('questionEdit');
    //Route::resource('form_question_mapping','SopCompliance\FormQuestionMappingController');
    //GRC Routes
    Route::group(['prefix' => 'grc', 'as' => 'grc.'], function () {
        Route::get('/','Grc\GrcController@index')->name('index');
        Route::get('import', 'Grc\GrcController@import')->name('import');
        Route::get('import/template', 'Grc\GrcController@importTemplate')->name('import.template');
        Route::post('import/save', 'Grc\GrcController@importSave')->name('import.save');
    });

    //PO Routes
    Route::group(['prefix'=> 'po', 'as'=>'po.'],function(){
        Route::get('/','Po\PoController@index')->name('index');
        Route::get('import/template', 'Po\PoController@importTemplate')->name('import.template');
        Route::get('import', 'Po\PoController@import')->name('import');
        Route::post('/import-save', 'Po\PoController@importSave')->name('import.save');
    });

    //PR Plan Routes
    Route::group(['prefix'=> 'pr-plan', 'as'=>'pr-plan.'],function(){
        Route::get('maj-cat','PrPlan\PrPlanController@majCat')->name('maj-cat');
        Route::get('maj-cat/import','PrPlan\PrPlanController@majCatImport')->name('maj-cat.import');

        Route::get('maj-cat-seg','PrPlan\PrPlanController@majCatSeg')->name('maj-cat-seg');
        Route::get('maj-cat-seg/import','PrPlan\PrPlanController@majCatSegImport')->name('maj-cat-seg.import');

        Route::get('maj-cat-seg-mvgr','PrPlan\PrPlanController@majCatSegMvgr')->name('maj-cat-seg-mvgr');
        Route::get('maj-cat-seg-mvgr/import','PrPlan\PrPlanController@majCatSegMvgrImport')->name('maj-cat-seg-mvgr.import');

        Route::get('reference-article','PrPlan\PrPlanController@referenceArticle')->name('reference-article');
        Route::get('reference-article/import','PrPlan\PrPlanController@referenceArticleImport')->name('reference-article.import');

        Route::get('options-article','PrPlan\PrPlanController@optionsArticle')->name('options-article');
        Route::get('options-article/import','PrPlan\PrPlanController@optionsArticleImport')->name('options-article.import');

        Route::get('auto-pr','PrPlan\PrPlanController@autoPr')->name('auto-pr');
        Route::get('auto-pr/import','PrPlan\PrPlanController@autoPrImport')->name('auto-pr.import');
    });
    // OTB Plan Routes
    Route::group(['prefix'=> 'otb-plan', 'as'=>'otb-plan.'],function(){
        Route::get('maj-cat','OtbPlan\OtbPlanController@majCat')->name('maj-cat');
        Route::get('maj-cat/import','OtbPlan\OtbPlanController@majCatImport')->name('maj-cat.import');

        Route::get('maj-cat-seg','OtbPlan\OtbPlanController@majCatSeg')->name('maj-cat-seg');
        Route::get('maj-cat-seg/import','OtbPlan\OtbPlanController@majCatSegImport')->name('maj-cat-seg.import');

        Route::get('maj-cat-seg-mvgr','OtbPlan\OtbPlanController@majCatSegMvgr')->name('maj-cat-seg-mvgr');
        Route::get('maj-cat-seg-mvgr/import','OtbPlan\OtbPlanController@majCatSegMvgrImport')->name('maj-cat-seg-mvgr.import');

        Route::get('reference-article','OtbPlan\OtbPlanController@referenceArticle')->name('reference-article');
        Route::get('reference-article/import','OtbPlan\OtbPlanController@referenceArticleImport')->name('reference-article.import');

        Route::get('options-article','OtbPlan\OtbPlanController@optionsArticle')->name('options-article');
        Route::get('options-article/import','OtbPlan\OtbPlanController@optionsArticleImport')->name('options-article.import');
    });

    //Zonning
    Route::view('zonning', 'zonning.zonning');
});
