<?php

use Illuminate\Http\Request;
date_default_timezone_set('Asia/Kolkata');
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Api'], function() {
      Route::post('/login', 'UserController@login');
      Route::group(['middleware' => 'auth:api'], function() {
   		Route::get('/profile', 'UserController@userDetais');
   		Route::post('/sop_forms', 'SopCompliance\FormController@index');

   		Route::post('/sop_form_data', 'SopCompliance\FormController@sop_form_data');
      Route::post('/question_start_time', 'SopCompliance\FormController@question_start_time');
      Route::post('/sop_form_question_fields', 'SopCompliance\FormController@sop_form_question_fields');
      Route::post('/sop_form_submission', 'SopCompliance\FormController@sop_form_submission');
      Route::post('/sop_form_image_submission', 'SopCompliance\FormController@sop_form_image_submission');

   		//Route::post('details', 'UserController@details');
   		Route::post('/logout', 'UserController@logout');
   });
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
