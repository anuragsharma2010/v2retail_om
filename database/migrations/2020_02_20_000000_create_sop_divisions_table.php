<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSopDivisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('sop_divisions')) {
            Schema::create('sop_divisions', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('div_name',50);
                $table->mediumText('div_code');
                $table->text('div_desc');
                $table->enum('div_status',[0,1])->dafault(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('sop_divisions')) {
            Schema::dropIfExists('sop_divisions');
        }
    }
}
