<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ColomnAddUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            if (!Schema::hasColumn('users', 'div_id')) {
            $table->integer('div_id')->default(0);
            }
            if (!Schema::hasColumn('users', 'dept_id')) {
            $table->integer('dept_id')->default(0);
            }
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'div_id')){
            Schema::dropIfExists('div_id');
        }
        if (Schema::hasColumn('users', 'dept_id')){
            Schema::dropIfExists('dept_id');
        }
    }
}
