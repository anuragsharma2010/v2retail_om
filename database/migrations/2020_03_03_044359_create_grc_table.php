<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grc', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('pstng_date');
            $table->string('site',15);
            $table->string('article',30);
            $table->string('mvt',15);
            $table->integer('quantity');
            $table->decimal('amount_lc',20,2);
            $table->string('vendor',30);
            $table->string('article_doc',30);
            $table->integer('item');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grc');
    }
}
