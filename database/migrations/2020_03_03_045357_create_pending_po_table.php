<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendingPoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pending_po', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('po_date');
            $table->string('site', 15)->nullable();
            $table->date('doc_date');
            $table->date('del_date');
            $table->string('purchase_doc')->nullable();
            $table->string('vendor',100)->nullable();
            $table->string('article',100)->nullable();
            $table->string('pgr',15)->nullable();
            $table->integer('item')->nullable();
            $table->decimal('net_price', 10, 2)->nullable();
            $table->decimal('act_po_q', 20, 2)->nullable();
            $table->decimal('act_po_v', 20, 2)->nullable();
            $table->decimal('gr_qty', 20, 2)->nullable();
            $table->decimal('pend_po_q', 20, 2)->nullable();
            $table->decimal('pend_po_v', 20, 2)->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pending_po');
    }
}
