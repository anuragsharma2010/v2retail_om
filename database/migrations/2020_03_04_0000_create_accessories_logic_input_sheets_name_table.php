<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
class CreateAccessoriesLogicInputSheetsNameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('accessories_logic_input_sheets_names')) {
            Schema::create('accessories_logic_input_sheets_names', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('excel_sheet_name');
                $table->string('link_table_info');
                $table->timestamps();
            });
        }
         if (!Schema::hasTable('st_stock')) {

             DB::Statement('CREATE TABLE `st_stock` (
                `store_code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                `link_article_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                `0001_st_cl_stk_p_v` float default null,
                `0002_st_cl_stk_p_v` float default null,
                `0003_st_cl_stk_p_v` float default null,
                `0004_st_cl_stk_p_v` float default null,
                `0005_st_cl_stk_p_v` float default null,
                `0006_st_cl_stk_p_v` float default null,
                `0007_st_cl_stk_p_v` float default null,
                `0008_st_cl_stk_p_v` float default null,
                `0009_st_cl_stk_p_v` float default null,
                `0010_st_cl_stk_p_v` float default null,
                `st_stk_in_tr_v` float default null,
                `st_prd_stk_v` float default null,
                `0001_st_cl_stk_p_q` float default null,
                `0002_st_cl_stk_p_q` float default null,
                `0003_st_cl_stk_p_q` float default null,
                `0004_st_cl_stk_p_q` float default null,
                `0005_st_cl_stk_p_q` float default null,
                `0006_st_cl_stk_p_q` float default null,
                `0007_st_cl_stk_p_q` float default null,
                `0008_st_cl_stk_p_q` float default null,
                `0009_st_cl_stk_p_q` float default null,
                `00010_st_cl_stk_p_q` float default null,
                `st_stk_in_tr_q` float default null,
                `sto_value` float default null,
                `sto_quatity` float default null,
                `st_prd_stk_q` float default null
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;');
        }
            if (!Schema::hasTable('st_art_sale')) {

             DB::Statement('CREATE TABLE `st_art_sale` (
              `store_code` varchar(100) not null,
              `link_article_code` varchar(100) default null,
              `f_m_y` varchar(100) default null,
              `day number` int(11) default null,
              `sale_v` float default null,
              `sale_q` decimal(10,0) default null,
              `gm_v` decimal(10,0) default null
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;');
            }
             if (!Schema::hasTable('st_option')) {
               DB::Statement( 'CREATE TABLE `st_option` (
            `store_code` varchar(100) NOT NULL,
            `store_name` varchar(100) DEFAULT NULL,
            `sub_division` varchar(100) DEFAULT NULL,
            `major_category_description` varchar(100) DEFAULT NULL,
            `range_segment` varchar(100) DEFAULT NULL,
            `mvgr_matrix` varchar(100) DEFAULT NULL,
            `season` varchar(100) DEFAULT NULL,
            `wg_display_type` varchar(100) DEFAULT NULL,
            `fg_display_type` varchar(100) DEFAULT NULL,
            `segment_mvgr_package_size` varchar(100) DEFAULT NULL,
            `fg_fix_density` varchar(100) DEFAULT NULL,
            `stcd_majcat_desc` varchar(100) DEFAULT NULL,
            `stcd_majcat_desc_rng_mvgr_desc` varchar(100) DEFAULT NULL,
            `stcd_majcat_desc_rng_desc` varchar(100) DEFAULT NULL,
            `major_category_segment_option_m_1` int(11) DEFAULT NULL,
            `major_category_segment_option_m_2` int(11) DEFAULT NULL,
            `major_category_segment_option_m_3` int(11) DEFAULT NULL,
            `major_category_segment_option_m_4` int(11) DEFAULT NULL,
            `major_category_segment_option_m_5` int(11) DEFAULT NULL,
            `null` int(11) DEFAULT NULL,
            `major_category_segment_option_m_6` int(11) DEFAULT NULL,
            `major_category_segment_option_m_7` int(11) DEFAULT NULL,
            `major_category_segment_option_m_8` int(11) DEFAULT NULL,
            `major_category_segment_option_m_9` int(11) DEFAULT NULL,
            `major_category_segment_option_m_10` int(11) DEFAULT NULL,
            `major_category_segment_option_m_11` int(11) DEFAULT NULL,
            `major_category_segment_option_m_12` int(11) DEFAULT NULL,
            `major_category_segment_option_m_13` int(11) DEFAULT NULL,
            `major_category_segment_display_q_m_1` int(11) DEFAULT NULL,
            `major_category_segment_display_q_m_2` int(11) DEFAULT NULL,
            `major_category_segment_display_q_m_3` int(11) DEFAULT NULL,
            `major_category_segment_display_q_m_4` int(11) DEFAULT NULL,
            `major_category_segment_display_q_m_5` int(11) DEFAULT NULL,
            `major_category_segment_display_q_m_6` int(11) DEFAULT NULL,
            `major_category_segment_display_q_m_7` int(11) DEFAULT NULL,
            `major_category_segment_display_q_m_8` int(11) DEFAULT NULL,
            `major_category_segment_display_q_m_9` int(11) DEFAULT NULL,
            `major_category_segment_display_q_m_10` int(11) DEFAULT NULL,
            `major_category_segment_display_q_m_11` int(11) DEFAULT NULL,
            `major_category_segment_display_q_m_12` int(11) DEFAULT NULL,
            `major_category_segment_display_q_m_13` int(11) DEFAULT NULL,
            `major_category_segment_sales_q_m_1` int(11) DEFAULT NULL,
            `major_category_segment_sales_q_m_2` int(11) DEFAULT NULL,
            `major_category_segment_sales_q_m_3` int(11) DEFAULT NULL,
            `major_category_segment_sales_q_m_4` int(11) DEFAULT NULL,
            `major_category_segment_sales_q_m_5` int(11) DEFAULT NULL,
            `major_category_segment_sales_q_m_6` int(11) DEFAULT NULL,
            `major_category_segment_sales_q_m_7` int(11) DEFAULT NULL,
            `major_category_segment_sales_q_m_8` int(11) DEFAULT NULL,
            `major_category_segment_sales_q_m_9` int(11) DEFAULT NULL,
            `major_category_segment_sales_q_m_10` int(11) DEFAULT NULL,
            `major_category_segment_sales_q_m_11` int(11) DEFAULT NULL,
            `major_category_segment_sales_q_m_12` int(11) DEFAULT NULL,
            `major_category_segment_sales_q_m_13` int(11) DEFAULT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;');
             }
             
          if (!Schema::hasTable('dc_stk')) {
               DB::Statement('CREATE TABLE `dc_stk` (
                `link_article_code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
                 `st_to_dc_in_tr_stk_v` decimal(10,0) default null,
                `msa_v02_stk_v` varchar(100) collate utf8mb4_unicode_ci default null,
                `msa_v01_stk_v` decimal(10,0) default null,
                `msa_v04_a_stk_v` decimal(10,0) default null,
                `msa_v04_b_stk_v` decimal(10,0) default null,
                `msa_v11_stk_v` decimal(10,0) default null,
                `ssnl_0034_stk_v` decimal(10,0) default null,
                `new_grt_0032_stk_v` decimal(10,0) default null,
                `pack_0031_stk_v` decimal(10,0) default null,
                `old_grt_0033_stk_v` decimal(10,0) default null,
                `grt_0035_stk_v` decimal(10,0) default null,
                `st_to_dc_in_tr_stk_q` decimal(10,0) default null,
                `msa_v02_stk_q` decimal(10,0) default null,
                `msa_v01_stk_q` decimal(10,0) default null,
                `msa_v04_a_stk_q` decimal(10,0) default null,
                `msa_v04_b_stk_q` decimal(10,0) default null,
                `msa_v11_stk_q` decimal(10,0) default null,
                `ssnl_0034_stk_q` decimal(10,0) default null,
                `new_grt_0032_stk_q` decimal(10,0) default null,
                `pack_0031_stk_q` decimal(10,0) default null,
                `old_grt_0033_stk_q` decimal(10,0) default null,
                `grt_0035_stk_q` decimal(10,0) default null,
                `cm_pend_po_q` decimal(10,0) default null,
                `cm_1_pend_po_q` decimal(10,0) default null,
                `cm_2_pend_po_q` decimal(10,0) default null,
                `cm_3_pend_po_q` decimal(10,0) default null,
                `cm_pend_po_v` decimal(10,0) default null,
                `cm_1_pend_po_v` decimal(10,0) default null,
                `cm_2_pend_po_v` decimal(10,0) default null,
                `cm_3_pend_po_v` decimal(10,0) default null
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');
               
        }
        if (!Schema::hasTable('st_art_sale')) {
               DB::Statement(
                    'CREATE TABLE `st_art_sale` (
              `store_code` varchar(100) NOT NULL,
              `link_article_code` varchar(100) DEFAULT NULL,
              `f_m_y` varchar(100) DEFAULT NULL,
              `day_number` int(11) DEFAULT NULL,
              `sale_v` float DEFAULT NULL,
              `sale_q` decimal(10,0) DEFAULT NULL,
              `gm_v` decimal(10,0) DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;');
        }
        
        if (!Schema::hasTable('bgt_sale')) {
               DB::Statement('CREATE TABLE `bgt_sale` (
                                `st_code` varchar(100) default null,
                  `st_name` varchar(250) default null,
                  `segment` varchar(100) default null,
                  `division` varchar(250) default null,
                  `sub_division` varchar(250) default null,
                  `maj_cat` varchar(250) default null,
                  `st_cd_maj_cat_rng_desc` varchar(100) default null,
                  `st_cd_maj_cat_rng_mvgr_desc` varchar(250) default null,
                  `ssn` varchar(100) default null,
                  `mvgr_matrix` varchar(100) default null,
                  `rng` varchar(100) default null,
                  `m1_q` decimal(10,0) default null,
                  `m2_q` decimal(10,0) default null,
                  `m3_q` decimal(10,0) default null,
                  `m4_q` decimal(10,0) default null,
                  `m5_q` decimal(10,0) default null,
                  `m6_q` decimal(10,0) default null,
                  `m7_q` decimal(10,0) default null,
                  `m8_q` decimal(10,0) default null,
                  `m9_q` decimal(10,0) default null,
                  `m10_q` decimal(10,0) default null,
                  `m11_q` decimal(10,0) default null,
                  `m12_q` decimal(10,0) default null,
                  `m13_q` decimal(10,0) default null,
                  `m1_v` decimal(10,0) default null,
                  `m2_v` decimal(10,0) default null,
                  `m3_v` decimal(10,0) default null,
                  `m4_v` decimal(10,0) default null,
                  `m5_v` decimal(10,0) default null,
                  `m6_v` decimal(10,0) default null,
                  `m7_v` decimal(10,0) default null,
                  `m8_v` decimal(10,0) default null,
                  `m9_v` decimal(10,0) default null,
                  `m10_v` decimal(10,0) default null,
                  `m11_v` decimal(10,0) default null,
                  `m12_v` decimal(10,0) default null,
                  `m13_v` decimal(10,0) default null
                              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;');
        }
         if (!Schema::hasTable('bgt_revised_sale')) {
               DB::Statement('CREATE TABLE `bgt_revised_sale` (
                `st_code` varchar(100) default null,
                `st_name` varchar(250) default null,
                `segment` varchar(100) default null,
                `division` varchar(250) default null,
                `sub_division` varchar(250) default null,
                `maj_cat` varchar(250) default null,
                `st_cd_maj_cat_rng_desc` varchar(100) default null,
                `st_cd_maj_cat_rng_mvgr_desc` varchar(250) default null,
                `ssn` varchar(100) default null,
                `mvgr_matrix` varchar(100) default null,
                `rng` varchar(100) default null,
                `m1_q` decimal(10,0) default null,
                `m2_q` decimal(10,0) default null,
                `m3_q` decimal(10,0) default null,
                `m4_q` decimal(10,0) default null,
                `m5_q` decimal(10,0) default null,
                `m6_q` decimal(10,0) default null,
                `m7_q` decimal(10,0) default null,
                `m8_q` decimal(10,0) default null,
                `m9_q` decimal(10,0) default null,
                `m10_q` decimal(10,0) default null,
                `m11_q` decimal(10,0) default null,
                `m12_q` decimal(10,0) default null,
                `m13_q` decimal(10,0) default null,
                `m1_v` decimal(10,0) default null,
                `m2_v` decimal(10,0) default null,
                `m3_v` decimal(10,0) default null,
                `m4_v` decimal(10,0) default null,
                `m5_v` decimal(10,0) default null,
                `m6_v` decimal(10,0) default null,
                `m7_v` decimal(10,0) default null,
                `m8_v` decimal(10,0) default null,
                `m9_v` decimal(10,0) default null,
                `m10_v` decimal(10,0) default null,
                `m11_v` decimal(10,0) default null,
                `m12_v` decimal(10,0) default null,
                `m13_v` decimal(10,0) default null
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;');
         }
        if (!Schema::hasTable('bgi_disp')) {
              DB::Statement('CREATE TABLE `bgi_disp` (
                `st_code` varchar(100) default null,
                `st_name` varchar(250) default null,
                `segment` varchar(100) default null,
                `division` varchar(250) default null,
                `sub_division` varchar(250) default null,
                `maj_cat` varchar(250) default null,
                `st_cd_maj_cat_rng_desc` varchar(100) default null,
                `st_cd_maj_cat_rng_mvgr_desc` varchar(250) default null,
                `ssn` varchar(100) default null,
                `mvgr_matrix` varchar(100) default null,
                `rng` varchar(100) default null,
                `m1_q` decimal(10,0) default null,
                `m2_q` decimal(10,0) default null,
                `m3_q` decimal(10,0) default null,
                `m4_q` decimal(10,0) default null,
                `m5_q` decimal(10,0) default null,
                `m6_q` decimal(10,0) default null,
                `m7_q` decimal(10,0) default null,
                `m8_q` decimal(10,0) default null,
                `m9_q` decimal(10,0) default null,
                `m10_q` decimal(10,0) default null,
                `m11_q` decimal(10,0) default null,
                `m12_q` decimal(10,0) default null,
                `m13_q` decimal(10,0) default null,
                `m1_v` decimal(10,0) default null,
                `m2_v` decimal(10,0) default null,
                `m3_v` decimal(10,0) default null,
                `m4_v` decimal(10,0) default null,
                `m5_v` decimal(10,0) default null,
                `m6_v` decimal(10,0) default null,
                `m7_v` decimal(10,0) default null,
                `m8_v` decimal(10,0) default null,
                `m9_v` decimal(10,0) default null,
                `m10_v` decimal(10,0) default null,
                `m11_v` decimal(10,0) default null,
                `m12_v` decimal(10,0) default null,
                `m13_v` decimal(10,0) default null
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1;');
           }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('accessories_logic_input_sheets_names')) {
            Schema::dropIfExists('accessories_logic_input_sheets_names');
        }
        if (Schema::hasTable('st_stock')) {
            Schema::dropIfExists('st_stock');
        }
          if (Schema::hasTable('st_art_sale')) {
            Schema::dropIfExists('st_art_sale');
        }
        if (Schema::hasTable('dc_stk')) {
            Schema::dropIfExists('dc_stk');
        }
        if (Schema::hasTable('st_art_sale')) {
            Schema::dropIfExists('st_art_sale');
        }
        //bgt_sale
        if (Schema::hasTable('bgt_sale')) {
            Schema::dropIfExists('bgt_sale');
        }
        //bgt_revised_sale
        if (Schema::hasTable('bgt_revised_sale')) {
            Schema::dropIfExists('bgt_revised_sale');
        }
        //bgi_disp
         if (Schema::hasTable('bgi_disp')) {
            Schema::dropIfExists('bgi_disp');
        }
    }
}
