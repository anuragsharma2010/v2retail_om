<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ColomnAddRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            if (!Schema::hasColumn('roles', 'div_id')) {
            $table->integer('div_id');
            }
            if (!Schema::hasColumn('roles', 'dept_id')) {
            $table->integer('dept_id');
            }
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('roles', 'div_id')){
            Schema::dropIfExists('div_id');
        }
        if (Schema::hasColumn('roles', 'dept_id')){
            Schema::dropIfExists('dept_id');
        }
    }
}
