@extends('layouts.app')
@section('style')
<link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1>GRC List</h1>
   <ol class="breadcrumb">
      <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
      <li class="active"> GRC List</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="container-fluid">
   <div class="card">
      <div class="card-header">
         <div class="col-sm-12 list_filter_head">
            <div class="search_box">
               <!-- search form -->
               <form action="#" method="get" class="sidebar-form">
                  <div class="form-group">
                     <div class="input-group">
                        <div class="input-group-prepend">
                           <span class="input-group-text">
                           <i class="far fa-calendar-alt"></i>
                           </span>
                        </div>
                        <input type="text" name="month_year" id="month_year" class="form-control dateinput form-control-2 to" placeholder="Select month and year" value="{{ app('request')->input('month')??\Carbon\Carbon::now()->format('m-Y') }}">
                     </div>
                  </div>
               </form>
            </div>
            <div class="userbtn">
               <a href="{{route('grc.import.template')}}" class="btn btn-sm btn-primary createuser-btn pt-0 mx-10"><i class="fa fa-download"></i>Download Template</a>
               <a href="{{route('grc.import')}}" class="btn btn-sm btn-primary createuser-btn pt-0"><i class="fa fa-upload"></i> Upload</a>
            </div>
         </div>
         <!-- /.col -->
      </div>
      <!-- /.card-header -->
      <div class="card-body">
         @if ($message = Session::get('success'))
         <div class="message" role="alert">
            <div class="alert alert-success alert-dismissible">
               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
         </div>
         @endif
         <div class="container-fluid p-0 show-enteries-parent">
            <div class="col-md-6 p-0"><label class="show-enteries-text">Show enteries
               {!! Form::select('per_page', [10=>10,50=>50,150=>150,500=>500], app('request')->input('perpage'), ['class'=>'item-per-page','id'=>'item-show-per-page']) !!}
               </label>
            </div>
            <div class="col-md-6 p-0">
               <p class="showing-enteries-text text-right">Showing 1 to 10 of 57 entries</p>
            </div>
         </div>
         <div class="table_data table-responsive">
            <table class="table table-bordered table-striped w-2400">
               <thead>
                  <tr>
                     <th>PSTNG DATE</th>
                     <th>SITE</th>
                     <th>ARTICLE</th>
                     <th>SEG</th>
                     <th>DIV</th>
                     <th>SUB DIV</th>
                     <th>MAJ CAT</th>
                     <th>RNG SEG</th>
                     <th>MVGR</th>
                     <th>MVT</th>
                     <th>QUANTITY</th>
                     <th>AMT IN LC</th>
                     <th>VENDOR SPP CD</th>
                     <th>VENDOR SPP NAME</th>
                     <th>ART DOC NO</th>
                     <th>ITEM</th>
                  </tr>
               </thead>
               <tbody>
                  @forelse($grc as $g)
                  <tr>
                     <td>{{ \Carbon\Carbon::parse($g->pstng_date)->format('d-m-Y') }}</td>
                     <td>{{ $g->site }}</td>
                     <td>{{ $g->article }}</td>
                     <td>{{ optional($g->articledesc)->mvgr_seg }}</td>
                     <td>{{ optional($g->articledesc)->division }}</td>
                     <td>{{ optional($g->articledesc)->subdivision }}</td>
                     <td>{{ optional($g->articledesc)->new_major_category }}</td>
                     <td>{{ optional($g->articledesc)->range_segment }}</td>
                     <td>{{ optional($g->articledesc)->mvgr_matrix }}</td>
                     <td>{{ $g->mvt }}</td>
                     <td>{{ $g->quantity }}</td>
                     <td>{{ $g->amount_lc }}</td>
                     <td>{{ $g->vendor }}</td>
                     <td></td>
                     <td>{{ $g->article_doc }}</td>
                     <td>{{ $g->item }}</td>
                  </tr>
                  @empty
                  <tr>
                     <td colspan="16" style="text-align:left;">No data found</td>
                  </tr>
                  @endforelse
               </tbody>
            </table>
         </div>
         <!-- /.card-body -->
         <div class="card-footer">
            <div class="col-md-12 text-right pagination">
               {{$grc->appends(['month' =>  app('request')->input('month'), 'perpage' => app('request')->input('perpage') ])->links()}}
            </div>
         </div>
      </div>
      <!-- /.card -->
   </div>
</section>
@endsection
@section('custom-js')
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script>
   $(document).ready(function () {
       $('.to').datepicker({
           autoclose: true,
           minViewMode: 1,
           format: 'mm-yyyy',
           default: true
       })
       $('#month_year').on("change",function(){
           var monthYear = $(this).val();
           var perPage = $('#item-show-per-page').val();
           var url = APP_URL+"/{{Request::segment(1)}}?month="+monthYear+"&perpage="+perPage;
           window.location.href = url;
       });
   
       $('#item-show-per-page').on('change', function () {
           var monthYear = $('#month_year').val();
           var perPage = $(this).val();
           var url = APP_URL+"/{{Request::segment(1)}}?month="+monthYear+"&perpage="+perPage;
           window.location.href = url;
       });
   });
</script>
@endsection