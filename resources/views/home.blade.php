@extends('layouts.app')

@section('style')
<link href="{{ asset('bower_components/chart.js/Chart.min.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('css/yearpicker.css') }}" rel="stylesheet"/> -->

@endsection

@section('content')
<section class="content-header">
    <h1>
        Dashboard
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div id="maindashboard" class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-shopping-cart clr-aqua"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-number clr-aqua"><?php echo $data->total_stores; ?></span>
                        <span class="info-box-text">Total Store</span>
                    </div>
                </div>
            </div>
                    <!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-red"><i class="fa fa-rocket clr-red"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-number clr-red"><?php echo $data->total_processes; ?></span>
                                <span class="info-box-text">Total Process</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->

                    <!-- fix for small devices only -->
                    <div class="clearfix visible-sm-block"></div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-green"><i class="fa fa-group clr-green"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-number clr-green"><?php echo $data->total_groups; ?></span>
                                <span class="info-box-text">Store Group</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-yellow"><i class="fa fa-bar-chart clr-yellow"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-number clr-yellow"><?php //echo $data->total_efficience; ?>54<small>%</small></span>
                                <span class="info-box-text">Overall Efficiency(%) test</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!---  first row --->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Store Status(Process Wise)</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                            class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
<!--                                <div class="d-flex justify_content_between py-1">
                                    <p class="d-flex flex-column">
                                        <span class="text-bold text-lg">$18,230.00</span>
                                        <span></span>
                                    </p>
                                    <p class="ml-auto d-flex flex-column text-right">
                                        <span class="text-success">
                                            <i class="fas fa-arrow-up"></i> 33.1%
                                        </span>
                                        <span class="text-muted">Since last month</span>
                                    </p>
                                </div>-->
                                <div class="chart" style="height:380px;">
                                    <canvas id="sales-chart" height="200"></canvas>
                                </div>
                                <div class="d-flex flex-row justify-content-end py-1">
                                    <span class="mr-2">
                                        <i class="fas fa-square  text-primary"></i> Completed
                                    </span>

                                    <span>
                                        <i class="fas fa-square  text-danger"></i> Pending
                                    </span>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->

                <div class="row">
                    <div class="col-md-6">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Top 5 Efficient Store</h3>

                                  <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fas fa-minus"></i>
                                    </button>
                                   <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <canvas id="EfficientChart"></canvas>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Top 5 Inefficient Store</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fas fa-minus"></i>
                                    </button>
                                   <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body">
                                <canvas id="InefficientChart"></canvas>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div>
</section>
@endsection


@section('custom-js')
<!--var dataProcess=-->
<script src="{{ asset('chartjs/Chart.min.js') }}"></script>
<!-- <script src="{{ asset('js/dashboard2.js') }}"></script> -->
<script src="{{ asset('js/dashboard3.js')}}"></script>

<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $("li.active").parent().addClass('currentTab');
        $('ul.currentTab').siblings().removeClass('currentTab');
        $('.delete_btn').click(function () {
            if (confirm("Are you sure?")) {
                return true;
            }
            return false;
        });
    });

    var ctx = document.getElementById("EfficientChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
                    //labels: ["M", "T", "W", "T", "F", "S", "S"],
                        labels: <?php echo json_encode($data->st_lables); ?>,
                        datasets: [{
                                backgroundColor: [
                                    "#2ecc71",
                                    "#3498db",
                                    "#95a5a6",
                                    "#9b59b6",
                                    "#f1c40f"
                                ],
                        data: <?php echo json_encode($data->processCount); ?>
                            }]
            }
    });
    var ctx = document.getElementById("InefficientChart").getContext('2d');
    var labelsCount = <?php echo json_encode($data->st_lables); ?>;
    var colors = [];
  
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: <?php echo json_encode($data->st_lables); ?>,
            datasets: [{
                backgroundColor:[
                                    "#2ecc71",
                                    "#3498db",
                                    "#95a5a6",
                                    "#9b59b6",
                                    "#f1c40f"
                                 ],
                data: <?php echo json_encode($data->processCount); ?>
            }]
        }
    });
var proces_list=[]; 
var Proces_store_count=[];
var Proces_store_count=[];
var completePer=[];
var incompletePer=[];
<?php 
foreach ($forms as $val)
{
    ?>
       proces_list.push("<?php echo $val->form_label; ?>");
       Proces_store_count.push("<?php echo count($val->stores); ?>");
    <?php
    foreach($val->stores as $valStore)
    {
        
        ?>
            completePer.push("<?php echo $valStore->percentage;?>");
            incompletePer.push(<?php echo 100-$valStore->percentage;  ?>);
        <?php
    }
}
?>

plotGraph(proces_list,Proces_store_count,completePer,incompletePer);
</script>
@endsection
