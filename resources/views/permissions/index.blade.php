
@extends('layouts.app')


@section('content')
  <section class="content-header">
      <h1>
      Permission Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"> Permission Management</li>
      </ol>
    </section>
<!-- Content Header (Page header) -->
   
    <!-- /.content-header -->


    <section class="content">
       <div class="container-fluid">
             <div class="card listcontainer">
             <div class="card-header">
             <div class="col-sm-12 list_filter_head">
             <div class="search_box">
                 <!-- search form -->
                <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control search" placeholder="Search...">
                        <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                          <i class="fa fa-search"></i>
                        </button>
                      </span>
                    </div>
                </form>
              </div>
              <div class="userbtn">
                    @can('permission-create')
              <a href="{{ route('permissions.create') }}" class="btn btn-sm btn-primary createuser-btn">
                      <i class="fa fa-plus-circle"></i>Add Permission
              </a>
                 @endcan
                  <select id="item-show-per-page" class="item-per-page btn">
          <option value="10">10</option>
          <option value="50">50</option>
          <option value="150">150</option>
          <option value="500">500</option>
          </select>         


            </div>

           
             
              
        
          </div><!-- /.col -->


            </div>
            <!-- /.card-header -->
            <div class="card-body list_body">

              <div class="message" role="alert">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
              </div>
              <div class="table_data">
              <table class="table table-stripd">
                <thead>
                <tr>
                <th>No</th>
               <th>Name</th>
               <th>Guard</th>
               <th >Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($permission as $key => $perm)
                  <tr>
                      <td>{{ ++$i }}</td>
                      <td>{{ $perm->name }}</td>
                      <td>{{ $perm->guard_name }}</td>
                      <td>
                          @can('permission-edit')
                              <a class="btn btn-primary btn_dis" href="{{ route('permissions.edit',$perm->id) }}" data-toggle="tooltip" title="Edit!"><i class="fa fa-edit"></i></a>
                          @endcan
                          @can('permission-delete')
                              {!! Form::open(['method' => 'DELETE','route' => ['permissions.destroy', $perm->id],'style'=>'display:inline']) !!}
                                  {!! Form::button('<i class="fa fa-archive"></i>', ['class' => 'btn btn-danger btn_dis']) !!}
                              {!! Form::close() !!}
                          @endcan
                      </td>
                  </tr>
                 @endforeach             
            </tbody>
              </table>
            </div>
            </div>
            <div class="card-footer">
            <div class="col-md-12 text-right pagination">
            {!! $permission->render() !!} 
            </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
      </div>
    </section>

@endsection