@extends('layouts.app')


@section('content')
<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0 text-dark">Add New Permission | <sub><a href="{{ route('permissions.index') }}">Back</a></h1>
          </div><!-- /.col -->
        
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid roleform">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <!-- /.card-header -->
          <div class="card-body">
             <div class="form_container">
             @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
          

    <form action="{{ route('permissions.store') }}" method="POST" class="form-horizontal">
        @csrf

             <div class="clearfix">
                <div class="form-group">
                 <label for="inputEmail3" class="col-sm-2 col-form-label label">Name</label>
                    <div class="col-sm-10"> <input type="text" name="name" class="form-control" placeholder="Name">
                 </div>
                </div>
                 <div class="form-group">
                 <label for="inputEmail3" class="col-sm-2 col-form-label label">Detail</label>
                    <div class="col-sm-10"> <input type="text" class="form-control"  name="guard_name" placeholder="Guard" value="web">
                </div>
            </div>
            <div class="col-md-12">
                  <button type="submit" class="btn btn-primary btn-submit">Submit</button>
             </div>
            </div>
                </form>
        </div>
        </div>
    </div>
</div>
</section>
   @endsection


