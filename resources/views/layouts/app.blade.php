<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'V2 Retail') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <!-- Styles -->
    <!--<link href="{{ asset('app.css') }}" rel="stylesheet">-->
    <link href="{{ asset('css/_all-skins.css') }}" rel="stylesheet">
    <link href="{{ asset('css/AdminLTE.css') }}" rel="stylesheet">

    <link href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet">

    <!--<link rel="stylesheet" href="/css/adminlte.min.css"> -->
    <link href="{{ asset('/css/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <link href="{{ asset('/css/custom.css')}}" rel="stylesheet">
    <link href="{{ asset('/css/new-style.css') }}" rel="stylesheet" />
    @yield('style')
</head>

<body class="hold-transition skin-blue sidebar-mini wysihtml5-supported">
    <style type="text/css">
        .error_field{
            color:red;
            padding-left: 10px;
        }
    </style>
    <section>
        <div class="wrapper">
            <div>
                @include('partials.leftPanel')
            </div>
            <div>
                @include('partials.headerBar')
            </div>
            <div>
                <div class="content-wrapper">
                    @yield('content')
                </div>
            </div>
            <div>
                @include('partials.footer')
            </div>
        </div>
    </section>

    <!-- JS Files-->
    <script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('bower_components/raphael/raphael.min.js') }}"></script>
    <!--<script src="{{ asset('bower_components/morris.js/morris.min.js') }}"></script>-->

    <script src="{{ asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
    <!--<script src="{{ asset('js/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('js/jquery-jvectormap-world-mill-en.js') }}"></script>-->
    <script src="{{ asset('bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
    <script src="{{ asset('bower_components/moment/min/moment.min.js') }}"></script>

    <script src="{{ asset('js/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
    <script src="{{ asset('js/adminlte.min.js') }}"></script>
    <!--<script src="{{ asset('js/app.js') }}"></script>-->
    <!-- <script src="{{asset('js/piechart.js')}}"></script> -->
    <script src="{{ asset('js/demo.js') }}"></script>
    <!-- Scripts -->
    <script src="{{ asset('js/custom.js') }}"></script>
    <script>
    var APP_URL = {!! json_encode(url('/')) !!}
    </script>
    @yield('custom-js')
</body>

</html>
