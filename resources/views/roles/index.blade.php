@extends('layouts.app')


@section('content')
<!-- Content Header (Page header) -->
  <section class="content-header">
      <h1>
        Role Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Role Management</li>
      </ol>
  </section>
    <!-- /.content-header -->
     <section class="content">
       <div class="container-fluid">
             <div class="card listcontainer">
              <div class="card-header">
                <div class="col-sm-12 list_filter_head">
             <div class="search_box">
                 <!-- search form -->
                <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="searchtxt" id="searchtxt" value="<?php echo isset($roles->searchtxt) ? $roles->searchtxt:'';  ?>" class="form-control search" placeholder="Search...">
                        <span class="input-group-btn">
                        <button type="submit" name="search" value="save" id="search-btn" class="btn btn-flat">
                          <i class="fa fa-search"></i>
                        </button>
                      </span>
                    </div>
                </form>
              </div>
              <div class="userbtn">
                 @can('role-create')
              <a href="{{ route('roles.create') }}" class="btn btn-sm btn-primary createuser-btn">
                      <i class="fa fa-plus-circle"></i>  Add Role
              </a>
                @endcan
            </div>
          </div><!-- /.col -->
              </div>
            <!-- /.card-header -->
            <div class="card-body">
                 
               @if ($message = Session::get('success'))
                <div class="message" role="alert">
                    <div class="alert alert-success alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <p>{{ $message }}</p>
                    </div>
                  </div>
                @endif
                <div class="container-fluid p-0 show-enteries-parent">

<div class="col-md-6 p-0"><label class="show-enteries-text">Show enteries
<select id="item-show-per-page-select" class="item-show-per-page-select-id">
<option value="10">10</option>
<option value="50">50</option>
<option value="150">150</option>
<option value="500">500</option>
</select></label>

</div>
<div class="col-md-6 p-0">
    <p class="showing-enteries-text text-right">Showing 1 to 10 of 57 entries</p>
    

</div>
</div>

              <div class="table_data">
              <table class="table table-stripd table-bordered">
                <thead>
                <tr>
                 <th>No</th>
                  <th>Name</th>
                  <th>Division</th>
                  <th>Department</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 @foreach ($roles as $key => $role)
                  <tr>
                    <td>{{ ++$i }}</td>
                        <td>{{ $role->name }}</td>
                        <td>{{ $role->div_name }}</td>
                        <td>{{ $role->dept_name }}</td>
                        <td>
                            @can('role-edit')

                                <a class="btn btn-primary btn_dis" href="{{ route('roles.edit',$role->id) }}" data-toggle="tooltip" title="Edit!"><i class="fa fa-edit"></i></a>
                            @endcan
                            @can('role-delete')
                                {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                                <a  href="#"  data-toggle="tooltip" title="delete!"> <button type="submit" class="btn btn-danger btn_dis delete_btn"><i class="fa fa-archive"></i></button></a>
                                {!! Form::close() !!}
                            @endcan
                        </td>
                  </tr>

                 @endforeach
                </tfoot>
              </table>

            </tbody>
            <div class="col-md-12 text-right pagination" style="">
             {!! $roles->render() !!}
            </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
      </div>
    </section>
@endsection
