@extends('layouts.app')
<?php 

 $div_id=isset($role->div_id) ? $role->div_id :'';
 $dept_id=isset($role->dept_id) ? $role->dept_id :'';
 //$department= isset($department) ? $department ;
?>
@section('content')
 <section class="content-header">
      <h1>
      Edit Role
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ route('roles.index') }}"> Role Management</a></li>
        <li class="active"> Edit Role</li>
      </ol>
</section>
    <!-- /.content-header -->

<!-- Main content -->
    <section class="content">
      <div class="container-fluid roleform">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <!-- /.card-header -->
          <div class="card-body">
             <div class="form_container">
      
          {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!}
             <div class="clearfix">
               <div class="col-md-6 right_pdng">
                   <div class="form-group">
                       <label >Division Name:</label>
                       <select name="div_id" id="div_id" onchange="getDeptInfo(this.value,'<?php echo url("/roles"); ?>')" class="form-control customtinput">
                           <option value=""> Select Division</option>
                           <?php foreach ($divisions as $key => $value) { ?>
                           <option value="<?php echo $value->id; ?>" <?php if($div_id == $value->id){ ?>selected=""<?php }  ?>><?php echo $value->div_name; ?></option>
                           <?php } ?>
                       </select>
                    @if ($errors->has('div_id'))
                        <div class="error_field" >{{ $errors->first('div_id') }}</div>
                    @enderror
                   </div>
                   <!-- /.form-group -->
                   <!-- /.form-group -->
               </div>
               <div class="col-md-6 left_pdng">
                   <div class="form-group">
                       <label >Department Name:</label>
                       <select name="dept_id" id="dept_id"  class="form-control customtinput">
                           <option value=""> Select</option>
                             <?php  echo $stoption; ?>
                       </select>
                    @if ($errors->has('dept_id'))
                        <div class="error_field" >{{ $errors->first('dept_id') }}</div>
                    @enderror
                   </div>
                   <!-- /.form-group -->
                   <!-- /.form-group -->
               </div>
               <div class="col-md-6 right_pdng">
                <div class="form-group">
                 <label for="inputEmail3" class="label">Name</label>
                 {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                 </div>
                </div>

            <div class="col-md-12">
               <div class="form-group">
                   <fieldset style="display: none;">
              <legend>Permission:</legend>
              @foreach($permission as $value)
                <div class="col-md-4">
                  <div class="icheck-primary">
                    {{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name', 'id' => 'permission['.$value->id.']')) }}<label for="permission[{{$value->id}}]"> {{ $value->name }}</label>
                </div>
            </div>

           @endforeach
             </fieldset>

              </div>
             </div>
                            <div class="row" >
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                                
                            </div>
                            <div class="col-md-6 right">
                                <button type="button" class="btn btn-primary btn-submit" onclick="javascript:location.replace('{{ route('roles.index') }}')">Back</button>
                            </div>
                            </div>
            </div>
          {!! Form::close() !!}
        </div>
        </div>
        </div>
    </div>
</section>
<script type="text/javascript" >
          
     
    function getDeptInfo(id,url){
       // console.log(url+"/"+id+"?flage_ajax=1");

       $.ajax({url: url+"/"+id+"?flage_ajax=1", success: function(result){
    $("#dept_id").html(result);
    }});
    }
   
    
   
</script>

@endsection
