<style>
    .treeview .fa.fa-angle-right:before{
         -webkit-transition: transform .5s ease;
    -o-transition: transform .5s ease;
    transition: transform .5s ease;
    }
    .treeview.menu-open .fa.fa-angle-right:before{
     content:"\f107";
    }
   
</style>
<!--class="{{ Route::currentRouteNamed( 'home' ) ?  'active' : '' }}"   -->
<aside class="main-sidebar" >
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <!--li class="header">MAIN NAVIGATION</li-->
          

            <li class="treeview {{ (Request::segment(1) === 'forms' || Request::segment(1) === 'sop') ? 'active' : '' }}">
                <a href="{{ route('sopDashboard') }}" class="nav-link">
                    <i class="fa fa-sitemap"></i>
                    <span>V2 Sop Compliance</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                      <li class="{{ (Request::segment(1) === 'home') ? 'active' : '' }}">
                <a href="{{route('home')}}">
                   <i class="fa fa-home mr-8"></i> <span>Dashboard</span>
                </a>
            </li>
                    <li  class="{{ Route::currentRouteNamed( 'sopDashboard' ) ?  'active' : '' }}"><a href="{{ route('sopDashboard') }}"><i class="fa fa-circle"></i> V2 KPI Index</a></li>
                    <li  class="{{ Route::currentRouteNamed( 'forms.index' ) ?  'active' : '' }}"><a href="{{ route('forms.index') }}"><i class="fa fa-circle"></i>Process</a></li>
                </ul>
            </li>

            <li class="treeview {{ (Request::segment(1) === 'master') ? 'active' : '' }}">
                <a href="#" class="nav-link">
                    <i class="fa fa-hourglass"></i>
                    <span>Masters</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>

                <ul class="treeview-menu">
                     <li class="{{ Route::currentRouteNamed( 'stores.index' ) ?  'active' : '' }}"><a href="{{ route('stores.index') }}"><i class="fa fa-circle"></i>Stores</a></li>
                     <li class="{{ Route::currentRouteNamed( 'groups.index' ) ?  'active' : '' }}"><a href="{{ route('groups.index') }}" ><i class="fa fa-circle"></i>Store Groups</a></li>
                     <!--
                   <li class="{{ Route::currentRouteNamed( 'articles.index' ) ?  'active' : '' }}"><a href="{{ route('articles.index') }}"><i class="fa fa-circle"></i>Articles</a></li>
                   <li class="{{ Route::currentRouteNamed( 'distributioncenters.index' ) ?  'active' : '' }}"><a href="{{ route('distributioncenters.index') }}"><i class="fa fa-circle"></i>Distribution Centers</a></li>
                  
                   <li class="{{ Route::currentRouteNamed( 'accessories-utility.index' ) ?  'active' : '' }}"><a href="{{ route('accessories-utility.index') }}"><i class="fa fa-circle"></i>Store Utility</a></li>
                   <li class="{{ Route::currentRouteNamed( 'storefloors.index' ) ?  'active' : '' }}"><a href="{{ route('storefloors.index') }}"><i class="fa fa-circle"></i>Store Floors</a></li>
                   <li class="{{ Route::currentRouteNamed( 'storetypes.index' ) ?  'active' : '' }}"><a href="{{ route('storetypes.index') }}"><i class="fa fa-circle"></i>Store Types</a></li>
                   
                    <li class="{{ Route::currentRouteNamed( 'categories.index' ) ?  'active' : '' }}"><a href="{{ route('categories.index') }}" ><i class="fa fa-circle"></i>Categories</a></li>
                    <li class="{{ Route::currentRouteNamed( 'subcategories.index' ) ?  'active' : '' }}"><a href="{{ route('subcategories.index') }}" ><i class="fa fa-circle"></i>Sub-Categories</a></li>

                    @can('segment-list')
                    <li class="{{ Route::currentRouteNamed( 'segments.index' ) ?  'active' : '' }}"><a href="{{ route('segments.index') }}"><i class="fa fa-circle"></i>Segments</a></li>
                    @endcan
                    <li class="{{ Route::currentRouteNamed( 'divisions.index' ) ?  'active' : '' }}"><a href="{{ route('divisions.index') }}"><i class="fa fa-circle"></i>Divisions</a></li>
                    <li class="{{ Route::currentRouteNamed( 'subdivisions.index' ) ?  'active' : '' }}"><a href="{{ route('subdivisions.index') }}"><i class="fa fa-circle"></i>Sub-Divisions</a></li>
                    <li class="{{ Route::currentRouteNamed( 'seasons.index' ) ?  'active' : '' }}"><a href="{{ route('seasons.index') }}"><i class="fa fa-circle"></i>Seasons</a></li>

                    <li class="{{ Route::currentRouteNamed( 'zones.index' ) ?  'active' : '' }}"><a href="{{ route('zones.index') }}" ><i class="fa fa-circle"></i>Zones</a></li>
                    <li class="{{ Route::currentRouteNamed( 'areas.index' ) ?  'active' : '' }}"><a href="{{ route('areas.index') }}" ><i class="fa fa-circle"></i>Areas</a></li>
                    <li class="{{ Route::currentRouteNamed( 'regions.index' ) ?  'active' : '' }}"><a href="{{ route('regions.index') }}" ><i class="fa fa-circle"></i>Regions</a></li>
                    <li class="{{ Route::currentRouteNamed( 'ranges.index' ) ?  'active' : '' }}"><a href="{{ route('ranges.index') }}" ><i class="fa fa-circle"></i>Ranges</a></li>
                    <li class="{{ Route::currentRouteNamed( 'states.index' ) ?  'active' : '' }}"><a href="{{ route('states.index') }}" ><i class="fa fa-circle"></i>States</a></li>-->
                </ul>

            </li>
<!--
            <li class="{{ (Request::segment(1) === 'zonning') ? 'active' : '' }}">
                <a href="{{url('zonning')}}">
                   <i class="fa fa-globe mr-8"></i> <span>Zonning</span>
                </a>
            </li>

            <li class="treeview hide">
                <a href="#" class="nav-link">
                    <i class="fa fa-briefcase"></i>
                    <span>Departments</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle"></i>Menu1</a></li>
                </ul>
            </li>
            <li class="treeview hide">
                <a href="#" class="nav-link">
                    <i class="fa fa-globe"></i>
                    <span>Zoning</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle"></i>Menu1</a></li>
                </ul>
            </li>

            <li class="treeview hide">
                <a href="#" class="nav-link">
                    <i class="fa fa-cube"></i>
                    <span>Space Plan</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle"></i>ST-FLR</a></li>
                    <li><a href="#"><i class="fa fa-circle"></i>SUB-DIV</a></li>
                </ul>
            </li>

            <li class="treeview hide">
                <a href="#" class="nav-link">
                    <i class="fa fa-map"></i>
                    <span>ACC Maping</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle"></i>Menu1</a></li>
                </ul>
            </li>

            <li class="treeview hide">
                <a href="#" class="nav-link">
                    <i class="fa fa-picture-o"></i>
                    <span>Display Plan</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle"></i>Menu1</a></li>
                </ul>
            </li>

            <li class="treeview hide">
                <a href="#" class="nav-link">
                    <i class="fa fa-book"></i>
                    <span>Sales Plan</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle"></i>Menu1</a></li>
                </ul>
            </li>

            <li class="treeview hide">
                <a href="#" class="nav-link">
                    <i class="fa fa-money"></i>
                    <span>Stock Fill Rate</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle"></i>Menu1</a></li>
                </ul>
            </li>

            <li class="treeview hide">
                <a href="#" class="nav-link">
                    <i class="fa fa-calendar"></i>
                    <span>ST TR-In Plan</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle"></i>Menu1</a></li>
                </ul>
            </li>

            <li class="treeview {{ Request::segment(1) === 'pr-plan' ? 'active' : '' }}">
                <a href="#" class="nav-link">
                    <i class="fa fa-calendar-plus-o"></i>
                    <span>PR Plan</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Route::currentRouteNamed( 'pr-plan.maj-cat' ) ?  'active' : '' }}"><a href="{{ route('pr-plan.maj-cat') }}" ><i class="fa fa-circle"></i>MAJ CAT</a></li>
                    <li class="{{ Route::currentRouteNamed( 'pr-plan.maj-cat-seg' ) ?  'active' : '' }}"><a href="{{ route('pr-plan.maj-cat-seg') }}" ><i class="fa fa-circle"></i></i>MAJ CAT SEG</a></li>
                    <li class="{{ Route::currentRouteNamed( 'pr-plan.maj-cat-seg-mvgr' ) ?  'active' : '' }}"><a href="{{ route('pr-plan.maj-cat-seg-mvgr') }}"><i class="fa fa-circle"></i>MAJ CAT SEG-MVGR</a></li>
                    <li class="{{ Route::currentRouteNamed( 'pr-plan.reference-article' ) ?  'active' : '' }}"><a href="{{ route('pr-plan.reference-article') }}"><i class="fa fa-circle"></i>Reference Article</a></li>
                    <li class="{{ Route::currentRouteNamed( 'pr-plan.options-article' ) ?  'active' : '' }}"><a href="{{ route('pr-plan.options-article') }}"><i class="fa fa-circle"></i>Options Article</a></li>
                    <li class="{{ Route::currentRouteNamed( 'pr-plan.auto-pr' ) ?  'active' : '' }}"><a href="{{ route('pr-plan.auto-pr') }}"><i class="fa fa-circle"></i>Auto PR</a></li>
                </ul>
            </li>

            <li class="treeview {{ Request::segment(1) === 'otb-plan' ? 'active' : '' }}">
                <a href="#" class="nav-link">
                    <i class="fa fa-bar-chart"></i>
                    <span>OTB (PRPO) Plan</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">

                    <li class="{{ Route::currentRouteNamed( 'otb-plan.maj-cat' ) ?  'active' : '' }}"><a href="{{ route('otb-plan.maj-cat') }}" ><i class="fa fa-circle"></i>MAJ CAT</a></li>
                    <li class="{{ Route::currentRouteNamed( 'otb-plan.maj-cat-seg' ) ?  'active' : '' }}"><a href="{{ route('otb-plan.maj-cat-seg') }}" ><i class="fa fa-circle"></i></i>MAJ CAT SEG</a></li>
                    <li class="{{ Route::currentRouteNamed( 'otb-plan.maj-cat-seg-mvgr' ) ?  'active' : '' }}"><a href="{{ route('otb-plan.maj-cat-seg-mvgr') }}"><i class="fa fa-circle"></i>MAJ CAT SEG-MVGR</a></li>
                    <li class="{{ Route::currentRouteNamed( 'otb-plan.reference-article' ) ?  'active' : '' }}"><a href="{{ route('otb-plan.reference-article') }}"><i class="fa fa-circle"></i>Reference Article</a></li>
                    <li class="{{ Route::currentRouteNamed( 'otb-plan.options-article' ) ?  'active' : '' }}"><a href="{{ route('otb-plan.options-article') }}"><i class="fa fa-circle"></i>Options Article</a></li>

                </ul>
            </li>

            <li class="treeview {{ Request::segment(1) === 'po' ? 'active' : '' }}">
                <a href="#" class="nav-link">
                    <i class="fa fa-barcode"></i>
                    <span>PO</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Route::currentRouteNamed( 'po.index' ) ?  'active' : '' }}"><a href="{{ route('po.index') }}"><i class="fa fa-circle"></i>Pending PO</a></li>
                </ul>
            </li>

            <li class="treeview {{ Request::segment(1) === 'grc' ? 'active' : '' }}">
                <a href="#" class="nav-link">
                    <i class="fa fa-briefcase"></i>
                    <span>GRC</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Route::currentRouteNamed( 'grc.index' ) ?  'active' : '' }}"><a href="{{ route('grc.index') }}"><i class="fa fa-circle"></i>GRC List</a></li>
                </ul>
            </li> -->

            <li class="treeview hide">
                <a href="#" class="nav-link">
                    <i class="fa fa-map-pin"></i>
                    <span>SKU Allocation</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-mouse-pointer"></i>Menu1</a></li>
                </ul>
            </li>

            <li class="treeview hide">
                <a href="#" class="nav-link">
                    <i class="fa fa-cubes"></i>
                    <span>Store Wise Packaging</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-mouse-pointer"></i>Menu1</a></li>
                </ul>
            </li>

            <li class="treeview hide">
                <a href="#" class="nav-link">
                    <i class="fa fa-users"></i>
                    <span>Manage Vendors</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-mouse-pointer"></i>Menu1</a></li>
                </ul>
            </li>
           
            <li class="treeview {{ ( Request::segment(1) === 'users' || Request::segment(1) === 'roles' || Request::segment(1) === 'division' || Request::segment(1) === 'department' || Request::segment(1) === 'permissions') ? 'active' : '' }}">

                <a href="#" class="nav-link">
                    <i class="fa fa-user nav-icon"></i><span>Manage Users </span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="{{ Route::currentRouteNamed( 'division.index' ) ?  'active' : '' }}"><a href="{{ route('division.index') }}"><i class="fa fa-circle"></i>Manage Divisions</a></li>
                    <li class="{{ Route::currentRouteNamed( 'department.index' ) ?  'active' : '' }}"><a href="{{ route('department.index') }}"><i class="fa fa-circle"></i>Manage Departments</a></li>
                    <li class="{{ Route::currentRouteNamed( 'users.index' ) ?  'active' : '' }}"><a href="{{ route('users.index') }}"><i class="fa fa-circle"></i>Manage Users</a></li>
                   
                    <li  class="{{ Route::currentRouteNamed( 'roles.index' ) ?  'active' : '' }}"><a href="{{ route('roles.index') }}"><i class="fa fa-circle"></i>Manage Role</a></li>
                   
                    @can('permission-list')
<!--                    <li  class="{{ Route::currentRouteNamed( 'permissions.index' ) ?  'active' : '' }}"><a href="{{ route('permissions.index') }}"><i class="fa fa-user-times"></i>Manage Permissions</a></li>-->
                    @endcan
                </ul>
            </li>
           

            <li class="treeview hide">
                <a href="#" class="nav-link">
                    <i class="fa fa-folder"></i>
                    <span>Report</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle"></i>Menu1</a></li>
                </ul>
            </li>

            <li class="treeview hide">
                <a href="#" class="nav-link">
                    <i class="fa fa-cog"></i>
                    <span>Settings</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-right pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle"></i>Menu1</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
