<header class="main-header">
    <!-- Logo -->
    <a href="{!! route('home') !!}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>V</b>2</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img src="{{asset('dist/img/header-logo.png')}}" alt="logo"></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
      
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
              <!-- Notifications Dropdown Menu -->
              <!--li class="nav-item dropdown Notifications">
                <a class="nav-link brdright" data-toggle="dropdown" href="#">
                  <i class="far fa-bell"></i>
                  <span class="badge badge-warning navbar-badge">15</span>
                </a>
              </li-->
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{ isset(Auth::user()->name) ? Auth::user()->name : '' }} </span><i class="fa fa-angle-down user-donw"></i>
                    </a>
                    <ul class="dropdown-menu userdrop">
                    <li><a href="{{ route('users.edit',Auth::user()->id) }}"><i class="fa fa-edit"></i>Edit Profile</a></li>
                    <!--<li><a href="#"><i class="fa fa-gear"></i> Setting</a></li>
                    <li><a href="#"><i class="fa fa-envelope"></i>My Messages</a></li>
                    <li><a href="#"> <i class="fa fa-bell"></i> Notification</a></li>-->
                    <li><a class="logout" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" ><i class="fa  fa-power-off"></i>Log out</a> <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>