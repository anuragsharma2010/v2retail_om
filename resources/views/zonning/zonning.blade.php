@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
      Zoning
   </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Zone List</li>
    </ol>
</section>
<!-- /.content-header -->
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <div class="col-sm-12 list_filter_head">
                    <div class="mtb-10">
               <form class="form-inline">
                  <select class="firstone form-control">
                     <option>ZONE</option>
                     <option>ZONE 1</option>
                     <option>ZONE 2</option>
                  </select>
                  <select class="secondone form-control mx-10">
                     <option>REGION</option>
                     <option>REGION 1</option>
                     <option>REGION 2</option>
                     <option>REGION 3</option>
                  </select>
                  <select class="thirdone form-control">
                     <option>AREA</option>
                     <option>AREA 1</option>
                     <option>AREA 2</option>
                  </select>
               </form>
            </div>
                    <div class="search_box">
                        <!-- search form -->
                        <form action="#" method="get" class="sidebar-form mr-0">
                            <div class="input-group">
                                <input type="text" name="searchtxt" id="searchtxt" value="<?php echo isset($data->searchtxt) ? $data->searchtxt:'';  ?>" class="form-control search" placeholder="Search...">
                                <span class="input-group-btn">
                     <button type="submit" name="search" value="save" id="search-btn" class="btn btn-flat">
                     <i class="fa fa-search"></i>
                     </button>
                     </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if ($message = Session::get('success'))
         <div class="message" role="alert">
            <div class="alert alert-success alert-dismissible">
               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
         </div>
         @endif
         <div class="container-fluid p-0 show-enteries-parent">
            <div class="col-md-6 p-0"><label class="show-enteries-text">Show enteries
               {!! Form::select('per_page', [10=>10,50=>50,150=>150,500=>500], app('request')->input('perpage'), ['class'=>'item-per-page','id'=>'item-show-per-page']) !!}
               </label>
            </div>
            <div class="col-md-6 p-0">
               <p class="showing-enteries-text text-right">Showing 1 to 10 of 57 entries</p>
            </div>
         </div>

                         <div class="table_data">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ZONE</th>
                                <th>REGION</th>
                                <th>AREA</th>
                                <th>ST - CD</th>
                                <th>ST - NM</th>
                                <th>FIX</th>
                                <th>AVG. AREA PER FIX</th>
                                <th>FLOOR</th>
                                <th>LAYOUT</th>
                                <th>ZONNING</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>EAST</td>
                                <td>R1</td>
                                <td>A5</td>
                                <td>HJOB</td>
                                <td>JMD</td>
                                <td>295</td>
                                <td>34.89</td>
                                <td>1<sup>st</sup></td>
                                <td>
                                    <div class="upload-file-btn">
                                        <a href="#" class="btn btn-primary btn_dis"> <i class="fa fa-upload"></i></a>
                                        <input type="file" name="" class="file-choosen-hidden">
                                    </div>
                                </td>
                                <td>
                                    <a class="btn btn-primary btn_dis" href="#" data-toggle="tooltip" data-original-title="Download"><i class="fa fa-download"></i></a>
                                    <a class="btn btn-primary btn_dis" href="#" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>EAST</td>
                                <td>R1</td>
                                <td>A5</td> 
                                <td>HJOB</td>
                                <td>JMD</td>
                                <td>295</td>
                                <td>34.89</td>
                                <td>1<sup>st</sup></td>
                                <td>
                                    <a class="btn btn-primary btn_dis" href="#" data-toggle="tooltip" data-original-title="Download"><i class="fa fa-download"></i></a>
                                    <a class="btn btn-primary btn_dis" href="#" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                </td>
                                <td>
                                    <div class="upload-file-btn">
                                        <a href="#" class="btn btn-primary btn_dis"> <i class="fa fa-upload"></i></a>
                                        <input type="file" name="" class="file-choosen-hidden">
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <div class="col-md-12 text-right pagination">
              
                
            </div>
        </div>
  </div>
</section>
@endsection