@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        MAJ CAT SEG - MVGR
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="#">OTB Plan</a></li>
        <li class="active">MAJ CAT SEG - MVGR</li>
    </ol>
</section>
<!-- /.content-header -->
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <div class="col-sm-12 list_filter_head">
                    <div class="search_box">
                        <!-- search form -->
                        <form action="#" method="get" class="sidebar-form">
                            <div class="input-group">
                                <input type="text" name="search" class="form-control search" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="userbtn">
                           <a href="#" class="btn btn-sm btn-primary createuser-btn"><i class="fa fa-download"></i>Download Template</a>
                        <a href="{{ route('otb-plan.maj-cat-seg-mvgr.import') }}" class="btn btn-sm btn-primary createuser-btn">
                        <i class="fa fa-upload"></i>Upload</a>
                          <select id="item-show-per-page" class="item-per-page btn">
          <option value="10">10</option>
          <option value="50">50</option>
          <option value="150">150</option>
          <option value="500">500</option>
          </select>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="message" role="alert">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                </div>
                <div class="table_data">
                    <table class="table table-bordered table-striped">
                         <thead>
                            <tr>
                                <th>MAJ CAT SEG - MVGR File</th>
                                <th>Month Range</th>
                                <th>Uploaded Date</th>
                                <th>Modified date</th>
                                <th>Actions</th>                               
                            </tr>
                        </thead> 
                        <tbody>
                            <tr>
                                <td><p class="file-name-inside-table green-color">file-name.csv</p></td>
                                <td>Feb to Mar</td>
                                <td>20-02-2020</td>
                                <td>22-02-2020</td>
                                <td><a class="btn btn-primary btn_dis" title=""  data-toggle="tooltip" data-original-title="Download"><i class="fa fa-download"></i></a>

                                </td>
                               
                            </tr> 

                        </tbody>
                    </table>
                      <div class="card-footer">
                    <div class="col-md-12 text-right pagination">
                    </div>
                </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
</section>

@endsection
