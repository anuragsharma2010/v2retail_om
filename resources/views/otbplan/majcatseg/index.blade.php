@extends('layouts.app')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
            MAJ CAT SEG
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="#">OTB Plan</a></li>
        <li class="active">MAJ CAT SEG</li>
    </ol>
</section>
<!-- /.content-header -->
<section class="content">
    <div class="container-fluid">
        
        <div class="card">
            <div class="card-header">
                <div class="col-sm-12 list_filter_head">
                    <div class="search_box">
                        <!-- search form -->
                        <form action="#" method="get" class="sidebar-form">
                            <div class="input-group">
                                <input type="text" name="search" class="form-control search" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="userbtn">
                        <a href="#" class="btn btn-sm btn-primary createuser-btn"> <i class="fa fa-download"></i>Download Template</a>
                        <a href="{{ route('otb-plan.maj-cat-seg.import') }}" class="btn btn-sm btn-primary createuser-btn"><i class="fa fa-upload"></i>Upload</a>
                        <select id="item-show-per-page" class="item-per-page btn">
          <option value="10">10</option>
          <option value="50">50</option>
          <option value="150">150</option>
          <option value="500">500</option>
          </select>


                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="message" role="alert">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                </div>
                <div class="table_data">
                    <table class="table table-bordered table-striped">
                            <thead> 
                        <tr>
                        <th rowspan="2">SEG</th>
                        <th rowspan="2">DIV</th>
                        <th rowspan="2">SUB DIV</th>
                        <th rowspan="2">MAJ - CAT</th>
                        <th rowspan="2">RNG SEG</th>
                        <th rowspan="2">SSN</th>
                        <th colspan="2">MAJ-CAT-OPTIONS</th>
                        <th rowspan="2">ART COUNT</th>
                        <th colspan="3">PO-PND-OPT</th>
                        <th rowspan="2">70% @bgt</th>
                        <th rowspan="2">DC-OPT-FILL RATE-%</th>
                        <th colspan="9">QTY</th>
                        <th rowspan="2">CL-OTB LM</th>
                        <th rowspan="2">GRC 1 TO 7</th>
                        <th colspan="20">Month Value</th>
                     </tr>

                        </thead> 
                        <tbody>
                                 

                        </tbody>
                    </table>
                      <div class="card-footer">
                    <div class="col-md-12 text-right pagination">
                    </div>
                </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
</section>

@endsection
