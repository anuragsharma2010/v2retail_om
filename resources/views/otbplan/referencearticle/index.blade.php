@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header hide ">
    <h1>
        Reference Article
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="#">OTB Plan</a></li>
        <li class="active">Reference Article</li>
    </ol>
</section>
<section class="content-header">
    <h1>
       Dashboard - Task Tracker
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="#">Dashboard - Task Tracker</a></li>
    </ol>
</section>
<style>
.border_top_ignore{
    border-top:0px solid #fff !important;
}
</style>
<!-- /.content-header -->
<section class="content hide">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <div class="col-sm-12 list_filter_head">
                    <div class="search_box">
                        <!-- search form -->
                        <form action="#" method="get" class="sidebar-form">
                            <div class="input-group">
                                <input type="text" name="search" class="form-control search" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="userbtn">
                        <a href="#" class="btn btn-sm btn-primary createuser-btn"><i class="fa fa-download"></i>Download Template</a>
                        <a href="{{ route('otb-plan.reference-article.import') }}" class="btn btn-sm btn-primary createuser-btn">Upload</a>
                        <select id="item-show-per-page" class="item-per-page btn">
          <option value="10">10</option>
          <option value="50">50</option>
          <option value="150">150</option>
          <option value="500">500</option>
          </select>

                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="message" role="alert">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                </div>
                <div class="table_data">
                    <table class="table table-bordered table-striped">
                        <thead>
                           <tr>
                                <th>Reference Article File</th>
                                <th>Month Range</th>
                                <th>Uploaded Date</th>
                                <th>Modified date</th>
                                <th>Actions</th>                               
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><p class="file-name-inside-table green-color">file-name.csv</p></td>
                                <td>Feb to Mar</td>
                                <td>20-02-2020</td>
                                <td>22-02-2020</td>
                                <td><a class="btn btn-primary btn_dis" title="" data-toggle="tooltip" data-original-title="Download"><i class="fa fa-download"></i></a>

                                </td>
                               
                            </tr>

                        </tbody>
                    </table>
                    <div class="col-md-12 text-right pagination">

                    </div>
                </div>
                <!-- /.card-body -->
            </div>  
            <!-- /.card -->
        </div>
</section>

<style>

</style>
<!--- dashboard  of task tracker --->
<section class="content">
    <div class="container-fluid">
        <div class="box nulled_bg">
            <div class="box-header with-border bg_set border_top_left border_top_right">
                <h3 class="box-title"> Today's Activity </h3>
                <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="box-body setborder border_top_ignore border_bottom_left border_bottom_right">
                  <div id="maindashboard">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="fa fa-tasks clr-aqua"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-number clr-aqua">200</span>
                                <span class="info-box-text">Total Open Task</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-red"><i class="fa fa-shopping-cart clr-red"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-number clr-red">100</span>
                                <span class="info-box-text">Total WIP</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-green"><i class="fa fa-tags clr-green"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-number clr-green">10</span>
                                <span class="info-box-text">Total Closed Task</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-yellow"><i class="fa  fa-file-text clr-yellow"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-number clr-yellow">10</span>
                                <span class="info-box-text">Total Handover Task</span>
                            </div>
                        </div>
                    </div>

                  </div>
            </div> 
            </div> 
            <!-- /.card -->
            <style>
                .setflex{
                  display:flex ;
                }
                .graph_stats {
                        width: 175px;
                        padding: 15px;
                        background: #00a65a !important;
                    }
                .graph_box{
                     padding-left:10px;
                }
                .graph_stats table.table>tbody>tr>td{
                     border-top:none;
                     color:#fff;
                     border-bottom:1px solid #fff;
                }
                
            </style>            
            <div class="row" >
            <div class="col-md-7">
            <div class="box">   
                     <div class="box-header with-border">
                       <h3 class="box-title">Task Overview</h3>
                       <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
                    </div>

                    <div class="box-body setflex" >
                <div class="graph_stats">
                   <table class="table">
                    <tbody>
                     <tr><td>Open Task</td></tr>
                      <tr><td> 100</td></tr> 
                      <tr><td> WIP</td></tr>
                      <tr><td>  70</td></tr>
                      <tr><td>Closed Task</td></tr>
                       <tr><td> 10</td></tr>
                       <tr><td> Handed Over Task</td></tr>
                         <tr><td> Reopened task</td></tr>
                            <tr><td>10</td></tr>
                        <tr><td> Overdue Task</td></tr>
                        <tr><td>5</td></tr>
                    </tbody>
                </table>
                </div>
                 <div class="graph_box">     
                  <div class="progress-group">
                    <span class="progress-text">Add Products to Cart</span>
                    <span class="progress-number"><b>160</b>/200</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Complete Purchase</span>
                    <span class="progress-number"><b>310</b>/400</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width: 80%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Visit Premium Page</span>
                    <span class="progress-number"><b>480</b>/800</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width: 80%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                  <div class="progress-group">
                    <span class="progress-text">Send Inquiries</span>
                    <span class="progress-number"><b>250</b>/500</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-yellow" style="width: 80%"></div>
                    </div>
                  </div>
                  <!-- /.progress-group -->
                </div>
            </div>

            </div>
            </div>
            <div class="col-md-5">
             <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">Donut Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div id="donut-chart" style="height: 300px;"></div>
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->


            </div>
            </div>
        </div>
        </div>
</section>
@endsection

@section('custom.js')
<script src="https://adminlte.io/themes/AdminLTE/bower_components/Flot/jquery.flot.pie.js"></script>
<script src="https://adminlte.io/themes/AdminLTE/bower_components/Flot/jquery.flot.categories.js"></script>
<script>
  $(function () {
    /*
     * Flot Interactive Chart
     * -----------------------
     */
    // We use an inline data source in the example, usually data would
    // be fetched from a server
    var data = [], totalPoints = 100

    function getRandomData() {

      if (data.length > 0)
        data = data.slice(1)

      // Do a random walk
      while (data.length < totalPoints) {

        var prev = data.length > 0 ? data[data.length - 1] : 50,
            y    = prev + Math.random() * 10 - 5

        if (y < 0) {
          y = 0
        } else if (y > 100) {
          y = 100
        }

        data.push(y)
      }

      // Zip the generated y values with the x values
      var res = []
      for (var i = 0; i < data.length; ++i) {
        res.push([i, data[i]])
      }

      return res
    }

    var interactive_plot = $.plot('#interactive', [getRandomData()], {
      grid  : {
        borderColor: '#f3f3f3',
        borderWidth: 1,
        tickColor  : '#f3f3f3'
      },
      series: {
        shadowSize: 0, // Drawing is faster without shadows
        color     : '#3c8dbc'
      },
      lines : {
        fill : true, //Converts the line chart to area chart
        color: '#3c8dbc'
      },
      yaxis : {
        min : 0,
        max : 100,
        show: true
      },
      xaxis : {
        show: true
      }
    })

    var updateInterval = 500 //Fetch data ever x milliseconds
    var realtime       = 'on' //If == to on then fetch data every x seconds. else stop fetching
    function update() {

      interactive_plot.setData([getRandomData()])

      // Since the axes don't change, we don't need to call plot.setupGrid()
      interactive_plot.draw()
      if (realtime === 'on')
        setTimeout(update, updateInterval)
    }

    //INITIALIZE REALTIME DATA FETCHING
    if (realtime === 'on') {
      update()
    }
    //REALTIME TOGGLE
    $('#realtime .btn').click(function () {
      if ($(this).data('toggle') === 'on') {
        realtime = 'on'
      }
      else {
        realtime = 'off'
      }
      update()
    })
    /*
     * END INTERACTIVE CHART
     */

    /*
     * LINE CHART
     * ----------
     */
    //LINE randomly generated data

    var sin = [], cos = []
    for (var i = 0; i < 14; i += 0.5) {
      sin.push([i, Math.sin(i)])
      cos.push([i, Math.cos(i)])
    }
    var line_data1 = {
      data : sin,
      color: '#3c8dbc'
    }
    var line_data2 = {
      data : cos,
      color: '#00c0ef'
    }
    $.plot('#line-chart', [line_data1, line_data2], {
      grid  : {
        hoverable  : true,
        borderColor: '#f3f3f3',
        borderWidth: 1,
        tickColor  : '#f3f3f3'
      },
      series: {
        shadowSize: 0,
        lines     : {
          show: true
        },
        points    : {
          show: true
        }
      },
      lines : {
        fill : false,
        color: ['#3c8dbc', '#f56954']
      },
      yaxis : {
        show: true
      },
      xaxis : {
        show: true
      }
    })
    //Initialize tooltip on hover
    $('<div class="tooltip-inner" id="line-chart-tooltip"></div>').css({
      position: 'absolute',
      display : 'none',
      opacity : 0.8
    }).appendTo('body')
    $('#line-chart').bind('plothover', function (event, pos, item) {

      if (item) {
        var x = item.datapoint[0].toFixed(2),
            y = item.datapoint[1].toFixed(2)

        $('#line-chart-tooltip').html(item.series.label + ' of ' + x + ' = ' + y)
          .css({ top: item.pageY + 5, left: item.pageX + 5 })
          .fadeIn(200)
      } else {
        $('#line-chart-tooltip').hide()
      }

    })
    /* END LINE CHART */

    /*
     * FULL WIDTH STATIC AREA CHART
     * -----------------
     */
    var areaData = [[2, 88.0], [3, 93.3], [4, 102.0], [5, 108.5], [6, 115.7], [7, 115.6],
      [8, 124.6], [9, 130.3], [10, 134.3], [11, 141.4], [12, 146.5], [13, 151.7], [14, 159.9],
      [15, 165.4], [16, 167.8], [17, 168.7], [18, 169.5], [19, 168.0]]
    $.plot('#area-chart', [areaData], {
      grid  : {
        borderWidth: 0
      },
      series: {
        shadowSize: 0, // Drawing is faster without shadows
        color     : '#00c0ef'
      },
      lines : {
        fill: true //Converts the line chart to area chart
      },
      yaxis : {
        show: false
      },
      xaxis : {
        show: false
      }
    })

    /* END AREA CHART */

    /*
     * BAR CHART
     * ---------
     */

    var bar_data = {
      data : [['January', 10], ['February', 8], ['March', 4], ['April', 13], ['May', 17], ['June', 9]],
      color: '#3c8dbc'
    }
    $.plot('#bar-chart', [bar_data], {
      grid  : {
        borderWidth: 1,
        borderColor: '#f3f3f3',
        tickColor  : '#f3f3f3'
      },
      series: {
        bars: {
          show    : true,
          barWidth: 0.5,
          align   : 'center'
        }
      },
      xaxis : {
        mode      : 'categories',
        tickLength: 0
      }
    })
    /* END BAR CHART */

    /*
     * DONUT CHART
     * -----------
     */

    var donutData = [
      { label: 'Series2', data: 30, color: '#3c8dbc' },
      { label: 'Series3', data: 20, color: '#0073b7' },
      { label: 'Series4', data: 50, color: '#00c0ef' }
    ]
    $.plot('#donut-chart', donutData, {
      series: {
        pie: {
          show       : true,
          radius     : 1,
          innerRadius: 0.5,
          label      : {
            show     : true,
            radius   : 2 / 3,
            formatter: labelFormatter,
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    })
    /*
     * END DONUT CHART
     */

  })

  /*
   * Custom Label formatter
   * ----------------------
   */
  function labelFormatter(label, series) {
    return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
      + label
      + '<br>'
      + Math.round(series.percent) + '%</div>'
  }
</script>

@endsection
