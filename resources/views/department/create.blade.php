@extends('layouts.app')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Create Department
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
        <li ><a href="{{ route('department.index') }}">Departments</a></li>
        <li class="active">Create Department</li>
    </ol>
</section>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="form_container">

                   
                    <form action="<?php if (isset($id)) { ?>{{ route('department.update',$id) }} <?php } else { ?>{{ route('department.store') }}<?php } ?>" method="POST">
                        @csrf
                        <?php if (isset($id)) { ?>
                            @method('PUT')
                        <?php } ?>
                        <input type="hidden" name='id' name='id' value="<?php if (isset($id)) {
                            echo $id;
                        } ?>" />
                        <div class="row">
                            <div class="clearfix">
                                <div class="col-md-6 right_pdng">
                                    <div class="form-group">
                                        <label >Division Name:</label>
                                        <select name="div_id" id="div_id" class="form-control customtinput">
                                            <option value=""> Select Division</option>

                                            <?php foreach ($DataObj->Divisions as $key => $value) { ?>                            
                                            <option value="<?php echo $value->id; ?>" <?php if($DataObj->div_id == $value->div_id){ ?>selected=""<?php } ?>><?php echo $value->div_name; ?></option>
<?php } ?>
                                            

                                            

                                        </select>
                                    @if ($errors->has('div_id'))
                                    <div class="error_field" >{{ $errors->first('div_id') }}</div>
                                    @enderror
                                    </div>
                                    <!-- /.form-group -->
                                    <!-- /.form-group -->
                                </div>
                                <div class="col-md-6 left_pdng">
                                    <div class="form-group">
                                        <label >Name:</label>
                                        <input  type="text" name="dept_name" id="dept_name" value="<?php if (isset($DataObj->dept_name)) {
    echo $DataObj->dept_name;
} ?>" class="form-control" placeholder="Name" maxlength="50">
                                    @if ($errors->has('dept_name'))
                                        <div class="error_field" >{{ $errors->first('dept_name') }}</div>
                                    @enderror
                                    </div>
                                    <!-- /.form-group -->
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6 right_pdng">
                                    <div class="form-group">
                                        <label>Code:</label>
                                        <input type="text" value="<?php if (isset($DataObj->dept_code)) {
    echo $DataObj->dept_code;
} ?>"   class="form-control" id="dept_code" name="dept_code" placeholder="code" maxlength="20">
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <div class="col-md-6 left_pdng">
                                    <div class="form-group">
                                        <label>Description:</label>
                                        <textarea   id="dept_desc" name="dept_desc" class="form-control" placeholder="Description" maxlength="300"><?php if (isset($DataObj->dept_desc)) {echo $DataObj->dept_desc;} ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="clearfix">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <div class="icheck-primary d-inline">
                                            <input type="radio" id="In-Active" name="dept_status"<?php if (isset($DataObj->dept_status) && $DataObj->dept_status==0) { ?>checked=""<?php } ?> value="0" class="custom-control-input">
                                            <label for="In-Active">In-Active
                                            </label>
                                        </div>
                                        <div class="icheck-primary d-inline">
                                            <input type="radio"  <?php if (isset($DataObj->dept_status) && $DataObj->dept_status==1) { ?>checked=""<?php } ?>  id="Active" name="dept_status" value="1"  class="custom-control-input">
                                            <label for="Active">Active 
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" >
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                                
                            </div>
                            <div class="col-md-6 right">
                                <button type="button" class="btn btn-primary btn-submit" onclick="javascript:location.replace('{{ route('department.index') }}')">Back</button>
                            </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
