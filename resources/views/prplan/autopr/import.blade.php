@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Auto PR
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="#"><i class="fa fa-home"></i>PR Plan</a></li>
        <li>Auto PR</li>
        <li class="active">Import Auto PR</li>
    </ol>
</section>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">s
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="form_container">
                    <form class="md-form" action="" id="importform" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="custom-file file-field">
                            <p class="file-box-main-heading">Upload Auto PR Detail</p>
                            <div class="file-field-inner">
                                <div class="browse-and-drag-drop-parent">
                                    <div class="upload-img-container">
                                        <img src="{{url('dist/img/upload-icon.png')}}" class="img-responsive">
                                    </div>
                                    <label class="drag-drop-text">Drag and Drop file here</label>
                                    <p class="or-text">or</p>
                                    <input type="file" name="file" id="custom-browse-btn">
                                    <label class="browse-file-lbl"> Browse file</label>
                                    <p class="file-format-text">(File format .xlsx)</p>
                                </div>
                                <span id="file-name"></span>
                                @error('file')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                                <div class="form-group pt-2 text-center">
                                    <input type="submit" class="btn btn-primary btn-submit" name="submit" id="submit" value="Submit">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('custom-js')
<script src="{{ asset('js/importjs.js') }}"></script>
@endsection


