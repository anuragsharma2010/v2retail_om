@extends('layouts.app')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
            MAJ CAT SEG
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="#">PR Plan</a></li>
        <li class="active">MAJ CAT SEG</li>
    </ol>
</section>
<!-- /.content-header -->
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <div class="col-sm-12 list_filter_head">
                    <div class="search_box">
                        <!-- search form -->
                        <form action="#" method="get" class="sidebar-form">
                            <div class="input-group">
                                <input type="text" name="search" class="form-control search" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="userbtn">
                        <a href="#" class="btn btn-sm btn-primary createuser-btn mx-10"> <i class="fa fa-download"></i>Download Template</a>
                        <a href="{{ route('pr-plan.maj-cat-seg.import') }}" class="btn btn-sm btn-primary createuser-btn"><i class="fa fa-upload"></i>Upload</a>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
           


            <div class="card-body">

                    @if ($message = Session::get('success'))
         <div class="message" role="alert">
            <div class="alert alert-success alert-dismissible">
               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
         </div>
         @endif
         <div class="container-fluid p-0 show-enteries-parent">
            <div class="col-md-6 p-0"><label class="show-enteries-text">Show enteries
               {!! Form::select('per_page', [10=>10,50=>50,150=>150,500=>500], app('request')->input('perpage'), ['class'=>'item-per-page','id'=>'item-show-per-page']) !!}
               </label>
            </div>
            <div class="col-md-6 p-0">
               <p class="showing-enteries-text text-right">Showing 1 to 10 of 57 entries</p>
            </div>
         </div> 
            
                <div class="table_data">
                    <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>MAJ CAT SEG File</th>
                                <th>Month Range</th>
                                <th>Uploaded Date</th>
                                <th>Modified date</th>
                                <th>Actions</th>                               
                            </tr>
                        </thead> 
                        <tbody>
                                  <tr>
                                <td><p class="file-name-inside-table green-color">file-name.csv</p></td>
                                <td>Feb to Mar</td>
                                <td>20-02-2020</td>
                                <td>22-02-2020</td>
                                <td><a class="btn btn-primary btn_dis" title=""  data-toggle="tooltip" data-original-title="Download"><i class="fa fa-download"></i></a>

                                </td>
                               
                            </tr> 

                        </tbody>
                    </table>
                      <div class="card-footer">
                    <div class="col-md-12 text-right pagination">
                    </div>
                </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
</section>

@endsection
