<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--<link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico"> -->
    <title>Login</title>
    <!-- Bootstrap core CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="/css/style_login.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
  </head>
  <body>
 <div id="login">
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box">
                        <form id="login-form" class="form" method="POST" action="{{ route('login') }}">
                            @csrf
                           <div class="d-flex justify-content-center"> <span class="brand-img"><img src="{{ asset('dist/img/logo_v2.png') }}" alt="logo"></span></div>
                            <h3 class="text-left heading-3 font3 py-3">{{ __('Login') }}</h3>

                            <div class="form-group">
                                <label for="username" class="text-muted">{{ __('E-Mail Address') }}</label>
                                <input type="text" name="email" id="email" value="{{ old('email') }}" placeholder="Enter your email" class="form-control @error('email') is-invalid @enderror">
                                 @error('email')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-muted">{{ __('Password') }}</label>
                                <input type="password" name="password" placeholder="Enter your password" id="password" class="form-control @error('password') is-invalid @enderror">
                                 @error('password')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group d-flex justify-content-between">
                                <div class="icheck-primary">
                                  <input type="checkbox" id="remember"  {{ old('remember') ? 'checked' : '' }}>
                                  <label for="remember" class="text-muted">
                                   {{ __('Remember Me') }}
                                  </label>
                                </div>
                            </div>
                             <div class="form-group text-center"><input type="submit" name="submit" class="btn btn-info btn-md btn-signin" value="{{ __('Login') }}"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  </body>
</html>