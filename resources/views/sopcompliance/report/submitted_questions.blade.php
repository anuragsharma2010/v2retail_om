@extends('layouts.app')

@section('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
<style>
label{margin-left: 20px;}
#datepicker{width:200px; margin:10px 10px 10px 0px;}
#datepicker > span:hover{cursor: pointer;}
#datepicker> span.input-group-addon {
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
}
.card-header {
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0,0,0,.03);
    border-bottom: 1px solid rgba(0,0,0,.125);
}
div.answers.remarsk {
      padding: 12px;
    padding-left: 2px;
    width: 66%;
  }
  .card.Questioncard>.card-header{
         padding: 10px 0px;
      border-bottom:0px ;
      background:transparent;
      font-size:24px;
  }
  .card.Questioncard {
    background: rgba(0,0,0,.03);
    margin-bottom: 30px;
    padding: 15px 15px;
    padding-bottom:5px;
}
    .Questioncard .card-header>span{
      background:transparent;
      color:#333;
      font-size:25px;
     
    }
    .card.Questioncard>.card-header{
      font-size:25px;
       border-bottom: 1px solid #fff;
    }
    .label.answers{
    font-size: 20px;
    font-weight: 300;
    }
    .card.Questioncard>.card-body{
        padding: 22px 35px;
    }
 p.timeshow strong, p.timeshow {
    font-size: 14px;
    line-height: 22px;
    margin-top: 10px;
    font-weight: 300;
}
.label.answers i.fa {
    margin-right: 10px;
    font-size: 16px;
    opacity: 0.6;
    border: 1px solid #333;
    border-radius: 50px;
    height: 25px;
    width: 25px;
    display: inline-block;
    text-align: center;
    line-height: 24px;
    font-size: 12px;
}
</style>
@endsection

@section('content')

<section class="content-header form_in_title">
    <h1>
      Questions 
    </h1>
    <ol class="breadcrumb">
      <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
      <li><a href="{{ route('assignedstore',base64_encode($process_id)) }}">Assigned process</a></li>
      <li class="active">Question completion</li>
    </ol>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="card listcontainer">
            <div class="card-header">
               <div class="col-sm-12 list_filter_head">
                  <div class="search_box">
                      <form>
                        <div class="d-flex pt-10">
                           <div class="form-inline"> 
                              <select class="firstone form-control" id="process" onchange="reloadPageForProcess(this.value)">
                              @foreach($processes as $process)
                              <option value="{{base64_encode($process->id)}}" @php if(base64_encode($process->id) == base64_encode($process_id)){ echo 'selected'; } @endphp >{{$process->form_label}}</option>
                              @endforeach
                              </select>
                              <select class="form-control mx-10" id="store" onchange="reloadPageForStore(this.value)">
                                @foreach($stores as $store)
                                <option value="{{base64_encode($store->id)}}" @php if(base64_encode($store->id) == base64_encode($store_id)){ echo 'selected'; } @endphp >{{$store->st_name}}</option>
                                @endforeach
                              </select>  
                            </div>
                          <div id="datepicker" class="input-group date mt-0" data-date-format="mm-dd-yyyy">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                              <input class="form-control" type="text" id="filter_date" value="{{ app('request')->input('dt')??\Carbon\Carbon::now()->format('d-m-Y') }}" readonly />
                          </div>
                       </div>
                    </form>
                  </div>
                  <div class="userbtn usedtails">
                     <!--label class="label">
                       User name <span>( Lat : 17.644022</span> - <span>Lon : 0.175781 )</span>
                     </label-->
                  </div>
            </div>
          </div>
            <!-- /.card-header -->
            <div class="card-body">
               @if ($message = Session::get('success'))
                <div class="message" form="alert">
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                </div>
                @endif
             
              <div class="table_data">
                @forelse($questions as $key=>$question)
                   <div class="card Questioncard">
                         <div class="card-header">
                           <div class="col-md-8">
                            <span>Q{{++$key}}. </span> {{optional(optional($question)->question)->question}}
                          </div>
                          <div class="col-md-4">
                              <p class="text-right timeshow">
                            <strong>Time Taken</strong> :{{optional($question)->time_taken}}</p>
                          </div>
                         </div>
                         <div class="card-body">
                         
                         @foreach($question->submissionValues as $values)
                         
                          @if($values->field->field_type == 'radio')
                          <label class="label answers"><i class="fa fa-check"></i>
                            {{$values->fieldValue->form_field_option_name}} 
                          </label>
                          @endif
                          @if($values->field->field_type == 'text' || $values->field->field_type == 'textarea')
                          <div class="answers remarsk">
                            <p><strong>{{$values->field->field_name}}</strong> :{{optional($values)->value}} </p>
                          </div>
                          @endif
                          @if($values->field->field_type == 'file')
                          <div calss="answers">
                            <h4>Attched File</h4>
                            <div class="clearfix attchedfile">
                              <a href="{{ asset('/form_images/f_id_'.$process_id.'/'.$values->value) }}" target="_blank">
                                <img src="{{ asset('/form_images/f_id_'.$process_id.'/'.$values->value) }}" alt="noimages" onerror="this.onerror=null;this.src='{{ asset("/dist/img/noimage.jpg") }}';" >
                              </a>
                            </div>
                          </div>
                          @endif
                        
                        @endforeach
                        </div>
                       <div class="text-right footer"> 
                        <div class="col-md-6 ">
                          <p class="text-left timeshow"> <strong>BGT StartTime</strong> :{{optional($question)->bgt_start_time}}
                            <strong>BGT EndTime</strong> :{{optional($question)->bgt_end_time}}</p>
                            
                          </div>
                          <div class="col-md-6">
                           <p class="text-right timeshow"><strong>Act StartTime</strong> :{{optional($question)->act_start_time}}
                            <strong>Act EndTime</strong> :{{optional($question)->act_end_time}}</p>
                         </div>
                        </div>
                   </div>
                   @empty
                   !! No Questions Found !!
                   @endforelse  
              </div>
            <!-- /.card-body -->
          </div>
          <div class="col-md-12 text-right pagination" >
             
            </div>
          <!-- /.card -->
      </div>
    </section>
@endsection


@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script>
  var APP_URL = {!! json_encode(url('/')) !!}
   function reloadPageForStore(store_id) {
    var process_id = $('#process').val();
    var date = $('#filter_date').val();
    document.location.href = APP_URL + '/submittedQuestions?stid=' + store_id+'&prosid='+process_id+'&dt='+date;
   }
   function reloadPageForProcess(process_id) {
     var store_id = $('#store').val();
     var date = $('#filter_date').val();
    document.location.href = APP_URL + '/submittedQuestions?stid=' + store_id+'&prosid='+process_id+'&dt='+date;
   }
  $("#datepicker").datepicker({
        autoclose: true,
        todayHighlight: true,
        format:'dd-mm-yyyy'
  });

  $('#datepicker').on('changeDate', function(event) {
      var date = event.format();
      var process_id = $('#process').val();
      var store_id = $('#store').val();
      document.location.href = APP_URL + '/submittedQuestions?stid=' + store_id+'&prosid='+process_id+'&dt='+date;
  });
</script>
@endsection