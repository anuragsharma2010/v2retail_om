@extends('layouts.app')

@section('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet"
    type="text/css" />
 <link href="{{asset('progress-bar/jQuery-plugin-progressbar.css')}}" rel="stylesheet"
    type="text/css" />
<style>
    label {
        margin-left: 20px;
    }

    #datepicker {
        width: 180px;
        margin: 20px 0 0;
    }

    #datepicker>span:hover {
        cursor: pointer;
    }

    #datepicker input.form-control {
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
    }

    #datepicker .input-group-addon {
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
    }

    .progress-bar{
        background:transparent;
        border-bottom:none;
        box-shadow:none;
        margin:10px 0px;
    }
.progress-bar div span{
    color:#4b86db;
}
.justify_content_center{
    justify-content:center;
    align-items: center;
}
</style>
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1>
      Store List
   </h1>
   <ol class="breadcrumb">
      <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
      <li class="active">Store List</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="card listcontainer">
            <div class="card-header">
                <div class="col-sm-12 list_filter_head">
                    <div class="search_box d-flex" style="align-items:center;">
                        <!-- search form -->
                        <form action="#" method="get" class="sidebar-form">
                            <div class="input-group">
                                <input type="text" name="searchtxt" value="<?php echo isset($stores->searchtxt) ? $stores->searchtxt : ''; ?>" class="form-control search" placeholder="Search...">
                                <span class="input-group-btn">
                                <button type="submit" name="search" value="save" id="search-btn" class="btn btn-flat">
                                  <i class="fa fa-search"></i>
                                </button>
                              </span>
                            </div>
                        </form>


                        <div class="label-and-select-together">                       
                            <form class="form-inline">
                                <label>Process Name</label>
                                <select class="firstone" id="process" onchange="reloadPage(this.value)">

                                    @foreach($processes as $process)
                                    <option value="{{base64_encode($process->id)}}" @php if(base64_encode($process->id)
                                        == base64_encode($process_id)){ echo 'selected'; } @endphp
                                        >{{optional($process)->form_label}}
                                    </option>
                                    @endforeach
                                </select>
                            </form>
                    </div>
                      </div>
                    
                    <div class="userbtn">
                        <form>
                            <div id="datepicker" class="input-group date" style="margin-top:10px;" data-date-format="dd-mm-yyyy">
                                <input class="form-control" id="filter_date" type="text" value="{{ app('request')->input('dt')??\Carbon\Carbon::now()->format('d-m-Y') }}" readonly />
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if ($message = Session::get('success'))
                <div class="message" form="alert">
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                </div>
                @endif
                <div class="table_data storelist">
                    <table class="table table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Store Name</th>
                                <th>Score (%)</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($stores as $key=>$store)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$store->st_name}}</td>
                                <td>
                                @if($store->percentage==0)
                                    <span class="badge bg-red">{{$store->percentage}}%</span>
                                @elseif(($store->percentage > 0) && ($store->percentage < 100))
                                    <span class="badge bg-orange">{{$store->percentage}}%</span>
                                @else
                                    <span class="badge bg-green">{{$store->percentage}}%</span>
                                @endif
                                </td>
                                <td> <a class="btn btn-primary btn_dis  view_question"
                                        id="{{base64_encode($store->id)}}" href="#" data-toggle="tooltip"
                                        title="View!"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            @empty
                            <tr>!! No Record Found !!</tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <div class="col-md-12 text-right pagination">
            </div>
            <!-- /.card -->
        </div>
</section>
@endsection



@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script src="{{asset('progress-bar/jQuery-plugin-progressbar.js')}}"></script>
<script>
    $("#datepicker").datepicker({
        autoclose: true,
        todayHighlight: true
    });
    
    $('#datepicker').on('changeDate', function(event) {
        var date = event.format();
        var process_id = $('#process').val();
        document.location.href = APP_URL  + '/assignedstore/' + process_id + '?dt='+date;
    });

    var APP_URL = {!!json_encode(url('/')) !!}
    function reloadPage(id) {
        var date = $('#filter_date').val();
        document.location.href = APP_URL  + '/assignedstore/' + id + '?dt='+date;
    }

    $(document).ready(function () {
        $('.to333').datepicker({
            autoclose: true,
            minViewMode: 1,
            format: 'mm-yyyy',
            default: true
        });
        $('.view_question').click(function () {
            var date = $('#filter_date').val();
            var store_id = $(this).attr('id');
            var process_id = $('#process').val();
            var url = APP_URL + '/submittedQuestions?stid=' + store_id + '&prosid=' + process_id+ '&dt='+date;
    
            window.open(url, '_blank');
            window.focus();
            //  document.location.href = APP_URL + '/submittedQuestions?stid=' + store_id+'&prosid='+process_id;
        });
    });
    $('#month_year').on("change", function () {
        var monthYear = $(this).val();
        //alert(monthYear);
        //var url = APP_URL+"/{{Request::segment(1)}}?month="+monthYear+"&perpage="+perPage;
        //window.location.href = url;
    });

</script>
    <script>
        $(".progress-bar").loading();
        $('input').on('click', function () {
             $(".progress-bar").loading();
        });
    </script>
@endsection
