@extends('layouts.app')


@section('content')
<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0 text-dark">Add New Question | <sub><a href="{{ route('questions.index') }}">Back</a></h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


 <section class="content">
      <div class="container-fluid roleform">
        <div class="card card-default">
          <div class="card-body">
             <div class="form_container">
            @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <strong>Whoops!</strong> There were some problems with your input.<br><br>
                      <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                  </div>
              @endif
           {!! Form::open(array('route' => 'questions.store','method'=>'POST', 'class' => 'formcustom')) !!}
             <div class="clearfix">      
                <div class="form-group clearfix">
                 <label for="Name" class="col-md-3 col-form-label label">Score </label>
                 <div class="col-md-9">
                 <select name="score" class="form-control customtinput">
                    <option>Short Answer</option>
                    <option>Description Answer</option>
                    <option>Multiple Choice Single Answer</option>
                    <option>Multiple Choice Multiple Answer</option>
                 </select>
                </div>
              </div>
                  <div class="form-group clearfix">
                 <label for="email" class="col-md-3 col-form-label label">Questions</label>
                <div class="col-md-9">
                <textarea name="questions" class="form-control"></textarea>
                </div>
                </div>
                <div class="form-group clearfix">
                 <label for="Address" class="col-md-3 col-form-label label">Mandatory Question</label>
                 <div class="col-md-9">
                  <input type="text" name="Address" id="Address" class="form-control customtinput" placeholder="Enter store Address ..">
                 </div>
                </div>
                 <div class="form-group clearfix">
                 <label for="Area" class="col-md-3 col-form-label label">Answer Options</label>
                 <div class="col-md-9">
                  <select name="options" class="form-control customtinput">
                     <option>None</option>

                  </select>
                 </div>
                </div>
                 <div class="form-group clearfix">
                 <label for="Country" class="col-md-3 col-form-label label">Advance Options </label>
                 <div class="col-md-9">
                  <div class="icheck-primary">
                  <input type="checkbox" value="" name="Allow_cmt" id="Allow_cmt" class="name">
                  <label for="Allow_cmt">Allow Comments</label>
                </div>
                  <div class="icheck-primary">
                  <input type="checkbox" value="" name="Allow_Att" id="Allow_Att" class="name">
                  <label for="Allow_Att">Allow Attachments</label>
                </div>
                 <div class="icheck-primary">
                  <input type="checkbox" value="" name="response_ques" id="response_ques" class="name">
                  <label for="response_ques">Link this question to the response of another question</label>
                </div>
              </div>
              </div>
                <div class="form-group clearfix">
                  <div class="col-md-9 pull-right">
                  <div class="icheck-primary">
                  <input type="checkbox" value="" name="add_another" id="add_another" class="name">
                  <label for="add_another">Add Another</label>
                </div>
                </div>
              </div>
              <div class="form-group clearfix">
               <div class="col-md-9 pull-right">
                 <button type="submit" class="btn btn-primary btn-submit">Add </button> 
                 <button type="submit" class="btn btn-primary btn-submit">Cancel </button>
             </div>
            </div>
            </div>
          {!! Form::close() !!}
        </div>
        </div>
        </div>
    </div>
</section>


    
@endsection