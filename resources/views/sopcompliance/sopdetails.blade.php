@extends('layouts.app')

@section('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
<style>
label{margin-left: 20px;}
#datepicker{width:180px; margin: 0 20px 20px 20px;}
#datepicker > span:hover{cursor: pointer;}
#datepicker input.form-control{
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
}
#datepicker .input-group-addon{
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
}

.question_box:nth-child(3n-1) .info-box-number {
 color:#dd4b39 !important;
}
.question_box:nth-child(3n-1) .badge.badge-primary{
   background:#dd4b39 !important;
}
.question_box:nth-child(3n-1) .progress-bar{
   background:#dd4b39 !important;
}
.question_box:nth-child(3n+1) .info-box-number {
 color:#00a65a !important;
}
.question_box:nth-child(3n+1) .badge.badge-primary{
   background:#00a65a !important;
}
.question_box:nth-child(3n+1) .progress-bar{
   background:#00a65a !important;
}
.question_box:nth-child(3n+1) .info-box-number {
 color:#f39c12 !important;
}
.question_box:nth-child(3n+1) .badge.badge-primary{
   background:#f39c12 !important;
}
.question_box:nth-child(3n+1) .progress-bar{
   background:#f39c12 !important;
}
</style>
@endsection

@section('content')

 <!-- Content Header (Page header) -->
  <section class="content-header">
      <h1>
       V2 Compliance Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">V2 Shop Compliance</li>
      </ol>
    </section>

 <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
  <!------ Not answered Store name with question --->

<div class="row">
     <div class="col-md-12">
       <?php //dd($data); ?>
       @if (!empty($data))
          <div class="card"><div class="card-header">  <h4 class="heading-5">Q. <?php echo $data[0]->question_alias; ?></h4></div>
          <div class="card-body storewith">
                <div class="table_data" style="width:65%;">
                  <table class="table table-bordered table-stripd">
                     <thead>
                        <tr>
                             <th>S. NO.</th>
                             <th>Store Name</th>
                             <th>Store Code</th>
                             <th>Store Desc</th>
                        </tr>
                     </thead>
                     <tbody>
                       <?php foreach ($data as $row){ ?>
                         <tr>
                           <td><?php echo $row->id; ?></td>
                          <td><?php echo $row->st_name; ?></td>
                          <td><?php echo $row->st_code; ?></td>
                          <td><?php echo $row->st_desc; ?></td>
                         </tr>
                         <?php } ?>
                     </tbody>
                  </table>
              </div>
          </div>
        </div>
      @endIf
     </div>
</div>

<!------ Not answered Store name with question --->



        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>


 </div>
</section>
@endsection

@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script>
$("#datepicker").datepicker({
      autoclose: true,
      todayHighlight: true
}).datepicker('update', new Date());
</script>
@endsection
