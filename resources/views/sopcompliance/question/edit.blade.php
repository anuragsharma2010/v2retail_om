@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0 text-dark">Add New Question | <sub><a href="{{ route('questions.index') }}">Back</a></h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


<section class="content">
    <div class="container-fluid roleform">
          <div class="card card-default">
              <div class="card-body">
                  <div class="form_container">
                      @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                        @endif
                        {!! Form::open(array('route' => 'questions.store','method'=>'POST', 'class' => 'formcustom')) !!}
                        <div class="clearfix">      
                            <div class="form-group clearfix">
                                <label for="email" class="col-md-3 col-form-label label">Question</label>
                                <div class="col-md-9">
                                  <textarea name="question"  class="form-control">{{$formQuestion->question}}</textarea>
                                  @error('question')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                  @enderror
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <label for="sort_order" class="col-md-3 col-form-label label">Sort Order</label>
                                <div class="col-md-9">
                                  <input type="text" value="{{$formQuestion->sort_order}}" name="sort_order" class="form-control" />
                                  @error('sort_order')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                  @enderror
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <div class="col-md-9 pull-right">
                                  <div class="icheck-primary">
                                  <input type="checkbox" value="" name="add_another" id="add_another" class="name">
                                  <label for="add_another">Add Another</label>
                                  </div>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                              <div class="col-md-9 pull-right">
                                  <button type="submit" class="btn btn-primary btn-submit">Add </button> 
                                 <!-- <button type="submit" class="btn btn-primary btn-submit">Cancel </button> -->
                              </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>


    
@endsection