@extends('layouts.app')


@section('content')
<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-5">
            <h1 class="m-0 text-dark">Questions</h1>
          </div><!-- /.col -->
          <div class="col-sm-7 list_filter_head">
             <div class="search_box">
                 <!-- search form -->
                <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control search" placeholder="Search...">
                        <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                          <i class="fa fa-search"></i>
                        </button>
                      </span>
                    </div>
                </form>
              </div>
              <div class="userbtn">
              <a href="{{ route('questions.create') }}" class="btn btn-sm btn-primary createuser-btn">
                      <i class="fas fa-user-plus"></i>  Add Questions
              </a>
            </div>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
       <div class="container-fluid">
             <div class="card listcontainer">
            <!-- /.card-header -->
            <div class="card-body list_body">
              <div class="table_data">
              <table class="table table-stripd">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Question</th>
                    <th>Sort Order</th>
                    <th>status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                @php $i=1; @endphp
                @forelse($formQuestions as $question)
                  <tr>
                     <td>{{$i++}}</td>
                     <td>{{$question->question}}</td>
                     <td>{{$question->sort_order}}</td>
                     <td>{{!empty($question->status) ? 'Active' : 'Inactive'}}</td>
                     <td>@can('form-edit')
                     <a class="btn btn-primary btn_dis" href="{{ route('questions.edit',$question->id) }}" data-toggle="tooltip" title="Edit!"><i class="fa fa-edit"></i></a>
                      @endcan
                      @can('form-delete')
                      {!! Form::open(['method' => 'DELETE','route' => ['questions.destroy', $question->id],'style'=>'display:inline']) !!}
                          {!! Form::submit('DELETE', ['class' => 'btn btn-danger btn_dis fa fa-archive delete_btn']) !!}

                      {!! Form::close() !!}
                      @endcan</td>
                  </tr>
                  @empty
                    No record Found !! 
                  @endforelse
                </tbody>
              </table>
              <div class="col-md-12 text-right pagination" >
                {!! $formQuestions->render() !!}
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
      </div>
    </section>
@endsection