@extends('layouts.app')

@section('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet"
    type="text/css" />
<style>
    label {
        margin-left: 20px;
    }

    #datepicker {
        width: 180px;
        margin: 0 20px 20px 20px;
    }

    #datepicker>span:hover {
        cursor: pointer;
    }

    #datepicker input.form-control {
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
    }

    #datepicker .input-group-addon {
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
    }

    .question_box:nth-child(3n-1) .info-box-number {
        color: #dd4b39 !important;
    }

    .question_box:nth-child(3n-1) .badge.badge-primary {
        background: #dd4b39 !important;
    }

    .question_box:nth-child(3n-1) .progress-bar {
        background: #dd4b39 !important;
    }

    .question_box:nth-child(3n+1) .info-box-number {
        color: #00a65a !important;
    }

    .question_box:nth-child(3n+1) .badge.badge-primary {
        background: #00a65a !important;
    }

    .question_box:nth-child(3n+1) .progress-bar {
        background: #00a65a !important;
    }

    .question_box:nth-child(3n+1) .info-box-number {
        color: #f39c12 !important;
    }

    .question_box:nth-child(3n+1) .badge.badge-primary {
        background: #f39c12 !important;
    }

    .question_box:nth-child(3n+1) .progress-bar {
        background: #f39c12 !important;
    }
    .info-box-text.menu{
             font-size:20px;
            white-space:pre-wrap;
        }
</style>
@endsection

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        V2 Compliance Dashboard
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">V2 Shop Compliance</li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-sm-12 list_filter_head py-1 hide">
                <div class="search_box pl-15">
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form mt-0">
                        <div class="input-group">
                            <input type="text" name="search" class="form-control search" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="userbtn">
                    <form>
                        <div id="datepicker" class="input-group date" data-date-format="mm-dd-yyyy">
                            <input class="form-control" type="text" readonly />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row dashboard_block">
            @foreach($data as $row)
                @if(($row->show_dashboard==1) && ($row->question!=''))
                    <div class="col-md-3 col-sm-6 col-xs-12 question_box">
                        <div class="info-box">
                            <div class="info-box-content">
                            <a href="{{ route('sopDetails',$row->id) }}">
                               <span class="info-box-text menu"><?php echo $row->question; ?></span>
                                </a>
                                <div class="d-flex justify_content_between_space"><span
                                        class="info-box-number "><?php echo $row->count; ?></span><span
                                        class="percentage"><small
                                            class="badge badge-primary hide"><?php echo round(($row->count/$row->totalStore)*100,0); ?>%</small></span>
                                </div>
                                <div class="progress-group">
                                    <div class="progress sm">
                                        <div class="progress-bar"
                                            style="width:<?php echo round(($row->count/$row->totalStore)*100,0); ?>%"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                @endif
            @endforeach
        </div>

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>


    </div>
</section>
@endsection

@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script>
    $("#datepicker").datepicker({
        autoclose: true,
        todayHighlight: true
    }).datepicker('update', new Date());

</script>
@endsection
