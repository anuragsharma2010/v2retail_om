@extends('layouts.app')


@section('content')
<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0 text-dark">Form | <sub><a href="{{ route('forms.index') }}">Back</a></h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
<!-- Main content -->
    <section class="content">
      <div class="container-fluid roleform">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <!-- /.card-header -->
          <div class="card-body">
           <div class="question_module">
           	 <h2 class="heading-2">Form</h2>
           	 <hr/>
              @if (count($errors) > 0)

              <div class="alert alert-danger">
                  <strong>Whoops!</strong> There were some problems with your input.<br>
                  <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                  </ul>
              </div>
              @endif
            {!! Form::open(array('route' => 'forms.store','method'=>'POST', 'class' => 'form-horizontal')) !!}
             <div class="clearfix">
                 <div class="form-group">
                 <label for="form_name" class="col-sm-2 col-form-label label">Form Name</label>
                 <div class="col-sm-6"> 
                  <input type="text" name="form_name" id="form_name" class="form-control customtinput" placeholder="Enter Form Name" required/>
                 </div>
                 <div class="col-md-4">
                  <div class="icheck-primary">
                  <!--<input type="checkbox" value="exclude_process" name="exclude_process" id="exclude_process" class="name">
                  <label for="exclude_process">Exclude this process form search</label> -->
                </div>
                 </div>
                </div>
                <div class="form-group">
                 <label for="form_label" class="col-sm-2 col-form-label label">Form Label</label>
                  <div class="col-sm-6"> 
                  <input type="text" name="form_label" id="form_label" class="form-control" placeholder="Enter Form Label" required/>
                 </div>
                </div>
                <div class="form-group">
                 <label for="process_Objective" class="col-sm-2 col-form-label label">Process Objective</label>
                  <div class="col-sm-6"> 
                  <textarea name="process_Objective" id="process_Objective" class="form-control" placeholder="To test the process"></textarea>
                 </div>
                </div>
                <div class="form-group">
                 <label for="Instruction" class="col-sm-2 col-form-label label">Instruction</label>
                  <div class="col-sm-6"> 
                  <textarea name="Instruction" id="Instruction" class="form-control" placeholder="To tell the process"></textarea>
                 </div>
                </div>
                 <!--<div class="form-group">
                 <label for="Instruction" class="col-sm-2 col-form-label label">Coverage</label>
                  <div class="col-sm-10"> 
                  <fieldset class="fieldset">
                  <div class="icheck-primary d-inline">
                        <input type="radio" id="Coverage1" name="Coverage" >
                        <label for="Coverage1">Clients
                   </label>
                 </div>
                  <div class="icheck-primary d-inline">
                        <input type="radio" id="Coverage2" name="Coverage">
                        <label for="Coverage2">Store
                  </label>
                 </div>
                 <div class="icheck-primary d-inline">
                        <input type="radio" id="Coverage3" name="Coverage" >
                        <label for="Coverage3">Modules
                  </label>
                 </div>
                  <div class="icheck-primary d-inline">
                        <input type="radio" id="Coverage4" name="Coverage" >
                        <label for="Coverage4">Users
                  </label>
                 </div>
                  <div class="icheck-primary d-inline">
                        <input type="radio" id="Coverage5" name="Coverage" >
                        <label for="Coverage5">Others
                  </label>
                 </div>
                   <div class="icheck-primary d-inline">
                        <input type="radio" id="Coverage6" name="Coverage" >
                        <label for="Coverage6">Process
                  </label>
                 </div>
                  <div class="icheck-primary d-inline">
                        <input type="radio" id="Coverage7" name="Coverage" >
                        <label for="Coverage7">Resources
                  </label>
                 </div>
                 </fieldset>
                 </div>
                </div>
                
                 <div class="form-group">
                 <label for="Instruction" class="col-sm-2 col-form-label label">Select Stores</label>
                  <div class="col-sm-5"> 
                  <label>V2 Retail Ltd.(HO) New Delhi</label><br/>
                  <a href="#">Select Store By Clicking Here</a>
                 </div>
                 <div class="col-md-5">
                  <div class="icheck-primary">
                  <input type="checkbox" value="addstore" name="addstore" id="addstore" class="name">
                  <label for="addstore">Add all stores and Automatically add new stores</label>
                 </div>
                </div>
                 </div>
                   <div class="form-group">
                 <label for="Instruction" class="col-sm-2 col-form-label label">Occurenace</label>
                  <div class="col-sm-6"> 
                  <div class="icheck-primary d-inline">
                        <input type="radio" id="priodic" name="Occurenace" >
                        <label for="priodic">priodic
                  </label>
                 </div>
                  <div class="icheck-primary d-inline">
                        <input type="radio" id="Single" name="Occurenace" >
                        <label for="Single">Single
                  </label>
                 </div>
                 </div>
                </div>
                      <div class="form-group">
                 <label for="Timezone" class="col-sm-2 col-form-label label">Timezone</label>
                  <div class="col-sm-6"> 
                   <select name="timezone" class="form-control customtinput">
                    <option hidden="true">Select Timezone</option>
                   </select>
                 </div>
                </div>
                <div class="form-group">
                 <label for="Periodicity" class="col-sm-2 col-form-label label">Periodicity</label>
                  <div class="col-sm-6"> 
                   <select name="timezone" class="form-control customtinput">
                    <option hidden="true">Select Periodicity</option>
                   </select>
                 </div>
                </div>
                <div class="form-group">
                 <label for="Periodicity" class="col-sm-2 col-form-label label">Cut-off</label>
                  <div class="col-sm-6"> 
                     <div class="icheck-primary">
	                  <input type="checkbox" value="Start-Time" name="Start-Time" id="Start-Time" class="name">
	                  <label for="Start-Time">Has a Start-Time</label>
	                 </div>
	                 <div class="timefield">
	                   <label class="time" >Time </label>
                       <select name="timehours" class="form-control timehours customtinput">
                         <option>9</option>
                       </select>
                        <select name="timesec" class="form-control timesec customtinput">
                         <option>45</option>
                       </select>
                        <select name="timemeridiem" class="form-control timemeridiem customtinput">
                         <option>AM</option> <option>PM</option>
                       </select>
	                 </div>
	                  <div class="icheck-primary">
	                  <input type="checkbox" value="Cut-off" name="Cut-off" id="Cut-off" class="name">
	                  <label for="Cut-off">Has a Cut-Off</label>
	                 </div>
	                 <div class="timefield">
	                   <label class="time" >Time </label>
                       <select name="timehours" class="form-control timehours customtinput">
                         <option>9</option>
                       </select>
                        <select name="timesec" class="form-control timesec customtinput">
                         <option>45</option>
                       </select>
                        <select name="timemeridiem" class="form-control timemeridiem customtinput">
                         <option>AM</option> <option>PM</option>
                       </select>
	                 </div>
	                <div class="afterclearfix">
                        <div class="icheck-primary d-inline">
                        <input type="radio" id="allow_cut_off" name="allow" >
                        <label for="allow_cut_off">Allow after cut-off
                  </label>
                 </div>
                  <div class="icheck-primary d-inline">
                        <input type="radio" id="not_allow_cut_off" name="allow" >
                        <label for="not_allow_cut_off">Do not allow after cut-off
                  </label>
                 </div>
             </div>
                 </div>
                </div>
                 <div class="form-group">
                 <label for="Periodicity" class="col-sm-2 col-form-label label">Location</label>
                  <div class="col-sm-10"> 
                      <div class="icheck-primary">
	                  <input type="checkbox" value="inside_store" name="inside_store" id="inside_store" class="name">
	                  <label for="inside_store">To be filled inside store</label>
	                 </div>
	                      <div class="afterclearfix">
                        <div class="icheck-primary d-inline">
                        <input type="radio" id="allow_submit_outside" name="allow_people" >
                        <label for="allow_submit_outside">Allow people to submit outside the fence
                          </label>
		                 </div>
		                  <div class="icheck-primary d-inline">
		                        <input type="radio" id="do_not_submit_outside" name="allow_people" >
		                        <label for="do_not_submit_outside ">Do not Allow people to submit outside the fence
		                  </label>
		                 </div>
                     </div>
                 </div>
                 </div>-->
                </div>
             <div class="col-md-12 text-center">
                 <button type="submit" class="btn btn-primary btn-submit">Submit</button>
             </div>
            </div>
            {!! Form::close() !!}
        </div>
        </div>
       </div>
    </div>
</section>


{!! Form::close() !!}


@endsection