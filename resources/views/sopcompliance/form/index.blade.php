@extends('layouts.app') 
@section('style')
<link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1>
      Process List
   </h1>
   <ol class="breadcrumb">
      <li><a href="{{ route('home')}}"><i class="fa fa-home"></i>Home</a></li>
      <li><a href="{{ route('sopDashboard')}}">V2 Shop Compliance</a></li>
      <li class="active">Process List</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="container-fluid">
   <div class="card listcontainer">
   <div class="card-header">
      <div class="col-sm-12 list_filter_head">
         <div class="search_box">
            <!-- search form -->
            <form action="#" method="get" class="sidebar-form">
               <div class="input-group">
                  <input type="text" name="searchtxt" value="<?php echo isset($forms->searchtxt) ? $forms->searchtxt : ''; ?>" class="form-control search" placeholder="Search...">
                  <span class="input-group-btn">
                  <button type="submit" name="search" value="save" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                  </button>
                  </span>
               </div>
            </form>
         </div>
         {!! Form::open(array('url' => 'process-report','method'=>'POST')) !!}
           
             <div class="col-sm-3 " style="margin-top:10px;">
                <select class="form-control" name="process" required>
                  <option value="">Select Process</option>
                  @foreach($forms as $process)
                   <option value="{{$process->id}}">{{$process->form_label}}</option>
                  @endforeach
                </select>
             </div>
             <div class="col-sm-3 " style="margin-top:10px;" >
                 <div class="input-group">
                  <div class="input-group-prepend">
                     <span class="input-group-text">
                     <i class="far fa-calendar-alt"></i>
                     </span>
                  </div>
                  <input type="text" name="month_year" id="month_year" class="form-control dateinput form-control-2 to" placeholder="Select month and year" value="{{ \Carbon\Carbon::now()->format('m-Y') }}">
               </div>
               </div>
             <div class="col-sm-1">
              <button type="submit" class="btn btn-primary btn-submit">Export</button>
            </div>
           {!! Form::close() !!}
         <div class="userbtn d-flex">
            <div class="clearfix">
            
            <a href="javascript:void(0);" class="btn btn-primary createuser-btn pt-0 " onclick="processStatus('1');">
            Active
            </a>
            <a href="javascript:void(0);" class="btn btn-primary mx-10 createuser-btn pt-0" onclick="processStatus('0');">
            Inactive
            </a>
            <a href="{{route('forms.create')}}" class="btn btn-primary createuser-btn pt-0">
            <i class="fa fa-plus-circle"></i> Add Process
            </a>
            </div>
         </div>
      </div>
   </div>
   <!-- /.card-header -->
   <div class="card-body">
      @if ($message = Session::get('success'))
      <div class="message" form="alert">
         <div class="alert alert-success">
            <p>{{ $message }}</p>
         </div>
      </div>
      @endif
      <!-- <div class="container-fluid p-0 show-enteries-parent">
         <div class="col-md-6 p-0">
            <label class="show-enteries-text">
               Show enteries
               <select id="item-show-per-page-select" class="item-show-per-page-select-id">
                  <option value="10">10</option>
                  <option value="50">50</option>
                  <option value="150">150</option>
                  <option value="500">500</option>
               </select>
            </label>
         </div>
         <div class="col-md-6 p-0">
            <p class="showing-enteries-text text-right">Showing 1 to 10 of 57 entries</p>
         </div>
      </div> -->
      <div class="table_data">
         <table class="table table-stripd table-bordered Process">
            <thead>
               <tr>
                  <th>
                     <div class="icheck-primary my-0">
                        <input type="checkbox" name="SelectAll" id="SelectAll">
                        <label for="SelectAll" class="selectall"></label>
                     </div>
                  </th>
                  <th>Process Full Name</th>
                  <th>Process Short Name</th>
                  <th>Created On</th>
                  <th>Status</th>
                  <th>Action</th>	
               </tr>
            </thead>
            <tbody>
               {{ Form::open(array('method'=>'post','class'=> 'col-md-6','url' => '#', 'id'=>'process-form')) }} @php $i=1; @endphp @forelse ($forms as $key => $form)
               <tr>
                  @php $arr=array('1'=>'Audit','2'=>'Data Collection','3'=>'Assesment'); @endphp
                  <td align="left">
                     <div class="icheck-primary my-0">
                        <input type="checkbox" name="IndivualSelect" id="IndivualSelect{{ $i }}" class="IndivualSelect" value="{{$form->id}}">
                        <label for="IndivualSelect{{ $i }}"></label>
                  </td>
                  <td>{{ $form->form_name }}</td>
                  <td>{{ $form->form_label }}</td>
                  <td>{{ \Carbon\carbon::parse($form->created_at)->format('d-m-Y') }}</td>
                  <td>{{ !empty($form->status) ? 'Active' : 'Deactive' }}</td>
                  <td>
                  @can('form-edit')
                  <a class="btn btn-primary btn_dis" href="{{ route('forms.edit',$form->id) }}" data-toggle="tooltip" title="Edit!"><i class="fa fa-edit"></i></a> @endcan @can('form-delete') {!! Form::open(['method' => 'DELETE','route' => ['forms.destroy', $form->id],'style'=>'display:inline']) !!}
                  <!--button type="submit" class="btn btn-danger btn_dis delete_btn"><i class="fa fa-archive"></i></button-->
                  {!! Form::close() !!} @endcan
                  <a class="btn btn-primary btn_dis" href="{{ route('assignedstore',base64_encode($form->id)) }}" title="View"><i class="fa fa-eye"></i></a>
                  </td>
               </tr>
               @php $i++ @endphp
               @empty
               <tr>
               <td colspan="3">
               !! No Record Found !!
               </td>
               </tr>
               @endforelse
               {{ Form::close() }}
            </tbody>
         </table>
         </div>
      </div>
      <div class="col-md-12 text-right pagination">
         {!! $forms->render() !!}
      </div>
   </div>
</section>
@endsection

@section('custom-js')
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script>
  $(document).ready(function() {
    $(document).on('click', 'label.selectall, input#SelectAll', function(){
        if($('input#SelectAll').is(":checked")){
            $('input.IndivualSelect').attr('checked', true);
        }else{
          $('input.IndivualSelect').attr('checked', false);
        }   
  });

});

jQuery(function() {
    jQuery('[data-check-pattern]').checkAll();
});

;(function($) {
    'use strict';
    
    $.fn.checkAll = function(options) {
        return this.each(function() {
            var mainCheckbox = $(this);
            var selector = mainCheckbox.attr('data-check-pattern');
            var onChangeHandler = function(e) {
                var $currentCheckbox = $(e.currentTarget);
                var $subCheckboxes;
                
                if ($currentCheckbox.is(mainCheckbox)) {
                    $subCheckboxes = $(selector);
                    $subCheckboxes.prop('checked', mainCheckbox.prop('checked'));
                } else if ($currentCheckbox.is(selector)) {
                    $subCheckboxes = $(selector);
                    mainCheckbox.prop('checked', $subCheckboxes.filter(':checked').length === $subCheckboxes.length);
                }
            };
            
            $(document).on('change', 'input[type="checkbox"]', onChangeHandler);
        });
    };
}(jQuery));

  // $(document).ready(function() {
   //  $(document).on('click', 'label.selectall, input#SelectAll', function(){
      //   if($('input#SelectAll').is(":checked")){
        //     $('input.IndivualSelect').attr('checked', true);
       //  }else{
      //     $('input.IndivualSelect').attr('checked', false);
      //   }
  // });
   
  // });
   
   function processStatus(status){
        var checkedVal = [];
        $("input:checkbox[name='IndivualSelect']:checked").each(function(){
         checkedVal.push($(this).val());
     });
        $.ajax({
            type: "POST",
            url: '{{ route('process_status')}}',
            data: {"_token": "{{ csrf_token() }}", data:checkedVal, status:status},
            success: function( msg ) {
             if(msg.status==1){
               location.reload(true);
             }
            }
        });
    }

    $(document).ready(function () {
       $('.to').datepicker({
           autoclose: true,
           minViewMode: 1,
           format: 'mm-yyyy',
           default: true
       })
       $('#month_year').on("change",function(){
           var monthYear = $(this).val();
           $("#process-report").attr("href", APP_URL+"/process-report?dt="+monthYear)
       });

   });
   
</script>
@endsection