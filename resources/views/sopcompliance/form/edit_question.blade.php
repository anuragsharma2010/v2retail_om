@extends('layouts.app')
@section('content')
@section('style')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/css/bootstrap-timepicker.css"/>
   <link href="{{ asset('/css/jquery.datetimepicker.min.css') }}" rel="stylesheet" />
   <style>
    .form-group label {
    font-weight: 600;
    color: #333;
    padding-left: 0px;
    margin-bottom: 5px;
    display: inline-block;
    }
    .form-group.inputwith input.form-control{
        width:582;
        max-width:100%;
    }
    .remove_field_option_val_group.plus_btn {
      position: absolute;
    right: -30px;
    top: 19px;
    font-size: 32px;
    font-weight: 200;
    }
   #mulitselectCheck, #groups{
    height:50px;
    }
    button.multiselect.dropdown-toggle.btn.btn-default {
        height: 50px;
        text-align: left;
        color: #777;
        font-family: Roboto;
        border-radius: 5px;
        font-size: 16px;
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
    .multiselect.dropdown-toggle + .multiselect-container.dropdown-menu{
     max-width: 100%;
        width: 100%;
    }
    .multiselect-container>li>a>label.checkbox {
        font-size:16px;
    }
    .dropdown-menu>.active>a, .dropdown-menu>.active>a:focus, .dropdown-menu>.active>a:hover{
       background:transparent;
    }
    .form-group label.without_label span {
    font-weight:600;
    width:100%;
    display: inline;
    padding-right:5px;
}
.form-group label.without_label span.full_lenth{
    width:19%;
    max-width:100%;
}
label.label.without_label, label.label.question_label{
    width:100%;
    font-weight:300;
    white-space: inherit;
}
.form-group label.question_label span{
   font-weight:600;
    width:100%;
    display: inline;
    padding-right:10px;
}
.heading-2{
    margin-bottom:15px;
}
.pl-0{
    padding-left:0px;
}
fieldset.savedValue{
    border: 2px solid #e0e0e0;
    padding: 15px;
    margin-bottom: 14px;
    border-radius: 5px;
}
legend.legend_title {
    background: #e0e0e0;
    border-radius: 5px;
    padding: 6px 15px;
}
.panel-primary > .panel-heading {
    border-top-right-radius: 5px;
    color: #fff;
    background-color: #3D3D3D;
    border-color: #337ab71c;
    padding: 18px 30px;
    border-top-left-radius: 5px;
}
.panel-body{
    border:1px solid #e0e0e0;
    border-top:0px;
}
.table_time{
    width:60%;
    max-width:100%;
}
.brdr_defaul{
 border-radius:5px !important;
}
.table-responsive.table_time table tr td, .table-responsive.table_time table tr th {
    padding: 12px 14px;
}
.table-responsive.table_time table tr th{
    background:#ddd;
}
a.remove_field_group.plus_btn {
    width: 35px;
    height: 35px;
    border: 2px solid #FE0000;
    display: block;
    text-align: center;
    line-height: 32px;
    border-radius: 50%;
   display: inline-block;
}
.bootstrap-timepicker-widget table td input{
    border:none;
}
</style>
@endsection
<!-- Content Header (Page header) -->
<section class="content-header">
      <h1> Process </h1>
      <ol class="breadcrumb">
         <li><a href="/"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="{{route('sopDashboard')}}">V2 Shop Compliance</a></li>
        <li><a href="{{ route('forms.index') }}">Process List</a>
        <li class="active">Add Question</li>
      </ol>
</section>
<section class="content">
    <div class="container-fluid roleform">
        <div class="card card-default">
            <div class="card-body">
                <div class="question_module">
                    <div class="clearfix">
                      <fieldset class="savedValue"> <legend class="legend_title">Process Details</legend>
                        <div class="clearfix">
                            <div class="col-md-6 right_pdng">
                                <div class="form-group ">
                                    <label for="form_name" class="label without_label "><span>Process Full Name : </span> {{ $form->form_name }}</label>
                                </div>
                            </div>
                            <div class="col-md-6 left_pdng">
                                <div class="form-group">
                                    <label for="form_label" class="label without_label"><span>Process Short Name : </span>  {{ $form->form_label }}</label>       
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label for="form_label" class="label without_label">  <span class="full_lenth">Assigned Roles : </span> 
                                        @foreach($roles as $role)
                                      @if ($loop->last)
                                        {{$role->role->name}}
                                        @else
                                        {{$role->role->name}},
                                        @endif
                                        @endforeach
                                    </label>   
                                </div>
                            </div>
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label for="form_label" class="label without_label"><span class="full_lenth">Assigned Store Groups : </span> 
                                        @foreach($groups as $group)
                                            @if ($loop->last)
                                                {{$group->groups->group_code}}
                                            @else
                                                {{$group->groups->group_code}},
                                            @endif
                                        @endforeach
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                        <div class="question_group_main">
                            <div class="question_group_0">
                                <div class="question_field_main">
                                {!! Form::open(array('route' => 'create_question','method'=>'POST')) !!}
                                        <fieldset class="savedValue">
                                         <legend class="legend_title">Edit Question</legend>
                                            <div class="clearfix">
                                                <div class="form-group col-md-6 right_pdng question_name_f_0">
                                                    <label for="question_name" class="label">Question Title</label>
                                                    <input type="hidden" name="form_id" id="" value="{{$form->id}}" />
                                                    <input type="text" value="{{isset($formQuestion->question) ? $formQuestion->question : ''}}" name="question_name" id="question_name_0" class="form-control customtinput" placeholder="Enter Question Title" required/>
                                                    @error('question_name')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="form-group col-md-6 left_pdng question_order_f_0">
                                                    <label for="question_order" class=" label">Question Order number</label>
                                                    <input type="text" value="{{isset($formQuestion->sort_order) ? $formQuestion->sort_order : ''}}" name="question_order" id="question_order_0" class="form-control customtinput" placeholder="Question Order No." value="0" required/>
                                                    @error('question_order')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            <!-- </div>
                                            <div class="clearfix"> -->
                                                <div class="form-group col-md-6  right_pdng field_require_f_0">
                                                    <label for="question_alias" class=" label">Question negative alias</label>    
                                                   
                                                        <div class="icheck-primary d-inline field_eable">
                                                            <input type="checkbox" {{!empty($formQuestion->show_on_dashboard) ? 'checked' : ''}} name="show_on_dashboard" id="show_on_dashboard" class="customtinput" placeholder="Enter Alias For V2 SOP KPI Report" /><label for="show_on_dashboard"></label>
                                                             <label for="field_require" class="label">Show In V2 SOP KPI Report</label>
                                                        </div>
                                                        @error('show_on_dashboard')
                                                        <span class="invalid-feedback d-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    <input type="text" value="{{isset($formQuestion->question_alias) ? $formQuestion->question_alias : ''}}" name="question_alias" id="question_alias" class="form-control customtinput" readonly placeholder="Enter Negative Alias To Show In V2 SOP KPI Report" />
                                                    @error('question_alias')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="clearfix">
                                                @forelse($formQuestion->SOPFormFieldMany as $key=>$formFields)
                                                <div class="field_group_main">
                                                    <div class="form_field_group_0_{{$key}}">
                                                        <hr/>
                                                        <div class="clearfix">
                                                            <input type="hidden" value="{{isset($formFields->id) ? $formFields->id : ''}}" name="field_id[{{$key}}]"  />
                                                            <div class="form-group right_pdng col-md-6 question_order_f_0_{{$key}}">
                                                                <label for="option_type" class="label">Option Type</label>

                                                            <div class="icheck-primary field_eable d-inline">
                                                                <input type="checkbox" name="field_require[{{$key}}]" {{!empty($formFields->form_field_required) ? 'checked' : ''}} id="field_require_0_{{$key}}" class="customtinput" /><label for="field_require_0_{{$key}}"></label> <label for="field_require" class="label">Mandatory</label>
                                                            </div>
                                                                    <select name="option_type[0]" id="form_field_group_0_{{$key}}"  rel="{{$key}}" class="form-control option_type" required>
                                                                        <option value="">Select Type</option>
                                                                        <option value="text" {{!empty($formFields->field_type=='text') ? 'selected' : ''}}>Text Box</option>
                                                                        <option value="textarea" {{!empty($formFields->field_type=='textarea') ? 'selected' : ''}}>Text Area</option>
                                                                        <option value="radio" {{!empty($formFields->field_type=='radio') ? 'selected' : ''}}>Radio Button</option>
                                                                        <option value="file" {{!empty($formFields->field_type=='file') ? 'selected' : ''}} >Attachment</option>
                                                                    </select>
                                                                    @error('option_type')
                                                                    <span class="invalid-feedback d-block" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                    @enderror

                                                            </div>
                                                            <div class="col-md-6 pt-20 left_pdng">
                                                                 <a class="remove_field_group plus_btn" attr-id-level-1="0" attr-id-level-2="{{$key}}"><i class="fa fa-minus theme_clr"></i>
                                                                    </a>
                                                                    <a class="  add_field_group plus_btn" attr-id-level-1="0" attr-id-level-2="{{$key}}"><i class="fa fa-plus add_qest theme_clr"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix">
                                                        <div class="form-group col-sm-6 right_pdng field_name_f_0_{{$key}}" @if(($formFields->field_type=='radio') || ($formFields->field_type=='file')) style="display:none" @endif >
                                                            <label for="field_name" class=" label">Field Name</label>
                                                            <input type="text" value="{{isset($formFields->field_name) ? $formFields->field_name : ''}}" name="field_name[{{$key}}]" id="field_name_0_0" class="form-control customtinput" placeholder="Enter Field Name" />
                                                            @error('field_name')
                                                            <span class="invalid-feedback d-block" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>  
                                                        <div class="form-group  left_pdng col-sm-6 field_placeholder_f_0_{{$key}}" @if(($formFields->field_type=='radio') || ($formFields->field_type=='file')) style="display:none" @endif >
                                                            <label for="field_placeholder" class="label">Field Placeholder</label>
                                                            <input type="text" value="{{isset($formFields->field_placeholder) ? $formFields->field_placeholder : ''}}"  name="field_placeholder[{{$key}}]" id="field_placeholder_0_{{$key}}" class="form-control customtinput" placeholder="Enter Field Placeholder" />
                                                            @error('field_placeholder')
                                                            <span class="invalid-feedback d-block" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                        </div>
                                                       
                                                    </div>
                                                        <div class="clearfix">
                                                          
                                                        </div>
                                                        <div class="field_value_main" @if($formFields->field_type!='radio') style="display:none" @else style="display:block" @endif clearfix>
                                                            <label for="field_option_name" class=" label">Field Option Name</label>
                                                            @forelse($formFields->FormFieldValueMany as $valKey=>$formFieldVal)
                                                            <div class="field_value_0_{{$key}}_{{$valKey}} clearfix">
                                                                <div class="col-md-6  right_pdng field_option_name_f_0_{{$key}}_{{$valKey}}">
                                                                    <div class="form-group">
                                                                        <input type="hidden" name="field_value_id[{{$key}}][{{$valKey}}]" id="field_value_id_0_{{$key}}_{{$valKey}}" value="{{isset($formFieldVal->id) ? $formFieldVal->id : ''}}" />

                                                                        <input type="hidden" name="field_option_value[{{$key}}][{{$valKey}}]" id="field_option_value_0_{{$key}}_{{$valKey}}" value="{{isset($formFieldVal->field_option_value) ? $formFieldVal->field_option_value : ''}}" />
                                                                        <div class="input-group">
                                                                            <input type="text" value="{{isset($formFieldVal->form_field_option_name) ? $formFieldVal->form_field_option_name : ''}}" name="field_option_name[{{$key}}][{{$valKey}}]" id="field_option_name_0_{{$key}}_{{$valKey}}" class="form-control customtinput" placeholder="Enter Field Option Name"/>
                                                                            <span class="input-group-addon inputplus"><a class="add_field_option_val_group " attr-id-level-1="0" attr-id-level-2="{{$key}}" attr-id-level-3="{{$valKey}}"><i class="fa fa-plus clr_white"></i></a></span>
                                                                        </div>
                                                                        <input type="radio" name="field_scorable[{{$key}}][{{$valKey}}]" {{!empty($formFieldVal->is_scorable) ? 'checked' : '' }} class="field_scorable_0" value="1">
                                                                       
                                                                    </div>
                                                                </div>
                                                            </div>
                                                             @empty
                                                             @endforelse
                                                        </div>
                                                    </div>
                                                </div>
                                                @empty
                                                !! No Field found on this question !!
                                                @endforelse
                                            </div>
                                            <h2 class="heading-2">Assign BGT Time</h2>
                                             @php $k=0; @endphp
                                             @foreach($groups as $group)
                                                <div class="panel panel-primary" id="panel_{{$group->groups->id}}">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">{{$group->groups->group_code}}</h3>
                                                        <span class=" clickable panel-collapsed"><i class="glyphicon glyphicon-chevron-down"></i></span>
                                                    </div>
                                                    <div class="panel-body" style="display: none;">   
                                                        <div class="form-group">
                                                            <div class="clearfix">
                                                                <div class="col-sm-6">
                                                                <input type="hidden" name="categories[{{$k}}]" value="{{$group->groups->id}}" class="category"/>
                                                                    <input type="text" name="start_time[{{$k}}]" id="start_time" class="form-control start_time" required autocomplete="off" placeholder="Start Time" />
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <input type="text" autocomplete="off" required name="end_time[{{$k}}]" id="end_time" class="form-control end_time" placeholder="End Time" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @php $k++; @endphp
                                                @endforeach

                                                <div class="clearfix text-center">
                                                    <!--a class="btn btn-primary btn-submit" href="{{ route('forms.edit',$form->id) }}" data-toggle="tooltip" title="Edit!">Back</a-->
                                                   <!--input type="submit" value="Save" name="submit" id="submit" class="btn btn-primary btn-submit"-->
                                                   <!--input type="submit" value="Save & Next" name="submit" id="submit" class="btn btn-primary btn-submit"-->
                                                </div>
                                        </fieldset>
                                         {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
<!--<div class="input-group clockpicker">
    <input type="text" class="form-control" value="18:00">
    <span class="input-group-addon">
        <span class="glyphicon glyphicon-time"></span>
    </span>
</div>   -->
</div>
</section>

@endsection
@section('custom-js')
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"  crossorigin="anonymous"></script>
<!----
  <link rel="stylesheet" href="{{ asset('css/bootstrap-clockpicker.min.css')}}"></script>
  <script src="{{ asset('js/bootstrap-clockpicker.min.js')}}"></script>
------>
<script>

$(document).ready(function(){

 $('#mulitselectCheck, #groups').multiselect({
  nonSelectedText: 'Select Framework',
  buttonWidth:'100%'
 });
 $('#start_time, #end_time').timepicker({
     showMeridian: false,
      use24hours: true,
        format: 'HH:mm',
        defaultTime:null
 });
 $(function () {
        $("#show_on_dashboard").click(function () {
            if ($(this).is(":checked")) {
                $("#question_alias").attr('readonly',false);
            } else {
                $("#question_alias").attr('readonly',true);
            }
        });
    });
  var fieldCounter = 0;
  var fieldValCounter=1;
  /******* field value append start ********/
        $(document).on('click', '.add_field_option_val_group', function(){
            fieldCount = $(this).attr('attr-id-level-2');
            fieldValCounter++;
        var fielddataset = 
        '<div class="field_value_0_'+fieldCount+'_'+fieldValCounter+'">'+
            '<div class="form-group clearfix field_option_name_f_0_'+fieldCount+'_'+fieldValCounter+'">'+
                '<div class="col-md-6 right_pdng">'+
                    '<input type="hidden" name="field_option_value['+fieldCount+']['+fieldValCounter+']" id="field_option_value_0_'+fieldCount+'_'+fieldValCounter+'" class="form-control customtinput" placeholder="Enter Field Option Value" value="'+fieldValCounter+'"/>'+
                    '<div class="input-group"><input type="text" name="field_option_name['+fieldCount+']['+fieldValCounter+']" id="field_option_name_0_'+fieldCount+'_'+fieldValCounter+'" class="form-control customtinput" placeholder="Enter Field Option Name"/>'+
                      '<span class="input-group-addon inputplus"><a class="remove_field_option_val_group" attr-id-level-1="0" attr-id-level-2="'+fieldCount+'" attr-id-level-3="'+fieldValCounter+'"><i class="fa fa-minus clr_white"></i></a></span></div>'+
                      '<input type="radio" name="field_scorable['+fieldCount+']['+fieldValCounter+']" class="field_scorable_'+fieldCount+'" value="1">'+
                '</div>'+
               
            '</div>'+
        '</div>';
                $('.question_group_0 .form_field_group_0_'+fieldCount+' .field_value_main').append(fielddataset);
        });

        $(document).on('click', '.remove_field_option_val_group', function(){
            field = $(this).attr('attr-id-level-2');
            fieldVal = $(this).attr('attr-id-level-3');
           // $('.question_group_0 .form_field_group_0_'+field+' .field_value_main .field_value_0_'+field+'_'+fieldVal+'').remove();
        });
    /******* field value append end ********/

    /******* field type append start ********/
   // $('.form_field_group_0_0 .field_value_main').hide();
        $(document).on('click', '.add_field_group', function(){
            //fieldCounter = $(this).attr('attr-id-level-2');
            fieldCounter++;
    var fieldtypedataset = 
    '<div class="form_field_group_0_'+fieldCounter+'" id="form_field_group_0_'+fieldCounter+'"><hr/>'+
        '<div class="clearfix">'+
            '<div class="form-group clearfix question_order_f_0_'+fieldCounter+'">'+
                '<div class=" col-md-6 right_pdng"><label for="option_type" class="label">Option Type</label>'+
                '<div class="icheck-primary field_eable d-inline">'+
                    '<input type="checkbox" name="field_require['+fieldCounter+']" id="field_require_0_'+fieldCounter+'" class="customtinput" />'+
                    '<label for="field_require_0_'+fieldCounter+'"></label>'+
                    '<label for="field_require" class="label">Mandatory</label>'+
                '</div>'+
                    '<select name="option_type['+fieldCounter+']" id="form_field_group_0_'+fieldCounter+'" rel="'+fieldCounter+'" class="form-control option_type" required>'+
                        '<option value="">Select Type</option>'+
                        '<option value="text">Text Box</option>'+
                        '<option value="textarea">Text Area</option>'+
                        '<option value="radio">Radio Button</option>'+
                        '<option value="file">Attachment</option>'+
                    '</select>'+
                '</div>'+
            '</div>'+
        '</div>'+
        '<div class="clearfix">'+
            '<div class="form-group col-sm-6 right_pdng field_name_f_0_'+fieldCounter+'">'+
                '<label for="field_name" class=" label">Field Name</label>'+
                '<input type="text" name="field_name['+fieldCounter+']" id="field_name_0_'+fieldCounter+'" class="form-control customtinput" placeholder="Enter Field Name" />'+
            '</div>'+
              '<div class="form-group col-sm-6 left_pdng field_placeholder_f_0_'+fieldCounter+'" >'+
                '<label for="field_placeholder" class=" label">Field Placeholder</label>'+
                '<input type="text" name="field_placeholder['+fieldCounter+']" id="field_placeholder_0_'+fieldCounter+'" class="form-control customtinput" placeholder="Enter Field Placeholder" />'+
            '</div>'+
        '</div>'+
       
        '<div class="field_value_main clearfix">'+
            '<div class="field_value_0_'+fieldCounter+'_0 clearfix">'+
                '<div class="col-md-6 right_pdng field_option_name_f_0_'+fieldCounter+'_0">'+
                    '<div class="form-group"><label for="field_option_name" class=" label">Field Option Name</label>'+
                     '<div class="input-group">'+
                    '<input type="hidden" name="field_option_value['+fieldCounter+'][0]" id="field_option_value_0_'+fieldCounter+'_0" class="form-control customtinput" placeholder="Enter Field Option Value" value="0"/><input type="text" name="field_option_name['+fieldCounter+'][0]" id="field_option_name_0_'+fieldCounter+'_0" class="form-control customtinput" placeholder="Enter Field Option Name"/>'+
                      '<span class="input-group-addon"><a class="add_field_option_val_group" attr-id-level-1="0" attr-id-level-2="'+fieldCounter+'" attr-id-level-3="0"><i class="fa fa-plus clr_white"></i></a></span>'+
                     '</div><input type="radio" name="field_scorable['+fieldCounter+'][0]" class="field_scorable_'+fieldCounter+'" value="1"></div>'+
                     '<div class="form-group">'+
                    '<input type="hidden" name="field_option_value['+fieldCounter+'][1]" id="field_option_value_0_'+fieldCounter+'_0" class="form-control customtinput" placeholder="Enter Field Option Value" value="0"/><input type="text" name="field_option_name['+fieldCounter+'][1]" id="field_option_name_0_'+fieldCounter+'_0" class="form-control customtinput" placeholder="Enter Field Option Name"/><input type="radio" name="field_scorable['+fieldCounter+'][1]" class="field_scorable_'+fieldCounter+'" value="1">'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+
        '</div>';
            $('.question_group_0 .field_group_main').append(fieldtypedataset);
            $('.form_field_group_0_'+fieldCounter+' .field_value_main').hide();
        });

        $(document).on('click', '.remove_field_group', function(){
            $('#form_field_group_0_'+fieldCounter).remove();
            fieldCounter--
        });$(document).on('click', '.remove_field_option_val_group', function(){
          var level2 = $(this).attr('attr-id-level-2');
          var level3 = $(this).attr('attr-id-level-3');
           $('.field_value_0_'+level2+'_'+level3).remove();
            
        });
        /******* field type append end ********/
        $(document).on("change",'select', function(){
            var rels=$(this).attr('rel');
              //alert(rels);
            if(this.value == 'radio'){

                    $('.field_placeholder_f_0_'+rels).hide();
                    $('.field_name_f_0_'+rels).hide();
                    $('.form_field_group_0_'+rels+' .field_value_main').show();
                }else if(this.value == 'file'){
                    $('.field_placeholder_f_0_'+rels).hide();
                    $('.field_name_f_0_'+rels).hide();
                    $('.form_field_group_0_'+rels+' .field_value_main').hide();
                }else{
                    $('.field_placeholder_f_0_'+rels).show();
                    $('.field_name_f_0_'+rels).show();
                    $('.'+this.id+' .field_value_main').hide();
                }
                
            });
        });
    $(document).on('click', '.panel-heading span.clickable', function(e){
        var $this = $(this);
        if(!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
    })
 
</script>

  <script>
        /*  $('#start_time, #end_time').datetimepicker({
                    timepicker:true,
                    datepicker:false,
                    format:'H:i:s',
                   //mask:'00:00:00',
                    use24Hours:true,
                    step:5
        }) */

        $(document).delegate('input[type="radio"]', 'click', function(){
            if($(this).is(":checked")){
                $('.'+this.className).not(this).prop('checked', false);  
            }
        });
       </script>
@endsection
