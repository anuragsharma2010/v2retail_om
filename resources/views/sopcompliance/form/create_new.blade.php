@extends('layouts.app')
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1> Process </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
      <li><a href="#">V2 Shop Compliance</a></li>
      <li><a href="{{ route('forms.index') }}">Process List</a>
      <li class="active">Add Process</li>
    </ol>
</section>
<style>
    .form-group label {
    font-weight: 600;
    color: #333;
    padding-left: 0px;
    margin-bottom: 5px;
    display: inline-block;
    }
    .form-group.inputwith input.form-control{
        width:582;
        max-width:100%;
    }
    .remove_field_option_val_group.plus_btn {
      position: absolute;
    right: -30px;
    top: 19px;
    font-size: 32px;
    font-weight: 200;
  }


   #mulitselectCheck, #groups{
    height:50px;
  }
  button.multiselect.dropdown-toggle.btn.btn-default {
      height: 50px;
      text-align: left;
      color: #777;
      font-family: Roboto;
      border-radius: 5px;
      font-size: 16px;
      display: flex;
      justify-content: space-between;
      align-items: center;
  }
  .multiselect.dropdown-toggle + .multiselect-container.dropdown-menu{
   max-width: 100%;
      width: 100%;
  }
  ul.dropdown-menu.multiselect-container>li{
          padding:6px 5px;
  }
  ul.multiselect-container.dropdown-menu>li>a:hover, ul.dropdown-menu.multiselect-container>li>a:focus,ul.dropdown-menu.multiselect-container>li.active>a:focus, .dropdown-menu.multiselect-container>li.active>a:hover, .dropdown-menu.multiselect-container>li.active>a{
      background:#fff !important ;
  } 
  .multiselect-container>li>a>label.checkbox{
      font-size:16 !important;
  }

</style>
    <section class="content">
        <div class="container-fluid roleform">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
                <!-- /.card-header -->
                <div class="card-body">
                    @if(isset($form->id))
                     {!! Form::model($form, ['method' => 'PATCH','route' => ['forms.update', $form->id]]) !!}
                    @endif
                    {!! Form::open(array('route' => 'forms.store','method'=>'POST')) !!}
                    <div class="question_module">
                        <div class="clearfix">
                            <div class="clearfix">
                            <div class="col-md-6 right_pdng">
                                <div class="form-group">
                                        <label for="form_name" class="label">Process Full Name</label>
                                        <input autocomplete="off" type="text" name="form_name" id="form_name" value="{{isset($form->form_name) ? $form->form_name : old('form_name')}}" class="form-control customtinput" placeholder="Enter Full Name" />
                                        @error('form_name')
                                        <span class="invalid-feedback d-block" role="alert">
                                          {{ $message }}
                                        </span>
                                        @enderror
                                </div>
                            </div>
                            <div class="col-md-6 left_pdng">
                                <div class="form-group">
                                        <label for="form_label" class="label">Process Short Name</label>      

                                        <input type="text" autocomplete="off" name="form_label" id="form_label"  value="{{isset($form->form_label) ? $form->form_label : old('form_label')}}" class="form-control customtinput" placeholder="Enter Short Name"/>

                                        @error('form_label')
                                        <span class="invalid-feedback d-block" role="alert">
                                            {{ $message }}
                                        </span>
                                        @enderror
                                </div>
                            </div>
                           </div>
                           <div class="clearfix">
                              <div class="col-md-6 right_pdng">
                                <div class="form-group ">
                                            <label for="form_label" class="label">Assigned To</label>
                                            <select name="roles[]" class ='form-control' id='mulitselectCheck' multiple >
                                                @foreach($roles as $role)
                                                <option class="roles"  <?php if(in_array($role->id,$selectedRoles)){ echo 'selected="selected"'; }?> value='{{$role->id}}'>{{$role->name}}</option>
                                                @endforeach
                                            </select> 
                                        @if ($errors->has('roles'))
                                        <div class="error_field" >{{ $errors->first('roles') }}</div>
                                        @enderror
                                  </div>
                               </div>
                               <div class="col-md-6 left_pdng">
                                 <div class="form-group">
                                       <label for="form_label" class="label d-block">Assign Store Group</label>
                                        <select name="groups[]" class ='form-control' id='groups' multiple>
                                            @foreach($groups as $group)
                                            <option class="groups" data-text="{{$group->group_code}}" value='{{$group->id}}'  <?php if(in_array($group->id,$selectedGroups)){ echo 'selected="selected"'; }?> >{{$group->group_code}}</option>
                                            @endforeach
                                        </select>
                                     @if ($errors->has('groups'))
                                        <div class="error_field" >{{ $errors->first('groups') }}</div>
                                     @enderror
                                </div>
                            </div>
                            </div>
                            
                            <div class="col-md-12 text-center">

                                <button type="submit" class="btn btn-primary btn-submit">Save & Next</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
 
@endsection
@section('custom-js')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
    <script>
    $(document).ready(function(){
     $('#mulitselectCheck').multiselect({
      nonSelectedText: 'Select User Roles',
      buttonWidth:'100%'
     });
     $('#groups').multiselect({
      nonSelectedText: 'Select Store Groups',
      buttonWidth:'100%'
     });
    $(document).on('click', '.panel-heading span.clickable', function(e){
        var $this = $(this);
        if(!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
    })
     })
</script>
@endsection
