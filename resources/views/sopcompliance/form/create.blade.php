@extends('layouts.app')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
      <h1> Process </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="#">V2 Shop Compliance</a></li>
        <li><a href="{{ route('forms.index') }}">Process List</a>
        <li class="active">Add Process</li>
      </ol>
</section>
    <style>
    .form-group label {
    font-weight: 600;
    color: #333;
    padding-left: 0px;
    margin-bottom: 5px;
    display: inline-block;
    }
    .form-group.inputwith input.form-control{
        width:582;
        max-width:100%;
    }
    .remove_field_option_val_group.plus_btn {
      position: absolute;
    right: -30px;
    top: 19px;
    font-size: 32px;
    font-weight: 200;
}

/*
   #mulitselectCheck, #groups{
    height:50px;
}
button.multiselect.dropdown-toggle.btn.btn-default {
    height: 50px;
    text-align: left;
    color: #777;
    font-family: Roboto;
    border-radius: 5px;
    font-size: 16px;
    display: flex;
    justify-content: space-between;
    align-items: center;
}
.multiselect.dropdown-toggle + .multiselect-container.dropdown-menu{
 max-width: 100%;
    width: 100%;
}
.multiselect-container>li>a>label.checkbox {
    font-size:16px;
}
.dropdown-menu>.active>a, .dropdown-menu>.active>a:focus, .dropdown-menu>.active>a:hover{
   background:transparent;
}*/
</style>
  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
    <script>
$(document).ready(function(){
 $('#mulitselectCheck, #groups').multiselect({
  nonSelectedText: 'Select Framework',
  buttonWidth:'100%'
 });
});
</script>-->
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid roleform">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
                <!-- /.card-header -->
                <div class="card-body">
                    {!! Form::open(array('route' => 'forms.store','method'=>'POST')) !!}
                    <div class="question_module">
                        <div class="clearfix">
                            <div class="clearfix">
                            <div class="col-md-6 right_pdng">
                                <div class="form-group">
                                    <label for="form_name" class="label">Process Full Name</label>
                                        <input type="text" name="form_name" id="form_name" class="form-control customtinput" placeholder="Enter Form Name" />
                                        @error('form_name')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                </div>
                            </div>
                            <div class="col-md-6 left_pdng">
                                <div class="form-group">
                                        <label for="form_label" class="label">Process Label</label>      
                                        <input type="text" name="form_label" id="form_label" class="form-control customtinput" placeholder="Enter Form Label" />
                                        @error('form_label')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                </div>
                            </div>
                           </div>
                           <div class="clearfix">
                              <div class="col-md-6">
                                <div class="form-group right_pdng">
                                        <label for="form_label" class="label">Role</label>
                                            <select name="roles[]" class ='form-control' id='mulitselectCheck' multiple>
                                                @foreach($roles as $role)
                                                <option class="roles"  value='{{$role->id}}'>{{$role->name}}</option>
                                                @endforeach
                                            </select> 
                                        @error('roles')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                  </div>
                               </div>
                               <div class="col-md-6 left_pdng">
                                 <div class="form-group">
                                       <label for="form_label" class="label d-block">Categories</label>

                                        <select name="groups[]" class ='form-control' id='groups' multiple>
                                            @foreach($groups as $group)
                                            <option class="groups" data-text="{{$group->group_code}}" value='{{$group->id}}'>{{$group->group_code}}</option>
                                            @endforeach
                                        </select>
                                    @error('groups')
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            </div>
                            <!-- Add question and fields -->
                            <div class="question_group_main">
                                <div class="question_group_0">
                                    <input type="hidden" id="q_id_0" value="0"/>
                                    <div class="question_field_main">
                                        <fieldset>
                                            <legend>
                                                <div class="flex_div">
                                                    <div><h3>Questions 1</h3></div> 
                                                    <div><button type="button" class="btn btn-primary btn-submit add_question_group"><i class="fa fa-plus-circle mr-10"></i>Question</button></div></div></legend>

                                            <div class="clearfix">
                                                <div class="form-group col-md-6 right_pdng question_name_f_0">
                                                    <label for="question_name" class="label">Question Title</label>
                                                    <input type="text" name="question_name[]" id="question_name_0" class="form-control customtinput" placeholder="Enter Question Title" required/>
                                                    @error('question_name')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                 <div class="form-group col-md-6 question_order_f_0">
                                                    <label for="question_order" class=" label">Weightage</label>
                                                    <input type="text" name="question_order[]" id="question_order_0" class="form-control customtinput" placeholder="Question Order No." required/>
                                                    @error('question_order')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                           </div>
                                           <div class="clearfix">
                                                <div class="form-group col-md-6 field_require_f_0">
                                                    <label for="field_require" class="label">Show Status on Dashboard</label>
                                                      <div class="icheck-primary d-inline">
                                                        <input type="checkbox" name="show_on_dashboard[]" id="field_require_0" class="customtinput" placeholder="Show Status on Dashboard" /><label for="field_require_0"></label></div>
                                                        @error('field_require')
                                                        <span class="invalid-feedback d-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                </div>
                                                <div class="form-group col-md-6 question_order_f_0">
                                                    <label for="question_alias" class=" label">Question Alias</label>
                                                    <input type="text" name="question_alias[]" id="question_alias_0" class="form-control customtinput" placeholder="Enter Alias for Dashboard" />
                                                    @error('question_alias')
                                                    <span class="invalid-feedback d-block" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                           </div>
                                           <div class="clearfix">
                                                <div class="field_group_main">
                                                    <div class="form_field_group_0_0">
                                                        <hr/>
                                                        <div class="clearfix">
                                                            <div class="form-group right_pdng col-md-6 question_order_f_0_0">
                                                                <label for="option_type" class="label">Option Type</label>
                                                                    <select name="option_type[0][]" id="form_field_group_0_0"  class="form-control option_type" required>
                                                                        <option value="">Select Type</option>
                                                                        <option value="text">Text Box</option>
                                                                        <option value="textarea">Text Area</option>
                                                                        <option value="radio">Radio Button</option>
                                                                        <option value="file">Attachment</option>
                                                                    </select>
                                                                    @error('option_type')
                                                                    <span class="invalid-feedback d-block" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                    @enderror
                                                                    <a class="add_field_group plus_btn" attr-id-level-1="0" attr-id-level-2="0"><i class="fa fa-plus add_qest theme_clr"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix">
                                                        <div class="form-group col-sm-6 right_pdng field_name_f_0_0">
                                                            <label for="field_name" class=" label">Field Name</label>
                                                            <input type="text" name="field_name[0][]" id="field_name_0_0" class="form-control customtinput" placeholder="Enter Field Name" required/>
                                                            @error('field_name')
                                                            <span class="invalid-feedback d-block" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror
                                                         </div>
                                                         
                                                        <div class="form-group left_pdng col-sm-6 field_placeholder_f_0_0">
                                                                <label for="field_placeholder" class=" label">Field Placeholder</label>
                                                                <input type="text" name="field_placeholder[0][]" id="field_placeholder_0_0" class="form-control customtinput" placeholder="Enter Field Placeholder" />
                                                                @error('field_placeholder')
                                                                <span class="invalid-feedback d-block" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                        </div>
                                                    </div>
                                                        <div class="clearfix">
                                                            <div class="form-group right_pdng col-sm-6 validation_rules_f_0_0">
                                                            <label for="validation_rules" class=" label">Validation Rules</label>
                                                                <input type="text" name="validation_rules[0][]" id="validation_rules_0_0" class="form-control customtinput" placeholder="Enter Validation Rules" /><span>For Exp:- (email,integar,min,max)</span>
                                                                @error('validation_rules')
                                                                <span class="invalid-feedback d-block" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>

                                                            <div class="form-group col-md-6 left_pdng field_require_f_0_0 pt2">

                                                            <label for="field_require" class="label">Field Require</label>
                                                              <div class="icheck-primary d-inline">
                                                                <input type="checkbox" name="field_require[0][]" id="field_require_0_0" class="customtinput" placeholder="Enter Field Require" /><label for="field_require_0_0">Require</label></div>
                                                                @error('field_require')
                                                                <span class="invalid-feedback d-block" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="field_value_main clearfix">
                                                            <div class="field_value_0_0_0 clearfix">
                                                                <div class="form-group col-md-6  right_pdng field_option_name_f_0_0_0">
                                                                    <label for="field_option_name" class=" label">Field Option Name</label>
                                                                    <input type="hidden" name="field_option_value[0][0][]" id="field_option_value_0_0_0" class="form-control customtinput" placeholder="Enter Field Option Value" value="0"/>
                                                                    <input type="text" name="field_option_name[0][0][]" id="field_option_name_0_0_0" class="form-control customtinput" placeholder="Enter Field Option Name"/>
                                                                    @error('field_option_name')
                                                                        <span class="invalid-feedback d-block" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                    @enderror
                                                                   <a class="add_field_option_val_group plus_btn" attr-id-level-1="0" attr-id-level-2="0" attr-id-level-3="0"><i class="fa fa-plus add_qest theme_clr"></i></a>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h2 class="heading-2">Assign BGT Time</h2>
                                            <hr/>
                                            <div id="category_0">
                                               
                                            </div> 
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <!-- Add question and fields end -->
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
 <div id="category" style="display: none;">
                                               
 </div>  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
   
$(document).ready(function(){
   
  $(".groups").click(function(){

    $("#category_0").html('');
     $("#category").html('');
    $("#groups option:selected").each(function (index) {
        
        
       var $this = $(this);
      
       if ($this.length) {
        var group_html='<div class="panel panel-primary" id="panel_'+$this.val()+'">'+
            '<div class="panel-heading">'+
                '<h3 class="panel-title">'+$this.text()+'</h3>'+
                '<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-down"></i></span>'+
            '</div>'+
            '<div class="panel-body" style="display: none;">'+   
                '<div class="form-group">'+
                    '<div class="clearfix">'+
                        '<div class="col-sm-6">'+
                        '<input type="hidden" name="categories[0]['+index+']" value="'+$this.val()+'" class="category"/>'+
                            '<input type="text" name="start_time[0]['+index+']" id="start_time" class="form-control start_time" placeholder="Strat Time"/>'+
                        '</div>'+
                        '<div class="col-sm-6">'+
                            '<input type="text" name="end_time[0]['+index+']" id="end_time" class="form-control end_time" placeholder="End Time" />'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>';
         $("#category_0").append(group_html);
         $("#category").append(group_html);
       }
    });
  });
  var questionCounter=0;
  var fieldCounter = 0;
  var fieldValCounter=0;
 $(document).on('click', '.add_question_group', function(){
      var grup_val=$("#groups option:selected").length;    
     
     fieldValCounter=0;
     //questionCounter = $(this).attr('attr-id-level-1');
    
    if(grup_val){
    questionCounter++;
     $("#groups").attr('readonly',true);
    }else{
        alert('Select Category First !!');
        return false;
    }
     //var cat_inputs= document.getElementById('category').querySelectorAll('.category');
        //console.log(cat_inputs.length);
    $.each(document.getElementById('category').querySelectorAll('.category'),function(index,value){
        value.name='categories['+questionCounter+']['+index+']';
   // console.log(value.name);
    });
     $.each(document.getElementById('category').querySelectorAll('.start_time'),function(index,value){
        value.name='start_time['+questionCounter+']['+index+']';
   // console.log(value.name);
    });
      $.each(document.getElementById('category').querySelectorAll('.end_time'),function(index,value){
        value.name='end_time['+questionCounter+']['+index+']';
   // console.log(value.name);
    });
    var category=$("#category").html();

    var questionDataSet='<div class="question_group_'+questionCounter+'">'+
                                    '<input type="hidden" id="q_id_'+questionCounter+'" value="0"/>'+
                                    '<div class="question_field_main">'+
                            '<fieldset>'+
                            '<legend>'+
                                '<div class="flex_div">'+
                                    '<div><h3>Questions '+(questionCounter+1)+'</h3>'+
                                    '</div>'+ 
                                     '<div>'+
                                        '<a class="remove_question_group plus_btn" attr-id-level-1="'+questionCounter+'" attr-id-level-2="0"><i class="fa fa-close text-danger"></i></a>'+
                                        '</div>'+
                                    '</div>'+
                            '</legend>'+
                            '<div class="clearfix">'+
                                '<div class="form-group col-md-6 question_name_f_'+questionCounter+'">'+
                                    '<label for="question_name" class="label">Question Title</label>'+
                                    '<input type="text" name="question_name['+questionCounter+']" id="question_name_'+questionCounter+'" class="form-control customtinput" placeholder="Enter Question Title" required/>'+
                                '</div>'+
                                 '<div class="form-group col-md-6 question_order_f_'+questionCounter+'">'+
                                    '<label for="question_order" class=" label">Weightage</label>'+
                                    '<input type="text" name="question_order['+questionCounter+']" id="question_order_'+questionCounter+'" class="form-control customtinput" placeholder="Weightage" required/>'+
                                '</div>'+
                           '</div>'+
                           '<div class="clearfix">'+
                                '<div class="form-group col-md-6 field_require_f_'+questionCounter+'">'+
                                    '<label for="field_require" class="label">Show Status on Dashboard</label>'+
                                    '<div class="icheck-primary d-inline">'+
                                    '<input type="checkbox" name="show_on_dashboard['+questionCounter+']" id="field_require_'+questionCounter+'" class="customtinput" placeholder="Show Status on Dashboard" /><label for="field_require_'+questionCounter+'"></label>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="form-group col-md-6 question_order_f_0">'+
                                    '<label for="question_alias" class=" label">Question Alias</label>'+
                                    '<input type="text" name="question_alias['+questionCounter+']" id="question_alias_'+questionCounter+'" class="form-control customtinput" placeholder="Enter Alias for Dashboard" />'+
                                '</div>'+
                            '</div>'+
                           '<div class="clearfix">'+
                                '<div class="field_group_main">'+
                                    '<div class="form_field_group_'+questionCounter+'_0">'+
                                        '<hr/>'+
                                        '<div class="clearfix">'+
                                            '<div class="form-group col-md-6 question_order_f_'+questionCounter+'_0">'+
                                                '<label for="option_type" class="label">Option Type</label>'+
                                                    '<select name="option_type['+questionCounter+'][0]" id="form_field_group_'+questionCounter+'_0"  onchange="changeType($(this).val(),this.id)" class="form-control option_type" required>'+
                                                        '<option value="">Select Type</option>'+
                                                        '<option value="text">Text Box</option>'+
                                                        '<option value="textarea">Text Area</option>'+
                                                        '<option value="radio">Radio Button</option>'+
                                                        '<option value="file">Attachment</option>'+
                                                    '</select>'+
                                                    '<a class="add_field_group plus_btn" attr-id-level-1="'+questionCounter+'" attr-id-level-2="0"><i class="fa fa-plus"></i></a>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="form-group col-sm-6 field_name_f_'+questionCounter+'_0">'+
                                            '<label for="field_name" class=" label">Field Name</label>'+
                                            '<input type="text" name="field_name['+questionCounter+'][0]" id="field_name_'+questionCounter+'_0" class="form-control customtinput" placeholder="Enter Field Name" required/>'+
                                         '</div>'+
                                        '<div class="form-group col-sm-6 field_placeholder_f_'+questionCounter+'_0">'+
                                                '<label for="field_placeholder" class=" label">Field Placeholder</label>'+
                                                '<input type="text" name="field_placeholder['+questionCounter+'][0]" id="field_placeholder_'+questionCounter+'_0" class="form-control customtinput" placeholder="Enter Field Placeholder"/>'+
                                        '</div>'+
                                        '<div class="clearfix">'+
                                            '<div class="form-group col-sm-6 validation_rules_f_'+questionCounter+'_0">'+
                                            '<label for="validation_rules" class=" label">Validation Rules</label>'+
                                                '<input type="text" name="validation_rules['+questionCounter+'][0]" id="validation_rules_'+questionCounter+'_0" class="form-control customtinput" placeholder="Enter Validation Rules" /><span>For Exp:- (email,integar,min,max)</span>'+
                                            '</div>'+
                                            '<div class="form-group col-md-6 field_require_f_'+questionCounter+'_0">'+
                                            '<label for="field_require" class="label">Field Require</label>'+
                                              '<div class="icheck-primary d-inline">'+
                                                '<input type="checkbox" name="field_require['+questionCounter+'][0]" id="field_require_'+questionCounter+'_0" class="customtinput" placeholder="Enter Field Require"/><label for="field_require_'+questionCounter+'_0">Require</label></div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="field_value_main clearfix" style="display:none">'+
                                            '<div class="field_value_'+questionCounter+'_0_0 clearfix">'+
                                                '<div class="form-group col-md-6 field_option_name_f_'+questionCounter+'_0_0">'+
                                                    '<label for="field_option_name" class=" label">Field Option Name</label>'+
                                                    '<input type="hidden" name="field_option_value['+questionCounter+'][0][0]" id="field_option_value_'+questionCounter+'_0_0" class="form-control customtinput" placeholder="Enter Field Option Value" value="0"/>'+
                                                    '<input type="text" name="field_option_name['+questionCounter+'][0][0]" id="field_option_name_'+questionCounter+'_0_0" class="form-control customtinput" placeholder="Enter Field Option Name"/>'+
                                                    '<a class="add_field_option_val_group plus_btn" attr-id-level-1="'+questionCounter+'" attr-id-level-2="0" attr-id-level-3="0"><i class="fa fa-plus"></i></a>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<h2 class="heading-2">Assign BGT Time</h2>'+
                            '<hr/>'+
                            '<div id="category_'+questionCounter+'">'+category+
                            '</div>'+  
                        '</fieldset>'+
                    '</div>'+
                '</div>';
                        $('.question_group_main').append(questionDataSet);
                       
 });
        $(document).on('click', '.remove_question_group', function(){
            questionCounter = $(this).attr('attr-id-level-1');
          //  $('.question_group_'+questionCounter).remove();
        });

  /******* field value append start ********/
  $('.form_field_group_'+questionCounter+'_'+fieldCounter+' .field_value_main').hide();
 
        $(document).on('click', '.add_field_option_val_group', function(){
            
            questionCounter = $(this).attr('attr-id-level-1');
            fieldCounter = $(this).attr('attr-id-level-2');
            //fieldValCounter = $(this).attr('attr-id-level-3');
            fieldValCounter++;
            //var level_3 = $(this).attr('attr-id-level-3');
            var fielddataset = '<div class="field_value_'+questionCounter+'_'+fieldCounter+'_'+fieldValCounter+'"><div class="form-group clearfix field_option_name_f_'+questionCounter+'_'+fieldCounter+'_'+fieldValCounter+'"><div class="col-md-6"><input type="hidden" name="field_option_value['+questionCounter+']['+fieldCounter+']['+fieldValCounter+']" id="field_option_value_'+questionCounter+'_'+fieldCounter+'_'+fieldValCounter+'" class="form-control customtinput" placeholder="Enter Field Option Value" value="'+fieldValCounter+'"/><input type="text" name="field_option_name['+questionCounter+']['+fieldCounter+']['+fieldValCounter+']" id="field_option_name_'+questionCounter+'_'+fieldCounter+'_'+fieldValCounter+'" class="form-control customtinput" placeholder="Enter Field Option Name"/></div><div class="col-md-2"><a class="remove_field_option_val_group plus_btn" attr-id-level-1="0" attr-id-level-2="'+fieldCounter+'" attr-id-level-3="'+fieldValCounter+'"><i class="fa fa-minus"></i></a></div><span class="invalid-feedback d-block" role="alert"><strong></strong></span></div></div>';


            $('.question_group_'+questionCounter+' .form_field_group_'+questionCounter+'_'+fieldCounter+' .field_value_main').append(fielddataset);
            
        });

        $(document).on('click', '.remove_field_option_val_group', function(){
            //level_3++;
            //alert(level_3);
            questionCounter = $(this).attr('attr-id-level-1');
            fieldCounter = $(this).attr('attr-id-level-2');
            fieldValCounter = $(this).attr('attr-id-level-3');

           // $('.question_group_'+questionCounter+' .form_field_group_'+questionCounter+'_'+fieldCounter+' .field_value_main .field_value_'+questionCounter+'_'+fieldCounter+'_'+fieldValCounter+'').remove();
        });
    /******* field value append end ********/


    /******* field type append start ********/
  //$('.form_field_group_0_0 .field_value_main').hide();
 
        $(document).on('click', '.add_field_group', function(){
            //fieldCounter = $(this).attr('attr-id-level-2');
            questionCounter = $(this).attr('attr-id-level-1');
            fieldCounter=$('#q_id_'+questionCounter).val();
            fieldCounter++;
             $('#q_id_'+questionCounter).val(fieldCounter);
            var fieldtypedataset = '<div class="form_field_group_'+questionCounter+'_'+fieldCounter+'"><hr/><div class="clearfix"><div class="form-group question_order_f_'+questionCounter+'_'+fieldCounter+'"><div class=" col-md-6"><label for="option_type" class="label">Option Type</label><select name="option_type['+questionCounter+']['+fieldCounter+']" id="form_field_group_'+questionCounter+'_'+fieldCounter+'"  onchange="changeType($(this).val(),this.id)" class="form-control option_type" required><option value="">Select Type</option><option value="text">Text Box</option><option value="textarea">Text Area</option><option value="radio">Radio Button</option><option value="file">Attachment</option></select></div><div class="col-md-2"><a class="remove_field_group plus_btn" attr-id-level-1="'+questionCounter+'" attr-id-level-2="'+fieldCounter+'"><i class="fa fa-minus"></i></a></div><span class="invalid-feedback d-block" role="alert"><strong></strong></span></div></div><div class="form-group col-sm-6 field_name_f_'+questionCounter+'_'+fieldCounter+'"><label for="field_name" class=" label">Field Name</label><input type="text" name="field_name['+questionCounter+']['+fieldCounter+']" id="field_name_'+questionCounter+'_'+fieldCounter+'" class="form-control customtinput" placeholder="Enter Field Name" required/><span class="invalid-feedback d-block" role="alert"><strong></strong></span></div><div class="form-group col-sm-6 field_placeholder_f_'+questionCounter+'_'+fieldCounter+'"><label for="field_placeholder" class=" label">Field Placeholder</label><input type="text" name="field_placeholder['+questionCounter+']['+fieldCounter+']" id="field_placeholder_0_'+fieldCounter+'" class="form-control customtinput" placeholder="Enter Field Placeholder" /><span class="invalid-feedback d-block" role="alert"><strong></strong></span></div><div class="clearfix"><div class="form-group col-sm-6 validation_rules_f_'+questionCounter+'_'+questionCounter+'"><label for="validation_rules" class=" label">Validation Rules</label><input type="text" name="validation_rules['+questionCounter+']['+fieldCounter+']" id="validation_rules_'+questionCounter+'_'+fieldCounter+'" class="form-control customtinput" placeholder="Enter Validation Rules" /><span>For Exp:- (email,integar,min,max)</span><span class="invalid-feedback d-block" role="alert"><strong></strong></span></div><div class="form-group col-md-6 field_require_f_'+questionCounter+'_'+fieldCounter+'"><label for="field_require" class="label">Field Require</label><div class="icheck-primary d-inline"><input type="checkbox" name="field_require['+questionCounter+']['+fieldCounter+']" id="field_require_'+questionCounter+'_'+fieldCounter+'" class="customtinput" placeholder="Enter Field Require" /><label for="field_require_'+questionCounter+'_'+fieldCounter+'">Require</label></div><span class="invalid-feedback d-block" role="alert"><strong></strong></span></div></div><div class="field_value_main clearfix"><div class="field_value_'+questionCounter+'_'+fieldCounter+'_0 clearfix"><div class="form-group col-md-6 field_option_name_f_'+questionCounter+'_'+fieldCounter+'_0"><label for="field_option_name" class=" label">Field Option Name</label><input type="hidden" name="field_option_value['+questionCounter+']['+fieldCounter+'][]" id="field_option_value_'+questionCounter+'_'+fieldCounter+'_0" class="form-control customtinput" placeholder="Enter Field Option Value" value="0"/><input type="text" name="field_option_name['+questionCounter+']['+fieldCounter+'][]" id="field_option_name_'+questionCounter+'_'+fieldCounter+'_0" class="form-control customtinput" placeholder="Enter Field Option Name"/><span class="invalid-feedback d-block" role="alert"><strong></strong></span><a class="add_field_option_val_group plus_btn" attr-id-level-1="'+questionCounter+'" attr-id-level-2="'+fieldCounter+'" attr-id-level-3="0"><i class="fa fa-plus"></i></a></div></div></div></div>';

            $('.question_group_'+questionCounter+' .field_group_main').append(fieldtypedataset);


            $('.form_field_group_'+questionCounter+'_'+fieldCounter+' .field_value_main').hide();
        });

        $(document).on('click', '.remove_field_group', function(){
            //level_3++;
            //alert(level_3);
            questionCounter = $(this).attr('attr-id-level-1');
            fieldCounter = $(this).attr('attr-id-level-2');
            fieldValCounter = $(this).attr('attr-id-level-3');

            //$('.question_group_'+questionCounter+' .form_field_group_'+questionCounter+'_'+fieldCounter+'').remove();
        });
    /******* field type append end ********/

                $(document).on("change",'input, select', function(){

                if(this.value == 'radio'){
                        $('.'+this.id+' .field_value_main').show();
                    }else{
                        $('.'+this.id+' .field_value_main').hide();
                    }
                });

   });


$(document).on('click', '.panel-heading span.clickable', function(e){
    var $this = $(this);
    if(!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
    }
})

</script>
@endsection
