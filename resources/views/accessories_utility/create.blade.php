@extends('layouts.app')


@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <h1>
    Import Accessories Logic Input Sheets 
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
         <li><a href="{{ route('accessories-utility.index') }}">Accessories Logic Input Sheets Upload</a></li>
        <li class="active"></li>
      </ol>
</section>
    <!-- /.content-header -->
     <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                  <!-- /.card-header -->
                  <div class="card-body">
                     <div class="form_container">

                            <?php if(isset($errors) && (count($errors->all()) > 0 )){  ?>
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            <?php } ?>
                        <form class="md-form" action="{{ route('accessories-utility.store') }}" method="POST" enctype="multipart/form-data" >  
                           @csrf 
                           <div class="row">   
                        <div class="custom-file file-field" style="display: flex;float:left;">
                         <input type="hidden" name="table_name" value="st_stock" />
                         <input required="" type="file" class="custom-file-input" id="upoadfile" name="upoadfile" accept="application/vnd.ms-excel">
                          <label class="custom-file-label" for="customFile">Upload your File for ST Stock</label>
                          
                        </div>
                        <div class="custom-file file-field" style="display: flex;">
                             <input type="submit" class="btn btn-primary btn-submit" name="submit" id="submit" value="save" >
                        </div>
                           </div>
                        </form>
                         
                         <form class="md-form" action="{{ route('accessories-utility.store') }}" method="POST" enctype="multipart/form-data" >  
                           @csrf 
                           <div class="row">   
                        <div class="custom-file file-field" style="display: flex;float:left;">
                         <input type="hidden" name="table_name" value="st_option" />
                         <input required="" type="file" class="custom-file-input" id="upoadfile" name="upoadfile" accept="application/vnd.ms-excel">
                          <label class="custom-file-label" for="customFile">Upload your File for ST Option</label>
                          
                        </div>
                        <div class="custom-file file-field" style="display: flex;">
                             <input type="submit" class="btn btn-primary btn-submit" name="submit" id="submit" value="save" >
                        </div>
                           </div>
                        </form>
                         
                          <form class="md-form" action="{{ route('accessories-utility.store') }}" method="POST" enctype="multipart/form-data" >  
                           @csrf
                           <div class="row"> 
                        <div class="custom-file file-field" style="display: flex;float:left;">
                         <input type="hidden" name="table_name" value="st_art_sale" />
                         <input required="" type="file" class="custom-file-input" id="upoadfile" name="upoadfile" accept="application/vnd.ms-excel">
                          <label class="custom-file-label" for="customFile">Upload your File for St Art Sale</label>
                          
                        </div>
                        <div class="custom-file file-field" style="display: flex;">
                             <input type="submit" class="btn btn-primary btn-submit" name="submit" id="submit" value="save" >
                        </div>
                           </div>
                        </form>
                         
                           <form class="md-form" action="{{ route('accessories-utility.store') }}" method="POST" enctype="multipart/form-data" >  
                           @csrf
                           <div class="row"> 
                        <div class="custom-file file-field" style="display: flex;float:left;">
                         <input type="hidden" name="table_name" value="bgt_sale" />
                         <input required="" type="file" class="custom-file-input" id="upoadfile" name="upoadfile" accept="application/vnd.ms-excel">
                          <label class="custom-file-label" for="customFile">Upload your File for Bgt Sale</label>
                          
                        </div>
                        <div class="custom-file file-field" style="display: flex;">
                             <input type="submit" class="btn btn-primary btn-submit" name="submit" id="submit" value="save" >
                        </div>
                           </div>
                        </form>
                         
                                <form class="md-form" action="{{ route('accessories-utility.store') }}" method="POST" enctype="multipart/form-data" >  
                           @csrf
                           <div class="row"> 
                        <div class="custom-file file-field" style="display: flex;float:left;">
                         <input type="hidden" name="table_name" value="dc_stock" />
                         <input required="" type="file" class="custom-file-input" id="upoadfile" name="upoadfile" accept="application/vnd.ms-excel">
                          <label class="custom-file-label" for="customFile">Upload your File for Dc Stock</label>
                          
                        </div>
                        <div class="custom-file file-field" style="display: flex;">
                             <input type="submit" class="btn btn-primary btn-submit" name="submit" id="submit" value="save" >
                        </div>
                           </div>
                        </form>
                      
                         
                          <form class="md-form" action="{{ route('accessories-utility.store') }}" method="POST" enctype="multipart/form-data" >  
                           @csrf
                           <div class="row"> 
                        <div class="custom-file file-field" style="display: flex;float:left;">
                         <input type="hidden" name="table_name" value="bgi_disp" />
                         <input required="" type="file" class="custom-file-input" id="upoadfile" name="upoadfile" accept="application/vnd.ms-excel">
                          <label class="custom-file-label" for="customFile">Upload your File for Bgt Display</label>
                          
                        </div>
                        <div class="custom-file file-field" style="display: flex;">
                             <input type="submit" class="btn btn-primary btn-submit" name="submit" id="submit" value="save" >
                        </div>
                           </div>
                        </form>
                         
                         
                          <form class="md-form" action="{{ route('accessories-utility.store') }}" method="POST" enctype="multipart/form-data" >  
                           @csrf
                           <div class="row"> 
                        <div class="custom-file file-field" style="display: flex;float:left;">
                         <input type="hidden" name="table_name" value="bgt_revised_sale" />
                         <input required="" type="file" class="custom-file-input" id="upoadfile" name="upoadfile" accept="application/vnd.ms-excel">
                          <label class="custom-file-label" for="customFile">Upload your File for Bgt Revised Sale</label>
                          
                        </div>
                        <div class="custom-file file-field" style="display: flex;">
                             <input type="submit" class="btn btn-primary btn-submit" name="submit" id="submit" value="save" >
                        </div>
                           </div>
                        </form>
                         
                         
                </div>
                </div>
                </div>
            </div>
        </section>

@endsection
