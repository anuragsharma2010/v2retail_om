@extends('layouts.app')


@section('content')
<!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
       Accessories Logic Input Sheets Upload Utility
      </h1>
    
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Accessories Logic Input Sheets Upload Utility</li>
      </ol>
    </section>
    <!-- /.content-header -->
    <section class="content">
       <div class="container-fluid">
             <div class="card listcontainer">
              <div class="card-header">
            <div class="col-sm-12 list_filter_head">
             <div class="search_box">
                 <!-- search form -->
                <form action="<?php echo Request::url(); ?>?tablei_id=42" method="get" class="sidebar-form">
                   
                    <div class="input-group">
                        <?php if(isset($data->table_id)){ ?>
                        <input type="hidden" name="table_id" id="table_id" value="<?php echo $data->table_id ?>" />
                        <?php } ?>
                        <input type="text" name="searchtxt" value="<?php echo isset($data->searchtxt) ? $data->searchtxt:'';  ?>" class="form-control search" placeholder="Search...">
                        
                        <span class="input-group-btn">
                        <button type="submit" name="search" value="save" id="search-btn" class="btn btn-flat">
                          <i class="fa fa-search"></i>
                        </button>
                      </span>
                    </div>
                </form>
              </div>
              <div class="userbtn">
                  <select name="sheet_name"  class="option_type  item-per-page btn" onchange="redirectPage(this.value,'{{ route('accessories-utility.index') }}')">
                            <option value="">Select Sheet Name</option>
                            <?php 
                                if(isset($data->table_list))
                                {
                                    foreach($data->table_list as $opRow)
                                    {
                                        ?>
                            <option value="<?php echo $opRow->id ?>" <?php if($data->table_id==$opRow->id) { ?>selected=""<?php } ?> ><?php echo $opRow->excel_sheet_name; ?></option>
                                        <?php
                                    }
                                }
                            ?>
                  </select>
              <a href="{{ route('accessories-utility.create') }}" class="btn btn-sm btn-primary createuser-btn">
                      <i class="fa fa-plus-circle"></i> Upload Excel Sheets
              </a>
                   
            </div>
          </div><!-- /.col -->
              </div>
            <!-- /.card-header -->
            <div class="card-body list_body">
            <div class="message" role="alert">
                @if ($message = Session::get('success'))

                <div class="alert alert-success">
                  <p>{{ $message }}</p>
                </div>

                @endif
              </div>
            
              <div class="table_data">
             <?php if(isset($data->table_id)){ ?>
                 <div class="table_data table-resonponsive" style="overflow-y: scroll;">
              <table class="table table-bordered table-striped" style="width:840px;">
                <thead>
                <tr>
                 
                 <?php foreach($data->fields as $col) {
                     
                     ?>
                    <th><?php echo $col; ?></th>
                     <?php
                 } ?>
                </tr>
                </thead>
                <tbody>
                 <?php  foreach ($data as $row){ 
                     $data_rec= $row->toArray();
                    
                    ?>
                   <tr>
                       
                       <?php 
                        foreach($data->fields as $col) { 
                         ?>
                       <td>
                           <?php echo $data_rec[$col]; ?>
                       </td>
                         <?php
                        }
                       ?>
                  </tr>
                 <?php }?>
                </tbody>
              </table> 
             <?php } ?> 
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
      </div>
    </section>
<?php if(isset($data->table_id)){ ?>
{!! $data->appends(request()->query())->links() !!}

<?php } ?>

@endsection

<script type="text/javascript">
//Used for redirect with table name    
function redirectPage(id,url){
    //window.location(url+"?table_id="+id);
    window.open(url+"?table_id="+id)
}
</script>

