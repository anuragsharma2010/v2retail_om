@extends('layouts.app')


@section('content')
<!-- Content Header (Page header) -->
      <section class="content-header">
      <h1>
       Add User
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ route('users.index') }}"> Users Management</a></li>
        <li class="active">Add User</li>
      </ol>
    </section>
    <!-- /.content-header -->
 <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <!-- /.card-header -->
          <div class="card-body">
             <div class="form_container">

            {!! Form::open(array('route' => 'users.store','method'=>'POST')) !!}
            <div class="row">
              <div class="col-md-6 right_pdng">
                  <div class="form-group">
                      <label >Division Name:</label>
                      <select name="div_id" id="div_id" class="form-control customtinput" onchange="getDeptInfo(this.value,'<?php echo url("/users"); ?>')" >
                          <option value=""> Select Division</option>
                          <?php foreach ($divisions as $key => $value) { ?>
                              <option value="<?php echo $value->id; ?>" <?php //($DataObj->sop_div_id == $value->id) ? 'selected' : ''; ?>><?php echo $value->div_name; ?></option>
                          <?php } ?>
                      </select>
                      @if ($errors->has('div_id'))
                        <div class="error_field" >{{ $errors->first('div_id') }}</div>
                    @enderror
                  </div>
                  <!-- /.form-group -->
                  <!-- /.form-group -->
              </div>
              <div class="col-md-6 left_pdng">
                  <div class="form-group">
                      <label >Department Name:</label>
                      <select name="dept_id" id="dept_id" class="form-control customtinput" onchange="getRoleInfo(this.value,'<?php echo url("/users"); ?>')">
                          <option value=""> Select Department</option>
                          <?php //foreach ($department as $key => $value) { ?>
<!--                              <option value="<?php //echo $value->id; ?>"><?php //echo $value->dept_name; ?></option>-->
                          <?php //} ?>
                      </select>
                      @if ($errors->has('dept_id'))
                        <div class="error_field" >{{ $errors->first('dept_id') }}</div>
                    @enderror
                  </div>
                  <!-- /.form-group -->
                  <!-- /.form-group -->
              </div>
              
              <div class="col-md-6 right_pdng">
                <div class="form-group">
                 <label class="text-muted label">Name:</label>
                   {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                    @if ($errors->has('name'))
                        <div class="error_field" >{{ $errors->first('name') }}</div>
                    @enderror
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="text-muted label">Email:</label>
                  {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
                   @if ($errors->has('email'))
                        <div class="error_field" >{{ $errors->first('email') }}</div>
                    @enderror
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6 left_pdng">
                <div class="form-group">
                  <label class="text-muted label">Password:</label>
                  {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
                   @if ($errors->has('password'))
                        <div class="error_field" >{{ $errors->first('password') }}</div>
                   @enderror
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label class="text-muted label">Confirm Password:</label>
                  {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}
                    @if ($errors->has('confirm-password'))
                        <div class="error_field" >{{ $errors->first('confirm-password') }}</div>
                   @enderror
                </div>
                <!-- /.form-group -->
              </div>
              <div class="col-md-6 right_pdng">
                <div class="form-group" id="div_store_id" style="display: none;">
                 <label class="text-muted label">Store Name:</label>
                 <select name="store_id" id="store_id"  class="form-control customtinput">
                   <option value=""> Select Store</option>
                   <?php// foreach ($stores as $key => $value) { ?>
                    
<!--                     <option value="<?php //echo $value->id; ?>"><?php // echo $value->st_name; ?></option>-->
                  <?php //} ?>
                 </select>
                   @if ($errors->has('store_id'))
                        <div class="error_field" >{{ $errors->first('store_id') }}</div>
                   @enderror
                </div>

              </div>
              <!-- /.col -->
                <!-- /.row -->
            <div class="col-md-12" >
               <div class="form-group" id="div_roles" style="display: none;">
                <label class="text-muted" >Role:</label>
                {!! Form::select('roles[]', $roles,[], array('class' => 'form-control','multiple','id'=>'roles')) !!}
                 @if ($errors->has('roles'))
                        <div class="error_field" >{{ $errors->first('roles') }}</div>
                   @enderror
               </div>
                
            </div>
                  <div class="col-md-12">

                                    <div class="form-group pt-45">
                                        <label class="text-muted label">Status</label>
                                        <div class="icheck-primary d-inline left_pdng">
                                            <input type="radio" id="In-Active"  name="status"<?php if (isset($DataObj->div_status) && $DataObj->div_status==0) { ?>checked=""<?php } ?> value="0" class="custom-control-input">
                                            <label for="In-Active">In-Active
                                            </label>
                                        </div>
                                        <div class="icheck-primary d-inline left_pdng">
                                            <input type="radio"  <?php if (isset($DataObj->status) && $DataObj->status==1) { ?>checked=""<?php } ?>  id="Active" name="status" value="1"  class="custom-control-input">
                                            <label for="Active">Active
                                            </label>
                                        </div>
                                         @if ($errors->has('div_status'))
                                    <div class="error_field" >{{ $errors->first('status') }}</div>
                                    @enderror
                                    </div>
                                </div>
                                        <div class="row" >
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                                
                            </div>
                            <div class="col-md-6 right">
                                <button type="button" class="btn btn-primary btn-submit" onclick="javascript:location.replace('{{ route('users.index') }}')">Back</button>
                            </div>
                            </div>
            </div>
            {!! Form::close() !!}
        </div>
        </div>

    </div>
</section>
<script type="text/javascript" >
    
    function getDeptInfo(id,url){
       // console.log(url+"/"+id+"?flage_ajax=1");
       $.ajax({url: url+"/"+id+"?flage_ajax=1", success: function(result){
         $("#dept_id").html(result);
    }});
   
       if($.trim($( "#div_id" ).find('option:selected').text())=="Store" ||
          $.trim($( "#div_id" ).find('option:selected').text())=="store"){
          $("#div_store_id").removeAttr("style"); 
          //st_name
         $.ajax({url: url+"/"+id+"?flage_ajax=2", success: function(result){
         $("#store_id").html(result);
         $("#dept_id").append("<option value='none'>None</option>");
        }});
       }else {
           $("#div_store_id").attr("style","display:none;"); 
       }
       
    }
    function getRoleInfo(id,url){
        
      
        
        if($.trim($( "#dept_id" ).find('option:selected').text())=="none" ||
          $.trim($( "#div_id" ).find('option:selected').text())=="None"){
            $("#div_store_id").removeAttr("style"); 
        }
        if(id>0){
            
              $.ajax({url: url+"/"+id+"?flage_ajax=3", success: function(result){
                 $("#roles").html(result);
                 
                }});
            
            $("#div_roles").removeAttr("style");
        }else {
            $("#div_roles").attr("style",'display:none');
        }
    }
</script>

@endsection
