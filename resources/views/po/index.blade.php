@extends('layouts.app')

@section('style')
<link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<section class="content-header">
    <h1>
        Pending PO List
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Pending PO List</li>
    </ol>
</section>
<!-- Content Header (Page header) -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <div class="col-sm-12 list_filter_head">
                    <div class="search_box">
                        <!-- search form -->
                        <form action="#" method="get" class="sidebar-form">
                          <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="month_year" id="month_year"
                                        class="form-control dateinput form-control-2 to"
                                        placeholder="Select month and year" value="{{ app('request')->input('month')??\Carbon\Carbon::now()->format('m-Y') }}">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="userbtn">
                        <a href="{{route('po.import.template')}}" class="btn btn-sm btn-primary createuser-btn pt-0 mx-10"> <i
                                class="fa fa-download"></i>Download Template</a>
                        <a href="<?php echo url("/po/import"); ?>" class="btn btn-sm btn-primary createuser-btn pt-0 ">
                            <i class="fa fa-upload"></i> Upload
                        </a>                        
                    </div>
                </div><!-- /.col -->
            </div>
            
            <!-- /.card-header -->
            <div class="card-body">
                     @if ($message = Session::get('success'))
         <div class="message" role="alert">
            <div class="alert alert-success alert-dismissible">
               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
         </div>
         @endif
         <div class="container-fluid p-0 show-enteries-parent">
            <div class="col-md-6 p-0"><label class="show-enteries-text">Show enteries
               {!! Form::select('per_page', [10=>10,50=>50,150=>150,500=>500], app('request')->input('perpage'), ['class'=>'item-per-page','id'=>'item-show-per-page']) !!}
               </label>
            </div>
            <div class="col-md-6 p-0">
               <p class="showing-enteries-text text-right">Showing 1 to 10 of 57 entries</p>
            </div>
         </div>
                <div class="table_data table-responsive">
                    <table class="table table-bordered table-striped w-2700">
                        <thead>
                            <tr>
                                <th>DEL DATE</th>
                                <th>DOC DATE</th>
                                <th>SITE</th>
                                <th>PR DOC NO</th>
                                <th>VENDOR SPP CD</th>
                                <th>VENDOR SPP NAME</th>
                                <th>ARTICLE </th>
                                <th>SEG</th>
                                <th>DIV </th>
                                <th>SUB DIV</th>
                                <th>MAJ-CAT</th>
                                <th>RNG SEG</th>
                                <th>MVGR</th>
                                <th>PGR</th>
                                <th>ITEM</th>
                                <th>NET PRICE</th>
                                <th>ACT-PO-Q</th>
                                <th>ACT-PO-V</th>
                                <th>GR Qty</th>
                                <th>PEND PO-Q</th>
                                <th>PEND PO-V</th>
                            </tr>
                        </thead>
                        <tbody>
                           @forelse($pos as $po)
                            <tr>
                                <td>{{ \Carbon\Carbon::parse($po->del_date)->format('d-m-Y') }}</td>
                                <td>{{ \Carbon\Carbon::parse($po->doc_date)->format('d-m-Y') }}</td>
                                <td> {{ $po->site }} </td>
                                <td> {{ $po->purchase_doc }}</td>
                                <td> {{ $po->vendor }} </td>
                                <td> {{ $po->vendor }} </td>
                                <td> {{ $po->article }} </td>
                                <td>{{ optional($po->articledesc)->mvgr_seg }}</td>
                                <td>{{ optional($po->articledesc)->division }}</td>
                                <td>{{ optional($po->articledesc)->subdivision }}</td>
                                <td>{{ optional($po->articledesc)->new_major_category }}</td>
                                <td>{{ optional($po->articledesc)->range_segment }}</td>
                                <td>{{ optional($po->articledesc)->mvgr_matrix }}</td>
                                <td> {{ $po->pgr }} </td>
                                <td> {{ $po->item }} </td>
                                <td> {{ $po->net_price }} </td>
                                <td> {{ $po->act_po_q }} </td>
                                <td> {{ $po->act_po_v }} </td>
                                <td> {{ $po->gr_qty }} </td>
                                <td> {{ $po->pend_po_q }} </td>
                                <td> {{ $po->pend_po_v }} </td>
                            </tr>
                            @empty
                            <tr>    <td colspan="21"><span class="pull-left">No data found</span></td>    </tr>
                            @endforelse
                        </tbody>
                    </table>
                  
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>

          <div class="col-md-12 text-right pagination">
                        {{$pos->appends(['month' =>  app('request')->input('month'), 'perpage' => app('request')->input('perpage') ])->links()}}
        </div>
</section>

<script>
    $('.remove-message-icon').on('click', function(){
    $(".message").remove();
});
</script>
@endsection
@section('custom-js')
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.to').datepicker({
            autoclose: true,
            minViewMode: 1,
            format: 'mm-yyyy',
            default: true
        });

        $('#month_year').on("change",function(){
            var monthYear = $(this).val();
            var perPage = $('#item-show-per-page').val();
            var url = APP_URL+"/{{Request::segment(1)}}?month="+monthYear+"&perpage="+perPage;
            window.location.href = url;
        });

        $('#item-show-per-page').on('change', function () {
            var monthYear = $('#month_year').val();
            var perPage = $(this).val();
            var url = APP_URL+"/{{Request::segment(1)}}?month="+monthYear+"&perpage="+perPage;
            window.location.href = url;
        });
    });
</script>
@endsection