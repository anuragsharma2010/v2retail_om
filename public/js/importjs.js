
          $("#importform").submit(function(event){
             var vidFileLength = $("input[name='upoadfile']")[0].files.length;
            //alert(vidFileLength)
            if(vidFileLength === 0){
               //alert('hello');
               const message = "Please select the valid file!";
                $('#error_massage').html(message);
            }
             event.preventDefault();
          });

         /* 
          */
        $("input[name='upoadfile']").on('change', function () {
           var fileExtension = ['xlsv', 'csv'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
               const message = "Please select the valid format e.g - "+fileExtension.join(', ');
                  $('#error_massage').html(message);
                  $('#file-name').css('color','#a94442');

               }
            if ($(this).val()) {
                let file_name = $(this).val().split('\\');
                $('#file-name').text(file_name[file_name.length - 1])
            }
        });