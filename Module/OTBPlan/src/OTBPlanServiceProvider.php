<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. ApiServiceProvider
 */
namespace OTBPlan;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class OTBPlanServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
         $this->mergeConfigFrom(__DIR__ . '/config/otbplan.php', 'otbplan');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
       
        // Load Routes
        //$this->loadRoutesFrom(__DIR__ . '/routes/api.php');
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');

        // Load Template Management and Core Views
      //  echo __DIR__ . '/views';die();
        $this->loadViewsFrom(__DIR__ . '/views', 'views');

        // To load the migration file
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');

    }
}
