<?php

namespace OTBPlan\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class MajCatSEGModel extends Model
{
    public $table = 'matcatsegs';
    protected $fillable = [

        'matcat_name', 'matcat_code', 'matcat_desc', 'matcat_status'

    ];


}
