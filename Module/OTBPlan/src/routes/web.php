<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use Illuminate\Support\Facades\Auth;
Auth::routes();
Route::group(['middleware' => ['web', 'auth']], function() {

        Route::resource('otbplan/', 'OTBPlan\Http\Controllers\MajCatController');
        Route::resource('otbplan/majcat', 'OTBPlan\Http\Controllers\MajCatController');
        Route::resource('otbplan/majcatseg', 'OTBPlan\Http\Controllers\MajCatSEGController');
        //Route::resource('otbplan/majcatsegmvgr', 'OTBPlan\Http\Controllers\MajCatSegMvgrController');
});
   //Route::get('/search-agent', function(){ return(redirect('home')); });
