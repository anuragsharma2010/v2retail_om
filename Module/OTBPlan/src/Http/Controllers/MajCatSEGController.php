<?php

namespace OTBPlan\Http\Controllers;



use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\DB;
use OTBPlan\Models\MajCatSEGModel;
use App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use OTBPlan\Models\MajCatSEGImport;
//use App\Helpers;
//use Illuminate\Routing\Controller as BaseController;
class MajCatSEGController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        //print_r(session()->get('success'));
       // print_r($_SESSION);
       return view('views::majcatseg.index'); die();
       if(isset($request->search)){
           $data=MajCatSEGModel::Where('majcatseg_name','like','%'.$request->search.'%')
                             ->orWhere('majcatseg_code','like','%'.$request->search.'%')->orderBy('id','DESC')->paginate(15);
       }else {
           $data=  MajCatSEGModel::orderBy('id','DESC')->paginate(15);
       }

        return view('views::majcatseg.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function edit($id)
    {
       $DataObj=MajCatSEGModel::where(['id'=>$id])->first();

       return view('views::majcatseg.create',['DataObj'=>$DataObj,'id'=>$id]);

    }
    public function create()
    {
       $MajCatSEGObj=new MajCatSEGModel();
       return view('views::majcatseg.create',['MajCatSEGObj'=>$MajCatSEGObj]);
    }
    public function show(Request $request)
    {
      echo "hi..show...";die();
    }

    public function store(Request $request)
    {

        $DataObj=new MajCatSEGModel();
        $validator = Validator::make($request->all(), [
        'majcatseg_name'=>'required|string|max:50',
        'majcatseg_code'=>'required|string|max:90',
        'majcatseg_desc'=>'required|string|max:200',
        'majcatseg_status'=>'required',

        ]);

       if($validator->fails()){

           return view('views::majcatseg.create',['errors'=>$validator->errors(),'DataObj'=>$request]);

       }
        $input = $request->all();

        $DataObj->create($input);
        return redirect()->route('majcatseg.index')

               ->with('success','MajCatSEG created successfully');

    }



    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'majcatseg_name'=>'required|string|max:50',
            'majcatseg_code'=>'required|string|max:90',
            'majcatseg_status'=>'required',
            'id' => 'required'
        ]);

       if($validator->fails()){
           return view('views::majcatseg.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
       }

       if(isset($request->id))
       {
           $DataObj=MajCatSEGModel::find($request->id);
           $DataObj->update($request->all());
           return redirect()->route('majcatseg.index')

               ->with('success','MajCatSEG Updated successfully');

       }else {
           return "Record not found";
       }


    }
    public function destroy($id)

    {

        MajCatSEGModel::find($id)->delete();

        return redirect()->route('majcatseg.index')

                        ->with('success','MajCatSEG deleted successfully');

    }
    public function importExcel(Request $request)
    {
        $MajCatSEGObj=new MajCatSEGModel();
        if($request->submit=='save'){

         $test=Excel::import(new MajCatSEGImport, $request->upoadfile);
         if($test){
         return redirect()->route('majcatseg.index')

               ->with('success','MajCatSEG Updated successfully');
            }
        }

       // $test=Excel::import(new ImportDistributor, $request->upoadfile);
      return view('views::majcatseg.import',['MajCatSEGObj'=>$MajCatSEGObj]);
    }




}
