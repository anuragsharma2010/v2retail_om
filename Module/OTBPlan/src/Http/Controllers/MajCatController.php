<?php

namespace OTBPlan\Http\Controllers;



use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\DB;
use OTBPlan\Models\MajCatModel;
use App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use OTBPlan\Models\MajCatImport;
//use App\Helpers;
//use Illuminate\Routing\Controller as BaseController;
class MajCatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        //print_r(session()->get('success'));
       // print_r($_SESSION);
       return view('views::majcat.index'); die();
       if(isset($request->search)){
           $data=MajCatModel::Where('majcat_name','like','%'.$request->search.'%')
                             ->orWhere('majcat_code','like','%'.$request->search.'%')->orderBy('id','DESC')->paginate(15);
       }else {
           $data=  MajCatModel::orderBy('id','DESC')->paginate(15);
       }

        return view('views::majcat.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function edit($id)
    {
       $DataObj=MajCatModel::where(['id'=>$id])->first();

       return view('views::majcat.create',['DataObj'=>$DataObj,'id'=>$id]);

    }
    public function create()
    {
       $MajCatObj=new MajCatModel();
       return view('views::majcat.create',['MajCatObj'=>$MajCatObj]);
    }
    public function show(Request $request)
    {
      echo "hi..show...";die();
    }

    public function store(Request $request)
    {

        $DataObj=new MajCatModel();
        $validator = Validator::make($request->all(), [
        'majcat_name'=>'required|string|max:50',
        'majcat_code'=>'required|string|max:90',
        'majcat_desc'=>'required|string|max:200',
        'majcat_status'=>'required',

        ]);

       if($validator->fails()){

           return view('views::majcat.create',['errors'=>$validator->errors(),'DataObj'=>$request]);

       }
        $input = $request->all();

        $DataObj->create($input);
        return redirect()->route('majcat.index')

               ->with('success','MajCat created successfully');

    }



    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'majcat_name'=>'required|string|max:50',
            'majcat_code'=>'required|string|max:90',
            'majcat_status'=>'required',
            'id' => 'required'
        ]);

       if($validator->fails()){
           return view('views::majcat.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
       }

       if(isset($request->id))
       {
           $DataObj=MajCatModel::find($request->id);
           $DataObj->update($request->all());
           return redirect()->route('majcat.index')

               ->with('success','MajCat Updated successfully');

       }else {
           return "Record not found";
       }


    }
    public function destroy($id)

    {

        MajCatModel::find($id)->delete();

        return redirect()->route('majcat.index')

                        ->with('success','MajCat deleted successfully');

    }
    public function importExcel(Request $request)
    {
        $MajCatObj=new MajCatModel();
        if($request->submit=='save'){

         $test=Excel::import(new MajCatImport, $request->upoadfile);
         if($test){
         return redirect()->route('majcat.index')

               ->with('success','MajCat Updated successfully');
            }
        }

       // $test=Excel::import(new ImportDistributor, $request->upoadfile);
      return view('views::majcat.import',['MajCatObj'=>$MajCatObj]);
    }




}
