@extends('layouts.app')


@section('content')

<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Create MajCat</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right bg-transparent">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('zones.index') }}">MajCats</a></li>
              <li class="breadcrumb-item active">Create MajCat</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
     <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                  <!-- /.card-header -->
                  <div class="card-body">
                     <div class="form_container">

                            <?php if(isset($errors) && (count($errors->all()) > 0 )){  ?>
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            <?php } ?>
                    <form action="<?php if(isset($id)){ ?>{{ route('zones.update',$id) }} <?php }  else { ?>{{ route('zones.store') }}<?php } ?>" method="POST">
                        @csrf
                        <?php if(isset($id)){ ?>
                       @method('PUT')
                        <?php } ?>
               <input type="hidden" name='id' name='id' value="<?php if(isset($id)){ echo $id; }  ?>" />
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                         <label >Name:</label>
                            <input  type="text" name="zone_name" id="zone_name" value="<?php if(isset($DataObj->zone_name)){ echo $DataObj->zone_name; } ?>" class="form-control" placeholder="Name" maxlength="50">
                        </div>
                        <!-- /.form-group -->
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6">
                         <div class="form-group">
                          <label>Code:</label>
                           <input type="text" value="<?php if(isset($DataObj->zone_code)){ echo $DataObj->zone_code; } ?>"   class="form-control" id="zone_code" name="zone_code" placeholder="code" maxlength="20">
                        </div>
                        <!-- /.form-group -->

                      </div>
                      <!-- /.col -->
                      <div class="col-md-12">
                         <div class="form-group">
                          <label>Description:</label>
                           <textarea id="zone_desc"  id="zone_desc" name="zone_desc" class="form-control" placeholder="Description" maxlength="300"><?php if(isset($DataObj->zone_code)){ echo $DataObj->zone_code; } ?></textarea>
                        </div>
                      </div>
                      <div class="col-md-12">
                         <div class="form-group">
                          <label>Status</label>

                            <div class="icheck-success d-inline">
                               <label> <input type="radio" id="zone_status" name="zone_status"<?php if(!isset($DataObj->zone_status)){ ?>checked=""<?php } ?> value="0" class="custom-control-input">
                                <label for="radioSuccess2">In-Active
                             </label></label>
                        </div>
                           <div class="icheck-success d-inline">
                                <label><input type="radio"  <?php if(isset($DataObj->zone_status)){ ?>checked=""<?php } ?>  id="zone_status" name="zone_status" value="1"  class="custom-control-input">
                                <label for="radioSuccess2">Active
                             </label></label>
                        </div>
                        </div>
                      </div>
                    <div class="col-md-12">
                         <button type="submit" class="btn btn-primary">Submit</button>
                     </div>
                    </div>
                    </form>
                </div>
                </div>
                </div>
            </div>
        </section>

@endsection
