@extends('layouts.app')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
      <h1>
    MAJ CAT List
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">MAJ CAT List</li>
      </ol>
    </section>
    <!-- /.content-header -->
     <section class="content">
       <div class="container-fluid">
             <div class="card">
              <div class="card-header">
                <div class="col-sm-12 list_filter_head">
                <div class="search_box">
                 <!-- search form -->
                 <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control search" placeholder="Search...">
                        <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                          <i class="fa fa-search"></i>
                        </button>
                      </span>
                    </div>
                </form>
              </div>
              <div class="userbtn">
                  <a href="<?php echo url("/OTBPlan/majcat-import-excel"); ?>" class="btn btn-sm btn-primary createuser-btn">
                       Import Excel
              </a>

            </div>
            </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
               <div class="message" role="alert">
               @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
              </div>
              <div class="table_data">
              <table class="table table-bordered table-striped">
                <thead>
                <tr>
                 <th>No</th>
                  <th>Name</th>
                  <th>Code</th>
                  <th>Description</th>
                   <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            <div class="col-md-12 text-right pagination">
             
            </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
      </div>
    </section>

@endsection
