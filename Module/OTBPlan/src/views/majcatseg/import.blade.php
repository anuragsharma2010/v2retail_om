@extends('layouts.app')


@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <h1>
     Import MajCatSEG
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="{{ route('zones.index') }}">MajCatSEGs</a></li>
        <li class="active">Excel Import</li>
      </ol>
</section>
    <!-- /.content-header -->
     <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                  <!-- /.card-header -->
                  <div class="card-body">
                     <div class="form_container">

                            <?php if(isset($errors) && (count($errors->all()) > 0 )){  ?>
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            <?php } ?>
                      <form class="md-form" action="" method="POST" enctype="multipart/form-data" >  
                           @csrf    
                        <div class="custom-file file-field">
                          <label class="uploadfile_lab">Choose File</label>
                          <input type="file" class="custom-file-input" id="customFile">
                          <label class="custom-file-label" for="customFile">Upload your File</label>
                        </div>
                        <div class="form-group pt-2 text-center">
                        <input type="submit" class="btn btn-primary btn-submit" name="submit" id="submit" value="Submit">
                        </div>
                    </form>
                </div>
                </div>
                </div>
            </div>
        </section>

@endsection
