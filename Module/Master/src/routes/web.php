<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use Illuminate\Support\Facades\Auth;
Auth::routes();

Route::group(['middleware' => ['web', 'auth']], function() {

     //   echo "hi";die();
        Route::resource('master/', 'Master\Http\Controllers\CategoriesController');

        Route::resource('master/articles', 'Master\Http\Controllers\ArticlesController');
        Route::get('master/articles-import-excel', 'Master\Http\Controllers\ArticlesController@importExcel');
        Route::post('master/articles-import-excel', 'Master\Http\Controllers\ArticlesController@importExcel');

        Route::resource('master/distributioncenters', 'Master\Http\Controllers\DistributioncentersController');
        Route::get('master/distributioncenters-import-excel', 'Master\Http\Controllers\DistributioncentersController@importExcel');
        Route::post('master/distributioncenters-import-excel', 'Master\Http\Controllers\DistributioncentersController@importExcel');

        Route::resource('master/stores', 'Master\Http\Controllers\StoresController');
        Route::get('master/stores-import-excel', 'Master\Http\Controllers\StoresController@importExcel');
        Route::post('master/stores-import-excel', 'Master\Http\Controllers\StoresController@importExcel');

        Route::resource('master/storefloors', 'Master\Http\Controllers\StorefloorsController');
        Route::get('master/storefloors-import-excel', 'Master\Http\Controllers\StorefloorsController@importExcel');
        Route::post('master/storefloors-import-excel', 'Master\Http\Controllers\StorefloorsController@importExcel');

        Route::resource('master/storetypes', 'Master\Http\Controllers\StoretypesController');
        Route::get('master/storetypes-import-excel', 'Master\Http\Controllers\StoretypesController@importExcel');
        Route::post('master/storetypes-import-excel', 'Master\Http\Controllers\StoretypesController@importExcel');

        Route::resource('master/categories', 'Master\Http\Controllers\CategoriesController');
        Route::get('master/categories-import-excel', 'Master\Http\Controllers\CategoriesController@importExcel');
        Route::post('master/categories-import-excel', 'Master\Http\Controllers\CategoriesController@importExcel');

        Route::resource('master/subcategories', 'Master\Http\Controllers\SubcategoriesController');
        Route::get('master/subcategories-import-excel', 'Master\Http\Controllers\SubcategoriesController@importExcel');
        Route::post('master/subcategories-import-excel', 'Master\Http\Controllers\SubcategoriesController@importExcel');

        Route::resource('master/segments', 'Master\Http\Controllers\SegmentsController');
        Route::get('master/segments-import-excel', 'Master\Http\Controllers\SegmentsController@importExcel');
        Route::post('master/segments-import-excel', 'Master\Http\Controllers\SegmentsController@importExcel');

        Route::resource('master/divisions', 'Master\Http\Controllers\DivisionsController');
        Route::get('master/divisions-import-excel', 'Master\Http\Controllers\DivisionsController@importExcel');
        Route::post('master/divisions-import-excel', 'Master\Http\Controllers\DivisionsController@importExcel');

        Route::resource('master/subdivisions', 'Master\Http\Controllers\SubdivisionsController');
        Route::get('master/subdivisions-import-excel', 'Master\Http\Controllers\SubdivisionsController@importExcel');
        Route::post('master/subdivisions-import-excel', 'Master\Http\Controllers\SubdivisionsController@importExcel');

        Route::resource('master/seasons', 'Master\Http\Controllers\SeasonsController');
        Route::get('master/seasons-import-excel', 'Master\Http\Controllers\SeasonsController@importExcel');
        Route::post('master/seasons-import-excel', 'Master\Http\Controllers\SeasonsController@importExcel');

        Route::resource('master/zones', 'Master\Http\Controllers\ZonesController');
        Route::get('master/zones-import-excel', 'Master\Http\Controllers\ZonesController@importExcel');
        Route::post('master/zones-import-excel', 'Master\Http\Controllers\ZonesController@importExcel');

        Route::resource('master/areas', 'Master\Http\Controllers\AreasController');
        Route::get('master/areas-import-excel', 'Master\Http\Controllers\AreasController@importExcel');
        Route::post('master/areas-import-excel', 'Master\Http\Controllers\AreasController@importExcel');

        Route::resource('master/regions', 'Master\Http\Controllers\RegionsController');
        Route::get('master/regions-import-excel', 'Master\Http\Controllers\RegionsController@importExcel');
        Route::post('master/regions-import-excel', 'Master\Http\Controllers\RegionsController@importExcel');

        Route::resource('master/ranges', 'Master\Http\Controllers\RangesController');
        Route::get('master/ranges-import-excel', 'Master\Http\Controllers\RangesController@importExcel');
        Route::post('master/ranges-import-excel', 'Master\Http\Controllers\RangesController@importExcel');

        Route::resource('master/groups', 'Master\Http\Controllers\GroupsController');
        Route::get('master/groups-import-excel', 'Master\Http\Controllers\GroupsController@importExcel');
        Route::post('master/groups-import-excel', 'Master\Http\Controllers\GroupsController@importExcel');

        Route::resource('master/states', 'Master\Http\Controllers\StatesController');
        Route::get('master/states-import-excel', 'Master\Http\Controllers\StatesController@importExcel');
        Route::post('master/states-import-excel', 'Master\Http\Controllers\StatesController@importExcel');
    });
   //Route::get('/search-agent', function(){ return(redirect('home')); });
