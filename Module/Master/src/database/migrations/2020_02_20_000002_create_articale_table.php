<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('articles')) {
            Schema::create('articles', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->mediumText('new_major_category')->nullable();
                $table->mediumText('new_major_category_code')->nullable();
                $table->mediumText('report_major_category_code')->nullable();
                $table->text('report_major_category_desc')->nullable();
                $table->string('range_status')->nullable();
                $table->string('master_range_status')->nullable();
                $table->string('brand')->nullable();
                $table->string('box_status')->nullable();
                $table->text('job_work')->nullable();
                $table->string('fabric_supplier')->nullable();
                $table->float('cost')->nullable();
                $table->integer('vnd_dzn_no')->length(10)->unsigned()->nullable();
                $table->text('major_category_reference_article')->nullable();
                $table->text('major_category_reference_description')->nullable();
                $table->string('major_category_reference_status')->nullable();
                $table->text('vnd_reference_article')->nullable();
                $table->text('vnd_reference_description')->nullable();
                $table->string('vnd_reference_status')->nullable();
                $table->string('refrence_article_type')->nullable();
                $table->text('purchase_requirement_store_request')->nullable();
                $table->text('fabric')->nullable();
                $table->text('sub_fab')->nullable();
                $table->string('style')->nullable();
                $table->string('sub_style')->nullable();
                $table->string('mvgr_matrix')->nullable();
                $table->string('display_mvgr_matrix')->nullable();
                $table->string('mvgr_seg')->nullable();
                $table->string('all_mvgr_combination')->nullable();
                $table->string('fabric_combination')->nullable();
                $table->string('style_combination')->nullable();
                $table->string('product_article')->nullable();
                $table->string('10_digit')->nullable();
                $table->string('general_article')->nullable();
                $table->string('general_article_description')->nullable();
                $table->bigInteger('article')->length(18)->unsigned()->nullable();
                $table->string('article_description')->nullable();
                $table->float('mrp_1')->nullable();
                $table->float('mrp_2')->nullable();
                $table->bigInteger('mrp_type')->nullable();
                $table->bigInteger('range_segment')->nullable();
                $table->string('package_size')->nullable();
                $table->string('general_article_status')->nullable();
                $table->string('variable_article_status')->nullable();
                $table->string('vendor_code')->nullable();
                $table->string('size')->nullable();
                $table->string('color')->nullable();
                $table->string('article_type')->nullable();
                $table->float('report_mrp')->nullable();
                $table->string('refernce_article')->nullable();
                $table->string('fab_1')->nullable();
                $table->string('fab_2')->nullable();
                $table->string('fab_3')->nullable();
                $table->string('fab_4')->nullable();
                $table->string('fabric_print_type')->nullable();
                $table->string('fabric_print_color')->nullable();
                $table->string('count_gsm')->nullable();
                $table->string('neck_collar')->nullable();
                $table->string('styles_1')->nullable();
                $table->string('styles_2')->nullable();
                $table->string('fitting')->nullable();
                $table->integer('length')->length(10)->unsigned()->nullable();
               
                $table->integer('shade')->length(10)->unsigned()->nullable();
                $table->string('slveeve_1')->nullable();
                $table->string('slveeve_2')->nullable();
                $table->string('garment_print')->nullable();
                $table->string('embroidery')->nullable();
                $table->string('wash')->nullable();
                $table->string('pocket')->nullable();
                $table->string('pattern')->nullable();
                $table->string('belt')->nullable();
                $table->string('brand_vendor')->nullable();
                $table->string('colour_generic_article_number')->nullable();
                $table->string('colour_generic_article_desc')->nullable();
                $table->string('division')->nullable();
                $table->string('subdivision')->nullable();
                $table->string('category_desc')->nullable();
                $table->string('sub_division_desc')->nullable();
                $table->string('division_name')->nullable();
                $table->float('average_price')->nullable();
                $table->float('price_range')->nullable();
                $table->tinyInteger('status')->nullable();
                //belt

                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('articles')) {
            Schema::dropIfExists('articles');
        }
    }
}
