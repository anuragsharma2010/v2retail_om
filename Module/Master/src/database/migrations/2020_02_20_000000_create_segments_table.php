<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSegmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('segments')) {
            Schema::create('segments', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('seg_name',50);
                $table->mediumText('seg_code');
                $table->text('seg_desc');
                $table->enum('seg_status',[0,1])->dafault(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('segments')) {
            Schema::dropIfExists('segments');
        }
    }
}
