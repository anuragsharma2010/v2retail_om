<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('subcategories')) {
            Schema::create('subcategories', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('subcat_name',50);
                $table->mediumText('subcat_code');
                $table->text('subcat_desc');
                $table->enum('subcat_status',[0,1])->dafault(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('subcategories')) {
            Schema::dropIfExists('subcategories');
        }
    }
}
