<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStorefloorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('storefloors')) {
            Schema::create('storefloors', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('storefloors_name',50);
                $table->mediumText('storefloors_code');
                $table->text('storefloors_desc');
                $table->enum('storefloors_status',[0,1])->dafault(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('storefloors')) {
            Schema::dropIfExists('storefloors');
        }
    }
}
