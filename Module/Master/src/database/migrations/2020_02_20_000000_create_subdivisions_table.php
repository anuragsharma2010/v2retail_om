<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubdivisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('subdivisions')) {
            Schema::create('subdivisions', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('subdiv_name',50);
                $table->mediumText('subdiv_code');
                $table->text('subdiv_desc');
                $table->enum('subdiv_status',[0,1])->dafault(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('subdivisions')) {
            Schema::dropIfExists('subdivisions');
        }
    }
}
