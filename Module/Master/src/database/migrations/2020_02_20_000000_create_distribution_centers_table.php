<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class CreateDistributionCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('distribution_center')) {
            Schema::create('distribution_center', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('dc_name',50);
                $table->mediumText('dc_code');
                $table->text('dc_desc');
                $table->enum('dc_status',[0,1])->dafault(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('distribution_center')) {
            Schema::dropIfExists('distribution_center');
        }
    }
}
