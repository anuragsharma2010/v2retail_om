<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('ranges')) {
            Schema::create('ranges', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('range_name',50);
                $table->mediumText('range_code');
                $table->text('range_desc');
                $table->enum('range_status',[0,1])->dafault(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('ranges')) {
            Schema::dropIfExists('ranges');
        }
    }
}
