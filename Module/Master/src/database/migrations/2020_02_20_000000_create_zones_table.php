<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('zones')) {
            Schema::create('zones', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('zone_name',50);
                $table->mediumText('zone_code');
                $table->text('zone_desc');
                $table->enum('zone_status',[0,1])->dafault(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('zones')) {
            Schema::dropIfExists('zones');
        }
    }
}
