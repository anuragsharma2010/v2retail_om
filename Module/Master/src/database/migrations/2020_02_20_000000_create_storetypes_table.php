<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoretypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('storetypes')) {
            Schema::create('storetypes', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('storetype_name',50);
                $table->mediumText('storetype_code');
                $table->text('storetype_desc');
                $table->enum('storetype_status',[0,1])->dafault(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('storetypes')) {
            Schema::dropIfExists('storetypes');
        }
    }
}
