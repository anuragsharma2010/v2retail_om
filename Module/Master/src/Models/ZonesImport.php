<?php

namespace Master\Models;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Master\Models\ZonesModel;
class ZonesImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
                $alreday_exist=ZonesModel::Where(['zone_code'=>$row['zone_code']])
                ->first();
        if(!trim($row['zone_name']) or !trim($row['zone_code']) or !trim($row['zone_desc']) or !trim($row['zone_status'])){
           
        } else if(isset($alreday_exist->zone_code)){
            
        }else {
        return new ZonesModel([
            'zone_name'=>$row['zone_name'],
            'zone_code'=>$row['zone_code'],
            'zone_desc'=>$row['zone_desc'],
            'zone_status'=>trim($row['zone_status']),
            
            
        ]);
         }
    }
}
