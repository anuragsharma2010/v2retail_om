<?php

namespace Master\Models;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Master\Models\SubdivisionsModel;
class SubdivisionsImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
                $alreday_exist=SubdivisionsModel::Where(['subdiv_code'=>$row['subdiv_code']])
                ->first();
        if(!trim($row['subdiv_name']) or !trim($row['subdiv_code']) or !trim($row['subdiv_desc']) or !trim($row['subdiv_status'])){

        } else if(isset($alreday_exist->subdiv_code)){

        }else {
        return new SubdivisionsModel([
            'subdiv_name'=>$row['subdiv_name'],
            'subdiv_code'=>$row['subdiv_code'],
            'subdiv_desc'=>$row['subdiv_desc'],
            'subdiv_status'=>trim($row['subdiv_status']),


        ]);
         }
    }
}
