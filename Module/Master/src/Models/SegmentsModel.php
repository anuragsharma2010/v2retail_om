<?php

namespace Master\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class SegmentsModel extends Model
{
    public $table = 'segments';
    protected $fillable = [

        'seg_name', 'seg_code', 'seg_desc', 'seg_status',

    ];

}
