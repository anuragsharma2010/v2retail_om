<?php

namespace Master\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class RegionsModel extends Model
{

    public $table = 'regions';
    protected $fillable = [

        'region_name', 'region_code', 'region_desc', 'region_status',

    ];
      public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }


}
