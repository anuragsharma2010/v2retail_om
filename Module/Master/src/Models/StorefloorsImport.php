<?php

namespace Master\Models;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Master\Models\StorefloorsModel;
class StorefloorsImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
                $alreday_exist=StorefloorsModel::Where(['storefloor_code'=>$row['storefloor_code']])
                ->first();
        if(!trim($row['storefloor_name']) or !trim($row['storefloor_code']) or !trim($row['storefloor_desc']) or !trim($row['storefloor_status'])){

        } else if(isset($alreday_exist->storefloor_code)){

        }else {
        return new StorefloorsModel([
            'storefloor_name'=>$row['storefloor_name'],
            'storefloor_code'=>$row['storefloor_code'],
            'storefloor_desc'=>$row['storefloor_desc'],
            'storefloor_status'=>trim($row['storefloor_status']),


        ]);
         }
    }
}
