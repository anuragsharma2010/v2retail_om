<?php

namespace Master\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ArticlesModel extends Model
{
    public $table = 'articles';
    protected $fillable = [

       'new_major_category',
        'new_major_category_code',
       'report_major_category_code',
       'report_major_category_desc',
       'range_status',
            'master_range_status',
            'brand',
            'box_status',
            'job_work',
             'fabric_supplier',
            'cost',
             'vnd_dzn_no',
            'major_category_reference_article',
             'major_category_reference_description',
             'major_category_reference_status',
            'vnd_reference_article',
            'vnd_reference_description',
            'vnd_reference_status',
            'refrence_article_type',
            'purchase_requirement_store_request',
            'fabric',
            'sub_fab',
            'style',
            'sub_style',
            'mvgr_matrix',
            'display_mvgr_matrix',
            'mvgr_seg',
            'all_mvgr_combination',
            'fabric_combination',
            'fabric_combination',
            'style_combination',
            'product_article',
            '10_digit',
            'general_article',
            'general_article_description',
            'article',
            'article_description',
            'mrp_1',
            'mrp_2',
            'mrp_type',
            'range_segment',
            'package_size',
            'general_article_status',
            'variable_article_status',
            'vendor_code',
            'size',
            'color',
            'article_type',
            'report_mrp',
            'refernce_article',
            'fab_1',
            'fab_2',
            'fab_3',
            'fab_4',
            'fabric_print_type',
            'fabric_print_color',
            'count_gsm',
            'neck_collar',
            'styles_1',
            'styles_2',
            'fitting',
            'length',
            'shade',
            'slveeve_1',
            'slveeve_2',
            'garment_print',
            'embroidery',
            'wash',
            'pocket',
            'pattern',
            'belt',
            'brand_vendor',
            'colour_generic_article_number',
            'colour_generic_article_desc',
            'division',
            'subdivision',
            'category_desc',
            'sub_division_desc',
            'division_name',
            'average_price',
            'price_range',
            'status',

    ];
 public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }

}
