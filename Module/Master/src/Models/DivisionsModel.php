<?php

namespace Master\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class DivisionsModel extends Model
{
    public $table = 'divisions';

    protected $fillable = [

        'div_name', 'div_code', 'div_desc', 'div_status',

    ];
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }

}
