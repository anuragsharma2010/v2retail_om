<?php

namespace Master\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class ZonesModel extends Model
{
    public $table = 'zones';
    protected $fillable = [

        'zone_name', 'zone_code', 'zone_desc', 'zone_status',

    ];
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }
    
    
}
