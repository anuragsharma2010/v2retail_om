<?php

namespace Master\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class StatesModel extends Model
{
    public $table = 'states';
    public $timestamps = false;
    protected $fillable = [

        'state', 'st_code'

    ];
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }


}
