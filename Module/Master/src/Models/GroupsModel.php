<?php

namespace Master\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class GroupsModel extends Model
{
    public $table = 'groups';
    protected $fillable = [

        'group_name', 'group_code', 'group_desc', 'group_status',

    ];
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }


}
