<?php

namespace Master\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class RangesModel extends Model
{
    public $table = 'ranges';
    protected $fillable = [

        'range_name', 'range_code', 'range_desc', 'range_status',

    ];
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }
    


}
