<?php

namespace Master\Models;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Master\Models\ArticlesModel;
class ArticlesImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        $dataobj=new ArticlesModel();
        $fields=$dataobj->getTableColumns();
//        $alreday_exist=ArticlesModel::Where(['zone_code'=>$row['zone_code']])
//                ->first();
//        if(!trim($row['zone_name']) or !trim($row['zone_code']) or !trim($row['zone_desc']) or !trim($row['zone_status'])){
//
//        } else if(isset($alreday_exist->zone_code)){
//
//        }else {
        return new ArticlesModel([
            'new_major_category'=>$row['new_major_category'],
            'report_major_category_code'=>$row['report_major_category_code'],
            'report_major_category_desc'=>$row['report_major_category_desc'],
            'range_status'=>trim($row['range_status']),
            'master_range_status'=>trim($row['master_range_status']),
            'brand'=>trim($row['brand']),
            'box_status'=>trim($row['box_status']),
            'job_work'=>trim($row['job_work']),
             'fabric_supplier'=>trim($row['fabric_supplier']),
            'cost'=>trim($row['cost']) ? trim($row['cost']):0.0,
             'vnd_dzn_no'=>trim($row['vnd_dzn_no']),
            'major_category_reference_article'=>trim($row['major_category_reference_article']),
             'major_category_reference_description'=>trim($row['major_category_reference_description']),
             'major_category_reference_status'=>trim($row['major_category_reference_status']),
            'vnd_reference_article'=>trim($row['vnd_reference_article']),
            'vnd_reference_description'=>trim($row['vnd_reference_description']),
            'vnd_reference_status'=>trim($row['vnd_reference_status']),
            'refrence_article_type'=>trim($row['refrence_article_type']),
            'purchase_requirement_store_request'=>trim($row['purchase_requirement_store_request']),
            'fabric'=>trim($row['fabric']),
            'sub_fab'=>trim($row['sub_fab']),
            'style'=>trim($row['style']),
            'sub_style'=>trim($row['sub_style']),
            'mvgr_matrix'=>trim($row['mvgr_matrix']),
            'display_mvgr_matrix'=>trim($row['display_mvgr_matrix']),
            'mvgr_seg'=>trim($row['mvgr_seg']),
            'all_mvgr_combination'=>trim($row['all_mvgr_combination']),
            'fabric_combination'=>trim($row['fabric_combination']),
            'fabric_combination'=>trim($row['fabric_combination']),
            'style_combination'=>trim($row['style_combination']),
            'product_article'=>trim($row['product_article']),
            '10_digit'=>trim($row['10_digit']),
            'general_article'=>trim($row['general_article']),
            'general_article_description'=>trim($row['general_article_description']),
            'article'=>trim($row['article']),
            'article_description'=>trim($row['article_description']),
            'mrp_1'=>trim($row['mrp_1']),
            'mrp_2'=>trim($row['mrp_2']),
            'mrp_type'=>trim($row['mrp_type']),
            'range_segment'=>trim($row['range_segment']),
            'package_size'=>trim($row['package_size']),
            'general_article_status'=>trim($row['general_article_status']),
            'variable_article_status'=>trim($row['variable_article_status']),
            'vendor_code'=>trim($row['vendor_code']),
            'size'=>trim($row['size']),
            'color'=>trim($row['color']),
            'article_type'=>trim($row['article_type']),
            'report_mrp'=>trim($row['report_mrp']),
            'refernce_article'=>trim($row['refernce_article']),
            'fab_1'=>trim($row['fab_1']),
            'fab_2'=>trim($row['fab_2']),
            'fab_3'=>trim($row['fab_3']),
            'fab_4'=>trim($row['fab_4']),
            'fabric_print_type'=>trim($row['fabric_print_type']),
            'fabric_print_color'=>trim($row['fabric_print_color']),
            'count_gsm'=>trim($row['count_gsm']),
            'neck_collar'=>trim($row['neck_collar']),
            'styles_1'=>trim($row['styles_1']),
            'styles_2'=>trim($row['styles_2']),
            'fitting'=>trim($row['fitting']),
            'length'=>trim($row['length']),
            'shade'=>trim($row['shade']),
            'slveeve_1'=>trim($row['slveeve_1']),
            'slveeve_2'=>trim($row['slveeve_2']),
            'garment_print'=>trim($row['garment_print']),
            'embroidery'=>trim($row['embroidery']),
            'wash'=>trim($row['wash']),
            'pocket'=>trim($row['pocket']),
            'pattern'=>trim($row['pattern']),
            'belt'=>trim($row['belt']),
             'brand_vendor'=>trim($row['brand_vendor']),
            'colour_generic_article_number'=>trim($row['colour_generic_article_number']),
            'colour_generic_article_desc'=>trim($row['colour_generic_article_desc']),
            'division'=>trim($row['division']),
            'subdivision'=>trim($row['subdivision']),
            'category_desc'=>trim($row['category_desc']),
            'sub_division_desc'=>trim($row['sub_division_desc']),
            'division_name'=>trim($row['division_name']),
            'average_price'=>trim($row['average_price']),
            'price_range'=>trim($row['price_range']),
            'status'=>trim($row['status']),
        ]);
    //     }
    }
}
