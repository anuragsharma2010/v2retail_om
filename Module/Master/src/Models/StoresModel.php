<?php

namespace Master\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class StoresModel extends Model
{
    public $table = 'stores';
    protected $fillable = [

        'st_name', 'st_code', 'st_ref_code', 'st_desc', 'st_email', 'st_address', 'st_type', 'st_noof_floor', 'st_area', 'st_radius', 'st_region', 'st_zone', 'st_city',
        'st_state', 'st_country', 'st_zipcode', 'st_phone', 'st_mobile', 'st_fax', 'st_key', 'st_contact', 'st_resource', 'st_image', 'st_dc', 'st_group',
        'st_opening_date', 'st_closing_date', 'st_age', 'st_longitude', 'st_latitude', 'st_status',

    ];
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }
    public function State()
    {
        return $this->belongsTo('Master\Models\StateModel', 'st_state');
    }
    // public function City()
    // {
    //      return $this->belongsTo('Master\Models\StoresModel', 'store_id');
    // }

}
