<?php

namespace Master\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class SubcategoriesModel extends Model
{
    public $table = 'subcategories';

    protected $fillable = [

        'subcat_name', 'subcat_code', 'subcat_desc', 'subcat_status',

    ];
    
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }

}
