<?php

namespace Master\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class AreasModel extends Model
{
    
    public $table = 'areas';
    protected $fillable = [

        'area_name', 'area_code', 'area_desc', 'area_status',

    ];
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }
    
    
}
