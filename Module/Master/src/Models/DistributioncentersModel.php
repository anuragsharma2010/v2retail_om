<?php

namespace Master\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class DistributioncentersModel extends Model
{
    public $table = 'distribution_center';
    protected $fillable = [

        'dc_name', 'dc_code', 'dc_desc', 'dc_status',

    ];


}
