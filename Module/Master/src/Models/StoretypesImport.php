<?php

namespace Master\Models;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Master\Models\StoretypesModel;
class StoretypesImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
                $alreday_exist=StoretypesModel::Where(['storetype_code'=>$row['storetype_code']])
                ->first();
        if(!trim($row['storetype_name']) or !trim($row['storetype_code']) or !trim($row['storetype_desc']) or !trim($row['storetype_status'])){

        } else if(isset($alreday_exist->storetype_code)){

        }else {
        return new StoretypesModel([
            'storetype_name'=>$row['storetype_name'],
            'storetype_code'=>$row['storetype_code'],
            'storetype_desc'=>$row['storetype_desc'],
            'storetype_status'=>trim($row['storetype_status']),


        ]);
         }
    }
}
