<?php

namespace Master\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
//use Master/
class StoretypesModel extends Model
{
    public $table = 'storetypes';
    protected $fillable = [

        'storetype_name', 'storetype_code', 'storetype_desc', 'storetype_status',

    ];

    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }
}
