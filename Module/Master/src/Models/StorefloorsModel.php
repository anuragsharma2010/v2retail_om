<?php

namespace Master\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class StorefloorsModel extends Model
{
    public $table = 'storefloors';
    protected $fillable = [

        'storefloors_name', 'storefloors_code', 'storefloors_desc', 'storefloors_tatus',

    ];
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }

}
