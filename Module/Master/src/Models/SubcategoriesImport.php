<?php

namespace Master\Models;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Master\Models\SubcategoriesModel;
class SubcategoriesImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {

        $alreday_exist=  SubcategoriesModel::Where(['subcat_code'=>$row['subcat_code']])
                ->first();
        if(!trim($row['subcat_name']) or !trim($row['subcat_code']) or !trim($row['subcat_desc']) or !trim($row['subcat_status'])){

        } else if(isset($alreday_exist->subcat_code)){

        }else {
        return new SubcategoriesModel([
            'subcat_name'=>$row['subcat_name'],
            'subcat_code'=>$row['subcat_code'],
            'subcat_desc'=>$row['subcat_desc'],
            'subcat_status'=>trim($row['subcat_status']),


        ]);
         }
    }
}
