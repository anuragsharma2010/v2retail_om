<?php

namespace Master\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class SubdivisionsModel extends Model
{
    public $table = 'subdivisions';

    protected $fillable = [

        'subdiv_name', 'subdiv_code', 'subdiv_desc', 'subdiv_status',

    ];
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }

}
