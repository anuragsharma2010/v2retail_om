<?php

namespace Master\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class SeasonsModel extends Model
{
    public $table = 'seasons';

    protected $fillable = [

        'ssn_name', 'ssn_code', 'ssn_desc', 'ssn_status',

    ];
    public function getTableColumns() {
        return $this
            ->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($this->getTable());
    }

}
