<?php

namespace Master\Http\Controllers;



use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\DB;
use Master\Models\RangesModel;
use App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Master\Models\RangesImport;
//use App\Helpers;
//use Illuminate\Routing\Controller as BaseController;
class RangesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        //print_r(session()->get('success'));
       // print_r($_SESSION);

       $data = new RangesModel();
        $fields=$data->getTableColumns(); 
       if(isset($request->search) && isset($request->searchtxt)){
          foreach ($fields as $value) {
              if($value=='id' or 
                      $value=='created_at' 
                      or $value=='updated_at' or $value=='st_status'){
                  continue;
              }
             $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');  
           }
         $data=$data->orderBy('id','DESC')->paginate(15);
       }else {
           $data=$data->orderBy('id','DESC')->paginate(15);
       }
       $data->searchtxt=$request->searchtxt?$request->searchtxt:'';

        return view('views::ranges.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);


    }
    public function edit($id)
    {
       $DataObj=RangesModel::where(['id'=>$id])->first();

       return view('views::ranges.create',['DataObj'=>$DataObj,'id'=>$id]);

    }
    public function create()
    {
       $RangeObj=new RangesModel();
       return view('views::ranges.create',['RangeObj'=>$RangeObj]);
    }
    public function show(Request $request)
    {
      echo "hi..show...";die();
    }

    public function store(Request $request)
    {

        $DataObj=new RangesModel();
        $validator = Validator::make($request->all(), [
        'range_name'=>'required|string|max:50',
        'range_code'=>'required|string|max:90',
        'range_desc'=>'required|string|max:200',
        'range_status'=>'required',

        ]);

       if($validator->fails()){

           return view('views::ranges.create',['errors'=>$validator->errors(),'DataObj'=>$request]);

       }
        $input = $request->all();

        $DataObj->create($input);
        return redirect()->route('ranges.index')

               ->with('success','Range created successfully');

    }



    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'range_name'=>'required|string|max:50',
            'range_code'=>'required|string|max:90',
            'range_status'=>'required',
            'id' => 'required'
        ]);

       if($validator->fails()){
           return view('views::ranges.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
       }

       if(isset($request->id))
       {
           $DataObj=RangesModel::find($request->id);
           $DataObj->update($request->all());
           return redirect()->route('ranges.index')

               ->with('success','Range Updated successfully');

       }else {
           return "Record not found";
       }


    }
    public function destroy($id)

    {

        RangesModel::find($id)->delete();

        return redirect()->route('ranges.index')

                        ->with('success','Range deleted successfully');

    }
    public function importExcel(Request $request)
    {
        $RangeObj=new RangesModel();
        if($request->submit=='save'){

         $test=Excel::import(new RangesImport, $request->upoadfile);
         if($test){
         return redirect()->route('ranges.index')

               ->with('success','Range Updated successfully');
            }
        }

       // $test=Excel::import(new ImportDistributor, $request->upoadfile);
      return view('views::ranges.import',['RangeObj'=>$RangeObj]);
    }




}
