<?php

namespace Master\Http\Controllers;



use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\DB;
use Master\Models\StatesModel;
use App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Master\Models\StatesImport;
//use App\Helpers;
//use Illuminate\Routing\Controller as BaseController;
class StatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        //print_r(session()->get('success'));
       // print_r($_SESSION);
       
       $data = new StatesModel();
        $fields=$data->getTableColumns(); 
       if(isset($request->search) && isset($request->searchtxt)){
          foreach ($fields as $value) {
              if($value=='id' or 
                      $value=='created_at' 
                      or $value=='updated_at' or $value=='st_status'){
                  continue;
              }
             $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');  
           }
         $data=$data->orderBy('id','DESC')->paginate(15);
       }else {
           $data=$data->orderBy('id','DESC')->paginate(15);
       }
       $data->searchtxt=$request->searchtxt?$request->searchtxt:'';

        return view('views::states.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);


    }
    public function edit($id)
    {
       $DataObj=StatesModel::where(['id'=>$id])->first();

       return view('views::states.create',['DataObj'=>$DataObj,'id'=>$id]);

    }
    public function create()
    {
       $StateObj=new StatesModel();
       return view('views::states.create',['StateObj'=>$StateObj]);
    }
    public function show(Request $request)
    {
      echo "hi..show...";die();
    }

    public function store(Request $request)
    {

        $DataObj=new StatesModel();
        $validator = Validator::make($request->all(), [
        'state'=>'required|string|max:50',
        'st_code'=>'required|string|max:90'
        ]);

       if($validator->fails()){

           return view('views::states.create',['errors'=>$validator->errors(),'DataObj'=>$request]);

       }
        $input = $request->all();

        $DataObj->create($input);
        return redirect()->route('states.index')

               ->with('success','State created successfully');

    }



    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'state'=>'required|string|max:50',
            'st_code'=>'required|string|max:90',
            'id' => 'required'
        ]);

       if($validator->fails()){
           return view('views::states.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
       }

       if(isset($request->id))
       {
           $DataObj=StatesModel::find($request->id);
           $DataObj->update($request->all());
           return redirect()->route('states.index')

               ->with('success','State Updated successfully');

       }else {
           return "Record not found";
       }


    }
    public function destroy($id)

    {

        StatesModel::find($id)->delete();

        return redirect()->route('states.index')

                        ->with('success','State deleted successfully');

    }
    public function importExcel(Request $request)
    {
        $StateObj=new StatesModel();
        if($request->submit=='save'){

         $test=Excel::import(new StatesImport, $request->upoadfile);
         if($test){
         return redirect()->route('states.index')

               ->with('success','State Updated successfully');
            }
        }

       // $test=Excel::import(new ImportDistributor, $request->upoadfile);
      return view('views::states.import',['StateObj'=>$StateObj]);
    }




}
