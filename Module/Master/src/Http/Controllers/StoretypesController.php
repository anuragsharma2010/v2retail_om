<?php

namespace Master\Http\Controllers;



use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\DB;
use Master\Models\StoretypesModel;
use App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Master\Models\StoretypesImport;
//use App\Helpers;
//use Illuminate\Routing\Controller as BaseController;
class StoretypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        //print_r(session()->get('success'));
       // print_r($_SESSION);
        $data = new StoretypesModel();
        $fields=$data->getTableColumns(); 
       if(isset($request->search) && isset($request->searchtxt)){
          foreach ($fields as $value) {
              if($value=='id' or 
                      $value=='created_at' 
                      or $value=='updated_at' or $value=='st_status'){
                  continue;
              }
             $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');  
           }
         $data=$data->orderBy('id','DESC')->paginate(15);
       }else {
           $data=$data->orderBy('id','DESC')->paginate(15);
       }
       $data->searchtxt=$request->searchtxt?$request->searchtxt:'';

        return view('views::storetypes.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);


    }
    public function edit($id)
    {
       $DataObj=StoretypesModel::where(['id'=>$id])->first();

       return view('views::storetypes.create',['DataObj'=>$DataObj,'id'=>$id]);

    }
    public function create()
    {
       $STTypeObj=new StoretypesModel();
       return view('views::storetypes.create',['STTypeObj'=>$STTypeObj]);
    }
    public function show(Request $request)
    {
      echo "hi..show...";die();
    }

    public function store(Request $request)
    {

        $DataObj=new StoretypesModel();
        $validator = Validator::make($request->all(), [
        'storetype_name'=>'required|string|max:50',
        'storetype_code'=>'required|string|max:90',
        'storetype_desc'=>'required|string|max:200',
        'storetype_status'=>'required',

        ]);

       if($validator->fails()){

           return view('views::storetypes.create',['errors'=>$validator->errors(),'DataObj'=>$request]);

       }
        $input = $request->all();

        $DataObj->create($input);
        return redirect()->route('storetypes.index')

               ->with('success','Storetype created successfully');

    }



    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'storetype_name'=>'required|string|max:50',
            'storetype_code'=>'required|string|max:90',
            'storetype_status'=>'required',
            'id' => 'required'
        ]);

       if($validator->fails()){
           return view('views::storetypes.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
       }

       if(isset($request->id))
       {
           $DataObj=StoretypesModel::find($request->id);
           $DataObj->update($request->all());
           return redirect()->route('storetypes.index')

               ->with('success','Storetype Updated successfully');

       }else {
           return "Record not found";
       }


    }
    public function destroy($id)

    {

        StoretypesModel::find($id)->delete();

        return redirect()->route('storetypes.index')

                        ->with('success','Storetype deleted successfully');

    }
    public function importExcel(Request $request)
    {
        $STTypeObj=new StoretypesModel();
        if($request->submit=='save'){

         $test=Excel::import(new StoretypesImport, $request->upoadfile);
         if($test){
         return redirect()->route('storetypes.index')

               ->with('success','Storetype Updated successfully');
            }
        }

       // $test=Excel::import(new ImportDistributor, $request->upoadfile);
      return view('views::storetypes.import',['STTypeObj'=>$STTypeObj]);
    }




}
