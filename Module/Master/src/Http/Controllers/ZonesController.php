<?php

namespace Master\Http\Controllers;



use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\DB;
use Master\Models\ZonesModel;
use App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Master\Models\ZonesImport;
//use App\Helpers;
//use Illuminate\Routing\Controller as BaseController;
class ZonesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        //print_r(session()->get('success'));
       // print_r($_SESSION);
       
      $data = new ZonesModel();
        $fields=$data->getTableColumns(); 
       if(isset($request->search) && isset($request->searchtxt)){
          foreach ($fields as $value) {
              if($value=='id' or 
                      $value=='created_at' 
                      or $value=='updated_at' or $value=='st_status'){
                  continue;
              }
             $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');  
           }
         $data=$data->orderBy('id','DESC')->paginate(15);
       }else {
           $data=$data->orderBy('id','DESC')->paginate(15);
       }
       $data->searchtxt=$request->searchtxt?$request->searchtxt:'';
      
        return view('views::zones.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);


    }
    public function edit($id)
    {
       $DataObj=ZonesModel::where(['id'=>$id])->first();
       
       return view('views::zones.create',['DataObj'=>$DataObj,'id'=>$id]);

    }
    public function create()
    {
       $ZoneObj=new ZonesModel();
       return view('views::zones.create',['ZoneObj'=>$ZoneObj]);
    }
    public function show(Request $request)
    {
      echo "hi..show...";die();
    }

    public function store(Request $request)
    {

        $DataObj=new ZonesModel();
        $validator = Validator::make($request->all(), [
        'zone_name'=>'required|string|max:50',
        'zone_code'=>'required|string|max:90',
        'zone_desc'=>'required|string|max:200',
        'zone_status'=>'required',

        ]);

       if($validator->fails()){
           
           return view('views::zones.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
           
       }
        $input = $request->all();
       
        $DataObj->create($input);
        return redirect()->route('zones.index')

               ->with('success','Zone created successfully');

    }


    
    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'zone_name'=>'required|string|max:50',
            'zone_code'=>'required|string|max:90',
            'zone_status'=>'required',
            'id' => 'required'
        ]);
       
       if($validator->fails()){
           return view('views::zones.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
       }
       
       if(isset($request->id))
       {
           $DataObj=ZonesModel::find($request->id);
           $DataObj->update($request->all());
           return redirect()->route('zones.index')

               ->with('success','Zone Updated successfully');

       }else {
           return "Record not found";
       }
        

    }
    public function destroy($id)

    {

        ZonesModel::find($id)->delete();

        return redirect()->route('zones.index')

                        ->with('success','Zone deleted successfully');

    }
    public function importExcel(Request $request)
    {
        $ZoneObj=new ZonesModel();
        if($request->submit=='save'){
         
         $test=Excel::import(new ZonesImport, $request->upoadfile);
         if($test){
         return redirect()->route('zones.index')

               ->with('success','Zone Updated successfully');
            }
        }
        
       // $test=Excel::import(new ImportDistributor, $request->upoadfile);
      return view('views::zones.import',['ZoneObj'=>$ZoneObj]);
    }
       
   
       

}
