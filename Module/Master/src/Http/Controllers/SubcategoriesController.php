<?php

namespace Master\Http\Controllers;



use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\DB;
use Master\Models\SubcategoriesModel;
use App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Master\Models\SubcategoriesImport;
//use App\Helpers;
//use Illuminate\Routing\Controller as BaseController;
class SubcategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
         $data = new SubcategoriesModel();
        $fields=$data->getTableColumns(); 
       if(isset($request->search) && isset($request->searchtxt)){
          foreach ($fields as $value) {
              if($value=='id' or 
                      $value=='created_at' 
                      or $value=='updated_at' or $value=='st_status'){
                  continue;
              }
             $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');  
           }
         $data=$data->orderBy('id','DESC')->paginate(15);
       }else {
           $data=$data->orderBy('id','DESC')->paginate(15);
       }
       $data->searchtxt=$request->searchtxt?$request->searchtxt:'';

         return view('views::subcategories.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);

    }
    public function edit($id)
    {
      $DataObj=SubcategoriesModel::where(['id'=>$id])->first();

      return view('views::subcategories.create',['DataObj'=>$DataObj,'id'=>$id]);

    }
    public function create()
    {
      $AreaObj=new SubcategoriesModel();
      return view('views::subcategories.create',['AreaObj'=>$AreaObj]);
    }
    public function show(Request $request)
    {
      echo "hi..show...";die();
    }

    public function store(Request $request)
    {

      $DataObj=new SubcategoriesModel();
      $validator = Validator::make($request->all(), [
      'subcat_name'=>'required|string|max:50',
      'subcat_code'=>'required|string|max:90',
      'subcat_desc'=>'required|string|max:200',
      'subcat_status'=>'required',

      ]);

     if($validator->fails()){

         return view('views::subcategories.create',['errors'=>$validator->errors(),'DataObj'=>$request]);

     }
      $input = $request->all();

      $DataObj->create($input);
      return redirect()->route('subcategories.index')

             ->with('success','Category created successfully');

    }
    public function update(Request $request)
    {

      $validator = Validator::make($request->all(), [
          'subcat_name'=>'required|string|max:50',
          'subcat_code'=>'required|string|max:90',
          'subcat_status'=>'required',
          'id' => 'required'
      ]);

     if($validator->fails()){
         return view('views::subcategories.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
     }

     if(isset($request->id))
     {
         $DataObj=SubcategoriesModel::find($request->id);
         $DataObj->update($request->all());
         return redirect()->route('subcategories.index')

             ->with('success','Category Updated successfully');

     }else {
         return "Record not found";
     }

    }

    public function destroy($id)

    {

        SubcategoriesModel::find($id)->delete();

        return redirect()->route('subcategories.index')

                        ->with('success','Category deleted successfully');

    }

    public function importExcel(Request $request)
    {
        $AreaObj=new SubcategoriesModel();
        if($request->submit=='save'){

         $test=Excel::import(new SubcategoriesImport, $request->upoadfile);
         if($test){
         return redirect()->route('subcategories.index')

               ->with('success','Subcategories imported successfully');
            }
        }

       // $test=Excel::import(new ImportDistributor, $request->upoadfile);
      return view('views::subcategories.import',['AreaObj'=>$AreaObj]);
    }



}
