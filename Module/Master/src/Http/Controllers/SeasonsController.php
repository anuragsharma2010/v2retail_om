<?php

namespace Master\Http\Controllers;



use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\DB;
use Master\Models\SeasonsModel;
use App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Master\Models\SeasonsImport;
//use App\Helpers;
//use Illuminate\Routing\Controller as BaseController;
class SeasonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        //print_r(session()->get('success'));
       // print_r($_SESSION);

        $data = new SeasonsModel();
        $fields=$data->getTableColumns(); 
       if(isset($request->search) && isset($request->searchtxt)){
          foreach ($fields as $value) {
              if($value=='id' or 
                      $value=='created_at' 
                      or $value=='updated_at' or $value=='st_status'){
                  continue;
              }
             $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');  
           }
         $data=$data->orderBy('id','DESC')->paginate(15);
       }else {
           $data=$data->orderBy('id','DESC')->paginate(15);
       }
       $data->searchtxt=$request->searchtxt?$request->searchtxt:'';


        return view('views::seasons.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);


    }
    public function edit($id)
    {
       $DataObj=SeasonsModel::where(['id'=>$id])->first();

       return view('views::seasons.create',['DataObj'=>$DataObj,'id'=>$id]);

    }
    public function create()
    {
       $SSNObj=new SeasonsModel();
       return view('views::seasons.create',['SSNObj'=>$SSNObj]);
    }
    public function show(Request $request)
    {
      echo "hi..show...";die();
    }

    public function store(Request $request)
    {

        $DataObj=new SeasonsModel();
        $validator = Validator::make($request->all(), [
        'ssn_name'=>'required|string|max:50',
        'ssn_code'=>'required|string|max:90',
        'ssn_desc'=>'required|string|max:200',
        'ssn_status'=>'required',

        ]);

       if($validator->fails()){

           return view('views::seasons.create',['errors'=>$validator->errors(),'DataObj'=>$request]);

       }
        $input = $request->all();

        $DataObj->create($input);
        return redirect()->route('seasons.index')

               ->with('success','Season created successfully');

    }



    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'ssn_name'=>'required|string|max:50',
            'ssn_code'=>'required|string|max:90',
            'ssn_status'=>'required',
            'id' => 'required'
        ]);

       if($validator->fails()){
           return view('views::seasons.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
       }

       if(isset($request->id))
       {
           $DataObj=SeasonsModel::find($request->id);
           $DataObj->update($request->all());
           return redirect()->route('seasons.index')

               ->with('success','Season Updated successfully');

       }else {
           return "Record not found";
       }


    }
    public function destroy($id)

    {

        SeasonsModel::find($id)->delete();

        return redirect()->route('seasons.index')

                        ->with('success','Season deleted successfully');

    }
    public function importExcel(Request $request)
    {
        $SSNObj=new SeasonsModel();
        if($request->submit=='save'){

         $test=Excel::import(new SeasonsImport, $request->upoadfile);
         if($test){
         return redirect()->route('seasons.index')

               ->with('success','Season Imported successfully');
            }
        }

       // $test=Excel::import(new ImportDistributor, $request->upoadfile);
      return view('views::seasons.import',['SSNObj'=>$SSNObj]);
    }




}
