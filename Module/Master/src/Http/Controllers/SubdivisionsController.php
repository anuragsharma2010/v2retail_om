<?php
namespace Master\Http\Controllers;



use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\DB;
use Master\Models\SubdivisionsModel;
use App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Master\Models\SubdivisionsImport;
//use App\Helpers;
//use Illuminate\Routing\Controller as BaseController;
class SubdivisionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $data = new SubdivisionsModel();
        $fields=$data->getTableColumns(); 
       if(isset($request->search) && isset($request->searchtxt)){
          foreach ($fields as $value) {
              if($value=='id' or 
                      $value=='created_at' 
                      or $value=='updated_at' or $value=='st_status'){
                  continue;
              }
             $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');  
           }
         $data=$data->orderBy('id','DESC')->paginate(15);
       }else {
           $data=$data->orderBy('id','DESC')->paginate(15);
       }
       $data->searchtxt=$request->searchtxt?$request->searchtxt:'';


         return view('views::subdivisions.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function edit($id) {
      $DataObj=SubdivisionsModel::where(['id'=>$id])->first();

      return view('views::subdivisions.create',['DataObj'=>$DataObj,'id'=>$id]);
    }

    public function create() {
      $SubdivObj=new SubdivisionsModel();
      return view('views::subdivisions.create',['SubdivObj'=>$SubdivObj]);
    }

    public function show(Request $request) {
        echo "hi..show...";
        die();
    }

    public function store(Request $request) {

      $DataObj=new SubdivisionsModel();
      $validator = Validator::make($request->all(), [
      'subdiv_name'=>'required|string|max:50',
      'subdiv_code'=>'required|string|max:90',
      'subdiv_desc'=>'required|string|max:200',
      'subdiv_status'=>'required',

      ]);

     if($validator->fails()){

         return view('views::subdivisions.create',['errors'=>$validator->errors(),'DataObj'=>$request]);

     }
      $input = $request->all();

      $DataObj->create($input);
      return redirect()->route('subdivisions.index')

             ->with('success','Division created successfully');
    }

    public function update(Request $request) {
      $validator = Validator::make($request->all(), [
          'subdiv_name'=>'required|string|max:50',
          'subdiv_code'=>'required|string|max:90',
          'subdiv_status'=>'required',
          'id' => 'required'
      ]);

      if($validator->fails()){
         return view('views::subdivisions.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
      }

      if(isset($request->id))
      {
         $DataObj=SubdivisionsModel::find($request->id);
         $DataObj->update($request->all());
         return redirect()->route('subdivisions.index')

             ->with('success','Subdivision Updated successfully');

      }else {
         return "Record not found";
      }
    }

    public function destroy($id)

    {

        SubdivisionsModel::find($id)->delete();

        return redirect()->route('subdivisions.index')

                        ->with('success','Division deleted successfully');

    }
    public function importExcel(Request $request)
    {
        $SubdivObj=new SubdivisionsModel();
        if($request->submit=='save'){

         $test=Excel::import(new SubdivisionsImport, $request->upoadfile);
         if($test){
         return redirect()->route('subdivisions.index')

               ->with('success','Division Imported successfully');
            }
        }

       // $test=Excel::import(new ImportDistributor, $request->upoadfile);
      return view('views::subdivisions.import',['SubdivObj'=>$SubdivObj]);
    }

}
