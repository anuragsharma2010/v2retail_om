<?php

namespace Master\Http\Controllers;



use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\DB;
use Master\Models\DivisionsModel;
use App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Master\Models\DivisionsImport;
//use App\Helpers;
//use Illuminate\Routing\Controller as BaseController;
class DivisionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $data = new DivisionsModel();
        $fields=$data->getTableColumns(); 
       if(isset($request->search) && isset($request->searchtxt)){
          foreach ($fields as $value) {
              if($value=='id' or 
                      $value=='created_at' 
                      or $value=='updated_at' or $value=='st_status'){
                  continue;
              }
             $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');  
           }
         $data=$data->orderBy('id','DESC')->paginate(15);
       }else {
           $data=$data->orderBy('id','DESC')->paginate(15);
       }
       $data->searchtxt=$request->searchtxt?$request->searchtxt:'';


         return view('views::divisions.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function edit($id) {
      $DataObj=DivisionsModel::where(['id'=>$id])->first();

      return view('views::divisions.create',['DataObj'=>$DataObj,'id'=>$id]);
    }

    public function create() {
      $DivisionObj=new DivisionsModel();
      return view('views::divisions.create',['DivisionObj'=>$DivisionObj]);
    }

    public function show(Request $request) {
        echo "hi..show...";
        die();
    }

    public function store(Request $request) {

      $DataObj=new DivisionsModel();
      $validator = Validator::make($request->all(), [
      'div_name'=>'required|string|max:50',
      'div_code'=>'required|string|max:90',
      'div_desc'=>'required|string|max:200',
      'div_status'=>'required',

      ]);

     if($validator->fails()){

         return view('views::divisions.create',['errors'=>$validator->errors(),'DataObj'=>$request]);

     }
      $input = $request->all();

      $DataObj->create($input);
      return redirect()->route('divisions.index')

             ->with('success','Division created successfully');
    }

    public function update(Request $request) {

        $validator = Validator::make($request->all(), [
            'div_name'=>'required|string|max:50',
            'div_code'=>'required|string|max:90',
            'div_status'=>'required',
            'id' => 'required'
        ]);

       if($validator->fails()){
           return view('views::divisions.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
       }

       if(isset($request->id))
       {
           $DataObj=DivisionsModel::find($request->id);
           $DataObj->update($request->all());
           return redirect()->route('divisions.index')

               ->with('success','Division Updated successfully');

       }else {
           return "Record not found";
       }
    }

    public function destroy($id)

    {

        DivisionsModel::find($id)->delete();

        return redirect()->route('divisions.index')

                        ->with('success','Division deleted successfully');

    }
    public function importExcel(Request $request)
    {
        $DivisionObj=new DivisionsModel();
        if($request->submit=='save'){

         $test=Excel::import(new DivisionsImport, $request->upoadfile);
         if($test){
         return redirect()->route('divisions.index')

               ->with('success','Division Imported successfully');
            }
        }

       // $test=Excel::import(new ImportDistributor, $request->upoadfile);
      return view('views::divisions.import',['DivisionObj'=>$DivisionObj]);
    }

}
