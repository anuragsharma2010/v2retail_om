<?php

namespace Master\Http\Controllers;



use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\DB;
use Master\Models\DistributioncentersModel;
use App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Master\Models\DistributioncentersImport;
//use App\Helpers;
//use Illuminate\Routing\Controller as BaseController;
class DistributioncentersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        //print_r(session()->get('success'));
       // print_r($_SESSION);

      if(isset($request->search) && isset($request->searchtxt)){
           $data=DistributioncentersModel::Where('dc_name','like','%'.$request->searchtxt.'%')
                             ->orWhere('dc_code','like','%'.$request->searchtxt.'%')->orderBy('id','DESC')->paginate(15);
       }else {
           $data=DistributioncentersModel::orderBy('id','DESC')->paginate(15);
       }
       $data->searchtxt=$request->searchtxt?$request->searchtxt:'';
        return view('views::distributioncenters.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);


    }
    public function edit($id)
    {
       $DataObj=DistributioncentersModel::where(['id'=>$id])->first();

       return view('views::distributioncenters.create',['DataObj'=>$DataObj,'id'=>$id]);

    }
    public function create()
    {
       $DistributionCenterObj=new DistributioncentersModel();
       return view('views::distributioncenters.create',['DistributionCenterObj'=>$DistributionCenterObj]);
    }
    public function show(Request $request)
    {
      echo "hi..show...";die();
    }

    public function store(Request $request)
    {

        $DataObj=new DistributioncentersModel();
         $messages = array(
            'required' => 'This field is required.',
        );
        $validator = Validator::make($request->all(), [
        'dc_name'=>'required|string|max:50',
        'dc_code'=>'required|string|max:90',
       // 'dc_desc'=>'required|string|max:200',
        'dc_status'=>'required',

        ],$messages);

       if($validator->fails()){

           return view('views::distributioncenters.create',['errors'=>$validator->errors(),'DataObj'=>$request]);

       }
        $input = $request->all();

        $DataObj->create($input);
        return redirect()->route('distributioncenters.index')

               ->with('success','Distribution Center created successfully');

    }



    public function update(Request $request)
    {
          $messages = array(
            'required' => 'This field is required.',
        );
        $validator = Validator::make($request->all(), [
            'dc_name'=>'required|string|max:50',
            'dc_code'=>'required|string|max:90',
        //    'dc_desc'=>'required|string|max:200',
            'dc_status'=>'required',
            'id' => 'required'
        ],$messages);
        
       if($validator->fails()){
           return view('views::distributioncenters.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
       }

       if(isset($request->id))
       {
           $DataObj=DistributioncentersModel::find($request->id);
           $DataObj->update($request->all());
           return redirect()->route('distributioncenters.index')

               ->with('success','Distribution Center Updated successfully');

       }else {
           return "Record not found";
       }


    }
    public function destroy($id)

    {

        DistributioncentersModel::find($id)->delete();

        return redirect()->route('distributioncenters.index')

                        ->with('success','Distribution Center deleted successfully');

    }
    public function importExcel(Request $request)
    {
        $DistributionCenterObj=new DistributioncentersModel();
        if($request->submit=='save'){

         $test=Excel::import(new DistributioncentersImport, $request->upoadfile);
         if($test){
         return redirect()->route('distributioncenters.index')

               ->with('success','Distribution Center Updated successfully');
            }
        }

       // $test=Excel::import(new ImportDistributor, $request->upoadfile);
      return view('views::distributioncenters.import',['DistributionCenterObj'=>$DistributionCenterObj]);
    }




}
