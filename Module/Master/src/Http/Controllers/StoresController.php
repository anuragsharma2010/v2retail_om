<?php

namespace Master\Http\Controllers;



use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\DB;
use Master\Models\StoresModel;
use Master\Models\RegionsModel;
use Master\Models\AreasModel;
use Master\Models\ZonesModel;
use Master\Models\GroupsModel;
use Master\Models\DistributioncentersModel;
use App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Master\Models\StoresImport;
//use App\Helpers;
//use Illuminate\Routing\Controller as BaseController;
class StoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        //print_r(session()->get('success'));
       // print_r($_SESSION);
      $data=new StoresModel();
      $fields=$data->getTableColumns();
     
      if(isset($request->search) && isset($request->searchtxt)){
          foreach ($fields as $value) {
              if($value=='id' or 
                      $value=='created_at' 
                      or $value=='updated_at' or $value=='st_status'){
                  continue;
              }
             $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');  
           }
         $data=$data->orderBy('id','DESC')->paginate(15);
       }else {
           $data=$data->orderBy('id','DESC')->paginate(15);
       }
       $data->searchtxt=$request->searchtxt?$request->searchtxt:'';
        return view('views::stores.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);


    }
    public function edit($id)
    {
       $DataObj=StoresModel::where(['id'=>$id])->first();
       $DataObj->Regions=  RegionsModel::orderBy('id','DESC')->get();
       $DataObj->Areas = AreasModel::orderBy('id','DESC')->get();
       $DataObj->Zones = ZonesModel::orderBy('id','DESC')->get();
       $DataObj->Groups = GroupsModel::orderBy('id','DESC')->get();
       $DataObj->Dcs = DistributioncentersModel::orderBy('id','DESC')->get();
       $DataObj->States = DB::table('states')->get();
       $DataObj->Countries = DB::table('countries')->get();
       //print_r($DataObj->Dcs);die;
       return view('views::stores.create',['DataObj'=>$DataObj,'id'=>$id]);

    }
    public function create()
    {
       $STObj=new StoresModel();
       $STObj->Regions=  RegionsModel::orderBy('id','DESC')->get();
       $STObj->Areas = AreasModel::orderBy('id','DESC')->get();
       $STObj->Zones = ZonesModel::orderBy('id','DESC')->get();
       $STObj->Groups = GroupsModel::orderBy('id','DESC')->get();
       $STObj->Dcs = DistributioncentersModel::orderBy('id','DESC')->get();
       $STObj->States = DB::table('states')->get();
       $STObj->Countries = DB::table('countries')->get();
       
       return view('views::stores.create',['DataObj'=>$STObj]);
    }
    public function show(Request $request)
    {
      echo "hi..show...";die();
    }

    public function store(Request $request)
    {

        $DataObj=new StoresModel();
         $messages = array(
            'required' => 'This field is required.',
        );
        $validator = Validator::make($request->all(), [
        'st_name'=>'required|string|max:50',
        'st_code'=>'required|string|max:90',
        

        ],$messages);

       if($validator->fails()){
           $STObj=new StoresModel();
            $STObj->Regions=  RegionsModel::orderBy('id','DESC')->get();
            $STObj->Areas = AreasModel::orderBy('id','DESC')->get();
            $STObj->Zones = ZonesModel::orderBy('id','DESC')->get();
            $STObj->Groups = GroupsModel::orderBy('id','DESC')->get();
            $STObj->Dcs = DistributioncentersModel::orderBy('id','DESC')->get();
            $STObj->States = DB::table('states')->get();
            $STObj->Countries = DB::table('countries')->get();
           return view('views::stores.create',['errors'=>$validator->errors(),'DataObj'=>$STObj]);

       }
        $input = $request->all();

        $DataObj->create($input);
        return redirect()->route('stores.index')

               ->with('success','Store created successfully');

    }



    public function update(Request $request)
    {
         $messages = array(
            'required' => 'This field is required.',
        );
        $validator = Validator::make($request->all(), [
            'st_name'=>'required|string|max:50',
            'st_code'=>'required|string|max:90',
            'st_status'=>'required',
            'id' => 'required'
        ],$messages);

       if($validator->fails()){
           return view('views::stores.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
       }

       if(isset($request->id))
       {
           $DataObj=StoresModel::find($request->id);
           $DataObj->update($request->all());
           return redirect()->route('stores.index')

               ->with('success','Store Updated successfully');

       }else {
           return "Record not found";
       }


    }
    public function destroy($id)

    {

        StoresModel::find($id)->delete();

        return redirect()->route('stores.index')

                        ->with('success','Store deleted successfully');

    }
    public function importExcel(Request $request)
    {
        $STObj=new StoresModel();
        if($request->submit=='save'){

         $test=Excel::import(new StoresImport, $request->upoadfile);
         if($test){
         return redirect()->route('stores.index')

               ->with('success','Store imported successfully');
            }
        }

       // $test=Excel::import(new ImportDistributor, $request->upoadfile);
      return view('views::stores.import',['STObj'=>$STObj]);
    }




}
