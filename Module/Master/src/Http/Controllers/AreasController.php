<?php

namespace Master\Http\Controllers;



use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\DB;
use Master\Models\AreasModel;
use App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Master\Models\AreasImport;
//use App\Helpers;
//use Illuminate\Routing\Controller as BaseController;
class AreasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        //print_r(session()->get('success'));
       // print_r($_SESSION);
       
        $data = new AreasModel();
        $fields=$data->getTableColumns(); 
       if(isset($request->search) && isset($request->searchtxt)){
          foreach ($fields as $value) {
              if($value=='id' or 
                      $value=='created_at' 
                      or $value=='updated_at' or $value=='st_status'){
                  continue;
              }
             $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');  
           }
         $data=$data->orderBy('id','DESC')->paginate(15);
       }else {
           $data=$data->orderBy('id','DESC')->paginate(15);
       }
       $data->searchtxt=$request->searchtxt?$request->searchtxt:'';

        
        return view('views::areas.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);


    }
    public function edit($id)
    {
       $DataObj=  AreasModel::where(['id'=>$id])->first();
       
       return view('views::areas.create',['DataObj'=>$DataObj,'id'=>$id]);

    }
    public function create()
    {
       $AreaObj=new AreasModel();
       return view('views::areas.create',['AreaObj'=>$AreaObj]);
    }
    public function show(Request $request)
    {
      echo "hi..show...";die();
    }

    public function store(Request $request)
    {

        $DataObj=new AreasModel;
        $validator = Validator::make($request->all(), [
        'area_name'=>'required|string|max:50',
        'area_code'=>'required|string|max:90',
        'area_desc'=>'required|string|max:200',
        'area_status'=>'required',

        ]);

       if($validator->fails()){
           
           return view('views::areas.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
           
       }
        $input = $request->all();
       
        $DataObj->create($input);
        return redirect()->route('areas.index')

               ->with('success','Area created successfully');

    }


    
    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'area_name'=>'required|string|max:50',
            'area_code'=>'required|string|max:90',
            'area_status'=>'required',
            'id' => 'required'
        ]);
       
       if($validator->fails()){
           return view('views::areas.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
       }
       
       if(isset($request->id))
       {
           $DataObj=  AreasModel::find($request->id);
           $DataObj->update($request->all());
           return redirect()->route('areas.index')

               ->with('success','Area Updated successfully');

       }else {
           return "Record not found";
       }
        

    }
    public function destroy($id)

    {

        AreasModel::find($id)->delete();

        return redirect()->route('areas.index')

                        ->with('success','Area deleted successfully');

    }
    public function importExcel(Request $request)
    {
        $AreaObj=new AreasModel();
        if($request->submit=='save'){
         
         $test=Excel::import(new AreasImport, $request->upoadfile);
         if($test){
         return redirect()->route('areas.index')

               ->with('success','Area Updated successfully');
            }
        }
        
       // $test=Excel::import(new ImportDistributor, $request->upoadfile);
      return view('views::areas.import',['AreaObj'=>$AreaObj]);
    }
       
   
       

}
