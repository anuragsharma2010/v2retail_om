<?php
namespace Master\Http\Controllers;



use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\DB;
use Master\Models\SegmentsModel;
use App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Master\Models\SegmentsImport;
//use App\Helpers;
//use Illuminate\Routing\Controller as BaseController;
class SegmentsController extends Controller
{

  /*function __construct()

  {

       $this->middleware('permission:segment-list');

       $this->middleware('permission:segment-create', ['only' => ['create','store']]);

       $this->middleware('permission:segment-edit', ['only' => ['edit','update']]);

       $this->middleware('permission:segment-delete', ['only' => ['destroy']]);

  }*/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
      //dd(Auth::user());
      $data=SegmentsModel::orderBy('id','DESC')->paginate(15);

      return view('views::segments.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);

    }

    public function edit($id)
    {

         $DataObj=SegmentsModel::where(['id'=>$id])->first();

         return view('views::segments.create',['DataObj'=>$DataObj,'id'=>$id]);

    }
    public function create()
    {
      $SEGObj=new SegmentsModel();
      return view('views::segments.create',['SEGObj'=>$SEGObj]);
    }
    public function show(Request $request)
    {
      echo "hi..show...";die();
    }

    public function store(Request $request)
    {

      $DataObj=new SegmentsModel();
      $validator = Validator::make($request->all(), [
      'seg_name'=>'required|string|max:50',
      'seg_code'=>'required|string|max:90',
      'seg_desc'=>'required|string|max:200',
      'seg_status'=>'required',

      ]);

     if($validator->fails()){

         return view('views::segments.create',['errors'=>$validator->errors(),'DataObj'=>$request]);

     }
      $input = $request->all();

      $DataObj->create($input);
      return redirect()->route('segments.index')

             ->with('success','Segment created successfully');

    }
    public function update(Request $request)
    {

      $validator = Validator::make($request->all(), [
          'seg_name'=>'required|string|max:50',
          'seg_code'=>'required|string|max:90',
          'seg_status'=>'required',
          'id' => 'required'
      ]);

     if($validator->fails()){
         return view('views::segments.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
     }

     if(isset($request->id))
     {
         $DataObj=SegmentsModel::find($request->id);
         $DataObj->update($request->all());
         return redirect()->route('segments.index')

             ->with('success','Segment Updated successfully');

     }else {
         return "Record not found";
     }

    }

    public function destroy($id)

    {

        SegmentsModel::find($id)->delete();

        return redirect()->route('segments.index')

                        ->with('success','Segment deleted successfully');

    }
    public function importExcel(Request $request)
    {
        $SEGObj=new SegmentsModel();
        if($request->submit=='save'){

         $test=Excel::import(new SegmentsImport, $request->upoadfile);
         if($test){
         return redirect()->route('segments.index')

               ->with('success','Segments Imported successfully');
            }
        }

       // $test=Excel::import(new ImportDistributor, $request->upoadfile);
      return view('views::segments.import',['SEGObj'=>$SEGObj]);
    }

}
