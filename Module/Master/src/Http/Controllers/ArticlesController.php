<?php

namespace Master\Http\Controllers;



use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\DB;
use Master\Models\ArticlesModel;
use App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Master\Models\ArticlesImport;
//use App\Helpers;
//use Illuminate\Routing\Controller as BaseController;
class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
      
       // print_r($_SESSION);
      $dataobj=new ArticlesModel();
      $fields=$dataobj->getTableColumns();
      $data=new ArticlesModel();
     
       if(isset($request->search) && isset($request->searchtxt)){
           foreach ($fields as $value) {
              if($value=='id' or 
                      $value=='created_at' 
                      or $value=='updated_at'){
                  continue;
              }
             $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');  
           }
          // echo "hi.....test....";die();
           
          
           $data=$data->orderBy('id','DESC')->paginate(15);
          
       }else {
           $data=ArticlesModel::orderBy('id','DESC')->paginate(15);
       }
      $data->fields=$fields;
      $data->searchtxt=$request->searchtxt?$request->searchtxt:'';
      return view('views::articles.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);
    }
    public function edit($id)
    {
       $DataObj=ArticlesModel::where(['id'=>$id])->first()->toArray();
       $ArticleObj=new ArticlesModel();
       $fields=$ArticleObj->getTableColumns();
       
       return view('views::articles.create',['DataObj'=>$DataObj,'id'=>$id,'fields'=>$fields]);

    }
    public function create()
    {
       $ArticleObj=new ArticlesModel();
        $dataobj=new ArticlesModel();
      $fields=$dataobj->getTableColumns();
       return view('views::articles.create',['ArticleObj'=>$ArticleObj,'fields'=>$fields]);
    }
    public function show(Request $request)
    {
      echo "hi..show...";die();
    }

    public function store(Request $request)
    {
        
//        $DataObj=new ArticlesModel();
//        $validator = Validator::make($request->all(), [
//        'zone_name'=>'required|string|max:50',
//        'zone_code'=>'required|string|max:90',
//        'zone_desc'=>'required|string|max:200',
//        'zone_status'=>'required',
//
//        ]);
//
//       if($validator->fails()){
//
//           return view('views::articles.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
//
//       }
//        $input = $request->all();
//
//        $DataObj->create($input);
//        return redirect()->route('articles.index')
//
//               ->with('success','Article created successfully');

    }



    public function update(Request $request)
    {
       // echo "hi....".$request->id;die();
         $ArticleObj=new ArticlesModel();
         $fields=$ArticleObj->getTableColumns();
       //  $validateArray=array();
         $requare_fields=['new_major_category_code',
                        'new_major_category',
                        'report_major_category_code',
                        'report_major_category_desc',
                        'range_status',
                        'brand',
                        'box_status',
                        'job_work',
                        'fabric_supplier',
                        'cost',
                        'vnd_dzn.no',
                        'Major_category_reference_article',
                        'Major_category_reference_description',
                        'Major_category_reference_status',
                        'vnd_reference_article',
                        'vnd_reference_description',
                        'vnd_reference_status',
                        'purchase_requirement_store_request',
                        'fabric',
                        'sub_fab',
                        'style',
                        'sub_style',
                        'mvgr_matrix',
                        'display_mvgr_matrix',
                        'mvgr_seg',
                        'all_mvgr_combination',
                        'product_article',
                        '10_digit',
                        'general_article',
                        'general_article_description',
                        'article',
                        'article_description',
                        'mrp_1',
                        'mrp_2',
                        'mrp_type',
                        'range_segment',
                        'general_article_status',
                        'variable_article_status',
                        'vendor_code',
                        'size',
                        'color',
                        'article_type'];
         $validateArray=array();
         $count=1;
         foreach($fields as $value){
            if(in_array($value, $requare_fields)) {               
            $validateArray[trim($value)]='required';
          
            
            }
        
        }
       
       $validator = Validator::make($request->all(), $validateArray );

       if($validator->fails()){
           return view('views::articles.create',['errors'=>$validator->errors(),'DataObj'=>$request,'fields'=>$fields]);
       }

       if(isset($request->id))
       {
           $DataObj=ArticlesModel::find($request->id);
           $DataObj->update($request->all());
           return redirect()->route('articles.index')

               ->with('success','Article Updated successfully');

       }else {
           return "Record not found";
       }


    }
    public function destroy($id)

    {

        ArticlesModel::find($id)->delete();

        return redirect()->route('articles.index')

                        ->with('success','Article deleted successfully');

    }
    public function importExcel(Request $request)
    {
        $ArticleObj=new ArticlesModel();
       // echo "hi....";die();
        if($request->submit=='save'){
      
         $test=Excel::import(new ArticlesImport, $request->upoadfile);
         if($test){
         return redirect()->route('articles.index')

               ->with('success','Article Updated successfully');
            }
        }

       // $test=Excel::import(new ImportDistributor, $request->upoadfile);
      return view('views::articles.import',['ArticleObj'=>$ArticleObj]);
    }




}
