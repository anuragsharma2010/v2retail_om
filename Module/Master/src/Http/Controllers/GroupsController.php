<?php

namespace Master\Http\Controllers;



use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\DB;
use Master\Models\GroupsModel;
use App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Master\Models\GroupsImport;
//use App\Helpers;
//use Illuminate\Routing\Controller as BaseController;
class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        //print_r(session()->get('success'));
       // print_r($_SESSION);

       $data = new GroupsModel();
        $fields=$data->getTableColumns(); 
       if(isset($request->search) && isset($request->searchtxt)){
          foreach ($fields as $value) {
              if($value=='id' or 
                      $value=='created_at' 
                      or $value=='updated_at' or $value=='st_status'){
                  continue;
              }
             $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');  
           }
         $data=$data->orderBy('id','DESC')->paginate(15);
       }else {
           $data=$data->orderBy('id','DESC')->paginate(15);
       }
        $data->searchtxt=$request->searchtxt?$request->searchtxt:'';
        return view('views::groups.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);


    }
    public function edit($id)
    {
       $DataObj=GroupsModel::where(['id'=>$id])->first();

       return view('views::groups.create',['DataObj'=>$DataObj,'id'=>$id]);

    }
    public function create()
    {
       $GroupObj=new GroupsModel();
       return view('views::groups.create',['GroupObj'=>$GroupObj]);
    }
    public function show(Request $request)
    {
      echo "hi..show...";die();
    }

    public function store(Request $request)
    {

        $DataObj=new GroupsModel();
        $validator = Validator::make($request->all(), [
        'group_name'=>'required|string|max:50',
        'group_code'=>'required|string|max:90',
        'group_desc'=>'required|string|max:200',
        'group_status'=>'required',

        ]);

       if($validator->fails()){

           return view('views::groups.create',['errors'=>$validator->errors(),'DataObj'=>$request]);

       }
        $input = $request->all();

        $DataObj->create($input);
        return redirect()->route('groups.index')

               ->with('success','Group created successfully');

    }



    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'group_name'=>'required|string|max:50',
            'group_code'=>'required|string|max:90',
            'group_status'=>'required',
            'id' => 'required'
        ]);

       if($validator->fails()){
           return view('views::groups.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
       }

       if(isset($request->id))
       {
           $DataObj=GroupsModel::find($request->id);
           $DataObj->update($request->all());
           return redirect()->route('groups.index')

               ->with('success','Group Updated successfully');

       }else {
           return "Record not found";
       }


    }
    public function destroy($id)

    {

        GroupsModel::find($id)->delete();

        return redirect()->route('groups.index')

                        ->with('success','Group deleted successfully');

    }
    public function importExcel(Request $request)
    {
        $GroupObj=new GroupsModel();
        if($request->submit=='save'){

         $test=Excel::import(new GroupsImport, $request->upoadfile);
         if($test){
         return redirect()->route('groups.index')

               ->with('success','Group Updated successfully');
            }
        }

       // $test=Excel::import(new ImportDistributor, $request->upoadfile);
      return view('views::groups.import',['GroupObj'=>$GroupObj]);
    }




}
