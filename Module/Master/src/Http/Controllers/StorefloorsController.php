<?php

namespace Master\Http\Controllers;



use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\DB;
use Master\Models\StorefloorsModel;
use App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Master\Models\StorefloorsImport;
//use App\Helpers;
//use Illuminate\Routing\Controller as BaseController;
class StorefloorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        //print_r(session()->get('success'));
       // print_r($_SESSION);

      
     $data = new StorefloorsModel();
     $fields=$data->getTableColumns(); 
     if(isset($request->search) && isset($request->searchtxt)){
          foreach ($fields as $value) {
              if($value=='id' or 
                      $value=='created_at' 
                      or $value=='updated_at' or $value=='st_status'){
                  continue;
              }
             $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');  
           }
         $data=$data->orderBy('id','DESC')->paginate(15);
       }else {
           $data=$data->orderBy('id','DESC')->paginate(15);
       }
        $data->searchtxt=$request->searchtxt?$request->searchtxt:'';
        return view('views::storefloors.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);

        //return view('views::storefloors.index');
    }
    public function edit($id)
    {
       $DataObj=StorefloorsModel::where(['id'=>$id])->first();

       return view('views::storefloors.create',['DataObj'=>$DataObj,'id'=>$id]);

    }
    public function create()
    {
       $STFloorObj=new StorefloorsModel();
       return view('views::storefloors.create',['STFloorObj'=>$STFloorObj]);
    }
    public function show(Request $request)
    {
      echo "hi..show...";die();
    }

    public function store(Request $request)
    {

        $DataObj=new StorefloorsModel();
        $validator = Validator::make($request->all(), [
        'storefloors_name'=>'required|string|max:50',
        'storefloors_code'=>'required|string|max:90',
        'storefloors_desc'=>'required|string|max:200',
        'storefloors_status'=>'required',

        ]);

       if($validator->fails()){

           return view('views::storefloors.create',['errors'=>$validator->errors(),'DataObj'=>$request]);

       }
        $input = $request->all();

        $DataObj->create($input);
        return redirect()->route('storefloors.index')

               ->with('success','Storefloor created successfully');

    }



    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'storefloors_name'=>'required|string|max:50',
            'storefloors_code'=>'required|string|max:90',
            'storefloors_status'=>'required',
            'id' => 'required'
        ]);

       if($validator->fails()){
           return view('views::storefloors.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
       }

       if(isset($request->id))
       {
           $DataObj=StorefloorsModel::find($request->id);
           $DataObj->update($request->all());
           return redirect()->route('storefloors.index')

               ->with('success','Storefloor Updated successfully');

       }else {
           return "Record not found";
       }


    }
    public function destroy($id)

    {

        StorefloorsModel::find($id)->delete();

        return redirect()->route('storefloors.index')

                        ->with('success','Storefloor deleted successfully');

    }
    public function importExcel(Request $request)
    {
        $STFloorObj=new StorefloorsModel();
        if($request->submit=='save'){

         $test=Excel::import(new StorefloorsImport, $request->upoadfile);
         if($test){
         return redirect()->route('storefloors.index')

               ->with('success','Storefloor Updated successfully');
            }
        }

       // $test=Excel::import(new ImportDistributor, $request->upoadfile);
      return view('views::storefloors.import',['STFloorObj'=>$STFloorObj]);
    }




}
