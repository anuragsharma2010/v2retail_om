<?php

namespace Master\Http\Controllers;



use Illuminate\Http\Request;
//use Validator;
use Illuminate\Support\Facades\Validator;


use Illuminate\Support\Facades\DB;
use Master\Models\RegionsModel;
use App;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Master\Models\RegionsImport;
//use App\Helpers;
//use Illuminate\Routing\Controller as BaseController;
class RegionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
       $data = new RegionsModel();
        $fields=$data->getTableColumns(); 
       if(isset($request->search) && isset($request->searchtxt)){
          foreach ($fields as $value) {
              if($value=='id' or 
                      $value=='created_at' 
                      or $value=='updated_at' or $value=='st_status'){
                  continue;
              }
             $data=$data->orWhere($value,'like','%'.$request->searchtxt.'%');  
           }
         $data=$data->orderBy('id','DESC')->paginate(15);
       }else {
           $data=$data->orderBy('id','DESC')->paginate(15);
       }
       $data->searchtxt=$request->searchtxt?$request->searchtxt:'';
        return view('views::regions.index',compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);


    }
    public function edit($id)
    {
       $DataObj=  RegionsModel::where(['id'=>$id])->first();

       return view('views::regions.create',['DataObj'=>$DataObj,'id'=>$id]);

    }
    public function create()
    {
       $AreaObj=new RegionsModel();
       return view('views::regions.create',['AreaObj'=>$AreaObj]);
    }
    public function show(Request $request)
    {
      echo "hi..show...";die();
    }

    public function store(Request $request)
    {

        $DataObj=new RegionsModel;
        $validator = Validator::make($request->all(), [
        'region_name'=>'required|string|max:50',
        'region_code'=>'required|string|max:90',
        'region_desc'=>'required|string|max:200',
        'region_status'=>'required',

        ]);

       if($validator->fails()){

           return view('views::regions.create',['errors'=>$validator->errors(),'DataObj'=>$request]);

       }
        $input = $request->all();

        $DataObj->create($input);
        return redirect()->route('regions.index')

               ->with('success','Region created successfully');

    }



    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'region_name'=>'required|string|max:50',
            'region_code'=>'required|string|max:90',
            'region_status'=>'required',
            'id' => 'required'
        ]);

       if($validator->fails()){
           return view('views::regions.create',['errors'=>$validator->errors(),'DataObj'=>$request]);
       }

       if(isset($request->id))
       {
           $DataObj=  RegionsModel::find($request->id);
           $DataObj->update($request->all());
           return redirect()->route('regions.index')

               ->with('success','Region Updated successfully');

       }else {
           return "Record not found";
       }


    }
    public function destroy($id)

    {

        RegionsModel::find($id)->delete();

        return redirect()->route('regions.index')

                        ->with('success','Region deleted successfully');

    }
    public function importExcel(Request $request)
    {
        $AreaObj=new RegionsModel();
        if($request->submit=='save'){

         $test=Excel::import(new RegionsImport, $request->upoadfile);
         if($test){
         return redirect()->route('regions.index')

               ->with('success','Region imported successfully');
            }
        }

       // $test=Excel::import(new ImportDistributor, $request->upoadfile);
      return view('views::regions.import',['AreaObj'=>$AreaObj]);
    }




}
