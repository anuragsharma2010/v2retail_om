@extends('layouts.app')


@section('content')

<!-- Content Header (Page header) -->
     <section class="content-header">
      <h1>
      Create Division
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
        <li ><a href="{{ route('divisions.index') }}">Divisions</a></li>
        <li class="active">Create Division</li>
      </ol>
</section>
    <!-- /.content-header -->
     <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                  <!-- /.card-header -->
                  <div class="card-body">
                     <div class="form_container">

                            <?php if(isset($errors) && (count($errors->all()) > 0 )){  ?>
                               <div class="alert alert-danger alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            <?php } ?>
                    <form action="<?php if(isset($id)){ ?>{{ route('divisions.update',$id) }} <?php }  else { ?>{{ route('divisions.store') }}<?php } ?>" method="POST">
                        @csrf
                        <?php if(isset($id)){ ?>
                       @method('PUT')
                        <?php } ?>
               <input type="hidden" name='id' name='id' value="<?php if(isset($id)){ echo $id; }  ?>" />
                    <div class="row">
                      <div class="clearfix">
                      <div class="col-md-6 right_pdng">
                        <div class="form-group">
                         <label >Name:</label>
                            <input  type="text" name="div_name" id="div_name" value="<?php if(isset($DataObj->div_name)){ echo $DataObj->div_name; } ?>" class="form-control" placeholder="Name" maxlength="50">
                        </div>
                        <!-- /.form-group -->
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6 left_pdng">
                         <div class="form-group">
                          <label>Code:</label>
                           <input type="text" value="<?php if(isset($DataObj->div_code)){ echo $DataObj->div_code; } ?>"   class="form-control" id="div_code" name="div_code" placeholder="code" maxlength="20">
                        </div>
                        <!-- /.form-group -->
                      </div>
                    </div>
                      <!-- /.col -->
                    <div class="clearfix">
                      <div class="col-md-6 right_pdng">
                         <div class="form-group">
                          <label>Description:</label>
                           <textarea id="div_desc"  id="div_desc" name="div_desc" class="form-control" placeholder="Description" maxlength="300"><?php if(isset($DataObj->div_code)){ echo $DataObj->div_code; } ?></textarea>
                        </div>
                      </div>
                      <div class="col-md-6 left_pdng">
                         <div class="form-group pt-45">
                          <label>Status</label>
                            <div class="icheck-primary d-inline">
                              <input type="radio" id="In-Active" name="div_status" <?php if($DataObj->div_status==0){ ?>checked=""<?php } ?> value="0" class="custom-control-input">
                                <label for="In-Active">In-Active
                             </label>
                        </div>
                           <div class="icheck-primary d-inline">
                               <input type="radio"  <?php if($DataObj->div_status==1){ ?>checked=""<?php } ?>  id="Active" name="div_status" value="1"  class="custom-control-input">
                                <label for="Active">Active
                             </label>
                        </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                         <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                     </div>
                    </div>
                    </form>
                </div>
                </div>
                </div>
            </div>
        </section>

@endsection
