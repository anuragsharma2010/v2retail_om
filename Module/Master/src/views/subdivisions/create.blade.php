@extends('layouts.app')


@section('content')

<!-- Content Header (Page header) -->
 <section class="content-header">
      <h1>
      Create Sub-Division
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ route('subdivisions.index') }}">Sub-Divisions</a></li>
        <li class="active">Create Sub-Division</li>
      </ol>
</section>
    <!-- /.content-header -->
     <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                  <!-- /.card-header -->
                  <div class="card-body">
                     <div class="form_container">

                            <?php if(isset($errors) && (count($errors->all()) > 0 )){  ?>
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            <?php } ?>
                    <form action="<?php if(isset($id)){ ?>{{ route('subdivisions.update',$id) }} <?php }  else { ?>{{ route('subdivisions.store') }}<?php } ?>" method="POST">
                        @csrf
                        <?php if(isset($id)){ ?>
                       @method('PUT')
                        <?php } ?>
               <input type="hidden" name='id' name='id' value="<?php if(isset($id)){ echo $id; }  ?>" />
                    <div class="row">
                      <div class="clearfix">
                      <div class="col-md-6 right_pdng">
                        <div class="form-group">
                         <label >Name:</label>
                            <input  type="text" name="subdiv_name" id="subdiv_name" value="<?php if(isset($DataObj->subdiv_name)){ echo $DataObj->subdiv_name; } ?>" class="form-control" placeholder="Name" maxlength="50">
                        </div>
                        <!-- /.form-group -->
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6 left_pdng">
                         <div class="form-group">
                          <label>Code:</label>
                           <input type="text" value="<?php if(isset($DataObj->subdiv_code)){ echo $DataObj->subdiv_code; } ?>"   class="form-control" id="subdiv_code" name="subdiv_code" placeholder="code" maxlength="20">
                        </div>
                        <!-- /.form-group -->
                      </div>
                    </div>
                      <!-- /.col -->
                      <div class="clearfix">
                      <div class="col-md-6 right_pdng">
                         <div class="form-group">
                          <label>Description:</label>
                           <textarea id="subdiv_desc"  id="subdiv_desc" name="subdiv_desc" class="form-control" placeholder="Description" maxlength="300"><?php if(isset($DataObj->subdiv_code)){ echo $DataObj->subdiv_code; } ?></textarea>
                        </div>
                      </div>
                      <div class="col-md-6 left_pdng">
                         <div class="form-group pt-45">
                          <label>Status</label>

                            <div class="icheck-primary d-inline">
                               <input type="radio" id="In-Active" name="subdiv_status"<?php if(!isset($DataObj->subdiv_status)){ ?>checked=""<?php } ?> value="0" class="custom-control-input">
                                <label for="In-Active">In-Active
                             </label>
                        </div>
                           <div class="icheck-primary d-inline">
                                <input type="radio"  <?php if(isset($DataObj->subdiv_status)){ ?>checked=""<?php } ?>  id="Active" name="subdiv_status" value="1"  class="custom-control-input">
                                <label for="Active">Active
                             </label>
                        </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                         <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                     </div>
                    </div>
                    </form>
                </div>
                </div>
                </div>
            </div>
        </section>

@endsection
