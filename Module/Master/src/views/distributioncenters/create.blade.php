@extends('layouts.app')


@section('content')

<!-- Content Header (Page header) -->
 <section class="content-header">
      <h1>
      Create Distributioncenter
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ route('distributioncenters.index') }}">Distributioncenters</a></li>
        <li class="active">Create Distributioncenter</li>
      </ol>
</section>
    <!-- /.content-header -->
     <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                  <!-- /.card-header -->
                  <div class="card-body">
                     <div class="form_container">


                    <form action="<?php if(isset($id)){ ?>{{ route('distributioncenters.update',$id) }} <?php }  else { ?>{{ route('distributioncenters.store') }}<?php } ?>" method="POST">
                        @csrf
                        <?php if(isset($id)){ ?>
                       @method('PUT')
                        <?php } ?>
               <input type="hidden" name='id' name='id' value="<?php if(isset($id)){ echo $id; }  ?>" />
                    <div class="row">
                      <div class="col-md-6 right_pdng">
                        <div class="form-group">
                         <label >Name</label>
                            <input  type="text" name="dc_name" id="dc_name" value="<?php if(isset($DataObj->dc_name)){ echo $DataObj->dc_name; } ?>" class="form-control" placeholder="Name" maxlength="50">
                                      @if ($errors->has('dc_name'))
                        <div class="error_field" >{{ $errors->first('dc_name') }}</div>
                    @enderror
                        </div>
                        <!-- /.form-group -->
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6 left_pdng">
                         <div class="form-group">
                          <label>Code</label>
                           <input type="text" value="<?php if(isset($DataObj->dc_code)){ echo $DataObj->dc_code; } ?>"   class="form-control" id="dc_code" name="dc_code" placeholder="code" maxlength="20">
                             @if ($errors->has('dc_code'))
                        <div class="error_field" >{{ $errors->first('dc_code') }}</div>
                    @enderror
                         </div>
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6 right_pdng">
                         <div class="form-group">
                          <label>Description:</label>
                           <textarea id="dc_desc" name="dc_desc" class="form-control" placeholder="Description" maxlength="300"><?php if(isset($DataObj->dc_desc)){ echo $DataObj->dc_desc; } ?></textarea>
                              @if ($errors->has('dc_desc'))
                        <div class="error_field" >{{ $errors->first('dc_desc') }}</div>
                    @enderror
                         </div>
                      </div>
                      <div class="col-md-6 left_pdng">
                         <div class="form-group pt-45">
                          <label>Status</label>
                            <div class="icheck-primary d-inline">
                               <input type="radio" id="In-Active" name="dc_status"<?php if(!isset($DataObj->dc_status)){ ?>checked=""<?php } ?> value="0" class="custom-control-input">
                                <label for="In-Active">In-Active
                             </label>
                        </div>
                           <div class="icheck-primary d-inline">
                                <input type="radio"  <?php if(isset($DataObj->dc_status)){ ?>checked=""<?php } ?>  id="Active" name="dc_status" value="1"  class="custom-control-input">
                                <label for="Active">Active
                             </label>
                        </div>
                                    @if ($errors->has('dc_status'))
                        <div class="error_field" >{{ $errors->first('dc_status') }}</div>
                    @enderror
                        </div>
                      </div>
                    <div class="col-md-12">
                         <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                     </div>
                    </div>
                    </form>
                </div>
                </div>
                </div>
            </div>
        </section>

@endsection
