@extends('layouts.app')

@section('style')
<style>
#error_massage{
  padding:8px 20px;
}
</style>
@endsection

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <h1>
    Import Article
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
         <li><a href="{{ route('articles.index') }}">Articles</a></li>
        <li class="active">Excel Import</li>
      </ol>
</section>
    <!-- /.content-header -->
     <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                  <!-- /.card-header -->
                  <div class="card-body">
                     <div class="form_container">

                            <?php if(isset($errors) && (count($errors->all()) > 0 )){  ?>
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            <?php } ?>
                                 <form class="md-form" id="importform" action="" method="POST" enctype="multipart/form-data">
                     @csrf
                  <div class="custom-file file-field">
                     <p class="file-box-main-heading">Import Articles list</p>
                     <div class="file-field-inner">
                        <div class="browse-and-drag-drop-parent">
                           <div class="upload-img-container">
                              <img src="{{url('dist/img/upload-icon.png')}}" class="img-responsive">                            
                           </div>
                           <label class="drag-drop-text">Drag and Drop files here</label>
                           <p class="or-text">or</p>
                           <input type="file" name="upoadfile" id="custom-browse-btn" accept="application/vnd.ms-excel">
                           <label class="browse-file-lbl"> Browse files</label>
                           <p class="file-format-text">(File format .xlsv or .csv)</p>
                         </div>
                         <span id="file-name"></span>
                              
                                    <div class="text-danger" id="error_massage"></div>
                             
                         <div class="form-group pt-2 text-center">
                              <input type="submit" class="btn btn-primary btn-submit" name="submit" id="submit" value="save">
                        </div>
                     </div>
                  </div>
               </form>
                </div>
                </div>
                </div>
            </div>
        </section>

@endsection
@section('custom-js')
<script src="{{ asset('js/importjs.js') }}"></script>
@endsection
<script>
    /*  $(document).ready(function () {
          $("#importform").submit(function(event){
             var vidFileLength = $("input[name='upoadfile']")[0].files.length;
            //alert(vidFileLength)
            if(vidFileLength === 0){
               //alert('hello');
               const message = "Please select the valid file!";
                $('#error_massage').html(message);
            }
             event.preventDefault();
          });

        
        $("input[name='upoadfile']").on('change', function () {
           var fileExtension = ['xlsv', 'csv'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
               const message = "Please select the valid format e.g - "+fileExtension.join(', ');
                  $('#error_massage').html(message);
                  $('#file-name').css('color','#a94442');

               }
            if ($(this).val()) {
                let file_name = $(this).val().split('\\');
                $('#file-name').text(file_name[file_name.length - 1])
            }
        });
    });

    */
</script>