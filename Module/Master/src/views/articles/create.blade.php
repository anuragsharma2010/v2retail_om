@extends('layouts.app')


@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
      <h1>
     Create Article
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('articles.index') }}">Articles</a></li>
        <li class="active">Create Article</li>
      </ol>
</section>
    <!-- /.content-header -->
     <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                  <!-- /.card-header -->
                  <div class="card-body">
                     <div class="form_container">

                            <?php if(isset($errors) && (count($errors->all()) > 0 )){  ?>
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            <?php } ?>
                    <form action="<?php if(isset($id)){ ?>{{ route('articles.update',$id) }} <?php }  else { ?>{{ route('articles.store') }}<?php } ?>" method="POST">
                        @csrf
                        <?php 
                        
                        if(isset($id)){
                            
                            ?>
                       @method('PUT')
                        <?php } ?>
               <input type="hidden" name='id' name='id' value="<?php if(isset($id)){ echo $id; }  ?>" />
                    <div class="row">
                        <?php 
                        foreach($fields as $value){
                            if($value=='id' or 
                               $value=='created_at' 
                               or $value=='updated_at'){
                                    continue;
                            }
                         //echo   $DataObj[$value];die();
                           ?>
                            <div class="col-md-6 right_pdng">
                            <div class="form-group">
                             <label ><?php echo ucwords(str_replace("_"," ",$value)) ?> :</label>
                             <input  type="text" name="<?php echo $value; ?>" id="<?php echo $value; ?>" value="<?php if(isset($DataObj[$value])){ echo $DataObj[$value]; } ?>" class="form-control" placeholder="<?php echo ucwords(str_replace("_"," ",$value)) ?>" maxlength="50">
                            </div>
                            <!-- /.form-group -->
                            <!-- /.form-group -->
                          </div>
                           <?php
                        }
                        ?>
                      
                    
                    <div class="col-md-12">
                         <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                     </div>
                    </div>
                    </form>
                </div>
                </div>
                </div>
            </div>
        </section>

@endsection
