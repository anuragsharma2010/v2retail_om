@extends('layouts.app')


@section('content')
<!-- Content Header (Page header) -->
 <section class="content-header">
      <h1>
        Categories List
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Categories List</li>
      </ol>
</section>
    <!-- /.content-header -->
     <section class="content">
       <div class="container-fluid">
             <div class="card">
              <div class="card-header">
                <div class="col-sm-12 list_filter_head">
                  <div class="search_box">
                 <!-- search form -->
                 <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="searchtxt" value="<?php echo isset($data->searchtxt) ? $data->searchtxt:'';  ?>" class="form-control search" placeholder="Search...">
                        <span class="input-group-btn">
                            <button type="submit" name="search" value="save" id="search-btn" class="btn btn-flat">
                          <i class="fa fa-search"></i>
                        </button>
                      </span>
                    </div>
                </form>
              </div>
              <div class="userbtn">
                <a href="<?php echo url("/excel_template/master/categories_master_template.xlsx"); ?>" class="btn btn-sm btn-primary createuser-btn">
                       <i class="fa fa-upload"></i> Excel Template
              </a>
                  <a href="<?php echo url("/master/categories-import-excel"); ?>" class="btn btn-sm btn-primary createuser-btn mx-10">
                  <i class="fa fa-upload"></i> Import Excel
              </a>
              <a href="{{ route('categories.create') }}" class="btn btn-sm btn-primary createuser-btn">
                       <i class="fa fa-plus-circle"></i> Add
              </a>

            </div>

            </div>
          </div>
            <!-- /.card-header -->
            <div class="card-body">
               
              @if ($message = Session::get('success'))
                    <div class="message" role="alert">
                        <div class="alert alert-success alert-dismissible">
                             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <p>{{ $message }}</p>
                        </div>
                    </div>
                    @endif


                         <div class="container-fluid p-0 show-enteries-parent">
                    <div class="col-md-6 p-0"><label class="show-enteries-text">Show enteries
                <select id="item-show-per-page-select" class="item-show-per-page-select-id">
                <option value="10">10</option>
                <option value="50">50</option>
                <option value="150">150</option>
                <option value="500">500</option>
                </select></label>

            </div>
                    <div class="col-md-6 p-0">
                        <p class="showing-enteries-text text-right">Showing 1 to 10 of 57 entries</p>
                        

                    </div>
                </div>
             
              <div class="table_data">
              <table class="table table-bordered table-striped">
                <thead>
                <tr>
                 <th>No</th>
                  <th>Name</th>
                  <th>Code</th>
                  <th>Description</th>
                   <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                 <?php foreach ($data as $row){ ?>
                   <tr>
                <td><?php echo $row->id ?></td>
                <td><?php echo $row->cat_name ?></td>
                <td><?php echo $row->cat_code ?></td>
                <td><?php echo $row->cat_desc ?></td>
                <td><?php echo $row->cat_status ? 'Active':'In-Active' ?></td>
                 <td>
                       <a class="btn btn-primary btn_dis" title="Edit" href="{{ route('categories.edit',$row->id) }}"  data-toggle="tooltip" title="Edit!"><i class="fa fa-edit"></i></a>

                        {!! Form::open(['method' => 'DELETE','route' => ['categories.destroy', $row->id],'style'=>'display:inline']) !!}

                {!! Form::button('<i class="fa fa-archive"></i>', ['class' => 'btn btn-danger btn_dis','data-toggle'=>'tooltip', 'title'=>'Delete!','type'=>'submit']) !!}

                        {!! Form::close() !!}
                    </td>
                  </tr>
                 <?php }?>
                </tbody>
              </table>
           
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
           <div class="col-md-12 text-right pagination">
             {!! $data->render() !!}
            </div>
      </div>
    </section>

@endsection
