@extends('layouts.app')


@section('content')
<!-- Content Header (Page header) -->
 <section class="content-header">
      <h1>
     Create Store
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ route('stores.index') }}">Stores</a></li>
        <li class="active">Create Store</li>
      </ol>
</section>
    <!-- /.content-header -->
     <!-- Main content -->
 <section class="content">
      <div class="container-fluid roleform">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <!-- /.card-header -->
          <div class="card-body">
             <div class="question_module">
         
           <form action="<?php if(isset($id)){ ?>{{ route('stores.update',$id) }} <?php }  else { ?>{{ route('stores.store') }}<?php } ?>" method="POST">
               @csrf
               <?php if(isset($id)){ ?>
              @method('PUT')
               <?php } ?>
               <input type="hidden" name='id' name='id' value="<?php if(isset($id)){ echo $id; }  ?>" />
             <div class="clearfix">
                <div class="form-group setpdng_profile_upload clearfix">
                 <div class="imageupload">
                   <div class="squarebox">
                     <!-- User Profile Image -->
                     <img class="profile-pic" src="https://v2retail.wooqer.com/images/email/store1.png">
                    
                   </div>
                     <div class="p-image">
                     <i class="fa fa-camera upload-button logo_clr"></i>
                      <input class="file-upload"  type="file" name="st_image" id="st_image" accept="image/*"/>
                   </div>
                </div>
                 <div class="text-center">
                      <label for="st_image" class="col-form-label label text-center">Upload Store Image</label>
                  </div>
              </div>
                 <div class="row">
              <div class="col-md-6 right_pdng">
                <div class="form-group ">
                 <label for="st_name" class="col-form-label label">Name </label>
                  <input type="text" name="st_name" id="st_name" value="<?php if(isset($DataObj->st_name)){ echo $DataObj->st_name; } ?>" class="form-control" placeholder="Enter Store name ..">
                    @if ($errors->has('st_name'))
                        <div class="error_field" >{{ $errors->first('st_name') }}</div>
                    @enderror
                </div>

              </div>
              <div class="col-md-6 left_pdng">
                <div class="form-group">
               <label for="st_code" class="col-form-label label">Store Code</label>
                <input type="text" name="st_code" id="st_code" value="<?php if(isset($DataObj->st_code)){ echo $DataObj->st_code; } ?>" class="form-control" placeholder="Enter store Store ID .." >
                @if ($errors->has('st_code'))
                        <div class="error_field" >{{ $errors->first('st_code') }}</div>
                    @enderror
                </div>
              </div>
                 </div>     
                 <div class="row">
              <div class="col-md-6 right_pdng">
                <div class="form-group">
               <label for="st_ref_code" class="col-form-label label">Store RefCode</label>
                <input type="text" name="st_ref_code" id="st_ref_code" value="<?php if(isset($DataObj->st_ref_code)){ echo $DataObj->st_ref_code; } ?>" class="form-control" placeholder="Enter store Store ID .." >
               </div>
              </div>
              <div class="col-md-6 left_pdng">
                  <div class="form-group">
                 <label for="st_email" class="col-form-label label">Email</label>
                  <input type="text" name="st_email" id="st_email" value="<?php if(isset($DataObj->st_email)){ echo $DataObj->st_email; } ?>" class="form-control" placeholder="Enter store Email .." >
                </div>
                </div>
                 </div>
                 <div class="row">
                <div class="col-md-6 right_pdng">
                 <div class="form-group">
                 <label for="st_area" class="col-form-label label">Area</label>
                 <select name="st_area" id="st_area" class="form-control customtinput">
                   <option value=""> Select Area</option>
                   <?php foreach ($DataObj->Areas as $key => $value) { ?>
                     
                     <option value="<?php echo $value->id; ?>"><?php  echo $value->area_code; ?></option>
                  <?php } ?>
                 </select>
                 </div>
                </div>
                <div class="col-md-6 left_pdng">
                 <div class="form-group">
                 <label for="st_radius" class="col-form-label label">Radius</label>
                  <input type="text" name="st_radius" id="st_radius" value="<?php if(isset($DataObj->st_radius)){ echo $DataObj->st_radius; } ?>" class="form-control" placeholder="Enter store Area ..">
                 </div>
                </div>
                 </div>
                 
                 <div class="row">  
                <div class="col-md-6 right_pdng">
                 <div class="form-group ">
                 <label for="st_region" class="col-form-label label">Regoin </label>
                 <select name="st_region" id="st_region" class="form-control customtinput">
                   <option value=""> Select Regoin</option>
                   <?php foreach ($DataObj->Regions as $key => $value) { ?>
                     // code...
                     <option value="<?php echo $value->id; ?>"><?php  echo $value->region_code; ?></option>
                  <?php } ?>
                 </select>
                 </div>
                </div>
                <div class="col-md-6 left_pdng">
                 <div class="form-group ">
                 <label for="st_zone" class="col-form-label label">Zone </label>
                 <select name="st_zone" id="st_zone" class="form-control customtinput">
                   <option value=""> Select Zone</option>
                   <?php foreach ($DataObj->Zones as $key => $value) { ?>
                     // code...
                     <option value="<?php echo $value->id; ?>"><?php  echo $value->zone_code; ?></option>
                  <?php } ?>
                 </select>
                </div>
                </div>
                 
                 </div>
                 
                    <div class="row">   
                <div class="col-md-6 right_pdng">
                 <div class="form-group ">
                 <label for="st_group" class="col-form-label label"> Group </label>
                 <select name="st_group" id="st_group" class="form-control customtinput">
                   <option value=""> Select Group</option>
                   <?php foreach ($DataObj->Groups as $key => $value) { ?>
                     // code...
                     <option value="<?php echo $value->id; ?>"><?php  echo $value->group_code; ?></option>
                  <?php } ?>
                 </select>
                 </div>
                </div>
                <div class="col-md-6 left_pdng">
                 <div class="form-group ">
                 <label for="st_dc" class="col-form-label label">Distribution Center </label>
                 <select name="st_dc" id="st_dc" class="form-control customtinput">
                   <option value=""> Select DC</option>
                   <?php foreach ($DataObj->Dcs as $key => $value) { ?>
                     // code...
                     <option value="<?php echo $value->id; ?>"><?php  echo $value->dc_code; ?></option>
                  <?php } ?>
                 </select>
                </div>
                </div>
                    </div>
                 
                     <div class="row">  
                <div class=" col-md-6 right_pdng">
                <div class="form-group">
                 <label for="st_address" class="col-form-label label">Address</label>
                  <input type="text" name="st_address" id="st_address" value="<?php if(isset($DataObj->st_address)){ echo $DataObj->st_address; } ?>" class="form-control" placeholder="Enter store Address ..">
                 </div>
                </div>
                <div class="col-md-6 left_pdng">
                <div class="form-group">
                 <label for="st_city" class="col-form-label label">City </label>
                 <input type="text" name="st_city" id="st_city" value="<?php if(isset($DataObj->st_city)){ echo $DataObj->st_city; } ?>" class="form-control" placeholder="Enter store State .." >
                 </div>
                </div>
                     </div>
                <div class="row">   
                <div class="col-md-6 right_pdng">
                  <div class="form-group">
                 <label for="st_state" class="col-form-label label">State </label>
                  <select name="st_state" id="st_state" class="form-control customtinput">
                    <option value=""> Select State</option>
                    <?php foreach ($DataObj->States as $key => $value) { ?>
                      // code...
                      <option value="<?php echo $value->id; ?>"><?php  echo $value->state; ?></option>
                   <?php } ?>
                  </select>
                 </div>
                </div>
                <div class=" col-md-6 left_pdng">
                 <div class="form-group">
                 <label for="st_country" class="col-form-label label">Country </label>
                 <select name="st_country" id="st_country" class="form-control customtinput">
                   <option value=""> Select Country</option>
                   <?php foreach ($DataObj->Countries as $key => $value) { ?>
                     // code...
                     <option value="<?php echo $value->id; ?>"><?php  echo $value->name; ?></option>
                  <?php } ?>
                 </select>
                 </div>
                </div>
                </div>
                
                <div class="row"> 
                <div class="col-md-6 right_pdng">
                <div class="form-group">
                 <label for="st_zipcode" class="col-form-label label">Zip</label>
                  <input type="text" name="st_zipcode" id="st_zipcode" value="<?php if(isset($DataObj->st_zipcode)){ echo $DataObj->st_zipcode; } ?>" class="form-control" placeholder="Enter store State .." >
                 </div>
                </div>
                <div class=" col-md-6 left_pdng">
                 <div class="form-group">
                 <label for="Geolocation" class="col-form-label label">Geolocation </label>
                 <div class="division_two">
                  <input type="text" name="st_longitude" class="form-control " id="st_longitude" value="<?php if(isset($DataObj->st_longitude)){ echo $DataObj->st_longitude; } ?>" placeholder="Enter longitude" >
                   <input type="text" name="st_latitude" class="form-control" id="st_latitude" value="<?php if(isset($DataObj->st_latitude)){ echo $DataObj->st_latitude; } ?>" placeholder="Enter latitude" >
                 </div>
                 </div>
                </div>
                </div>
                 <div class="row"> 
                   <div class="col-md-6 right_pdng">
                  <div class="form-group">
                 <label for="st_phone" class="col-form-label label">Phone</label>
                  <input type="text" name="st_phone" id="st_phone" value="<?php if(isset($DataObj->st_phone)){ echo $DataObj->st_phone; } ?>" class="form-control" placeholder="Enter store Phone .." >
                 </div>
                </div>
                <div class="col-md-6 left_pdng">
                 <div class="form-group">
                 <label for="st_mobile" class="col-form-label label">Mobile</label>
                  <input type="text" name="st_mobile" id="st_mobile" value="<?php if(isset($DataObj->st_mobile)){ echo $DataObj->st_mobile; } ?>" class="form-control" placeholder="Enter store Fax .." >
                 </div>
                </div>
                 </div>
                <div class="row">  
                <div class="col-md-6 right_pdng">
                <div class="form-group">
                 <label for="st_fax" class="col-form-label label">Fax</label>
                  <input type="text" name="st_fax" id="st_fax" value="<?php if(isset($DataObj->st_fax)){ echo $DataObj->st_fax; } ?>" class="form-control" placeholder="Enter store Fax .." >
                 </div>
                </div>

              <div class="col-md-6 left_pdng">
                 <div class="form-group">
                 <label for="st_key" class="col-form-label label">Key </label>
                  <input type="text" name="st_key" id="st_key" value="<?php if(isset($DataObj->st_key)){ echo $DataObj->st_key; } ?>" class="form-control" placeholder="Enter store Key .." >
                 </div>
                </div>
                </div>
                 <div class="row"> 
                <div class="col-md-6 right_pdng">
                 <div class="form-group ">
                 <label for="st_contact" class="col-form-label label">Contact</label>
                  <input type="text" name="st_contact" id="st_contact" value="<?php if(isset($DataObj->st_contact)){ echo $DataObj->st_contact; } ?>" class="form-control" placeholder="Enter store Contact .." >
                 </div>
                </div>

                <div class="col-md-6 left_pdng">
                  <div class="form-group">
                 <label for="st_resource" class="col-form-label label">Resource</label>
                  <input type="text" name="st_resource" id="st_resource" value="<?php if(isset($DataObj->st_resource)){ echo $DataObj->st_resource; } ?>" class="form-control" placeholder="Enter store Resource .." >
                 </div>
                </div>
                 
                 </div>
               <div class="row">   
                <div class="col-md-6 right_pdng">
                  <div class="form-group">
                 <label for="st_opening_date" class="col-form-label label">Opening Date</label>
                  <input type="text" name="st_opening_date" id="st_opening_date" value="<?php if(isset($DataObj->st_opening_date)){ echo $DataObj->st_opening_date; } ?>" class="form-control" placeholder="Enter store Resource .." >
                 </div>
                </div>
                <div class="col-md-6 left_pdng">
                  <div class="form-group">
                 <label for="st_closing_date" class="col-form-label label">Closing Date</label>
                  <input type="text" name="st_closing_date" id="st_closing_date" value="<?php if(isset($DataObj->st_closing_date)){ echo $DataObj->st_closing_date; } ?>" class="form-control" placeholder="Enter store Resource .." >
                 </div>
                </div>
               </div>
                <div class="col-md-12">
                   <div class="form-group">
                    <label>Status</label>
                      <div class="icheck-primary d-inline">
                         <input type="radio" id="In-Active" name="st_status"<?php if(!isset($DataObj->st_status)){ ?>checked=""<?php } ?> value="0" class="custom-control-input">
                          <label for="In-Active">In-Active
                       </label>
                  </div>
                     <div class="icheck-primary d-inline px-40">
                          <input type="radio"  <?php if(isset($DataObj->st_status)){ ?>checked=""<?php } ?>  id="Active" name="st_status" value="1"  class="custom-control-input">
                          <label for="Active">Active
                       </label>
                  </div>
                  </div>
                </div>
            <div class="col-md-12 text-center">
                 <button type="submit" class="btn btn-primary btn-submit">Submit</button>
             </div>
            </div>
          {!! Form::close() !!}
        </div>
        </div>
        </div>
    </div>
</section>




@endsection
@section('custom-js')
<script>
$(document).ready(function() {
    
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    

    $(".file-upload").on('change', function(){
        readURL(this);
    });
    
    $(".upload-button").on('click', function() {
       $(".file-upload").click();
    });
});
</script>

@endsection
