@extends('layouts.app')


@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
      <h1>
       Store List
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Store List</li>
      </ol>
</section>
<!-- /.content-header -->
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <div class="col-sm-12 list_filter_head">
                  <div class="search_box">
                 <!-- search form -->
                 <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="searchtxt" value="<?php echo isset($data->searchtxt) ? $data->searchtxt:'';  ?>" class="form-control search" placeholder="Search..." >
                        <span class="input-group-btn">
                        <button type="submit" name="search" value="save" id="search-btn" class="btn btn-flat">
                          <i class="fa fa-search"></i>
                        </button>
                      </span>
                    </div>
                </form>
              </div>
                <div class="userbtn">
                  <a href="<?php echo url("/excel_template/master/stores_master_template.xlsx"); ?>" class="btn btn-sm btn-primary createuser-btn">
                       <i class="fa fa-upload"></i> Store Template
              </a>  
                  <a href="<?php echo url("/master/stores-import-excel"); ?>" class="btn btn-sm btn-primary createuser-btn mx-10">
                       <i class="fa fa-upload"></i> Import Stores
              </a>

                    <a href="{{ route('stores.create') }}" class="btn btn-sm btn-primary createuser-btn">
                        <i class="fa fa-plus-circle"></i>  Create Store
                    </a>
           
                </div>
            </div>
            </div>
            <!-- /.card-header -->



            <div class="card-body">
                     @if ($message = Session::get('success'))
                    <div class="message" role="alert">
                        <div class="alert alert-success ">
                             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <p>{{ $message }}</p>
                        </div>
                    </div>
                    @endif
                <div class="container-fluid p-0 show-enteries-parent">
                    <div class="col-md-6 p-0"><label class="show-enteries-text">Show enteries
                <select id="item-show-per-page-select" class="item-show-per-page-select-id">
                <option value="10">10</option>
                <option value="50">50</option>
                <option value="150">150</option>
                <option value="500">500</option>
                </select></label>

            </div>
                    <div class="col-md-6 p-0">
                        <p class="showing-enteries-text text-right">Showing 1 to 10 of 57 entries</p>
                        

                    </div>
                </div>
                 
               
               
                <div class="table_data table-responsive" style="overflow-x:scroll;">
                    <table class="table table-bordered table-striped" style="width:850px;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Sht.Name</th>
                                <th>Type</th>
                                <th>Group</th>
                                <th>Zone</th>
                                <th>Area</th>
                                <th>Region</th>
                                <th>Floors</th>
                                <th>Radius</th>
                                <th>Longitude</th>
                                <th>Latitude</th>
                                <th>DC</th>
                                <th>Age</th>
                                <th>State</th>
                                <th>Ref.Code</th>
                                <th>Opening Date</th>
                                <th>Closing Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach ($data as $row) { ?>
                          <tr>
                            <td><?php echo $row->id ?></td>
                            <td><?php echo $row->st_name ?></td>
                            <td><?php echo $row->st_code ?></td>
                            <td><?php echo $row->st_desc ?></td>
                            <td><?php echo $row->st_type ?></td>
                            <td><?php echo $row->st_group ?></td>
                            <td><?php echo $row->st_zone ?></td>
                            <td><?php echo $row->st_area ?></td>
                            <td><?php echo $row->st_region ?></td>
                            <td><?php echo $row->st_noof_floor ?></td>
                            <td><?php echo $row->st_radius ?></td>
                            <td><?php echo $row->st_longitude ?></td>
                            <td><?php echo $row->st_latitude ?></td>
                            <td><?php echo $row->st_dc ?></td>
                            <td><?php echo $row->st_age ?></td>
                            <td><?php echo $row->st_state ?></td>
                            <td><?php echo $row->st_ref_code ?></td>
                            <td><?php echo $row->st_opening_date ?></td>
                            <td><?php echo $row->st_closing_date ?></td>
                            <td><?php echo $row->st_status ? 'Active' : 'In-Active' ?></td>
                            <td>

                                <a class="btn btn_dis btn-primary" title="Edit" href="{{ route('stores.edit',$row->id) }}"  data-toggle="tooltip" title="Edit!"><i class="fa fa-edit"></i></a>

                                {!! Form::open(['method' => 'DELETE','route' => ['stores.destroy', $row->id],'style'=>'display:inline']) !!}


                                {!! Form::button('<i class="fa fa-archive"></i>', ['class' => 'btn btn_dis btn-danger','type'=>'submit','data-toggle'=>'tooltip', 'title'=>'Delete!']) !!}

                                {!! Form::close() !!}
                            </td>
                          </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                 
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
               <div class="col-md-12 text-right pagination">
                      {!! $data->render() !!}
              </div>
        </div>
</section>




@endsection
