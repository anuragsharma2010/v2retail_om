@extends('layouts.app')


@section('content')

<!-- Content Header (Page header) -->
  <section class="content-header">
      <h1>
   Create Storefloor
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ route('storefloors.index') }}">Storefloors</a></li>
        <li class="active">Create Storefloor</li>
      </ol>
</section>
    <!-- /.content-header -->
     <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                  <!-- /.card-header -->
                  <div class="card-body">
                     <div class="form_container">

                            <?php if(isset($errors) && (count($errors->all()) > 0 )){  ?>
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            <?php } ?>
                    <form action="<?php if(isset($id)){ ?>{{ route('storefloors.update',$id) }} <?php }  else { ?>{{ route('storefloors.store') }}<?php } ?>" method="POST">
                        @csrf
                        <?php if(isset($id)){ ?>
                       @method('PUT')
                        <?php } ?>
               <input type="hidden" name='id' name='id' value="<?php if(isset($id)){ echo $id; }  ?>" />
                    <div class="row">
                     <div class="clearfix">
                      <div class="col-md-6 right_pdng">
                        <div class="form-group">
                         <label >Name:</label>
                            <input  type="text" name="storefloors_name" id="storefloors_name" value="<?php if(isset($DataObj->storefloors_name)){ echo $DataObj->storefloors_name; } ?>" class="form-control" placeholder="Name" maxlength="50">
                        </div>
                        <!-- /.form-group -->
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6 left_pdng">
                         <div class="form-group">
                          <label>Code:</label>
                           <input type="text" value="<?php if(isset($DataObj->storefloors_code)){ echo $DataObj->storefloors_code; } ?>"   class="form-control" id="storefloors_code" name="storefloors_code" placeholder="code" maxlength="20">
                        </div>
                        <!-- /.form-group -->

                      </div>
                    </div>
                      <!-- /.col -->
                      <div class="clearfix">
                      <div class="col-md-6 right_pdng">
                         <div class="form-group">
                          <label>Description:</label>
                           <textarea id="storefloors_desc" name="storefloors_desc" class="form-control" placeholder="Description" maxlength="300"><?php if(isset($DataObj->storefloors_desc)){ echo $DataObj->storefloors_desc; } ?></textarea>
                        </div>
                      </div>
                      <div class="col-md-6 left_pdng pt-45">
                         <div class="form-group">
                          <label>Status</label>

                            <div class="icheck-primary d-inline">
                             <input type="radio" id="InActive" name="storefloors_status"<?php if(!isset($DataObj->storefloors_status)){ ?>checked=""<?php } ?> value="0" class="custom-control-input">
                                <label for="InActive">In-Active
                             </label>
                        </div>
                           <div class="icheck-primary d-inline">
                               <input type="radio" <?php if(isset($DataObj->storefloors_status)){ ?>checked=""<?php } ?>  id="Active" name="storefloors_status" value="1"  class="custom-control-input">
                                <label for="Active">Active
                             </label>
                        </div>
                        </div>
                      </div>
                      </div>
                    <div class="col-md-12">
                         <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                     </div>
                    </div>
                    </form>
                </div>
                </div>
                </div>
            </div>
        </section>

@endsection
