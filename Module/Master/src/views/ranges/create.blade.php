@extends('layouts.app')


@section('content')

<!-- Content Header (Page header) -->
 <section class="content-header">
      <h1>
      Create Range
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('ranges.index') }}">Ranges</a></li>
        <li class="active">Create Range</li>
      </ol>
</section>
    <!-- /.content-header -->
     <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                  <!-- /.card-header -->
                  <div class="card-body">
                     <div class="form_container">

                            <?php if(isset($errors) && (count($errors->all()) > 0 )){  ?>
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            <?php } ?>
                    <form action="<?php if(isset($id)){ ?>{{ route('ranges.update',$id) }} <?php }  else { ?>{{ route('ranges.store') }}<?php } ?>" method="POST">
                        @csrf
                        <?php if(isset($id)){ ?>
                       @method('PUT')
                        <?php } ?>
               <input type="hidden" name='id' name='id' value="<?php if(isset($id)){ echo $id; }  ?>" />
                    <div class="row">
                      <div class="clearfix">
                      <div class="col-md-6 right_pdng">
                        <div class="form-group">
                         <label >Name:</label>
                            <input  type="text" name="range_name" id="range_name" value="<?php if(isset($DataObj->range_name)){ echo $DataObj->range_name; } ?>" class="form-control" placeholder="Name" maxlength="50">
                        </div>
                        <!-- /.form-group -->
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6 left_pdng">
                         <div class="form-group">
                          <label>Code:</label>
                           <input type="text" value="<?php if(isset($DataObj->range_code)){ echo $DataObj->range_code; } ?>"   class="form-control" id="range_code" name="range_code" placeholder="code" maxlength="20">
                        </div>
                        <!-- /.form-group -->

                      </div>
                      <!-- /.col -->
                    <div class="clearfix">
                      <div class="col-md-6 right_pdng">
                         <div class="form-group">
                          <label>Description:</label>
                           <textarea id="range_desc" name="range_desc" class="form-control" placeholder="Description" maxlength="300"><?php if(isset($DataObj->range_desc)){ echo $DataObj->range_desc; } ?></textarea>
                        </div>
                      </div>
                      <div class="col-md-6 left_pdng">
                         <div class="form-group pt-45">
                          <label>Status</label>

                            <div class="icheck-primary d-inline">
                               <input type="radio" id="In-Active" name="range_status"<?php if(!isset($DataObj->range_status)){ ?>checked=""<?php } ?> value="0" class="custom-control-input">
                                <label for="In-Active">In-Active
                             </label>
                        </div>
                           <div class="icheck-primary d-inline">
                                <input type="radio"  <?php if(isset($DataObj->range_status)){ ?>checked=""<?php } ?>  id="Active" name="range_status" value="1"  class="custom-control-input">
                                <label for="Active">Active
                             </label>
                        </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                         <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                     </div>
                    </div>
                    </form>
                </div>
                </div>
                </div>
            </div>
        </section>

@endsection
