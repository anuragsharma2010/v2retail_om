@extends('layouts.app')


@section('content')

<!-- Content Header (Page header) -->
 <section class="content-header">
      <h1>
      Create State
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ route('states.index') }}">States</a></li>
        <li class="active">Create State</li>
      </ol>
</section>
    <!-- /.content-header -->
     <!-- Main content -->
            <section class="content">
              <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                  <!-- /.card-header -->
                  <div class="card-body">
                     <div class="form_container">

                            <?php if(isset($errors) && (count($errors->all()) > 0 )){  ?>
                                <div class="alert alert-danger alert-dismissible">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            <?php } ?>
                    <form action="<?php if(isset($id)){ ?>{{ route('states.update',$id) }} <?php }  else { ?>{{ route('states.store') }}<?php } ?>" method="POST">
                        @csrf
                        <?php if(isset($id)){ ?>
                       @method('PUT')
                        <?php } ?>
               <input type="hidden" name='id' name='id' value="<?php if(isset($id)){ echo $id; }  ?>" />
                    <div class="row">
                      <div class="col-md-6 right_pdng">
                        <div class="form-group">
                         <label >Name:</label>
                            <input  type="text" name="state" id="state" value="<?php if(isset($DataObj->state)){ echo $DataObj->state; } ?>" class="form-control" placeholder="Name" maxlength="50">
                        </div>
                        <!-- /.form-group -->
                        <!-- /.form-group -->
                      </div>
                      <!-- /.col -->
                      <div class="col-md-6 left_pdng">
                         <div class="form-group">
                          <label>Code:</label>
                           <input type="text" value="<?php if(isset($DataObj->st_code)){ echo $DataObj->st_code; } ?>"   class="form-control" id="st_code" name="st_code" placeholder="code" maxlength="20">
                        </div>
                        <!-- /.form-group -->

                      </div>
                      <!-- /.col -->
                      
                    <div class="col-md-12">
                         <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                     </div>
                    </div>
                    </form>
                </div>
                </div>
                </div>
            </div>
        </section>

@endsection
